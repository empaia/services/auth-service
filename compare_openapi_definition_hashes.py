import difflib
import json
import os
import sys

from tests.utils.api_test.api_results_manager import APIResultsManager


def read_last_line(filename: str):
    with open(filename, "rb") as f:
        try:
            f.seek(-2, os.SEEK_END)
            while f.read(1) != b"\n":
                f.seek(-2, os.SEEK_CUR)
        except OSError:
            f.seek(0)
        last_line = f.readline().decode()
    return last_line


def read_openapi_definition(filename: str):
    openapi_definition = None
    if os.path.exists(filename):
        with open(filename, encoding="utf-8") as f:
            openapi_definition = json.load(f)
    return openapi_definition


def read_openapi_definition_as_lines(filename: str):
    lines = []
    openapi_definition = read_openapi_definition(filename)
    if openapi_definition is not None:
        lines = json.dumps(openapi_definition, sort_keys=True, indent=4).splitlines(keepends=True)
    return lines


def read_openapi_definition_hash(filename: str):
    openapi_definition = read_openapi_definition(filename)
    if openapi_definition is not None:
        with open(filename, encoding="utf8") as f:
            openapi_definition = json.load(f)
            openapi_definition_hash = openapi_definition["info"]["checksum"]
    return openapi_definition_hash


def compare_openapi_definitions():
    previous_openapi_definition_file = sys.argv[1]
    previous_hash_value = read_openapi_definition_hash(previous_openapi_definition_file)
    print(f"Hash value from {previous_openapi_definition_file}: {previous_hash_value}")
    openapi_definition_file = sys.argv[2]
    hash_value = read_openapi_definition_hash(openapi_definition_file)
    print(f"Hash value from {openapi_definition_file}: {hash_value}")
    sys.stdout.write(f"Comparing api auth test result hashes... ")
    if previous_hash_value != hash_value:
        print("ERROR.")
        previous_openapi_definition_lines = read_openapi_definition_as_lines(previous_openapi_definition_file)
        openapi_definition_lines = read_openapi_definition_as_lines(openapi_definition_file)
        sys.stdout.writelines(
            difflib.unified_diff(
                previous_openapi_definition_lines,
                openapi_definition_lines,
                fromfile=previous_openapi_definition_file,
                tofile=openapi_definition_file,
            )
        )
        filename = os.path.basename(openapi_definition_file)
        raise AssertionError(f"Openapi definition has changed, but the {filename} was not updated.")
    else:
        print("ok.")


def main():
    if sys.argv[1] == "--set-openapi-definition-from-url":
        APIResultsManager.load_openapi_definition_insert_checksum_and_save_it(sys.argv[2])
    else:
        compare_openapi_definitions()


if __name__ == "__main__":
    main()
