#!/bin/bash
docker compose --profile dev stop auth-db
docker compose --profile dev rm -f auth-db
docker volume rm auth-db-vol
docker compose --profile dev up -d

