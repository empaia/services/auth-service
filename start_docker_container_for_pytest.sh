#!/bin/bash
docker login -u $INTERNAL_REGISTRY_USERNAME -p $INTERNAL_REGISTRY_PASSWORD $INTERNAL_REGISTRY_URL
docker compose --profile pytest down -v
docker compose down -v
./build_auth_service_image.sh
docker compose -f docker-compose.yml --profile pytest up -d kcl
apt-get install -y netcat
poetry run python3 -m tests.data_generators.keycloak_initialize
docker compose -f docker-compose.yml --profile pytest up -d
