# CHANGELOG

## 0.1.29

* bugfix to prevent role change MANAGER for last manager in organization

## 0.1.24

* added global client medical_device_comanion_client to keycloak client validation

## 0.1.23

* initial public commit