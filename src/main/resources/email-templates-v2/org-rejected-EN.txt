Rejected – new organization

Hello [( ${first_name} )] [( ${first_name} )],

the creation of a new organization has been rejected.

Open EMPAIA Portal: [( ${portal_base_url} )]

Please contact the EMPAIA Support Team if you have questions about this process.