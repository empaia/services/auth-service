Abgelehnt - Rolle in Organisation

Hallo [( ${first_name} )] [( ${first_name} )],

die Änderung einer Rolle in einer Organisation wurde abgelehnt.

Zum EMPAIA Portal: [( ${portal_base_url} )]

Bei Fragen wenden Sie sich bitte an die Verantwortlichen Ihrer Organisation.