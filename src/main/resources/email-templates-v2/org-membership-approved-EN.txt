Approved – organization membership

Hello [( ${first_name} )] [( ${first_name} )],

an organization membership request has been approved.

Open EMPAIA Portal: [( ${portal_base_url} )]