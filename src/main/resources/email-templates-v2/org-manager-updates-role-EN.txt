Organization membership role has been altered

Hello [( ${first_name} )] [( ${first_name} )],

an organization membership role has been altered.

Open EMPAIA Portal: [( ${portal_base_url} )]

Please contact a manager of your organization if you have questions about this process.