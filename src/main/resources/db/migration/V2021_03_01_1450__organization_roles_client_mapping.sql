create table organizations_roles_clients_mapping
(
    organization_id int not null
        constraint clients_mapping_organization_organization_id_fk
            references organizations,
    role_id int not null
        constraint clients_mapping_roles_role_id_fk
            references roles,
    client_group_id int not null
        constraint client_groups_client_group_id_fk
            references client_groups
);

create unique index organization_role_clients_mapping_id_uindex
	on organizations_roles_clients_mapping (organization_id, role_id, client_group_id);

alter table organizations_roles_clients_mapping
    add constraint organizations_roles_clients_mapping_pk
        primary key (organization_id, role_id, client_group_id);
