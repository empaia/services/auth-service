alter table users alter column street_name drop not null;
alter table users alter column street_number drop not null;
alter table users alter column zip_code drop not null;
alter table users alter column place_name drop not null;