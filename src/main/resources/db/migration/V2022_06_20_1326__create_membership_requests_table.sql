create table if not exists membership_requests
(
    membership_request_id        serial                                      not null
            constraint membership_requests_pk
                primary key,
        organization_id        varchar                                         not null,
        organization_name      varchar                                         not null,
        user_id                varchar                                         not null,
        user_name              varchar                                         not null
);
