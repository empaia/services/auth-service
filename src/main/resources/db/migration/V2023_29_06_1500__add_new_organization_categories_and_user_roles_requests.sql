-- Alter organization_category_requests

alter table public.organization_category_requests
    add IF NOT EXISTS compute_provider_category_requested boolean default false not null,
    add IF NOT EXISTS product_provider_category_requested boolean default false not null;

-- Alter organization_user_role_requests

alter table public.organization_user_role_requests
    add IF NOT EXISTS clearance_maintainer_role_requested boolean default false not null;
