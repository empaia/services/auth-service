create table if not exists passwords
(
    user_id      integer                 not null
        constraint passwords_users_user_id_fk
            references users,
    hash         varchar                 not null,
    salt         varchar                 not null,
    date_created timestamp default now() not null,
    constraint passwords_pk
        primary key (user_id, hash)
);
