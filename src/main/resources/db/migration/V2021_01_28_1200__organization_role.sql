-- Create organization roles

create table organization_roles
(
    organization_role_id serial not null,
    organization_role_name varchar not null
);

create unique index organization_roles_organization_role_id_uindex
    on organization_roles (organization_role_id);

create unique index organization_roles_organization_role_name_uindex
    on organization_roles (organization_role_name);

alter table organization_roles
    add constraint organization_roles_pk
        primary key (organization_role_id);

-- Seed roles

insert into public.organization_roles (organization_role_id, organization_role_name) values (1, 'Hospital') on conflict do nothing;
insert into public.organization_roles (organization_role_id, organization_role_name) values (2, 'AI Vendor') on conflict do nothing;
insert into public.organization_roles (organization_role_id, organization_role_name) values (3, 'Data Provider') on conflict do nothing;

-- Update Organization table and set default if none

alter table organizations
    add organization_role_id int not null default 1;

alter table organizations
    add constraint organizations_organization_roles_organization_role_id_fk
        foreign key (organization_role_id) references organization_roles;