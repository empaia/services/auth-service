create table if not exists organization_details
(
    organization_details_id        serial                                      not null
            constraint organization_details_pk
                primary key,
        creator_id             varchar                                         not null,
        description            varchar,
        account_state          int                                             not null,
        keycloak_id            varchar                                         not null
);
