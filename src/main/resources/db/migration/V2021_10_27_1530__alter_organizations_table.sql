alter table organizations
    add IF NOT EXISTS description varchar;

alter table organizations
    add IF NOT EXISTS is_user_count_public boolean default false not null;

alter table organizations
    add IF NOT EXISTS is_phone_number_public boolean default false not null;

alter table organizations
    add IF NOT EXISTS is_email_public boolean default false not null;
