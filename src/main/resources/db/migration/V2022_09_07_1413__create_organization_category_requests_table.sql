create sequence organization_category_requests_id_seq;
alter sequence organization_category_requests_id_seq owner to empaia;

create table if not exists organization_category_requests
(
    organization_category_request_id bigint default nextval('organization_category_requests_id_seq'::regclass) not null
            constraint organization_category_requests_pk
                primary key,
        organization_id        varchar                                         not null,
        organization_category  varchar                                         not null
);
