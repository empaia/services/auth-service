alter table passwords drop constraint passwords_users_user_id_fk;

alter table passwords
    add constraint passwords_users_user_id_fk
        foreign key (user_id) references users
            on update cascade on delete cascade;