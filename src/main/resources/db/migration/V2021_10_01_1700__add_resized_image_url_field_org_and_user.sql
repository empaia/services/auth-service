ALTER TABLE users
    ADD COLUMN IF NOT EXISTS resized_picture_urls VARCHAR;

ALTER TABLE organizations
    ADD COLUMN IF NOT EXISTS resized_logo_urls VARCHAR;
