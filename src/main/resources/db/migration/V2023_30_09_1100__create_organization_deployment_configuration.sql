create sequence organization_deployment_configuration_id_seq;
alter sequence organization_deployment_configuration_id_seq owner to empaia;

create table if not exists organization_deployment_configurations
(
    organization_deployment_configuration_id bigint default nextval('organization_deployment_configuration_id_seq'::regclass) not null
            constraint organization_deployment_configuration_pk
                primary key,
        organization_id                 varchar                 not null,
        allow_localhost_web_clients     boolean default false   not null,
        allow_http                      boolean default false   not null,
        workbench_deployment_domain     varchar                 not null,
        data_manager_deployment_domain  varchar                 not null,
        updated_by                      varchar                 not null,
        updated_at                      bigint                  not null
);
alter table organization_deployment_configurations owner to empaia;