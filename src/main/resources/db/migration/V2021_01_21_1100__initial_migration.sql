create sequence client_groups_client_group_id_seq;

alter sequence client_groups_client_group_id_seq owner to empaia;

create sequence client_group_authorization_client_group_authorization_id_seq;

alter sequence client_group_authorization_client_group_authorization_id_seq owner to empaia;

create table if not exists account_states
(
    account_state_id serial  not null
        constraint account_states_pk primary key,
    state_name       varchar not null
);

alter table account_states owner to empaia;

create unique index if not exists account_states_account_state_id_uindex
    on account_states (account_state_id);

create unique index if not exists account_states_state_name_uindex
    on account_states (state_name);

create table if not exists countries
(
    country_code varchar not null
        constraint countries_pk
            primary key,
    de           varchar not null,
    en           varchar not null
);

alter table countries
    owner to empaia;

create unique index if not exists countries_country_code_uindex
    on countries (country_code);

create table if not exists organizations
(
    organization_id        serial                                      not null
        constraint organizations_pk
            primary key,
    name                   varchar                                     not null,
    street_name            varchar                                     not null,
    street_number          varchar                                     not null,
    zip_code               varchar                                     not null,
    place_name             varchar                                     not null,
    country_code           varchar                                     not null,
    department             varchar,
    email                  varchar                                     not null,
    phone_number           varchar,
    fax_number             varchar,
    website                varchar,
    logo_url               varchar,
    account_state_id       integer                                     not null,
    date_created           timestamp default now()                     not null,
    date_last_change       timestamp default now()                     not null,
    contact_person_user_id bigint,
    keycloak_id            varchar   default 'null'::character varying not null
);

alter table organizations
    owner to empaia;

create unique index if not exists organizations_name_uindex
    on organizations (name);

create unique index if not exists organizations_organization_id_uindex
    on organizations (organization_id);

create table if not exists roles
(
    role_id          bigserial         not null
        constraint roles_pk
            primary key,
    role_name        varchar           not null,
    role_description varchar           not null,
    role_flag        integer default 0 not null
);

alter table roles
    owner to empaia;

create unique index if not exists roles_role_id_uindex
    on roles (role_id);

create table if not exists solutions
(
    solution_id   bigserial not null
        constraint solutions_pk
            primary key,
    solution_name varchar   not null
);

alter table solutions
    owner to empaia;

create table if not exists apps
(
    app_id          bigserial not null
        constraint apps_pk
            primary key,
    app_solution_id bigint    not null
        constraint apps_solutions_solution_id_fk
            references solutions,
    app_name        varchar   not null
);

alter table apps
    owner to empaia;

create unique index if not exists apps_app_id_uindex
    on apps (app_id);

create table if not exists organizations_solutions_mappings
(
    organization_id bigint not null
        constraint organizations_solutions_mapping_organizations_organization_id_f
            references organizations,
    solution_id     bigint not null
        constraint organizations_solutions_mapping_solutions_solution_id_fk
            references solutions,
    constraint organizations_solutions_mapping_pk
        primary key (organization_id, solution_id)
);

alter table organizations_solutions_mappings
    owner to empaia;

create unique index if not exists solutions_solutions_id_uindex
    on solutions (solution_id);

create table if not exists user_types
(
    user_type_id serial  not null
        constraint user_types_pk
            primary key,
    name         varchar not null
);

alter table user_types
    owner to empaia;

create unique index if not exists user_types_name_uindex
    on user_types (name);

create unique index if not exists user_types_user_type_id_uindex
    on user_types (user_type_id);

create table if not exists users
(
    user_id           serial                  not null
        constraint users_pk
            primary key,
    keycloak_user_id  varchar                 not null,
    email             varchar                 not null,
    first_name        varchar                 not null,
    last_name         varchar                 not null,
    street_name       varchar                 not null,
    street_number     varchar                 not null,
    zip_code          varchar                 not null,
    place_name        varchar                 not null,
    country_code      varchar
        constraint users_countries_country_code_fk
            references countries,
    phone_number      varchar,
    picture_url       varchar,
    additional_data   varchar,
    settings          varchar,
    user_type_id      integer                 not null
        constraint users_user_types_user_type_id_fk
            references user_types,
    account_state_id  integer                 not null
        constraint users_account_states_account_state_id_fk
            references account_states,
    date_created      timestamp default now() not null,
    date_last_changed timestamp               not null,
    user_name         varchar                 not null
);

alter table users
    owner to empaia;

create unique index if not exists users_email_uindex
    on users (email);

create unique index if not exists users_keycloak_user_id_uindex
    on users (keycloak_user_id);

create unique index if not exists users_user_id_uindex
    on users (user_id);

create unique index if not exists users_user_name_uindex
    on users (user_name);

create table if not exists users_organizations_mappings
(
    user_id                 bigint not null
        constraint users_organizations_mapping_users_user_id_fk
            references users,
    organization_id         bigint not null
        constraint users_organizations_mapping_organizations_organization_id_fk
            references organizations,
    user_organization_state bigint
        constraint users_organizations_mapping_account_states_account_state_id_fk
            references account_states,
    is_default              boolean,
    is_selected             boolean,
    remarks                 varchar,
    constraint users_organizations_mapping_pk
        primary key (user_id, organization_id)
);

alter table users_organizations_mappings
    owner to empaia;

create table if not exists organizations_users_roles_mappings
(
    organization_id bigint not null,
    user_id         bigint not null,
    role_id         bigint not null
        constraint organizations_users_roles_mapping_roles_role_id_fk
            references roles,
    constraint organizations_users_roles_mapping_pk
        primary key (organization_id, user_id, role_id),
    constraint organizations_users_roles_mapping_users_organizations_mapping_
        foreign key (organization_id, user_id) references users_organizations_mappings (organization_id, user_id)
);

alter table organizations_users_roles_mappings
    owner to empaia;

create table if not exists client_groups
(
    client_group_id              bigint default nextval('client_groups_client_group_id_seq'::regclass) not null
        constraint client_groups_pk
            primary key,
    client_group_organisation_id integer                                                               not null
        constraint client_groups_organizations_organization_id_fk
            references organizations,
    client_group_type            varchar                                                               not null,
    client_group_namespace       varchar                                                               not null
);

alter table client_groups
    owner to empaia;

create table if not exists clients
(
    client_id                     varchar          not null
        constraint clients_pk
            primary key,
    client_name                   varchar          not null,
    client_url                    varchar          not null,
    client_group_id               bigint default 0 not null
        constraint clients_client_groups_client_group_id_fk
            references client_groups,
    keycloak_id                   varchar          not null,
    client_description            varchar,
    client_token_lifetime_seconds integer
);

alter table clients
    owner to empaia;

create unique index if not exists client_groups_client_group_id_uindex
    on client_groups (client_group_id);

create unique index if not exists client_groups_client_group_namespace_uindex
    on client_groups (client_group_namespace);

create table if not exists client_group_authorization
(
    client_group_authorization_id bigint default nextval(
            'client_group_authorization_client_group_authorization_id_seq'::regclass) not null
        constraint client_group_authorization_pk
            primary key,
    group_authorization_from      bigint                                              not null
        constraint client_group_authorization_client_groups_client_group_id_fk
            references client_groups,
    group_authorization_for       bigint                                              not null
        constraint client_group_authorization_client_groups_client_group_id_fk_2
            references client_groups
);

alter table client_group_authorization
    owner to empaia;

create unique index if not exists client_group_authorization_client_group_authorization_id_uindex
    on client_group_authorization (client_group_authorization_id);

-- Create default entries

insert into public.account_states (account_state_id, state_name) values (1, 'ACTIVE') on conflict do nothing;
insert into public.account_states (account_state_id, state_name) values (2, 'INACTIVE') on conflict do nothing;
insert into public.account_states (account_state_id, state_name) values (3, 'UNVERIFIED') on conflict do nothing;

insert into public.user_types (user_type_id, name) values (0, 'ADMIN') on conflict do nothing;
insert into public.user_types (user_type_id, name) values (1, 'USER') on conflict do nothing;
insert into public.user_types (user_type_id, name) values (2, 'SUPPORTER') on conflict do nothing;

insert into public.roles (role_id, role_name, role_description, role_flag) values (1, 'Administrator', 'Organization administrator', 1) on conflict do nothing;
insert into public.roles (role_id, role_name, role_description, role_flag) values (2, 'Accountant', 'Organization accountant', 2) on conflict do nothing;
insert into public.roles (role_id, role_name, role_description, role_flag) values (3, 'Vendor', 'Organization vendor', 4) on conflict do nothing;
insert into public.roles (role_id, role_name, role_description, role_flag) values (4, 'Member', 'Organization member', 8) on conflict do nothing;

insert into public.countries (country_code, de, en) values ('DE', 'Deutschland', 'Germany') on conflict do nothing;
insert into public.countries (country_code, de, en) values ('AT', 'Österreich', 'Austria') on conflict do nothing;