alter table clients
    add client_refresh_token_lifetime_seconds int;

alter table clients
    add redirect_uris varchar;