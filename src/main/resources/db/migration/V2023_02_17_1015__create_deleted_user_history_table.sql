create sequence deleted_user_history_id_seq;
alter sequence deleted_user_history_id_seq owner to empaia;

create table if not exists deleted_user_history
(
        deleted_user_history_id        bigint default nextval('deleted_user_history_id_seq'::regclass) not null constraint deleted_user_history_pk primary key,
        user_id                varchar not null,
        deleted_by             varchar not null,
        deleted_on             bigint  not null
);
