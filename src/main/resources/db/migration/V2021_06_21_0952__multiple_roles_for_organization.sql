-- Creates a mapping table for organization <-> organization role
create table organizations_roles_mapping
(
    organization_id int not null
        constraint organizations_roles_mapping_organizations_organization_id_fk
            references organizations,
    organization_role_id int not null
        constraint organizations_mapping_roles_organization_role_id_fk
            references organization_roles
);

-- Before dropping all organization roles, we need to migrate them to the new mapping table
INSERT INTO organizations_roles_mapping ( organization_id, organization_role_id )
SELECT organization_id, organization_role_id
FROM organizations;

-- Drop role id from organization
alter table organizations drop constraint organizations_organization_roles_organization_role_id_fk;

alter table organizations drop column organization_role_id;
