-- Alter organization_user_role_requests

alter table public.organization_user_role_requests
    drop column organization_user_role;

alter table public.organization_user_role_requests
    add IF NOT EXISTS manager_role_requested boolean default false not null;

alter table public.organization_user_role_requests
    add IF NOT EXISTS app_maintainer_role_requested boolean default false not null;

alter table public.organization_user_role_requests
    add IF NOT EXISTS pathologist_role_requested boolean default false not null;

alter table public.organization_user_role_requests
    add IF NOT EXISTS data_manager_role_requested boolean default false not null;

-- Alter organization_category_requests

alter table public.organization_category_requests
    drop column organization_category;

alter table public.organization_category_requests
    add IF NOT EXISTS app_customer_category_requested boolean default false not null;

alter table public.organization_category_requests
    add IF NOT EXISTS app_vendor_category_requested boolean default false not null;