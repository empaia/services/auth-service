alter table users drop constraint users_user_types_user_type_id_fk;
alter table users drop column user_type_id;
drop table user_types;

-- do we have to drop the constraints explicitly?
-- alter table drop constraint organizations_users_roles_mapping_pk;
-- alter table drop constraint organizations_users_roles_mapping_roles_role_id_fk;
-- alter table drop constraint organizations_users_roles_mapping_users_organizations_mapping_;
drop table organizations_users_roles_mappings;