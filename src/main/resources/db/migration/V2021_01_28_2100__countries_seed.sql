insert into public.countries (country_code, de, en)
values ('AF', 'Afghanistan', 'Afghanistan')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('EG', 'Ägypten', 'Egypt')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('AL', 'Albanien', 'Albania')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('DZ', 'Algerien', 'Algeria')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('AD', 'Andorra', 'Andorra')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('AO', 'Angola', 'Angola')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('AI', 'Anguilla', 'Anguilla')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('AQ', 'Antarktis', 'Antarctica')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('AG', 'Antigua und Barbuda', 'Antigua and Barbuda')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('GQ', 'Äquatorial Guinea', 'Equatorial Guinea')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('AR', 'Argentinien', 'Argentina')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('AM', 'Armenien', 'Armenia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('AW', 'Aruba', 'Aruba')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('AZ', 'Aserbaidschan', 'Azerbaijan')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('ET', 'Äthiopien', 'Ethiopia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('AU', 'Australien', 'Australia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('BS', 'Bahamas', 'Bahamas')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('BH', 'Bahrain', 'Bahrain')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('BD', 'Bangladesh', 'Bangladesh')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('BB', 'Barbados', 'Barbados')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('BE', 'Belgien', 'Belgium')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('BZ', 'Belize', 'Belize')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('BJ', 'Benin', 'Benin')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('BM', 'Bermudas', 'Bermuda')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('BT', 'Bhutan', 'Bhutan')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('MM', 'Birma', 'Myanmar')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('BO', 'Bolivien', 'Bolivia (Plurinational State of)')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('BA', 'Bosnien-Herzegowina', 'Bosnia and Herzegovina')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('BW', 'Botswana', 'Botswana')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('BV', 'Bouvet Inseln', 'Bouvet Island')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('BR', 'Brasilien', 'Brazil')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('IO', 'Britisch-Indischer Ozean', 'British Indian Ocean Territory')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('BN', 'Brunei', 'Brunei Darussalam')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('BG', 'Bulgarien', 'Bulgaria')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('BF', 'Burkina Faso', 'Burkina Faso')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('BI', 'Burundi', 'Burundi')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('CL', 'Chile', 'Chile')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('CN', 'China', 'China')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('CX', 'Christmas Island', 'Christmas Island')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('CK', 'Cook Inseln', 'Cook Islands')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('CR', 'Costa Rica', 'Costa Rica')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('DK', 'Dänemark', 'Denmark')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('DE', 'Deutschland', 'Germany')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('DJ', 'Djibuti', 'Djibouti')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('DM', 'Dominika', 'Dominica')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('DO', 'Dominikanische Republik', 'Dominican Republic')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('EC', 'Ecuador', 'Ecuador')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('SV', 'El Salvador', 'El Salvador')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('ER', 'Eritrea', 'Eritrea')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('EE', 'Estland', 'Estonia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('FK', 'Falkland Inseln', 'Falkland Islands (Malvinas)')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('FO', 'Färöer Inseln', 'Faroe Islands')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('FJ', 'Fidschi', 'Fiji')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('FI', 'Finnland', 'Finland')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('FR', 'Frankreich', 'France')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('GF', 'französisch Guyana', 'French Guiana')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('PF', 'Französisch Polynesien', 'French Polynesia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('TF', 'Französisches Süd-Territorium', 'French Southern Territories')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('GA', 'Gabun', 'Gabon')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('GM', 'Gambia', 'Gambia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('GE', 'Georgien', 'Georgia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('GH', 'Ghana', 'Ghana')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('GI', 'Gibraltar', 'Gibraltar')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('GD', 'Grenada', 'Grenada')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('GR', 'Griechenland', 'Greece')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('GL', 'Grönland', 'Greenland')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('UK', 'Großbritannien', 'United Kingdom')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('GB', 'Großbritannien (UK)', 'United Kingdom of Great Britain and Northern Ireland')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('GP', 'Guadeloupe', 'Guadeloupe')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('GU', 'Guam', 'Guam')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('GT', 'Guatemala', 'Guatemala')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('GN', 'Guinea', 'Guinea')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('GW', 'Guinea Bissau', 'Guinea-Bissau')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('GY', 'Guyana', 'Guyana')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('HT', 'Haiti', 'Haiti')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('HM', 'Heard und McDonald Islands', 'Heard Island and McDonald Islands')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('HN', 'Honduras', 'Honduras')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('HK', 'Hong Kong', 'Hong Kong')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('IN', 'Indien', 'India')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('ID', 'Indonesien', 'Indonesia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('IQ', 'Irak', 'Iraq')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('IR', 'Iran', 'Iran (Islamic Republic of)')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('IE', 'Irland', 'Ireland')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values (' IS ', 'Island', 'Iceland')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('IL', 'Israel', 'Israel')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('IT', 'Italien', 'Italy')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('JM', 'Jamaika', 'Jamaica')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('JP', 'Japan', 'Japan')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('YE', 'Jemen', 'Yemen')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('JO', 'Jordanien', 'Jordan')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('KY', 'Kaiman Inseln', 'Cayman Islands')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('KH', 'Kambodscha', 'Cambodia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('CM', 'Kamerun', 'Cameroon')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('CA', 'Kanada', 'Canada')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('CV', 'Kap Verde', 'Cabo Verde')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('KZ', 'Kasachstan', 'Kazakhstan')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('KE', 'Kenia', 'Kenya')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('KG', 'Kirgisistan', 'Kyrgyzstan')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('KI', 'Kiribati', 'Kiribati')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('CC', 'Kokosinseln', 'Cocos (Keeling) Islands')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('CO', 'Kolumbien', 'Colombia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('KM', 'Komoren', 'Comoros')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('CG', 'Kongo', 'Congo')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('HR', 'Kroatien', 'Croatia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('CU', 'Kuba', 'Cuba')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('KW', 'Kuwait', 'Kuwait')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('LS', 'Lesotho', 'Lesotho')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('LV', 'Lettland', 'Latvia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('LB', 'Libanon', 'Lebanon')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('LR', 'Liberia', 'Liberia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('LY', 'Libyen', 'Libya')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('LI', 'Liechtenstein', 'Liechtenstein')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('LT', 'Litauen', 'Lithuania')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('LU', 'Luxemburg', 'Luxembourg')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('MO', 'Macao', 'Macao')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('MG', 'Madagaskar', 'Madagascar')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('MW', 'Malawi', 'Malawi')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('MY', 'Malaysia', 'Malaysia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('MV', 'Malediven', 'Maldives')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('ML', 'Mali', 'Mali')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('MT', 'Malta', 'Malta')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('MP', 'Marianen', 'Northern Mariana Islands')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('MA', 'Marokko', 'Morocco')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('MH', 'Marshall Inseln', 'Marshall Islands')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('MQ', 'Martinique', 'Martinique')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('MR', 'Mauretanien', 'Mauritania')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('MU', 'Mauritius', 'Mauritius')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('YT', 'Mayotte', 'Mayotte')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('MK', 'Mazedonien', 'North Macedonia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('MX', 'Mexiko', 'Mexico')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('FM', 'Mikronesien', 'Micronesia (Federated States of)')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('MZ', 'Mocambique', 'Mozambique')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('MC', 'Monaco', 'Monaco')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('MN', 'Mongolei', 'Mongolia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('MS', 'Montserrat', 'Montserrat')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('NA', 'Namibia', 'Namibia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('NR', 'Nauru', 'Nauru')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('NP', 'Nepal', 'Nepal')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('NC', 'Neukaledonien', 'New Caledonia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('NZ', 'Neuseeland', 'New Zealand')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('NI', 'Nicaragua', 'Nicaragua')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('NL', 'Niederlande', 'Netherlands')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('NE', 'Niger', 'Niger')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('NG', 'Nigeria', 'Nigeria')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('NU', 'Niue', 'Niue')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('KP', 'Nord Korea', 'Korea (Democratic Peoples Republic of)')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('NF', 'Norfolk Inseln', 'Norfolk Island')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('NO', 'Norwegen', 'Norway')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('OM', 'Oman', 'Oman')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('AT', 'Österreich', 'Austria')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('PK', 'Pakistan', 'Pakistan')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('PW', 'Palau', 'Palau')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('PA', 'Panama', 'Panama')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('PG', 'Papua Neuguinea', 'Papua New Guinea')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('PY', 'Paraguay', 'Paraguay')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('PE', 'Peru', 'Peru')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('PH', 'Philippinen', 'Philippines')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('PN', 'Pitcairn', 'Pitcairn')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('PL', 'Polen', 'Poland')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('PT', 'Portugal', 'Portugal')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('PR', 'Puerto Rico', 'Puerto Rico')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('QA', 'Qatar', 'Qatar')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('RE', 'Reunion', 'Réunion')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('RW', 'Ruanda', 'Rwanda')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('RO', 'Rumänien', 'Romania')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('RU', 'Russland', 'Russian Federation')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('LC', 'Saint Lucia', 'Saint Lucia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('ZM', 'Sambia', 'Zambia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('AS', 'Samoa', 'American Samoa')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('WS', 'Samoa', 'Samoa')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('SM', 'San Marino', 'San Marino')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('ST', 'Sao Tome', 'Sao Tome and Principe')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('SA', 'Saudi Arabien', 'Saudi Arabia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('SE', 'Schweden', 'Sweden')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('CH', 'Schweiz', 'Switzerland')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('SN', 'Senegal', 'Senegal')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('SC', 'Seychellen', 'Seychelles')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('SL', 'Sierra Leone', 'Sierra Leone')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('SG', 'Singapur', 'Singapore')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('SK', 'Slowakei', 'Slovakia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('SI', 'Slowenien', 'Slovenia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('SB', 'Solomon Inseln', 'Solomon Islands')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('SO', 'Somalia', 'Somalia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('GS', 'Südgeorgien und die Südlichen Sandwichinseln', 'South Georgia and the South Sandwich Islands')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('ES', 'Spanien', 'Spain')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('LK', 'Sri Lanka', 'Sri Lanka')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('KN', 'St. Kitts Nevis Anguilla', 'Saint Kitts and Nevis')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('PM', 'St. Pierre und Miquelon', 'Saint Pierre and Miquelon')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('VC', 'St. Vincent', 'Saint Vincent and the Grenadines')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('ZA', 'Südafrika', 'South Africa')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('SD', 'Sudan', 'Sudan')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('SR', 'Surinam', 'Suriname')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('SJ', 'Svalbard und Jan Mayen Islands', 'Svalbard and Jan Mayen')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('SZ', 'Swasiland', 'Eswatini')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('SY', 'Syrien', 'Syrian Arab Republic')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('TJ', 'Tadschikistan', 'Tajikistan')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('TH', 'Thailand', 'Thailand')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('TG', 'Togo', 'Togo')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('TK', 'Tokelau', 'Tokelau')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('TO', 'Tonga', 'Tonga')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('TT', 'Trinidad Tobago', 'Trinidad and Tobago')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('TD', 'Tschad', 'Chad')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('CZ', 'Tschechische Republik', 'Czechia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('TN', 'Tunesien', 'Tunisia')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('TR', 'Türkei', 'Turkey')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('TM', 'Turkmenistan', 'Turkmenistan')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('TC', 'Turks und Kaikos Inseln', 'Turks and Caicos Islands')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('TV', 'Tuvalu', 'Tuvalu')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('UG', 'Uganda', 'Uganda')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('UA', 'Ukraine', 'Ukraine')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('HU', 'Ungarn', 'Hungary')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('UY', 'Uruguay', 'Uruguay')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('UZ', 'Usbekistan', 'Uzbekistan')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('VU', 'Vanuatu', 'Vanuatu')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('VA', 'Vatikan', 'Holy See')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('VE', 'Venezuela', 'Venezuela (Bolivarian Republic of)')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('AE', 'Vereinigte Arabische Emirate', 'United Arab Emirates')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('US', 'Vereinigte Staaten von Amerika', 'United States of America')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('VN', 'Vietnam', 'Viet Nam')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('VG', 'Virgin Island (Brit.)', 'Virgin Islands (British)')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('VI', 'Virgin Island (USA)', 'Virgin Islands (U.S.)')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('WF', 'Wallis et Futuna', 'Wallis and Futuna')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('BY', 'Weissrussland', 'Belarus')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('EH', 'Westsahara', 'Western Sahara')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('CF', 'Zentralafrikanische Republik', 'Central African Republic')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('ZW', 'Zimbabwe', 'Zimbabwe')
on conflict do nothing;
insert into public.countries (country_code, de, en)
values ('CY', 'Zypern', 'Cyprus')
on conflict do nothing;
