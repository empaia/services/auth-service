create sequence organization_user_role_requests_id_seq;
alter sequence organization_user_role_requests_id_seq owner to empaia;

create table if not exists organization_user_role_requests
(
    organization_user_role_requests_id bigint default nextval('organization_user_role_requests_id_seq'::regclass) not null
            constraint organization_user_role_requests_pk
                primary key,
    organization_id          varchar                not null,
    user_id                  varchar                not null,
    organization_user_role   varchar                not null
);
alter table organization_user_role_requests owner to empaia;
