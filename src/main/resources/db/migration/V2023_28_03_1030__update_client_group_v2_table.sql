-- Alter membership_requests

alter table public.client_group_v2
    add IF NOT EXISTS has_service_app_reader_role boolean default false not null,
    add IF NOT EXISTS has_service_app_config_reader_role boolean default false not null,
    add IF NOT EXISTS has_service_compute_provider_role boolean default false not null;
