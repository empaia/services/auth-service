alter table users_organizations_mappings drop constraint users_organizations_mapping_account_states_account_state_id_fk;

alter table users_organizations_mappings
	add constraint users_organizations_mapping_account_states_account_state_id_fk
		foreign key (user_organization_state) references account_states
			on update cascade on delete cascade;

alter table users_organizations_mappings drop constraint users_organizations_mapping_organizations_organization_id_fk;

alter table users_organizations_mappings
	add constraint users_organizations_mapping_organizations_organization_id_fk
		foreign key (organization_id) references organizations
			on update cascade on delete cascade;

alter table users_organizations_mappings drop constraint users_organizations_mapping_users_user_id_fk;

alter table users_organizations_mappings
	add constraint users_organizations_mapping_users_user_id_fk
		foreign key (user_id) references users
			on update cascade on delete cascade;

alter table organizations_users_roles_mappings drop constraint organizations_users_roles_mapping_roles_role_id_fk;

alter table organizations_users_roles_mappings
	add constraint organizations_users_roles_mapping_roles_role_id_fk
		foreign key (role_id) references roles
			on update cascade on delete cascade;

alter table organizations_users_roles_mappings drop constraint organizations_users_roles_mapping_users_organizations_mapping_;

alter table organizations_users_roles_mappings
	add constraint organizations_users_roles_mapping_users_organizations_mapping_
		foreign key (organization_id, user_id) references users_organizations_mappings (organization_id, user_id)
			on update cascade on delete cascade;
