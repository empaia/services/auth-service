-- Create a sequence for the client_group_v2.id primary key column and set its owner to empaia.
-- This is done because it is also done for the client_groups_client_group_id_seq sequence used by the V1 API.
-- The reason might be that the lifetime of this sequence depends on its owner. This way the sequence will
-- live as long as the owner and no previous value will be reused.
create sequence client_group_v2_id_seq;
alter sequence client_group_v2_id_seq owner to empaia;

create table if not exists client_group_v2
(
    id bigint default nextval('client_group_v2_id_seq'::regclass) not null
        constraint client_group_v2_pk
            primary key,
    organization_id         varchar                               not null,
    type                    varchar                               not null,
    client_scope_name       varchar                               not null
);
alter table client_group_v2 owner to empaia;


-- The V2 API uses separate DB tables for managing client groups, because the V1 API duplicates too much data
-- that should be stored in Keycloak only. The V2 API tables store only the data that is necessary to implement
-- the client groups concept (i.e. manage client access via client type, e.g. allow all JES clients of an organization
-- to access another client).
create table if not exists client_v2
(
    client_id          varchar not null constraint client_v2_pk primary key,
    keycloak_id        varchar not null,
    client_group_id bigint default 0 not null
        constraint client_group_v2_id_fk
            references client_group_v2(id)
);
alter table client_v2 owner to empaia;


create sequence client_group_access_id_seq;
alter sequence client_group_access_id_seq owner to empaia;

create table if not exists client_group_access
(
    id bigint default nextval('client_group_access_id_seq'::regclass) not null
        constraint client_group_access_pk
            primary key,
    accessing_client_group_id  bigint not null
        constraint client_group_access_client_group_v2_id_fk
            references client_group_v2,
    target_client_group_id       bigint not null
        constraint client_group_access_client_group_v2_id_fk_2
            references client_group_v2
);
alter table client_group_access owner to empaia;