alter table public.users_organizations_mappings
    add was_requested_by_user bool not null default true;