-- Alter membership_requests

alter table public.membership_requests
    add IF NOT EXISTS created_at bigint default 0 not null;

alter table public.membership_requests
    add IF NOT EXISTS reviewer_comment varchar;

alter table public.membership_requests
    add IF NOT EXISTS reviewer_id varchar;

alter table public.membership_requests
    add IF NOT EXISTS updated_at bigint;

alter table public.membership_requests
    add IF NOT EXISTS request_state integer not null;

-- Alter organization_user_role_requests

alter table public.organization_user_role_requests
    add IF NOT EXISTS created_at bigint default 0 not null;

alter table public.organization_user_role_requests
    add IF NOT EXISTS reviewer_comment varchar;

alter table public.organization_user_role_requests
    add IF NOT EXISTS reviewer_id varchar;

alter table public.organization_user_role_requests
    add IF NOT EXISTS updated_at bigint;

alter table public.organization_user_role_requests
    add IF NOT EXISTS request_state integer not null;

-- Alter organization_category_requests

alter table public.organization_category_requests
    add IF NOT EXISTS creator_id varchar not null;

alter table public.organization_category_requests
    add IF NOT EXISTS created_at bigint default 0 not null;

alter table public.organization_category_requests
    add IF NOT EXISTS reviewer_comment varchar;

alter table public.organization_category_requests
    add IF NOT EXISTS reviewer_id varchar;

alter table public.organization_category_requests
    add IF NOT EXISTS updated_at bigint;

alter table public.organization_category_requests
    add IF NOT EXISTS request_state integer not null;