create table if not exists organization_role_change_requests
(
    id bigserial not null primary key,
    remarks varchar not null,
    author_user_id bigint not null references users(user_id),
    organization_id bigint not null references organizations(organization_id),
    date_created timestamp default now() not null,
    roles varchar not null
);
