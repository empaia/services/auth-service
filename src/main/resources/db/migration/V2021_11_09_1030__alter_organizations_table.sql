alter table organizations
    add IF NOT EXISTS is_fax_number_public boolean default false not null;
