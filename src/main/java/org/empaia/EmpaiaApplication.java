package org.empaia;

import org.empaia.configs.KeycloakInstanceConfiguration;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.env.Environment;

@SpringBootApplication
@EnableAspectJAutoProxy
@EnableConfigurationProperties( KeycloakInstanceConfiguration.class )
public class EmpaiaApplication implements CommandLineRunner {

    final Environment env;

    public EmpaiaApplication( Environment env ) {
        this.env = env;
    }

    public static void main( String[] args ) {
        SpringApplication.run( EmpaiaApplication.class, args );
    }

    @Override
    public void run( String... args ) {
        // Does nothing yet
    }
}