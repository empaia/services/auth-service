package org.empaia.services;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.empaia.configs.KeycloakInstanceConfiguration;
import org.empaia.exceptions.generic.AlreadyExistException;
import org.empaia.exceptions.generic.NotFoundException;
import org.empaia.exceptions.keycloak.KeycloakClientIdException;
import org.empaia.exceptions.keycloak.KeycloakEntityAlreadyExistsException;
import org.empaia.exceptions.keycloak.KeycloakInternalException;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.DefaultActiveOrganizationEntity;
import org.empaia.models.dto.v2.OrganizationMember;
import org.empaia.models.enums.v2.Protocol;
import org.empaia.models.enums.v2.ClientServiceRole;
import org.empaia.models.enums.v2.OrganizationCategory;
import org.empaia.models.enums.v2.OrganizationUserRoleV2;
import org.empaia.models.enums.v2.UserRole;
import org.empaia.utils.KeycloakUtils;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.*;
import org.keycloak.representations.idm.*;
import org.springframework.stereotype.Service;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Service
@Slf4j
public class KeycloakService {
    public static final String SUPER_ADMIN = "super_admin";
    public static final String SUPPORTER = "supporter";

    // API V2 user roles:
    public static final String ADMIN = "admin";
    public static final String MODERATOR = "moderator";
    public static final String EMPAIA_INTERNATIONAL_ASSOCIATE = "empaia-international-associate";

    private static final String UNKNOWN_ERROR = "An unknown error occurred";
    private static final String CLIENT_NOT_FOUND = "Unable to find the requested client";
    private static final String CLIENT_ID_ERROR = "Unable to determine client, please specify full client id";

    private final KeycloakInstanceConfiguration keycloakConfig;
    @Getter
    private final Keycloak keycloak;
    // V1 API roles
    private final RoleRepresentation superAdminRoleRepresentation;
    private final RoleRepresentation supporterRoleRepresentation;
    // V2 API roles
    private final RoleRepresentation adminRoleRepresentation;
    private final RoleRepresentation moderatorRoleRepresentation;
    private final RoleRepresentation empaiaIntAssociateRoleRepresentation;

    public KeycloakService( KeycloakInstanceConfiguration keycloakInstanceConfiguration, Keycloak keycloak ) {
        this.keycloakConfig = keycloakInstanceConfiguration;
        this.keycloak = keycloak;

        this.superAdminRoleRepresentation = findRequiredRole( SUPER_ADMIN );
        this.supporterRoleRepresentation = findRequiredRole( SUPPORTER );
        this.adminRoleRepresentation = findRequiredRole( ADMIN );
        this.moderatorRoleRepresentation = findRequiredRole( MODERATOR );
        this.empaiaIntAssociateRoleRepresentation = findRequiredRole( EMPAIA_INTERNATIONAL_ASSOCIATE );
    }

    //region Configuration getters

    public String getRealm() {
        return keycloakConfig.getRealm();
    }

    public String getClientId() {
        return keycloakConfig.getClientId();
    }

    //endregion

    //region User management

    private RealmResource getRealmResource() {
        return keycloak.realm( keycloakConfig.getRealm() );
    }

    public GroupsResource getGroupsResource() {
        return getRealmResource().groups();
    }

    public GroupResource getGroupResource( String groupId ) {
        return getGroupsResource().group( groupId );
    }

    public Optional<GroupResource> getOptionalGroupResource( KeycloakId organizationId ) {
        Optional<GroupResource> groupResource;
        try {
            groupResource = Optional.of( getGroupResource( organizationId.toString() ) );
        } catch ( Exception e ) {
            groupResource = Optional.empty();
        }
        return groupResource;
    }

    public UsersResource getUsersResource() {
        return getRealmResource().users();
    }

    public UserResource getUserById( String userId ) {
        return getUsersResource().get( userId );
    }

    public UserResource getUserById( KeycloakId userId ) {
        return getUsersResource().get( userId.toString() );
    }

    public List<UserRepresentation> getUsers( Integer page, Integer pageSize ) {
        return getUsersResource().list( page, pageSize );
    }

    public List<UserRepresentation> getUnapprovedUsers( Integer page, Integer pageSize ) {
        boolean enabled = true;
        boolean emailVerified = false;
        boolean briefRepresentation = true;
        // Search for an "@" in email addresses, because if it is not null, keycloak will exclude service accounts
        // from the search result, which we do not want to include.
        return getUsersResource().search( null, null, null, "@", emailVerified, page, pageSize, enabled, briefRepresentation );
    }

    public Optional<UserRepresentation> getUserByEmail( String email ) {
        try {
            return Optional.of( getUsersResource().search( null, null, null, email, 0, 1 ).get( 0 ) );
        } catch ( Exception e ) {
            return Optional.empty();
        }
    }

    public List<GroupRepresentation> getUserGroups( String userId ) {
        List<GroupRepresentation> result = null;
        UserResource user = getUserById( userId );
        if ( user != null ) {
            try {
                result = user.groups();
            } catch ( javax.ws.rs.NotFoundException e ) {
                log.error( "Failed to access user groups", e );
            }
        }
        if ( result == null ) {
            result = new ArrayList<>();
        }
        return result;
    }

    public boolean isGroupMember( OrganizationMember organizationMember ) {
        Set<String> groupIds = getUserGroups( organizationMember.getUserId().toString() ).stream()
                .map( GroupRepresentation::getId )
                .collect( Collectors.toSet() );
        return groupIds.contains( organizationMember.getOrganizationId().toString() );
    }

    public List<KeycloakId> getOrganizationIdsOfMember( KeycloakId userId ) {
        return getOrganizationGroupsOfMember( userId, false ).stream()
                .map( GroupRepresentation::getId )
                .map( KeycloakId::new )
                .collect( Collectors.toList() );
    }

    public List<String> getOrganizationNamesForUser( KeycloakId userId ) {
        return getOrganizationGroupsOfMember( userId, true ).stream()
                .map( this::getOrganizationName )
                .filter( Objects::nonNull )
                .collect( Collectors.toList() );
    }

    public String getOrganizationName( KeycloakId organizationId ) {
        GroupRepresentation groupRepresentation = getGroup( organizationId.toString() ).orElseThrow(
                () -> new NotFoundException( "Organization does not exist " )
        );
        return getOrganizationName( groupRepresentation );
    }

    private String getOrganizationName( GroupRepresentation groupRepresentation ) {
        Map<String, List<String>> attributes = groupRepresentation.getAttributes();
        String organizationName = null;
        if ( attributes == null ) {
            log.error( "Organization is missing keycloak attributes: " + groupRepresentation.getId() );
        } else {
            final List<String> defaultList = Collections.singletonList( null );
            organizationName = attributes.getOrDefault( "name", defaultList ).get( 0 );
        }
        return organizationName;
    }

    public List<GroupRepresentation> getOrganizationGroupsOfMember( KeycloakId userId, boolean queryGroupAttributes ) {
        var groups = getUserGroups( userId.toString() ).stream().filter(
                // return only top level groups, i.e. filter out groups that represent organization user roles
                groupRepresentation -> StringUtils.countMatches( groupRepresentation.getPath(), "/" ) == 1
        );
        if ( queryGroupAttributes ) {
            // getUserGroups() returns groups only briefly, without attributes. We have to fetch the groups now
            // explicitly with attributes:
            groups = groups.map(
                    groupRepresentation -> getGroupResource( groupRepresentation.getId() ).toRepresentation()
            );
        }
        return groups.collect( Collectors.toList() );
    }

    public List<UserRepresentation> getOrganizationMembers( String organizationId ) {
        GroupResource groupResource = getGroupResource( organizationId );
        return groupResource.members().stream().filter(
                userRepresentation -> !userRepresentation.getUsername().startsWith( "service-account-" )
        ).collect( Collectors.toList() );
    }

    public List<UserRepresentation> getOrganizationMembersOnlyInThisOrganization( String organizationId ) {
        String groupName = getGroupResource( organizationId ).toRepresentation().getName();
        return getOrganizationMembers( organizationId ).stream().filter(

                userRepresentation -> {
                    var IsMembersOnlyInThisOrganization = false;
                    if ( userRepresentation != null ) {
                        var groups = userRepresentation.getGroups();
                        IsMembersOnlyInThisOrganization = (
                                groups != null &&
                                        groups.size() == 1 &&
                                        groups.get( 0 ) != null &&
                                        groups.get( 0 ).equals( groupName )
                        );
                    }
                    return IsMembersOnlyInThisOrganization;
                }
        ).collect( Collectors.toList() );
    }

    public void renameSubGroupName( String organizationId, String subGroupName, String newSubGroupName ) {
        Optional<GroupRepresentation> group = getGroup( organizationId );

        if ( group.isPresent() ) {
            List<GroupRepresentation> subGroups = group.get().getSubGroups();
            for ( GroupRepresentation subGroup : subGroups ) {
                if ( subGroup.getName().equals( subGroupName ) ) {
                    subGroup.setName( newSubGroupName );
                    getGroupsResource().group( subGroup.getId() ).update( subGroup );
                }
            }
        }
    }

    public GroupRepresentation getOrganizationSubGroup( String organizationId, String subGroupName ) {
        Optional<GroupRepresentation> group = getGroup( organizationId );
        if ( group.isPresent() ) {
            return getOrganizationSubGroup( group.get(), subGroupName );
        } else {
            throw new NotFoundException( "Organization not found" );
        }
    }

    private GroupRepresentation getOrganizationSubGroup( GroupRepresentation organization, String subGroupName ) {
        return organization
                .getSubGroups()
                .stream()
                .filter( subGroupRepresentation -> subGroupRepresentation.getName().equalsIgnoreCase( subGroupName ) )
                .findFirst()
                .orElseThrow( () -> new NotFoundException( "The sub group was not found" ) );
    }

    public List<GroupRepresentation> getOrganizationSubGroups( String organizationId, Set<String> subGroupNames ) {
        Optional<GroupRepresentation> group = getGroup( organizationId );
        if ( group.isPresent() ) {
            GroupRepresentation groupRepresentation = group.get();
            return groupRepresentation.getSubGroups().stream().filter(
                    subGroupRepresentation -> subGroupNames.contains( subGroupRepresentation.getName() )
            ).collect( Collectors.toList() );
        } else {
            throw new NotFoundException( "Organization not found" );
        }
    }

    public List<GroupRepresentation> getUserOrganisationSubGroups( String userId, String organizationId ) {
        return getUserOrganisationSubGroups( userId, organizationId, false, true );
    }

    public List<GroupRepresentation> getUserOrganisationSubGroups( String userId, String organizationId, boolean includeClientServiceRoleGroups ) {
        return getUserOrganisationSubGroups( userId, organizationId, false, includeClientServiceRoleGroups );
    }

    public List<GroupRepresentation> getUserOrganisationSubGroups(
            String userId, String organizationId, boolean assertUserIsMemberOfTopLevelGroup, boolean includeClientServiceRoleGroups
    ) {
        UserResource user;
        try {
            user = getUserById( userId );
        } catch ( javax.ws.rs.NotFoundException e ) {
            log.error( "User not found", e );
            return Collections.emptyList();
        } catch ( NullPointerException e ) {
            log.error( "Unable to fetch user data", e );
            throw new NotFoundException( "There was an error finding the user" );
        } catch ( Exception e ) {
            log.error( "Unknown error", e );
            throw new KeycloakInternalException( "Unknown" );
        }

        return getUserOrganisationSubGroups( user, userId, organizationId, assertUserIsMemberOfTopLevelGroup, includeClientServiceRoleGroups );
    }

    private List<GroupRepresentation> getUserOrganisationSubGroups(
            UserResource user, String userId, String organizationId, boolean assertUserIsMemberOfTopLevelGroup, boolean includeClientServiceRoleGroups
    ) {
        List<GroupRepresentation> userGroups;

        try {
            userGroups = user.groups();
        } catch ( javax.ws.rs.NotFoundException e ) {
            log.error( "User organization sub group not found", e );
            return Collections.emptyList();
        } catch ( Exception e ) {
            log.error( "Unknown error", e );
            throw new KeycloakInternalException( "Unknown" );
        }

        GroupResource organisationResource = getGroupResource( organizationId );
        if ( organisationResource == null ) {
            return new ArrayList<>();
        }

        if ( assertUserIsMemberOfTopLevelGroup ) {
            // TODO: why does this fail after joining the tl group`?
            if ( userGroups.stream().noneMatch( userGroup -> userGroup.getId().equals( organizationId ) ) ) {
                log.error( String.format( "User '%s' is no member of the organization group '%s'", userId, organizationId ) );
                log.error( "member of: " + userGroups.stream().map( GroupRepresentation::getPath ).collect( Collectors.joining() ) );
                throw new BadRequestException( "User is no member of the given organization" );
            }
        }

        GroupRepresentation organization = organisationResource.toRepresentation();
        Set<String> subGroupIds = organization
                .getSubGroups()
                .stream()
                .filter( groupRepresentation -> includeClientServiceRoleGroups || !ClientServiceRole.isClientServiceRole( groupRepresentation.getName() ) )
                .map( GroupRepresentation::getId )
                .collect( Collectors.toSet() );

        return userGroups.stream().filter(
                userGroup -> subGroupIds.contains( userGroup.getId() )
        ).collect( Collectors.toUnmodifiableList() );
    }

    public boolean isMissingUser( String userId ) {
        boolean isMissingUser;
        UserResource userResource = getUserById( userId );
        try {
            isMissingUser = !userResource.toRepresentation().getId().equals( userId );
        } catch ( javax.ws.rs.NotFoundException e ) {
            isMissingUser = true;
        }
        return isMissingUser;
    }

    public void addUserToOrganization( OrganizationMember organizationMember ) {
        String organizationId = organizationMember.getOrganizationId().toString();
        String userId = organizationMember.getUserId().toString();
        GroupRepresentation groupRepresentation = getGroup( organizationId ).orElseThrow(
                () -> new NotFoundException( "Organization not found " + organizationId )
        );
        UserResource userResource = getUserById( userId );
        addUserToGroup( userId, userResource, organizationId, groupRepresentation );
        UserRepresentation userRepresentation = userResource.toRepresentation();
        Map<String, List<String>> attributes = userRepresentation.getAttributes();
        if ( attributes == null ) {
            attributes = new HashMap<>();
            userRepresentation.setAttributes( attributes );
        }
        if ( !attributes.containsKey( "default-active-organization" ) ) {
            attributes.put( "default-active-organization", Collections.emptyList() );
            userResource.update( userRepresentation );
        }
    }

    public void removeUserFromOrganization( OrganizationMember organizationMember ) {
        if ( this.isLastOrganizationManagerInCharge( organizationMember ) ) {
            throw new BadRequestException( "Last manager in organization must not leave the organization" );
        }
        String organizationId = organizationMember.getOrganizationId().toString();
        String userId = organizationMember.getUserId().toString();
        GroupRepresentation groupRepresentation = getGroup( organizationId ).orElseThrow(
                () -> new NotFoundException( "Organization not found " + organizationId )
        );
        UserResource userResource;
        try {
            userResource = getUserById( userId );
        } catch ( Exception e ) {
            log.error( "An error occurred when removing user from group", e );
            throw new KeycloakInternalException( "Unable to remove user from group" );
        }
        List<GroupRepresentation> subGroups = getUserOrganisationSubGroups( userId, organizationId, true );
        for ( GroupRepresentation subGroup : subGroups ) {
            userResource.leaveGroup( subGroup.getId() );
        }
        userResource.leaveGroup( groupRepresentation.getId() );
        UserRepresentation userRepresentation = userResource.toRepresentation();
        Map<String, List<String>> attributes = userRepresentation.getAttributes();
        if ( attributes != null && attributes.containsKey( "default-active-organization" ) ) {
            List<String> defaultActiveOrganization = attributes.getOrDefault( "default-active-organization", Collections.emptyList() );
            if ( !defaultActiveOrganization.isEmpty() ) {
                if ( defaultActiveOrganization.get( 0 ).equals( groupRepresentation.getId() ) ) {
                    attributes.remove( "default-active-organization" );
                    userResource.update( userRepresentation );
                }
            }
        }
    }

    public boolean isLastOrganizationManagerInCharge( OrganizationMember organizationMember ) {
        String organizationId = organizationMember.getOrganizationId().toString();
        List<OrganizationUserRoleV2> organizationMemberRoles = this.getOrganizationUserRoles( organizationMember );
        if ( !organizationMemberRoles.contains( OrganizationUserRoleV2.MANAGER ) ) {
            return false;
        }
        List<UserRepresentation> allOrganizationMembers = this.getOrganizationMembers( organizationId );
        AtomicBoolean isLast = new AtomicBoolean( true );
        allOrganizationMembers.forEach( member -> {
            if ( !member.getId().equals( organizationMember.getUserId().toString() ) ) {
                OrganizationMember otherOrganizationMember = OrganizationMember.builder()
                        .organizationId( new KeycloakId( organizationId ) )
                        .userId( new KeycloakId( member.getId() ) )
                        .build();
                List<OrganizationUserRoleV2> roles = this.getOrganizationUserRoles( otherOrganizationMember );
                if ( roles.contains( OrganizationUserRoleV2.MANAGER ) ) {
                    isLast.set( false );
                }
            }
        } );
        return isLast.get();
    }

    public DefaultActiveOrganizationEntity setDefaultActiveOrganization( OrganizationMember organizationMember ) {
        if ( organizationMember.getOrganizationId() == null || isGroupMember( organizationMember ) ) {
            UserResource userResource = getUserById( organizationMember.getUserId().toString() );
            UserRepresentation userRepresentation = userResource.toRepresentation();
            Map<String, List<String>> attributes = userRepresentation.getAttributes();
            if ( attributes == null ) {
                attributes = new HashMap<>();
                userRepresentation.setAttributes( attributes );
            }
            if ( organizationMember.getOrganizationId() == null ) {
                attributes.put( "default-active-organization", Collections.emptyList() );
            } else {
                attributes.put( "default-active-organization", Collections.singletonList( organizationMember.getOrganizationId().toString() ) );
            }
            userResource.update( userRepresentation );
            return DefaultActiveOrganizationEntity.builder().organizationId( organizationMember.getOrganizationId() ).build();
        } else {
            throw new BadRequestException( "Cannot set default active organization: user is not a member" );
        }
    }

    public void deleteDefaultActiveOrganization( KeycloakId userId ) {
        UserResource userResource = getUserById( userId );
        UserRepresentation userRepresentation = userResource.toRepresentation();
        Map<String, List<String>> attributes = userRepresentation.getAttributes();
        if ( attributes != null ) {
            attributes.remove( "default-active-organization" );
            userResource.update( userRepresentation );
        }
    }

    public KeycloakId getDefaultActiveOrganization( KeycloakId userId ) {
        KeycloakId organizationId = null;
        UserResource userResource = getUserById( userId.toString() );
        UserRepresentation userRepresentation = userResource.toRepresentation();
        Map<String, List<String>> attributes = userRepresentation.getAttributes();
        if ( attributes != null && attributes.containsKey( "default-active-organization" ) ) {
            List<String> idString = attributes.getOrDefault( "default-active-organization", Collections.emptyList() );
            if ( !idString.isEmpty() ) {
                organizationId = new KeycloakId( idString.get( 0 ), true );
            }
        }
        return organizationId;
    }

    public Set<OrganizationUserRoleV2> getOrganizationUserRoles( KeycloakId userId, KeycloakId organizationId ) {
        OrganizationMember organizationMember = OrganizationMember.builder()
                .userId( userId )
                .organizationId( organizationId )
                .build();
        List<OrganizationUserRoleV2> activeOrganizationUserRoles = getOrganizationUserRoles( organizationMember );
        return new HashSet<>( activeOrganizationUserRoles );
    }

    public List<OrganizationUserRoleV2> getOrganizationUserRoles( OrganizationMember organizationMember ) {
        return getOrganizationUserRoleGroupNames( organizationMember, false, false ).stream()
                .map( OrganizationUserRoleV2::fromName )
                .collect( Collectors.toList() );
    }

    private Set<String> getOrganizationUserRoleGroupNames( OrganizationMember organizationMember, boolean assertUserIsMemberOfTopLevelGroup, boolean includeClientServiceRoleGroups ) {
        String userId = organizationMember.getUserId().toString();
        String organizationId = organizationMember.getOrganizationId().toString();
        return getOrganizationUserRoleGroupNames( userId, organizationId, assertUserIsMemberOfTopLevelGroup, includeClientServiceRoleGroups );
    }

    private Set<String> getOrganizationUserRoleGroupNames(
            String userId, String organizationId, boolean assertUserIsMemberOfTopLevelGroup, boolean includeClientServiceRoleGroups
    ) {
        UserResource userResource = getUserById( userId );
        return getUserOrganisationSubGroups(
                userResource, userId, organizationId, assertUserIsMemberOfTopLevelGroup, includeClientServiceRoleGroups
        ).stream()
                .map( GroupRepresentation::getName )
                .collect( Collectors.toSet() );
    }

    public void setOrganizationUserRoles( OrganizationMember organizationMember, List<OrganizationUserRoleV2> roles ) {
        Set<String> roleNamesToSet = roles.stream()
                .map( OrganizationUserRoleV2::getName )
                .collect( Collectors.toSet() );
        setOrganizationUserRoles( organizationMember, roleNamesToSet, true );
    }

    private void setOrganizationUserRoles( OrganizationMember organizationMember, Set<String> roleNamesToSet, boolean assertUserIsMemberOfTopLevelGroup ) {
        String userId = organizationMember.getUserId().toString();
        String organizationId = organizationMember.getOrganizationId().toString();
        setOrganizationUserRoles( userId, organizationId, roleNamesToSet, assertUserIsMemberOfTopLevelGroup );
    }

    public void setOrganizationUserRoles( String userId, String organizationId, Set<String> roleNamesToSet, boolean assertUserIsMemberOfTopLevelGroup ) {
        UserResource userResource = getUserById( userId );
        Set<String> activeRoleNames = getOrganizationUserRoleGroupNames( userId, organizationId, assertUserIsMemberOfTopLevelGroup, true );
        Set<String> roleNamesToRemove = new HashSet<>( activeRoleNames );
        roleNamesToRemove.removeAll( roleNamesToSet );
        Set<String> roleNamesToAdd = new HashSet<>( roleNamesToSet );
        roleNamesToAdd.removeAll( activeRoleNames );
        if ( !roleNamesToRemove.isEmpty() ) {
            getOrganizationSubGroups( organizationId, roleNamesToRemove ).forEach(
                    groupRepresentation -> removeUserFromGroup( userId, userResource, groupRepresentation )
            );
        }
        if ( !roleNamesToAdd.isEmpty() ) {
            getOrganizationSubGroups( organizationId, roleNamesToAdd ).forEach(
                    groupRepresentation -> addUserToGroup( userId, userResource, groupRepresentation )
            );
        }
    }

    public Set<OrganizationCategory> getOrganizationCategories( KeycloakId organizationId ) {
        GroupResource groupResource;
        GroupRepresentation groupRepresentation;
        try {
            groupResource = getGroupResource( organizationId.toString() );
            groupRepresentation = groupResource.toRepresentation();
        } catch ( javax.ws.rs.NotFoundException e ) {
            log.error( "Organization " + organizationId + " not found while checking organization categories " );
            throw new NotFoundException( "Organization not found" );
        }
        Map<String, List<String>> attributes = groupRepresentation.getAttributes();
        return KeycloakUtils.getAttributeValueList( attributes, "categories", new ArrayList<>() ).stream()
                .filter( categoryName -> !categoryName.isEmpty() )
                .map( OrganizationCategory::fromName )
                .collect( Collectors.toSet() );
    }

    public Set<OrganizationUserRoleV2> getAllowedOrganizationUserRoles( OrganizationMember organizationMember ) {
        Set<OrganizationCategory> categories = getOrganizationCategories( organizationMember.getOrganizationId() );
        return getAllowedOrganizationUserRolesForCategories( categories );
    }

    public Set<OrganizationUserRoleV2> getAllowedOrganizationUserRolesForCategories( Set<OrganizationCategory> categories ) {
        Set<OrganizationUserRoleV2> allowedRoles = new HashSet<>();
        allowedRoles.add( OrganizationUserRoleV2.MANAGER );
        if ( categories.contains( OrganizationCategory.APP_CUSTOMER ) ) {
            allowedRoles.add( OrganizationUserRoleV2.DATA_MANAGER );
            allowedRoles.add( OrganizationUserRoleV2.PATHOLOGIST );
        }
        if ( categories.contains( OrganizationCategory.APP_VENDOR ) ) {
            allowedRoles.add( OrganizationUserRoleV2.APP_MAINTAINER );
        }
        if ( categories.contains( OrganizationCategory.PRODUCT_PROVIDER ) ) {
            allowedRoles.add( OrganizationUserRoleV2.CLEARANCE_MAINTAINER );
        }
        return allowedRoles;
    }

    public Optional<String> createUser( String userName, String firstName, String lastName, String email,
                                        String password, Map<String, List<String>> attributes ) {
        Optional<String> keycloakUserId = Optional.empty();
        try {
            UserRepresentation userRepresentation = new UserRepresentation();
            userRepresentation.setUsername( userName );
            userRepresentation.setFirstName( firstName );
            userRepresentation.setLastName( lastName );
            userRepresentation.setEmail( email );
            userRepresentation.setEnabled( true );
            userRepresentation.setAttributes( attributes );

            CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
            credentialRepresentation.setTemporary( false );
            credentialRepresentation.setType( CredentialRepresentation.PASSWORD );
            credentialRepresentation.setValue( password );
            userRepresentation.setCredentials( Collections.singletonList( credentialRepresentation ) );

            try ( Response response = getUsersResource().create( userRepresentation ) ) {
                if ( response.getStatus() == 409 ) {
                    throw new KeycloakEntityAlreadyExistsException(
                            "A user with this username or email address already exists."
                    );
                }

                if ( response.getStatusInfo().getFamily().equals( Response.Status.Family.SUCCESSFUL ) ) {
                    keycloakUserId = Optional.of( response.getLocation().getPath().replaceAll( ".*/([^/]+)$", "$1" ) );
                } else {
                    log.error( String.format(
                            "Failed to create user, keycloak response was [%d] %s",
                            response.getStatus(),
                            response.readEntity( String.class )
                    ) );
                }
            }
        } catch ( KeycloakEntityAlreadyExistsException ex ) {
            throw ex;
        } catch ( Exception ex ) {
            log.error( "Unknown error while creating user", ex );
            throw new KeycloakInternalException( UNKNOWN_ERROR );
        }

        return keycloakUserId;
    }

    public Optional<UserResource> findUserById( String userId ) {
        try {
            return Optional.ofNullable( getUserById( userId ) );
        } catch ( Exception e ) {
            log.error( "An error occurred when finding user by id", e );
            return Optional.empty();
        }
    }

    public void deleteUser( String keycloakUserId ) {
        try ( Response response = getUsersResource().delete( keycloakUserId ) ) {
            if ( !response.getStatusInfo().getFamily().equals( Response.Status.Family.SUCCESSFUL ) ) {
                if ( response.getStatus() != HttpStatus.SC_NOT_FOUND ) {
                    throw new KeycloakInternalException(
                            String.format(
                                    "Failed to delete user, keycloak response was [%d] %s",
                                    response.getStatus(),
                                    response.readEntity( String.class )
                            )
                    );
                }
            }
        } catch ( Exception e ) {
            log.error( "Error deleting user", e );
            throw new KeycloakInternalException( "An error occurred deleting the user" );
        }
    }

    //endregion

    //region Groups

    public List<GroupRepresentation> getGroups() {
        try {
            return getGroupsResource().groups();
        } catch ( Exception e ) {
            log.error( "An error occurred when getting groups", e );
            throw new KeycloakInternalException( "Unable to get groups" );
        }
    }

    public Optional<GroupRepresentation> getGroup( String groupId ) {
        try {
            return Optional.of( getGroupResource( groupId ).toRepresentation() );
        } catch ( Exception e ) {
            log.error( "An error occurred when getting group by id", e );
            return Optional.empty();
        }
    }

    public List<String> getOrganizationGroupNames() {
        return getGroupsResource().groups().stream().map(
                GroupRepresentation::getName
        ).collect( Collectors.toList() );
    }

    public KeycloakId createOrganizationGroup( GroupRepresentation organisation ) {
        Response response = getGroupsResource().add( organisation );
        return new KeycloakId( extractIdFromLocation( response, "Organization already exists" ), true );
    }

    public Optional<String> createSubGroup( String groupId, GroupRepresentation subGroup, boolean allowExistingSubGroup ) {
        GroupResource parentGroup = getGroupResource( groupId );
        boolean existsAlready = false;
        if ( allowExistingSubGroup ) {
            existsAlready = parentGroup.toRepresentation()
                    .getSubGroups()
                    .stream()
                    .anyMatch( subGroupRepresentation -> subGroupRepresentation.getName().equalsIgnoreCase( subGroup.getName() ) );
        }
        Optional<String> result = Optional.empty();
        if ( !existsAlready ) {
            try ( Response response = parentGroup.subGroup( subGroup ) ) {
                result = Optional.ofNullable( extractIdFromLocation( response, "Role already exists" ) );
            } catch ( Exception e ) {
                log.error( "An error occurred when creating sub group", e );
            }
        }
        return result;
    }

    public void deleteGroup( String keycloakGroupId ) {
        try {
            getGroupResource( keycloakGroupId ).remove();
        } catch ( Exception e ) {
            log.error( "An error occurred when deleting group " + keycloakGroupId, e );
            throw new KeycloakInternalException( "Unable to delete group" );
        }
    }

    public void updateGroup( GroupRepresentation group ) {
        getGroupResource( group.getId() ).update( group );
    }

    public void addUserToGroup( String userId, UserResource userResource, GroupRepresentation group ) {
        String groupId;
        try {
            groupId = group.getId();
        } catch ( Exception e ) {
            log.error( String.format( "An error occurred when adding user '%s' to group '%s - %s' ", userId, group.getId(), group.getPath() ), e );
            throw new KeycloakInternalException( "Unable to add user to the group" );
        }
        addUserToGroup( userId, userResource, groupId, group );
    }

    public void addUserToGroup( String userId, UserResource userResource, String groupId, GroupRepresentation group ) {
        try {
            userResource.joinGroup( groupId );
        } catch ( Exception e ) {
            log.error( String.format( "An error occurred when adding user '%s' to group '%s - %s' %s", userId, groupId, group.getPath(), e.getMessage() ), e );
            throw new KeycloakInternalException( "Unable to add user to the group" );
        }
    }

    public void removeUserFromGroup( String userId, UserResource userResource, GroupRepresentation group ) {
        try {
            // TODO: remove!
            log.warn( String.format( "Removing user '%s' from group '%s - %s' ", userId, group.getId(), group.getPath() ) );
            userResource.leaveGroup( group.getId() );
        } catch ( Exception e ) {
            log.error( String.format( "An error occurred when removing user '%s' from group '%s - %s' ", userId, group.getId(), group.getPath() ), e );
            throw new KeycloakInternalException( "Unable to remove user from group" );
        }
    }

    public Map<String, List<String>> getAllowedAudiencesByOrganizationUserRoleNames( KeycloakId organizationId, Set<String> organizationUserRoleNames ) {
        List<GroupRepresentation> groupRepresentations = getOrganizationSubGroups( organizationId.toString(), organizationUserRoleNames );
        Map<String, List<String>> allowedAudiencesByOrganizationUserRoleName = new HashMap<>();
        groupRepresentations.forEach(
                groupRepresentation -> {
                    List<String> allowedAudience;
                    Map<String, List<String>> attributes = groupRepresentation.getAttributes();
                    if ( attributes != null ) {
                        allowedAudience = new ArrayList<>( KeycloakUtils.getAllowedAudience( attributes ) );
                    } else {
                        allowedAudience = Collections.emptyList();
                    }
                    allowedAudiencesByOrganizationUserRoleName.put( groupRepresentation.getName(), allowedAudience );
                }
        );
        return allowedAudiencesByOrganizationUserRoleName;
    }

    //endregion

    //region Security

    public KeysMetadataRepresentation getKeys() {
        return getRealmResource().keys().getKeyMetadata();
    }

    //endregion

    //region Clients

    public ClientsResource getClientsResource() {
        return getRealmResource().clients();
    }

    public List<ClientRepresentation> getClients( int firstResult, int maxResults ) {
        boolean viewableOnly = false;
        boolean search = true;
        return findClients( null, viewableOnly, search, firstResult, maxResults );
    }

    public boolean hasClient( String clientId ) throws KeycloakInternalException {
        try {
            getClientByClientId( clientId );
            return true;
        } catch ( KeycloakClientIdException ignored ) {
            return false;
        }
    }

    public ClientRepresentation getClientByClientId( String clientId ) throws KeycloakInternalException, KeycloakClientIdException {
        List<ClientRepresentation> representations;
        try {
            representations = getClientsResource().findByClientId( clientId );
        } catch ( Exception e ) {
            log.error( String.format( "An unknown error occurred while looking for client with client id: %s", clientId ), e );
            throw new KeycloakInternalException( UNKNOWN_ERROR );
        }
        if ( representations.size() != 1 ) {
            log.error( String.format( "Failed to find client with client id: %s ", clientId ) );
            throw new KeycloakClientIdException( CLIENT_ID_ERROR );
        }
        return representations.get( 0 );
    }

    public Set<ClientRepresentation> getAllClientsOfOrganization( KeycloakId organizationId ) {
        return getClientsResource().findAll().stream().filter( client -> client.getClientId().startsWith( organizationId.toString() ) ).collect( Collectors.toSet() );
    }

    private ClientResource getClientById( String keycloakIdOfClient ) {
        return getClientsResource().get( keycloakIdOfClient );
    }

    public CredentialRepresentation getClientSecret( String clientId ) {
        ClientRepresentation representation = getClientByClientId( clientId );
        return getClientById( representation.getId() ).getSecret();
    }

    public List<ClientRepresentation> findClients(
            String clientId, boolean viewableOnly, boolean search, int firstResult, int lastResult
    ) {
        try {
            return getClientsResource().findAll( clientId, viewableOnly, search, firstResult, lastResult );
        } catch ( Exception e ) {
            log.error( "Error when finding clients", e );
            return List.of();
        }
    }

    public List<ClientRepresentation> findClientsByClientIdPrefix( String clientIdPrefix, int firstResult, int maxResults ) {
        boolean viewableOnly = false;
        boolean search = true;
        return getClientsResource().findAll( clientIdPrefix, viewableOnly, search, firstResult, maxResults );
    }

    public UserRepresentation getAuthServiceClientServiceAccountUser() {
        return getClientServiceAccountUser( new KeycloakId( getIdOfAuthServiceClient() ) );
    }

    public UserRepresentation getClientServiceAccountUser( KeycloakId clientId ) {
        ClientResource clientResource = getClientsResource().get( clientId.toString() );
        return clientResource.getServiceAccountUser();
    }

    public void addClientServiceAccountUserToOrganizationGroup( KeycloakId clientId, KeycloakId organizationId ) {
        UserRepresentation userRepresentation = getClientServiceAccountUser( clientId );
        UserResource userResource = getUserById( userRepresentation.getId() );
        userResource.joinGroup( organizationId.toString() );
    }

    public boolean isClientServiceAccountUserMemberOfOrganizationGroup( KeycloakId clientId, KeycloakId organizationId ) {
        try {
            UserRepresentation userRepresentation = getClientServiceAccountUser( clientId );
            Set<String> groupIds = getUserGroups( userRepresentation.getId() ).stream()
                    .map( GroupRepresentation::getId )
                    .collect( Collectors.toSet() );
            return groupIds.contains( organizationId.toString() );
        } catch ( BadRequestException ignored ) {
            return false;
        }
    }

    public ClientRepresentation createClient( ClientRepresentation representation, KeycloakId organizationId ) {
        try ( Response ignored = getClientsResource().create( representation ) ) {
            // we need to re-fetch the client since keycloak won't give us the id on creation
            representation = getClientByClientId( representation.getClientId() );
            if ( representation.isServiceAccountsEnabled() ) {
                assert !isClientServiceAccountUserMemberOfOrganizationGroup( new KeycloakId( representation.getId() ), organizationId );
                addClientServiceAccountUserToOrganizationGroup( new KeycloakId( representation.getId() ), organizationId );
            }
            return representation;
        } catch ( Exception e ) {
            log.error( "Error creating client", e );
            throw new KeycloakInternalException( "Unable to create client" );
        }
    }

    public void deleteClient( String keycloakIdOfClient ) {
        try {
            ClientResource resource = getClientById( keycloakIdOfClient );

            if ( resource == null ) {
                throw new NotFoundException( CLIENT_NOT_FOUND );
            }

            try {
                resource.remove();
            } catch ( javax.ws.rs.NotFoundException e ) {
                log.warn( "Could not delete client with id " + keycloakIdOfClient + ", because it was not found", e );
            }
        } catch ( NotFoundException e ) {
            throw new NotFoundException( CLIENT_NOT_FOUND );
        } catch ( Exception e ) {
            log.error( "An error occurred while deleting client", e );
            throw new KeycloakInternalException( UNKNOWN_ERROR );
        }
    }

    public void deleteAllClientsAndClientGroupsOfOrganization( KeycloakId organizationId ) {
        Map<String, String> clientScopeIdsByName = getClientScopeIdsByClientScopeName();
        clientScopeIdsByName.forEach( ( key, value ) -> {
            if ( key.contains( organizationId.toString() ) ) {
                deleteGroupScope( key );
            }
        } );
        Set<ClientRepresentation> clients = getAllClientsOfOrganization( organizationId );
        for ( ClientRepresentation client : clients ) {
            deleteClient( client.getId() );
        }
    }

    public void addScopeToClient( String keycloakIdOfClient, String clientScopeId ) throws KeycloakInternalException {
        try {
            getClientById( keycloakIdOfClient ).addDefaultClientScope( clientScopeId );
        } catch ( Exception e ) {
            log.error( "An error occurred when adding scope to client", e );
            throw new KeycloakInternalException( "Unable to add scope to the client" );
        }
    }

    public void removeScopeFromClient( String keycloakIdOfClient, String clientScopeId ) {
        try {
            ClientResource clientResource = getClientById( keycloakIdOfClient );
            boolean clientExists;
            try {
                clientResource.toRepresentation();
                clientExists = true;
            } catch ( javax.ws.rs.NotFoundException e ) {
                log.warn( "Cannot remove scope " + clientScopeId + " from client with id " + keycloakIdOfClient + ", because the client does not exist", e );
                clientExists = false;
            }
            if ( clientExists ) {
                try {
                    clientResource.removeDefaultClientScope( clientScopeId );
                } catch ( javax.ws.rs.NotFoundException e ) {
                    log.warn( "Cannot remove scope " + clientScopeId + " from client with id " + keycloakIdOfClient + ", because the scope was not found", e );
                }
            }
        } catch ( Exception e ) {
            log.error( "An error occurred when removing scope from client", e );
            throw new KeycloakInternalException( "Unable to remove scope from the client" );
        }
    }

    public Set<ClientScopeRepresentation> getClientScopes( String id ) {
        try {
            return getClientById( id )
                    .getDefaultClientScopes()
                    .stream()
                    .map( scope -> getClientScopesResource().get( scope.getId() ).toRepresentation() )
                    .collect( Collectors.toSet() );
        } catch ( Exception e ) {
            log.error( "An error occurred when getting client scopes", e );
            return Set.of();
        }
    }

    public Optional<String> getScopeId( String scopeName ) {
        try {
            return Optional.ofNullable( getClientScopesResource()
                    .findAll()
                    .stream()
                    .filter( scope -> scope.getName().equals( scopeName ) )
                    .toList()
                    .get( 0 )
                    .getId() );
        } catch ( Exception e ) {
            log.error( "Error finding scope id", e );
            return Optional.empty();
        }

    }

    private ClientScopesResource getClientScopesResource() {
        return getRealmResource().clientScopes();
    }

    public Map<String, String> getClientScopeIdsByClientScopeName() throws NotFoundException {
        try {
            return getClientScopesResource().findAll().stream()
                    .collect(
                            HashMap::new,
                            ( m, v ) -> m.put( v.getName(), v.getId() ),
                            Map::putAll
                    );
        } catch ( Exception e ) {
            log.error( "Failed to get client scopes from keycloak", e );
            throw new NotFoundException( "Failed to get client scopes from keycloak" );
        }
    }

    private Optional<RoleRepresentation> findRole( String roleName ) {
        try {
            List<RoleRepresentation> roles = getClientById( getIdOfAuthServiceClient() ).roles().list();
            return roles.stream().filter( r -> r.getName().equalsIgnoreCase( roleName ) ).findFirst();
        } catch ( NotFoundException e ) {
            return Optional.empty();
        } catch ( Exception e ) {
            log.error( "Error finding role", e );
            return Optional.empty();
        }
    }

    private RoleRepresentation findRequiredRole( String roleName ) {
        return findRole( roleName ).orElseThrow(
                () -> new KeycloakInternalException( "RoleRepresentation not found: " + roleName )
        );
    }

    public boolean userHasAnyRole( UserResource userResource, List<RoleRepresentation> roles ) {
        Set<String> roleIds = roles.stream().map( RoleRepresentation::getId ).collect( Collectors.toSet() );
        RoleScopeResource roleScopeResource = userResource.roles().clientLevel( getIdOfAuthServiceClient() );
        return roleScopeResource.listEffective().stream().anyMatch(
                effectiveRole -> roleIds.contains( effectiveRole.getId() )
        );
    }

    public boolean isAdmin( UserResource userResource ) {
        return userHasRole( userResource, adminRoleRepresentation );
    }

    public boolean isModerator( UserResource userResource ) {
        return userHasRole( userResource, moderatorRoleRepresentation );
    }

    public boolean isEmpaiaInternationalAssociate( UserResource userResource ) {
        return userHasRole( userResource, empaiaIntAssociateRoleRepresentation );
    }

    public List<UserRole> getUserRoles( String userId ) {
        return getUserRoles( getUserById( userId ) );
    }

    public List<UserRole> getUserRoles( UserResource userResource ) {
        RoleScopeResource roleScopeResource = userResource.roles().clientLevel( getIdOfAuthServiceClient() );
        List<RoleRepresentation> effectiveRoles = roleScopeResource.listEffective();
        return effectiveRoles.stream()
                .map( this::mapRoleRepresentationToUserRole )
                .filter( Objects::nonNull ).collect( Collectors.toList() );
    }

    private UserRole mapRoleRepresentationToUserRole( RoleRepresentation roleRepresentation ) {
        Set<String> userRolesToIgnore = Set.of(
                adminRoleRepresentation.getId(), // do nothing, there is no UserRole value for "admin",
                // because that role should not be exposed to clients
                superAdminRoleRepresentation.getId(), // skip V1 API role
                supporterRoleRepresentation.getId() // skip V1 API role
        );
        UserRole userRole = null;
        if ( !userRolesToIgnore.contains( roleRepresentation.getId() ) ) {
            if ( !roleRepresentation.getName().equals( "uma_protection" ) ) {
                if ( roleRepresentation.getId().equals( moderatorRoleRepresentation.getId() ) ) {
                    userRole = UserRole.MODERATOR;
                } else if ( roleRepresentation.getId().equals( empaiaIntAssociateRoleRepresentation.getId() ) ) {
                    userRole = UserRole.EMPAIA_INTERNATIONAL_ASSOCIATE;
                } else {
                    log.error( "Could not map role representation to user role: " + roleRepresentation.getName() + " " + roleRepresentation.getId() );
                }
            }
        }
        return userRole;
    }

    public void setUserRoles( KeycloakId userId, List<UserRole> userRoles ) {
        setUserRoles( getUserById( userId.toString() ), userRoles );
    }

    public void setUserRoles( UserResource userResource, List<UserRole> userRoles ) {
        List<RoleRepresentation> rolesToAdd = userRoles.stream()
                .map( this::getRoleRepresentationFromUserRole )
                .collect( Collectors.toList() );
        RoleScopeResource roleScopeResource = userResource.roles().clientLevel( getIdOfAuthServiceClient() );
        List<RoleRepresentation> rolesToRemove = roleScopeResource.listEffective();
        rolesToRemove.removeAll( rolesToAdd );
        if ( !rolesToRemove.isEmpty() ) {
            roleScopeResource.remove( rolesToRemove );
        }
        if ( !rolesToAdd.isEmpty() ) {
            roleScopeResource.add( rolesToAdd );
        }
    }

    public void grantAdminPrivileges( KeycloakId userId ) {
        grantOrRevokeAdminPrivileges( userId, true );
    }

    public void revokeAdminPrivileges( KeycloakId userId ) {
        grantOrRevokeAdminPrivileges( userId, false );
    }

    private void grantOrRevokeAdminPrivileges( KeycloakId userId, boolean grantAdminPrivileges ) {
        UserResource userResource = getUserById( userId.toString() );
        RoleScopeResource roleScopeResource = userResource.roles().clientLevel( getIdOfAuthServiceClient() );
        if ( grantAdminPrivileges ) {
            roleScopeResource.add( Collections.singletonList( this.adminRoleRepresentation ) );
        } else {
            roleScopeResource.remove( Collections.singletonList( this.adminRoleRepresentation ) );
        }
    }

    private RoleRepresentation getRoleRepresentationFromUserRole( UserRole userRole ) {
        if ( userRole == UserRole.MODERATOR ) {
            return moderatorRoleRepresentation;
        }
        if ( userRole == UserRole.EMPAIA_INTERNATIONAL_ASSOCIATE ) {
            return empaiaIntAssociateRoleRepresentation;
        }
        throw new RuntimeException( "Unhandled user role: " + userRole.getName() );
    }

    public boolean userHasRole( UserResource userResource, RoleRepresentation role ) {
        List<RoleRepresentation> roles = new ArrayList<>();
        roles.add( role );
        return userHasAnyRole( userResource, roles );
    }

    public String getIdOfAuthServiceClient() {
        List<ClientRepresentation> clientSearch = findClients( getClientId(), true, true, 0, 1 );
        if ( clientSearch.isEmpty() ) {
            throw new NotFoundException( "Id of Auth Service Client not found" );
        }
        return clientSearch.get( 0 ).getId();
    }

    //endregion

    //region Scopes

    public void renameClientScope( String oldScopeName, String newScopeName ) {
        if ( hasClientScope( newScopeName ) ) {
            throw new AlreadyExistException(
                    "Cannot rename client scope " + oldScopeName +
                            ", because the new client name exists already: " + newScopeName
            );
        }

        try {
            ClientScopeRepresentation clientScopeRepresentation = getClientScopesResource()
                    .findAll()
                    .stream()
                    .filter( s -> s.getName().equals( oldScopeName ) )
                    .toList()
                    .stream()
                    .findFirst()
                    .orElseThrow(
                            () -> new KeycloakInternalException( "Group scope not found: " + oldScopeName )
                    );

            clientScopeRepresentation.setName( newScopeName );
            assert clientScopeRepresentation.getDescription().equals( String.format( "Scope for %s", oldScopeName ) );
            clientScopeRepresentation.setDescription( String.format( "Scope for %s", newScopeName ) );

            ProtocolMapperRepresentation mapper = clientScopeRepresentation.getProtocolMappers().get( 0 );
            Map<String, String> mapperConfig = mapper.getConfig();
            assert mapperConfig.get( "included.custom.audience" ).equals( oldScopeName );
            mapperConfig.put( "included.custom.audience", newScopeName );
            mapper.setConfig( mapperConfig );

            ClientScopeResource clientScopeResource = getClientScopesResource().get( clientScopeRepresentation.getId() );
            clientScopeResource.getProtocolMappers().update( mapper.getId(), mapper );
            clientScopeResource.update( clientScopeRepresentation );

        } catch ( Exception e ) {
            log.error( "Rename client scope error", e );
            throw new KeycloakInternalException( "An error occurred while renaming client scope" );
        }
    }

    public void createGroupScope( String namespace ) throws KeycloakInternalException {
        try {
            HashMap<String, String> mapperConfig = new HashMap<>();
            String audience = namespace.replace( ".scope", "" );
            mapperConfig.put( "included.custom.audience", audience );

            createScope( namespace,
                    String.format( "Scope for %s", namespace ),
                    "audience mapper",
                    Protocol.MAPPER_AUDIENCE,
                    mapperConfig
            );
        } catch ( Exception e ) {
            log.error( "Error creating group scope", e );
            throw new KeycloakInternalException( "An error occurred while creating internal group rights" );
        }
    }

    public void deleteGroupScope( String namespace ) {
        try {
            Optional<ClientScopeRepresentation> scope = getClientScopesResource()
                    .findAll()
                    .stream()
                    .filter( s -> s.getName().equals( namespace ) )
                    .toList()
                    .stream()
                    .findFirst();

            if ( scope.isEmpty() ) {
                return;
            }
            getClientScopesResource().get( scope.get().getId() ).remove();
        } catch ( Exception e ) {
            log.error( "Delete group scope error", e );
            throw new KeycloakInternalException( "An error occurred while deleting internal group rights" );
        }
    }

    //endregion

    //region Internal

    public boolean hasClientScope( String scopeName ) {
        boolean clientScopeFound = false;
        List<ClientScopeRepresentation> clientScopeRepresentations = getClientScopesResource().findAll();
        for ( ClientScopeRepresentation clientScopeRepresentation : clientScopeRepresentations ) {
            if ( clientScopeRepresentation.getName().equals( scopeName ) ) {
                clientScopeFound = true;
                break;
            }
        }
        return clientScopeFound;
    }

    private void createScope(
            String scopeName,
            String scopeDescription,
            String mapperName,
            String mapperType,
            HashMap<String, String> additionalParams
    ) {
        try {
            HashMap<String, String> mapperConfig = new HashMap<>();
            mapperConfig.put( "access.token.claim", "true" );
            mapperConfig.put( "id.token.claim", "true" );
            mapperConfig.putAll( additionalParams );

            ProtocolMapperRepresentation mapper = new ProtocolMapperRepresentation();
            mapper.setName( mapperName );
            mapper.setProtocolMapper( mapperType );
            mapper.setProtocol( Protocol.OPENID_CONNECT );
            mapper.setConfig( mapperConfig );

            ClientScopeRepresentation scope = new ClientScopeRepresentation();
            scope.setName( scopeName );
            scope.setDescription( scopeDescription );
            scope.setProtocol( Protocol.OPENID_CONNECT );
            scope.setProtocolMappers( List.of( mapper ) );

            Response ignored = getClientScopesResource().create( scope );
            ignored.close();
        } catch ( Exception e ) {
            log.error( "Create scope error", e );
            throw new KeycloakInternalException( "An error occurred inside the identity provider" );
        }
    }

    private String extractIdFromLocation( Response response, String errorMessage ) {
        if ( response.getStatusInfo().getFamily().equals( Response.Status.Family.SUCCESSFUL ) ) {
            String path = response.getLocation().getPath();
            int slash = path.lastIndexOf( '/' );
            if ( slash > 0 ) {
                return path.substring( slash + 1 );
            }
        } else if ( response.getStatusInfo().getStatusCode() == HttpStatus.SC_CONFLICT ) {
            throw new AlreadyExistException( errorMessage );
        }
        return null;
    }

    //endregion

    public void removeProtocolMapperFromClientScope( String clientScopeName, String protocolMapperName ) {
        var clientScope = getRealmResource().clientScopes().findAll().stream().filter(
                clientScopeRepresentation -> clientScopeRepresentation.getName().equals( clientScopeName )
        ).findFirst();
        if ( clientScope.isPresent() ) {
            var clientScopeRepresentation = clientScope.get();
            var protocolMappers = clientScopeRepresentation.getProtocolMappers();
            Optional<ProtocolMapperRepresentation> protocolMapper;
            try {
                protocolMapper = protocolMappers.stream().filter(
                        protocolMapperRepresentation -> protocolMapperRepresentation.getName().equals( protocolMapperName )
                ).findFirst();
            } catch ( javax.ws.rs.NotFoundException e ) {
                protocolMapper = Optional.empty();
            }
            if ( protocolMapper.isPresent() ) {
                var clientScopeResource = getRealmResource().clientScopes().get( clientScopeRepresentation.getId() );
                clientScopeResource.getProtocolMappers().delete( protocolMapper.get().getId() );
                log.info( "Deleted protocol mapper '" + protocolMapperName + "' from client scope '" + clientScopeName + "'" );
            } else {
                log.warn( "Cannot deleted protocol mapper '" + protocolMapperName + "' from client scope '" + clientScopeName + "', because the protocol mapper does not exist" );
            }
        } else {
            log.warn( "Cannot deleted protocol mapper '" + protocolMapperName + "' from client scope '" + clientScopeName + "', because the client scope does not exist" );
        }
    }
}
