package org.empaia.services.v2;

import lombok.extern.slf4j.Slf4j;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.db.v2.DBOrganizationDetails;
import org.empaia.models.dto.v2.*;
import org.empaia.models.enums.v2.*;
import org.empaia.repositories.v2.OrganizationDetailsRepository;
import org.empaia.services.KeycloakService;
import org.empaia.utils.KeycloakUtils;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.BadRequestException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
@Transactional
public class ModeratorService {

    private final ConfidentialOrganizationService confidentialOrganizationService;
    private final OrganizationCategoryRequestService organizationCategoryRequestService;
    private final KeycloakClientService keycloakClientService;
    private final UserServiceV2 userServiceV2;
    private final KeycloakService keycloakService;
    private final ShoutOutService shoutOutService;
    private final OrganizationDetailsRepository organizationDetailsRepository;

    public ModeratorService(
            ConfidentialOrganizationService confidentialOrganizationService,
            KeycloakService keycloakService,
            ShoutOutService shoutOutService,
            OrganizationDetailsRepository organizationDetailsRepository,
            UserServiceV2 userServiceV2,
            OrganizationCategoryRequestService organizationCategoryRequestService,
            KeycloakClientService keycloakClientService
    ) {
        this.confidentialOrganizationService = confidentialOrganizationService;
        this.keycloakService = keycloakService;
        this.shoutOutService = shoutOutService;
        this.organizationDetailsRepository = organizationDetailsRepository;
        this.userServiceV2 = userServiceV2;
        this.organizationCategoryRequestService = organizationCategoryRequestService;
        this.keycloakClientService = keycloakClientService;
    }

    public UserProfileEntity getUserProfile( KeycloakId userId ) {
        return userServiceV2.getUserProfile( userId );
    }

    public ConfidentialOrganizationsEntity getConfidentialOrganizationsOfUser( int page, int pageSize, KeycloakId userId ) {
        return this.confidentialOrganizationService.getApprovedConfidentialOrganizationsEntityForUser( page, pageSize, userId );
    }

    public RegisteredUsersEntity getUsers( Integer page, Integer pageSize ) {
        List<RegisteredUserEntity> users = keycloakService.getUsers( page, pageSize ).stream()
                .map( this::createRegisteredUserEntityFromUserRepresentation )
                .collect( Collectors.toList() );
        return RegisteredUsersEntity.builder()
                .users( users )
                .totalUserCount( keycloakService.getUsersResource().count() )
                .build();
    }

    private RegisteredUserEntity createRegisteredUserEntityFromUserRepresentation( UserRepresentation userRepresentation ) {
        KeycloakId userId = new KeycloakId( userRepresentation.getId() );
        List<String> organizationNames = keycloakService.getOrganizationNamesForUser( userId );
        List<UserRole> userRoles = keycloakService.getUserRoles( userRepresentation.getId() );
        return RegisteredUserEntity.builder()
                .memberOf( organizationNames )
                .userId( userId )
                .userName( userRepresentation.getUsername() )
                .userRoles( userRoles )
                .isActive( userRepresentation.isEnabled() )
                .build();
    }

    public RegisteredUsersEntity getUnapprovedUsers( Integer page, Integer pageSize ) {
        List<RegisteredUserEntity> users = keycloakService.getUnapprovedUsers( page, pageSize ).stream()
                .map( this::createRegisteredUserEntityFromUserRepresentation )
                .collect( Collectors.toList() );
        // TODO: this also returns service accounts! really? check that in a test
        boolean emailVerified = false;
        return RegisteredUsersEntity.builder()
                .users( users )
                .totalUserCount( keycloakService.getUsersResource().count( null, null, "@", emailVerified, null ) )
                .build();
    }

    public UserProfileEntity getUserProfileOfUnapprovedUser( KeycloakId userId ) {
        return userServiceV2.getUserProfileOfUnapprovedUser( userId );
    }

    public void deleteUnapprovedUser( KeycloakId userId, KeycloakId deletedByUserId ) {
        userServiceV2.deleteUnapprovedUser( userId, deletedByUserId );
    }

    public List<UserRole> setUserRoles( KeycloakId userId, UserRolesEntity userRoles ) {
        keycloakService.setUserRoles( userId, userRoles.getUserRoles() );
        return keycloakService.getUserRoles( userId.toString() );
    }

    public void acceptOrganization( KeycloakId organizationId ) {
        DBOrganizationDetails organizationDetails = getValidOrganizationDetails( organizationId );
        OrganizationAccountState accountState = organizationDetails.getOrganizationAccountState();
        if ( accountState == OrganizationAccountState.AWAITING_ACTIVATION ) {
            organizationDetails.setOrganizationAccountState( OrganizationAccountState.ACTIVE );
            // The keycloak subgroups for the organization user roles are created when the organization
            // is approved, not before. Because the unapproved organization can be easier deleted this way and
            // no user can accidentally be added to any of the subgroups before the organization is approved:
            ConfidentialOrganizationProfileEntity profileEntity = confidentialOrganizationService.getActiveConfidentialOrganizationProfileEntity( organizationId );
            keycloakClientService.setupDefaultOrganizationClients( organizationId, profileEntity.getDetails().getCategories() ); // default clients are created but initially disabled
            createOrganizationUserRolesSubGroups( organizationId, profileEntity.getDetails().getCategories() );
            makeCreatorOrganizationManager( organizationId, organizationDetails );
        } else {
            throw new BadRequestException( "Organization cannot be approved, because the account state is " + accountState.toName() );
        }
        shoutOutService.notifyUserOrganizationApproved( new KeycloakId( organizationDetails.getCreatorId() ) );
    }

    private void createOrganizationUserRolesSubGroups( KeycloakId organizationId, List<OrganizationCategory> categories ) {
        createOrganizationUserRolesSubGroupWithAllowedAudiences( organizationId, OrganizationUserRoleV2.MANAGER );
        for ( OrganizationCategory category : categories ) {
            if ( category.equals( OrganizationCategory.APP_CUSTOMER ) ) {
                createOrganizationUserRolesSubGroupWithAllowedAudiences( organizationId, OrganizationUserRoleV2.PATHOLOGIST );
                createOrganizationUserRolesSubGroupWithAllowedAudiences( organizationId, OrganizationUserRoleV2.DATA_MANAGER );
            }
            if ( category.equals( OrganizationCategory.APP_VENDOR ) ) {
                createOrganizationUserRolesSubGroupWithAllowedAudiences( organizationId, OrganizationUserRoleV2.APP_MAINTAINER );
            }
            if ( category.equals( OrganizationCategory.PRODUCT_PROVIDER ) ) {
                createOrganizationUserRolesSubGroupWithAllowedAudiences( organizationId, OrganizationUserRoleV2.CLEARANCE_MAINTAINER );
            }
        }
    }

    private void createOrganizationUserRolesSubGroupWithAllowedAudiences( KeycloakId organizationId, OrganizationUserRoleV2 role ) {
        GroupRepresentation subGroup = new GroupRepresentation();
        subGroup.setName( role.getName() );
        keycloakService.createSubGroup( organizationId.toString(), subGroup, true );
        if ( role == OrganizationUserRoleV2.APP_MAINTAINER || role == OrganizationUserRoleV2.CLEARANCE_MAINTAINER ) {
            addAllowedAudienceFromOrganizationUserRoleGroup( organizationId, role, new HashSet<>(
                    Collections.singletonList( "marketplace_service_client" ) )
            );
        }
        if ( role == OrganizationUserRoleV2.PATHOLOGIST ) {
            addAllowedAudienceFromOrganizationUserRoleGroup( organizationId, role, new HashSet<>(
                    Collections.singletonList( String.format( "%s.%s", organizationId, ClientType.WORKBENCH_SERVICE.getShortName() ) ) )
            );
        }
        if ( role == OrganizationUserRoleV2.DATA_MANAGER ) {
            addAllowedAudienceFromOrganizationUserRoleGroup( organizationId, role, new HashSet<>( Set.of(
                    String.format( "%s.%s", organizationId, ClientType.MEDICAL_DATA_SERVICE.getShortName() ),
                    String.format( "%s.%s", organizationId, ClientType.ID_MAPPER_SERVICE.getShortName() ),
                    String.format( "%s.%s", organizationId, ClientType.UPLOAD_SERVICE.getShortName() )
            ) ) );
        }
    }

    public void addAllowedAudienceFromOrganizationUserRoleGroup( KeycloakId organizationId, OrganizationUserRoleV2 organizationUserRole, Set<String> audiences ) {
        GroupRepresentation groupRepresentation = this.getOrCreateUserRoleSubgroupForOrganizationGroup( organizationId, organizationUserRole );
        Map<String, List<String>> attributes = groupRepresentation.getAttributes();
        if ( attributes == null ) {
            attributes = new HashMap<>();
        }
        HashSet<String> allowedAudience = KeycloakUtils.getAllowedAudience( attributes );
        allowedAudience.addAll( audiences );
        KeycloakUtils.setAllowedAudience( attributes, allowedAudience );
        keycloakService.updateGroup( groupRepresentation );
    }

    public GroupRepresentation getOrCreateUserRoleSubgroupForOrganizationGroup( KeycloakId organizationId, OrganizationUserRoleV2 role ) {
        GroupRepresentation subgroup = new GroupRepresentation();
        subgroup.setName( role.getName() );
        keycloakService.createSubGroup( organizationId.toString(), subgroup, true );
        return keycloakService.getOrganizationSubGroup( organizationId.toString(), role.getName() );
    }

    public void removeUserRoleSubgroupFromOrganizationGroup( KeycloakId organizationId, List<OrganizationUserRoleV2> organizationUserRole ) {
        Set<String> subgroups = organizationUserRole.stream().map( OrganizationUserRoleV2::getName ).collect( Collectors.toSet() );
        List<GroupRepresentation> subgroupRepresentations = keycloakService.getOrganizationSubGroups( organizationId.toString(), subgroups );
        subgroupRepresentations.forEach( subgroup -> keycloakService.deleteGroup( subgroup.getId() ) );
    }

    private void makeCreatorOrganizationManager( KeycloakId organizationId, DBOrganizationDetails organizationDetails ) {
        OrganizationMember creator = OrganizationMember.builder()
                .organizationId( organizationId )
                .userId( new KeycloakId( organizationDetails.getCreatorId() ) )
                .build();
        keycloakService.addUserToOrganization( creator );
        keycloakService.setOrganizationUserRoles( creator, Collections.singletonList( OrganizationUserRoleV2.MANAGER ) );
    }

    public void denyOrganization( KeycloakId organizationId, PostReviewerComment reviewerComment ) {
        DBOrganizationDetails organizationDetails = getValidOrganizationDetails( organizationId );
        OrganizationAccountState accountState = organizationDetails.getOrganizationAccountState();
        if ( accountState == OrganizationAccountState.AWAITING_ACTIVATION ) {
            organizationDetails.setOrganizationAccountState( OrganizationAccountState.REQUIRES_ACTIVATION );
        } else {
            throw new BadRequestException( "Organization cannot be denied, because the account state is " + accountState.toName() );
        }
        shoutOutService.notifyUserOrganizationRejected( new KeycloakId( organizationDetails.getCreatorId() ) );
    }

    public OrganizationCategoryRequestsEntity getOrganizationCategoryRequests( int page, int pageSize ) {
        return organizationCategoryRequestService.getOrganizationCategoryRequests( page, pageSize );
    }

    public OrganizationCategoryRequestEntity acceptOrganizationCategoryRequest( KeycloakId userId, int categoryRequestId ) {
        OrganizationCategoryRequestEntity categoryRequestEntity = organizationCategoryRequestService.getOrganizationCategoryRequest( categoryRequestId );
        KeycloakId organizationId = categoryRequestEntity.getOrganizationId();

        categoryRequestEntity.getRetractedCategories().forEach( category -> removeSubgroupsAndAllowedAudiencesForCategory( organizationId, category ) );
        removeAffectedOrganizationUserRolesForRetractedCategories( organizationId, categoryRequestEntity.getRetractedCategories() );
        categoryRequestEntity.getRequestedCategories().forEach( category -> addSubgroupsAndAllowedAudiencesForCategory( organizationId, category ) );

        OrganizationCategoryRequestEntity request = organizationCategoryRequestService.acceptOrganizationCategoryRequest( userId, categoryRequestId );
        updateOrganizationClientsAndClientScopes( organizationId, categoryRequestEntity.getRetractedCategories(), categoryRequestEntity.getRequestedCategories() );
        return request;
    }

    public void removeAffectedOrganizationUserRolesForRetractedCategories( KeycloakId organizationId, List<OrganizationCategory> categoryList ) {
        Set<OrganizationCategory> retractedCategories = new HashSet<>( categoryList );
        Set<OrganizationUserRoleV2> rolesToRetract = keycloakService.getAllowedOrganizationUserRolesForCategories( retractedCategories );
        rolesToRetract.remove( OrganizationUserRoleV2.MANAGER );

        List<UserRepresentation> userRepresentations = keycloakService.getOrganizationMembers( organizationId.toString() );
        userRepresentations.forEach( user -> {
            KeycloakId userId = new KeycloakId( user.getId() );
            Set<OrganizationUserRoleV2> userRoles = keycloakService.getOrganizationUserRoles( userId, organizationId );
            userRoles.removeAll( rolesToRetract );

            OrganizationMember organizationMember = OrganizationMember.builder()
                    .organizationId( organizationId )
                    .userId( userId )
                    .build();
            keycloakService.setOrganizationUserRoles( organizationMember, new ArrayList<>( userRoles ) );
        } );
    }

    public void removeSubgroupsAndAllowedAudiencesForCategory( KeycloakId organizationId, OrganizationCategory category ) {
        switch ( category ) {
            case APP_CUSTOMER ->
                    removeUserRoleSubgroupFromOrganizationGroup( organizationId, List.of( OrganizationUserRoleV2.DATA_MANAGER, OrganizationUserRoleV2.PATHOLOGIST ) );
            case APP_VENDOR ->
                    removeUserRoleSubgroupFromOrganizationGroup( organizationId, List.of( OrganizationUserRoleV2.APP_MAINTAINER ) );
            case PRODUCT_PROVIDER ->
                    removeUserRoleSubgroupFromOrganizationGroup( organizationId, List.of( OrganizationUserRoleV2.CLEARANCE_MAINTAINER ) );
            case COMPUTE_PROVIDER -> {
            }
        }
    }

    public void addSubgroupsAndAllowedAudiencesForCategory( KeycloakId organizationId, OrganizationCategory category ) {
        switch ( category ) {
            case APP_CUSTOMER -> {
                Set<String> dataManagerAudiences = Stream.of( "idms", "mds", "us" )
                        .map( aud -> String.format( "%s.%s", organizationId, aud ) )
                        .collect( Collectors.toSet() );
                addAllowedAudienceFromOrganizationUserRoleGroup(
                        organizationId, OrganizationUserRoleV2.DATA_MANAGER, dataManagerAudiences
                );
                addAllowedAudienceFromOrganizationUserRoleGroup(
                        organizationId, OrganizationUserRoleV2.PATHOLOGIST,
                        new HashSet<>( Collections.singletonList( String.format( "%s.%s", organizationId, "wbs" ) ) )
                );
            }
            case APP_VENDOR -> addAllowedAudienceFromOrganizationUserRoleGroup(
                    organizationId, OrganizationUserRoleV2.APP_MAINTAINER,
                    new HashSet<>( Collections.singletonList( "marketplace_service_client" ) )
            );
            case PRODUCT_PROVIDER -> addAllowedAudienceFromOrganizationUserRoleGroup(
                    organizationId, OrganizationUserRoleV2.CLEARANCE_MAINTAINER,
                    new HashSet<>( Collections.singletonList( "marketplace_service_client" ) )
            );
            case COMPUTE_PROVIDER -> {
            }
        }
    }

    public OrganizationCategoryRequestEntity denyOrganizationCategoryRequest( KeycloakId userId, int categoryRequestId, String reviewerComment ) {
        return organizationCategoryRequestService.denyOrganizationCategoryRequest( userId, categoryRequestId, reviewerComment );
    }

    public UserProfileEntity activateUser( KeycloakId userId ) {
        UserResource userResource = keycloakService.getUserById( userId.toString() );
        UserRepresentation userRepresentation = userResource.toRepresentation();
        if ( !userRepresentation.isEnabled() ) {
            userRepresentation.setEnabled( false );
            userResource.update( userRepresentation );
            return this.getUserProfile( userId );
        } else {
            throw new BadRequestException( "User account is already active" );
        }
    }

    public UserProfileEntity deactivateUser( KeycloakId userId, KeycloakId moderatorUserId ) {
        if ( !moderatorUserId.equals( userId ) ) {
            UserResource userResource = keycloakService.getUserById( userId.toString() );
            UserRepresentation userRepresentation = userResource.toRepresentation();
            userRepresentation.setEnabled( false );
            userResource.update( userRepresentation );
            shoutOutService.notifyUserAccountDeactivated( userId );
            return this.getUserProfile( userId );
        } else {
            throw new BadRequestException( "You cannot deactivate your own user account. Ask another moderator to do that." );
        }
    }

    public ConfidentialOrganizationProfileEntity getUnapprovedOrganizationProfileEntity( KeycloakId organizationId ) {
        return confidentialOrganizationService.getUnapprovedConfidentialOrganizationProfileEntity( organizationId );
    }

    public void activateOrganization( KeycloakId organizationId ) {
        DBOrganizationDetails organizationDetails = getValidOrganizationDetails( organizationId );
        OrganizationAccountState accountState = organizationDetails.getOrganizationAccountState();
        if ( accountState == OrganizationAccountState.INACTIVE ) {
            organizationDetails.setOrganizationAccountState( OrganizationAccountState.ACTIVE );
            keycloakClientService.setEnabledFlagForOrganizationClients( organizationId, true );
        } else {
            throw new BadRequestException( "Organization cannot be activated, because the account state is " + accountState.toName() );
        }
    }

    public void deactivateOrganization( KeycloakId organizationId ) {
        DBOrganizationDetails organizationDetails = getValidOrganizationDetails( organizationId );
        OrganizationAccountState accountState = organizationDetails.getOrganizationAccountState();
        if ( accountState == OrganizationAccountState.AWAITING_ACTIVATION || accountState == OrganizationAccountState.ACTIVE ) {
            organizationDetails.setOrganizationAccountState( OrganizationAccountState.INACTIVE );
            keycloakClientService.setEnabledFlagForOrganizationClients( organizationId, false );
        } else {
            throw new BadRequestException( "Organization cannot be deactivated, because the account state is " + accountState.toName() );
        }
    }

    private DBOrganizationDetails getValidOrganizationDetails( KeycloakId organizationId ) {
        DBOrganizationDetails organizationDetails = organizationDetailsRepository.findByKeycloakId(
                organizationId.toString()
        ).orElseThrow( () -> new BadRequestException( "Organization not found" ) );
        keycloakService.getGroup( organizationDetails.getKeycloakId() ).orElseThrow(
                () -> new BadRequestException( "Organization does not exist" )
        );
        if ( keycloakService.isMissingUser( organizationDetails.getCreatorId() ) ) {
            throw new BadRequestException( "Organization creator does not exist" );
        }
        return organizationDetails;
    }

    public ConfidentialOrganizationsEntity getApprovedOrganizationsEntity( Integer page, Integer pageSize ) {
        return confidentialOrganizationService.getApprovedConfidentialOrganizationsEntity( page, pageSize );
    }

    public ConfidentialOrganizationProfileEntity getConfidentialOrganizationProfileEntity( KeycloakId organizationId ) {
        return confidentialOrganizationService.getConfidentialOrganizationProfileEntity( organizationId );
    }

    public ConfidentialOrganizationsEntity getOrganizationsEntity(
            Integer page, Integer pageSize, List<OrganizationAccountState> organizationAccountStates
    ) {
        return confidentialOrganizationService.getConfidentialOrganizationsEntity( page, pageSize, organizationAccountStates );
    }

    public void setOrganizationCategories( KeycloakId organizationId, KeycloakId userId, OrganizationCategoriesEntity organizationCategoriesEntity ) {
        // this should follow the same process as when a organization manager requests a category change
        PostOrganizationCategoryRequestEntity postOrganizationCategoryRequest = PostOrganizationCategoryRequestEntity.builder()
                .requestedOrganizationCategories( organizationCategoriesEntity.getOrganizationCategories() ).build();
        OrganizationCategoryRequestEntity categoryRequest = organizationCategoryRequestService.requestOrganizationCategory(
                userId, organizationId, postOrganizationCategoryRequest
        );
        acceptOrganizationCategoryRequest( userId, categoryRequest.getOrganizationCategoryRequestId() );
    }

    private void updateOrganizationClientsAndClientScopes( KeycloakId organizationId, List<OrganizationCategory> retractedCategories, List<OrganizationCategory> newCategories ) {
        // delete unused clients and client scopes
        for ( OrganizationCategory category : retractedCategories ) {
            keycloakClientService.deleteClientsAndClientsScopesForOrganizationCategory( organizationId, category );
        }
        // add new clients and client scopes that do NOT already exist
        keycloakClientService.setupDefaultOrganizationClients( organizationId, newCategories );
    }

    public void deleteUser( KeycloakId targetUserId, KeycloakId deletedByUserId, boolean checkUserRole, boolean checkManagerInOrg ) {
        userServiceV2.deleteUser( targetUserId, deletedByUserId, checkUserRole, checkManagerInOrg );
    }
}
