package org.empaia.services.v2;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import lombok.extern.slf4j.Slf4j;
import org.empaia.configs.EmpaiaConfiguration;
import org.empaia.exceptions.generic.AlreadyExistException;
import org.empaia.exceptions.jwt.JwtInvalidException;
import org.empaia.exceptions.keycloak.KeycloakEntityAlreadyExistsException;
import org.empaia.exceptions.user.ValidationException;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.PostUserEntity;
import org.empaia.models.dto.v2.PublicOrganizationProfileEntity;
import org.empaia.models.dto.v2.PublicOrganizationsEntity;
import org.empaia.services.KeycloakService;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
@Transactional
public class PublicService {

    private final EmpaiaConfiguration empaiaConfiguration;
    private final KeycloakService keycloakService;
    private final PublicOrganizationService publicOrganizationService;
    private final UserServiceV2 userService;

    public PublicService(
            EmpaiaConfiguration empaiaConfiguration,
            KeycloakService keycloakService,
            PublicOrganizationService publicOrganizationService,
            UserServiceV2 userService
    ) {
        this.empaiaConfiguration = empaiaConfiguration;
        this.keycloakService = keycloakService;
        this.publicOrganizationService = publicOrganizationService;
        this.userService = userService;
    }

    public void createNewUser( PostUserEntity userEntity ) {
        try {
            userService.createNewUser( userEntity );
        } catch (KeycloakEntityAlreadyExistsException e) {
            throw new AlreadyExistException( "A user with this email address exists already ");
        }
    }

    private String expectVerifyEMailTokenTypeAndReturnKeycloakUserId( String token ) {
        Jws<Claims> claims = null;
        String malformedError = "";
        try{
            claims = Jwts.parser()
                    .setSigningKey( empaiaConfiguration.getTokenSecret() )
                    .requireAudience( empaiaConfiguration.getServerUrl() )
                    .requireIssuer( empaiaConfiguration.getServerUrl() )
                    .parseClaimsJws( token );
        } catch (MalformedJwtException ex) {
            log.error( String.format("Malformed JWT token: %s", token) );
            malformedError = ex.getMessage();
        }
        if ( claims == null ) {
            throw new JwtInvalidException( String.format( "Malformed JWT token: %s", malformedError ) );
        }
        String expectedTokenType = "VERIFY_EMAIL";
        if ( ! claims.getBody().get( "typ" ).equals( expectedTokenType ) ) {
            throw new ValidationException( String.format( "Invalid token type (required: %s )", expectedTokenType ) );
        }

        return claims.getBody().getSubject();
    }

    public void verifyRegistration( String emailAddress, String tokenString ) throws JwtInvalidException, ValidationException {
        if ( tokenString == null || tokenString.isBlank() ) {
            throw new JwtInvalidException( "Token is required" );
        }

        String keycloakUserId = expectVerifyEMailTokenTypeAndReturnKeycloakUserId( tokenString );

        UserResource userResource = keycloakService.getUserById( keycloakUserId );
        UserRepresentation userRepresentation = userResource.toRepresentation();
        if ( !userRepresentation.getEmail().equals(emailAddress.toLowerCase()) ) {
            throw new ValidationException( "Email address doesn't match" );
        }

        userRepresentation.setEmailVerified( true );
        // TODO: if we want to ensure valid contact data, then add "UPDATE_PROFILE" to required actions:
        userRepresentation.setRequiredActions( List.of() );
        userResource.update( userRepresentation );
    }

    public PublicOrganizationsEntity getActiveOrganizations( int page, int size ) {
        return publicOrganizationService.getActivePublicOrganizationsEntity( page, size );
    }

    public PublicOrganizationProfileEntity getActiveOrganization( KeycloakId organizationId ) {
        return publicOrganizationService.getActivePublicOrganizationProfileEntity( organizationId );
    }
}
