package org.empaia.services.v2;

import com.jlefebure.spring.boot.minio.MinioException;
import org.empaia.exceptions.file.FileIsNotAnImageException;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.ResizedPictureUrlsEntity;
import lombok.extern.slf4j.Slf4j;
import org.imgscalr.Scalr;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URLConnection;
import java.util.Arrays;

@Service
@Slf4j
@Transactional
public class ImageFileService {
    private static final String[] IMAGE_MIME_TYPES = new String[]{ "image/png", "image/jpeg", "image/jpg" };


    private final FileServiceV2 fileService;

    public ImageFileService(FileServiceV2 fileService ) {
        this.fileService = fileService;
    }

    protected static String makeImageFileName( KeycloakId id, String objectNameTemplate ) {
        return String.format( objectNameTemplate, id.toString() );
    }

    protected static String makeImageFileName( KeycloakId id, String objectNameTemplate, int width ) {
        return String.format( objectNameTemplate, id.toString(), width );
    }

    public String getImageFileUrl( KeycloakId id, String objectNameTemplate ) {
        String url = null;
        String imageFileName = makeImageFileName( id, objectNameTemplate );
        try {
            url = fileService.getPresignedUrl( imageFileName, null );
        } catch ( Exception e ) {
            log.error( "Failed to get presigned url for image file", e );
        }
        return url;
    }

    public ResizedPictureUrlsEntity getResizedImageFileUrls( KeycloakId id, String objectNameTemplate ) {
        return ResizedPictureUrlsEntity.builder()
                .w60( getImageFileUrl( id, objectNameTemplate, 60 ) )
                .w400( getImageFileUrl( id, objectNameTemplate, 400 ) )
                .w800( getImageFileUrl( id, objectNameTemplate, 800 ) )
                .w1200( getImageFileUrl( id, objectNameTemplate, 1200 ) )
                .build();
    }

    public String getImageFileUrl( KeycloakId id, String objectNameTemplate, int width ) {
        String url = null;
        String imageFileName = makeImageFileName( id, objectNameTemplate, width );
        try {
            url = fileService.getPresignedUrl( imageFileName, null );
        } catch ( Exception e ) {
            log.error( "Failed to get presigned url for image file", e );
        }
        return url;
    }

    public ResponseEntity<InputStreamResource> getImageFile( KeycloakId id, String objectNameTemplate ) throws FileNotFoundException {
        String imageFileName = makeImageFileName( id, objectNameTemplate );
        InputStreamResource inputStreamResource = fileService.downloadObject( imageFileName );
        return ResponseEntity
                .ok()
                .header( HttpHeaders.CONTENT_DISPOSITION, String.format( "attachment; filename=\"%s\"", imageFileName ) )
                .contentType( MediaType.valueOf( URLConnection.guessContentTypeFromName( imageFileName ) ) )
                .body( inputStreamResource );
    }

    public ResponseEntity<InputStreamResource> getImageFile( KeycloakId id, String objectNameTemplate, int width ) throws FileNotFoundException {
        String imageFileName = makeImageFileName( id, objectNameTemplate, width );
        InputStreamResource inputStreamResource = fileService.downloadObject( imageFileName );
        return ResponseEntity
                .ok()
                .header( HttpHeaders.CONTENT_DISPOSITION, String.format( "attachment; filename=\"%s\"", imageFileName ) )
                .contentType( MediaType.valueOf( URLConnection.guessContentTypeFromName( imageFileName ) ) )
                .body( inputStreamResource );
    }

    protected boolean isImage( MultipartFile profilePicture ) {
        return Arrays.asList( IMAGE_MIME_TYPES ).contains( profilePicture.getContentType() );
    }

    public void setImageFile( KeycloakId id, String objectNameTemplate, String resizedObjectNameTemplate, MultipartFile imageFile ) throws Exception {
        if ( !isImage( imageFile ) || imageFile.getSize() == 0 ) {
            throw new FileIsNotAnImageException( imageFile.getOriginalFilename() );
        }
        setImageFile( id, objectNameTemplate, resizedObjectNameTemplate, imageFile.getInputStream() );
    }

    public void setImageFile( KeycloakId id, String objectNameTemplate, String resizedObjectNameTemplate, InputStream inputStream ) throws Exception {
        deleteImageFiles( id, objectNameTemplate, resizedObjectNameTemplate );
        storeImageFiles( id, objectNameTemplate, resizedObjectNameTemplate, inputStream );
    }

    protected void deleteImageFiles( KeycloakId id, String objectNameTemplate, String resizedObjectNameTemplate ) throws MinioException {
        String imageFileName = makeImageFileName( id, objectNameTemplate );
        fileService.deleteObject( imageFileName );
        for ( Integer width : ResizedPictureUrlsEntity.RESIZED_IMAGE_WIDTHS ) {
            String resizedImageFileName = makeImageFileName( id, resizedObjectNameTemplate, width );
            fileService.deleteObject( resizedImageFileName );
        }
    }

    private BufferedImage convertToJpeg( InputStream inputStream ) throws IOException {
        BufferedImage image = ImageIO.read( inputStream );

        // TODO: why is it necessary to draw the image onto a white background image?
        BufferedImage finalImage = new BufferedImage(
                image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_BGR
        );
        finalImage.createGraphics().drawImage( image, 0, 0, Color.white, null );
        return finalImage;
    }

    private void uploadImage( String objectName, BufferedImage image ) throws IOException, MinioException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ImageIO.write( image, "jpeg", out );
        ByteArrayInputStream inputStream = new ByteArrayInputStream( out.toByteArray() );
        fileService.uploadObject( objectName, inputStream, "image/jpeg" );
    }

    public void storeImageFiles(
            KeycloakId id, String objectNameTemplate, String resizedObjectNameTemplate, InputStream inputStream
    ) throws Exception {
        BufferedImage image = convertToJpeg( inputStream );
        storeImageFiles( id, objectNameTemplate, resizedObjectNameTemplate, image );
    }

    private void storeImageFiles(
            KeycloakId id, String objectNameTemplate, String resizedObjectNameTemplate, BufferedImage image
    ) throws IOException, MinioException {
        String imageFileName = makeImageFileName( id, objectNameTemplate );
        uploadImage( imageFileName, image );
        for ( Integer width : ResizedPictureUrlsEntity.RESIZED_IMAGE_WIDTHS ) {
            String resizedImageFileName = makeImageFileName( id, resizedObjectNameTemplate, width );
            BufferedImage resizedImage = Scalr.resize( image, width );
            uploadImage( resizedImageFileName, resizedImage );
        }
    }
}
