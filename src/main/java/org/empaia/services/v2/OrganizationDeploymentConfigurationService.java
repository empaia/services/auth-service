package org.empaia.services.v2;

import lombok.extern.slf4j.Slf4j;
import org.empaia.exceptions.generic.NotFoundException;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.db.v2.DBOrganizationDeploymentConfiguration;
import org.empaia.models.dto.v2.OrganizationDeploymentConfiguration;
import org.empaia.models.dto.v2.PostOrganizationDeploymentConfiguration;
import org.empaia.models.enums.v2.ClientType;
import org.empaia.repositories.v2.OrganizationDeploymentConfigurationRepository;
import org.empaia.services.KeycloakService;
import org.empaia.utils.KeycloakUtils;
import org.empaia.utils.ValidationUtils;
import org.joda.time.LocalDateTime;
import org.keycloak.representations.idm.ClientRepresentation;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class OrganizationDeploymentConfigurationService {
    private final OrganizationDeploymentConfigurationRepository organizationDeploymentConfigurationRepository;
    private final KeycloakClientService keycloakClientService;
    private final KeycloakService keycloakService;

    public OrganizationDeploymentConfigurationService(
            OrganizationDeploymentConfigurationRepository organizationDeploymentConfigurationRepository,
            KeycloakClientService keycloakClientService,
            KeycloakService keycloakService
    ) {
        this.organizationDeploymentConfigurationRepository = organizationDeploymentConfigurationRepository;
        this.keycloakClientService = keycloakClientService;
        this.keycloakService = keycloakService;
    }

    public OrganizationDeploymentConfiguration setOrganizationDeploymentConfiguration(
            KeycloakId organizationId, PostOrganizationDeploymentConfiguration postConf, KeycloakId userId
    ) {
        String wbcDomain = postConf.getWorkbenchDeploymentDomain();
        String dmcDomain = postConf.getDataManagerDeploymentDomain();
        boolean localhostAllowed = postConf.isAllowLocalhostWebClients();
        boolean httpAllowed = postConf.isAllowHttp();
        ValidationUtils.validateUriList( postConf.getWorkbenchRedirectUris(), wbcDomain, localhostAllowed, httpAllowed );
        ValidationUtils.validateUriList( postConf.getDataManagerRedirectUris(), dmcDomain, localhostAllowed, httpAllowed );
        ValidationUtils.validateUriList( postConf.getWorkbenchWebOrigins(), wbcDomain, localhostAllowed, httpAllowed );
        ValidationUtils.validateUriList( postConf.getDataManagerWebOrigins(), dmcDomain, localhostAllowed, httpAllowed );

        // check if clients exist
        Set<ClientRepresentation> clientRepresentation = keycloakService.getAllClientsOfOrganization( organizationId );
        if ( clientRepresentation.isEmpty() )
            throw new NotFoundException( String.format( "No valid clients found for organization %s", organizationId ) );

        Set<ClientRepresentation> frontendClients = clientRepresentation.stream().filter( ClientRepresentation::isPublicClient ).collect( Collectors.toSet() );
        for ( ClientRepresentation client : frontendClients ) {
            keycloakClientService.updateFrontendClient( client, postConf );
        }

        // save additional config in db
        Optional<DBOrganizationDeploymentConfiguration> optDbConfig = organizationDeploymentConfigurationRepository.findByOrganizationId( organizationId.toString() );
        DBOrganizationDeploymentConfiguration dbConfig;
        if ( optDbConfig.isPresent() ) {
            dbConfig = optDbConfig.get();
            dbConfig.setAllowLocalhostWebClients( postConf.isAllowLocalhostWebClients() );
            dbConfig.setAllowHttp( postConf.isAllowHttp() );
            dbConfig.setWorkbenchDeploymentDomain( postConf.getWorkbenchDeploymentDomain() );
            dbConfig.setDataManagerDeploymentDomain( postConf.getDataManagerDeploymentDomain() );
            dbConfig.setUpdatedBy( userId.toString() );
            dbConfig.setUpdatedAt( LocalDateTime.now().toDate().getTime() );
        } else {
            dbConfig = DBOrganizationDeploymentConfiguration.builder()
                    .organizationId( organizationId.toString() )
                    .allowLocalhostWebClients( postConf.isAllowLocalhostWebClients() )
                    .allowHttp( postConf.isAllowHttp() )
                    .workbenchDeploymentDomain( postConf.getWorkbenchDeploymentDomain() )
                    .dataManagerDeploymentDomain( postConf.getDataManagerDeploymentDomain() )
                    .updatedBy( userId.toString() )
                    .updatedAt( LocalDateTime.now().toDate().getTime() )
                    .build();
        }

        organizationDeploymentConfigurationRepository.save( dbConfig );

        return getOrganizationDeploymentConfiguration( organizationId );
    }

    public OrganizationDeploymentConfiguration getOrganizationDeploymentConfiguration( KeycloakId organizationId ) throws NotFoundException {
        Optional<DBOrganizationDeploymentConfiguration> configuration = organizationDeploymentConfigurationRepository.findByOrganizationId( organizationId.toString() );
        if ( configuration.isPresent() ) {
            return buildOrganizationDeploymentConfiguration( configuration.get() );
        } else {
            throw new NotFoundException( "No valid deployment configuration for organization" );
        }
    }

    private OrganizationDeploymentConfiguration buildOrganizationDeploymentConfiguration(
            DBOrganizationDeploymentConfiguration dbOrganizationDeploymentConfiguration
    ) {
        KeycloakId organizationId = new KeycloakId( dbOrganizationDeploymentConfiguration.getOrganizationId() );
        ClientRepresentation wbcClient = keycloakService.getClientByClientId(
                KeycloakUtils.createClientName( organizationId, ClientType.WORKBENCH_CLIENT.getShortName() )
        );
        ClientRepresentation mdcClient = keycloakService.getClientByClientId(
                KeycloakUtils.createClientName( organizationId, ClientType.DATA_MANAGEMENT_CLIENT.getShortName() )
        );
        return OrganizationDeploymentConfiguration.builder()
                .organizationId( organizationId )
                .allowLocalhostWebClients( dbOrganizationDeploymentConfiguration.isAllowLocalhostWebClients() )
                .allowHttp( dbOrganizationDeploymentConfiguration.isAllowHttp() )
                .workbenchDeploymentDomain( dbOrganizationDeploymentConfiguration.getWorkbenchDeploymentDomain() )
                .dataManagerDeploymentDomain( dbOrganizationDeploymentConfiguration.getDataManagerDeploymentDomain() )
                .workbenchRedirectUris( wbcClient.getRedirectUris() )
                .workbenchWebOrigins( wbcClient.getWebOrigins() )
                .dataManagerRedirectUris( mdcClient.getRedirectUris() )
                .dataManagerWebOrigins( mdcClient.getWebOrigins() )
                .build();
    }
}
