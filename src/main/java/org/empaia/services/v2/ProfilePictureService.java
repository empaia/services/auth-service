package org.empaia.services.v2;

import com.jlefebure.spring.boot.minio.MinioException;
import lombok.extern.slf4j.Slf4j;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.ResizedPictureUrlsEntity;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.InputStream;

@Service
@Slf4j
@Transactional
public class ProfilePictureService extends ImageFileService {
    private static final String ORIGINAL_PROFILE_PICTURE_NAME = "users/%s/picture/original.jpeg";
    private static final String RESIZED_PROFILE_PICTURE_NAME = "users/%s/picture/%d.jpeg";


    public ProfilePictureService( FileServiceV2 fileService ) {
        super( fileService );
    }

    public String getProfilePictureUrl( KeycloakId userId ) {
        return getImageFileUrl( userId, ORIGINAL_PROFILE_PICTURE_NAME );
    }

    public ResizedPictureUrlsEntity getResizedProfilePictureUrls( KeycloakId userId ) {
        return getResizedImageFileUrls( userId, RESIZED_PROFILE_PICTURE_NAME );
    }

    public ResponseEntity<InputStreamResource> getProfilePicture( KeycloakId userId ) throws FileNotFoundException {
        return getImageFile( userId, ORIGINAL_PROFILE_PICTURE_NAME );
    }

    // TODO: delete?
    public String getOriginalProfilePictureName( KeycloakId userId ) {
        return makeImageFileName( userId, ORIGINAL_PROFILE_PICTURE_NAME );
    }

    // TODO: delete?
    public String getResizedProfilePictureName( KeycloakId userId, int width ) {
        return makeImageFileName( userId, RESIZED_PROFILE_PICTURE_NAME, width );
    }

    public ResponseEntity<InputStreamResource> getProfilePicture( KeycloakId userId, int width ) throws FileNotFoundException {
        return getImageFile( userId, RESIZED_PROFILE_PICTURE_NAME, width );
    }

    public void setProfilePicture( UserResource userResource, MultipartFile profilePicture ) throws Exception {
        UserRepresentation userRepresentation = userResource.toRepresentation();
        KeycloakId userId = new KeycloakId( userRepresentation.getId() );
        setProfilePicture( userId, profilePicture.getInputStream() );
    }

    public void deleteProfilePictures( UserResource userResource ) throws MinioException {
        UserRepresentation userRepresentation = userResource.toRepresentation();
        deleteImageFiles( new KeycloakId( userRepresentation.getId() ), ORIGINAL_PROFILE_PICTURE_NAME, RESIZED_PROFILE_PICTURE_NAME );
    }

    public void setProfilePicture( KeycloakId userId, InputStream inputStream ) throws Exception {
        setImageFile( userId, ORIGINAL_PROFILE_PICTURE_NAME, RESIZED_PROFILE_PICTURE_NAME, inputStream );
    }
}
