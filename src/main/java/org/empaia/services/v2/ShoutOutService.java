package org.empaia.services.v2;

import lombok.extern.slf4j.Slf4j;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.ConfidentialOrganizationProfileEntity;
import org.empaia.models.enums.v2.*;
import org.empaia.models.sos.*;
import org.empaia.services.KeycloakService;
import org.empaia.utils.KeycloakUtils;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ShoutOutService {

    private final String sosUrl;
    private final String portalBaseUrl;
    private final int rabbitmqMessageTimeout;


    private final TemplateService templateService;
    private final KeycloakService keycloakService;
    private final ConfidentialOrganizationService confOrgService;
    private final UserServiceV2 userServiceV2;

    @Autowired
    public ShoutOutService(
            @Value( "${sos.url}" ) String sosUrl,
            @Value( "${sos.portal_base_url}" ) String portalBaseUrl,
            @Value( "${rabbitmq.message_timeout}" ) String rabbitmqMessageTimeout,
            TemplateService templateService,
            KeycloakService keycloakService,
            ConfidentialOrganizationService confOrgService, UserServiceV2 userServiceV2
    ) {
        this.sosUrl = sosUrl;
        this.portalBaseUrl = portalBaseUrl;
        this.rabbitmqMessageTimeout = Integer.parseInt( rabbitmqMessageTimeout );
        this.templateService = templateService;
        this.keycloakService = keycloakService;
        this.confOrgService = confOrgService;
        this.userServiceV2 = userServiceV2;
    }

    // NEW ORGANIZATION CREATED

    public void notifyModeratorsNewOrganizationCreated( KeycloakId organizationId ) {
        // TODO: add further details to notifications
        ConfidentialOrganizationProfileEntity orgEntity = confOrgService.getConfidentialOrganizationProfileEntity( organizationId );
        String orgName = orgEntity.getOrganizationName();
        Map<String, String> templateArguments = new HashMap<>() {{
            //put( "organization_name", orgName );
        }};
        notifyModerators(
                Notification.USER_CREATES_ORGANIZATION,
                templateArguments,
                PortalEvent.UNAPPROVED_ORGANIZATION,
                EventAction.CREATED,
                NotificationChannel.SMTP_AMQP
        );
    }

    public void notifyUserOrganizationApproved( KeycloakId userId ) {
        notifyUser(
                userId,
                Notification.ORGANIZATION_IS_APPROVED,
                null,
                PortalEvent.UNAPPROVED_ORGANIZATION,
                EventAction.APPROVED,
                NotificationChannel.SMTP_AMQP
        );
    }

    public void notifyUserOrganizationRejected( KeycloakId userId ) {
        notifyUser(
                userId,
                Notification.ORGANIZATION_IS_REJECTED,
                null,
                PortalEvent.UNAPPROVED_ORGANIZATION,
                EventAction.REJECTED,
                NotificationChannel.SMTP_AMQP
        );
    }

    // ORGANIZATION MEMBERSHIP REQUEST

    public void notifyManagersOrganizationMembership( KeycloakId organizationId ) {
        notifyOrganizationManagers(
                Notification.USER_REQUESTS_ORGANIZATION_MEMBERSHIP,
                null,
                PortalEvent.MEMBERSHIP_REQUEST,
                EventAction.CREATED,
                organizationId,
                NotificationChannel.SMTP_AMQP
        );
    }

    public void notifyUserOrganizationMembershipApproved(
            KeycloakId organizationId,
            KeycloakId userId
    ) {
        notifyUser(
                userId,
                Notification.ORGANIZATION_MEMBERSHIP_IS_APPROVED,
                null,
                PortalEvent.MEMBERSHIP_REQUEST,
                EventAction.APPROVED,
                NotificationChannel.SMTP_AMQP
        );
        notifyOrganizationManagers(
                Notification.ORGANIZATION_MEMBERSHIP_IS_APPROVED,
                null,
                PortalEvent.MEMBERSHIP_REQUEST,
                EventAction.APPROVED,
                organizationId,
                NotificationChannel.AMQP
        );
    }

    public void notifyUserOrganizationMembershipRejected(
            KeycloakId organizationId,
            KeycloakId userId
    ) {
        notifyUser(
                userId,
                Notification.ORGANIZATION_MEMBERSHIP_IS_REJECTED,
                null,
                PortalEvent.MEMBERSHIP_REQUEST,
                EventAction.REJECTED,
                NotificationChannel.SMTP_AMQP
        );
        notifyOrganizationManagers(
                Notification.ORGANIZATION_MEMBERSHIP_IS_REJECTED,
                null,
                PortalEvent.MEMBERSHIP_REQUEST,
                EventAction.REJECTED,
                organizationId,
                NotificationChannel.AMQP
        );
    }

    // ROLE CHANGE REQUEST

    public void notifyManagersRoleChangeRequested( KeycloakId organizationId ) {
        notifyOrganizationManagers(
                Notification.ORGANIZATION_MEMBER_REQUESTS_ROLE,
                null,
                PortalEvent.ROLE_REQUEST,
                EventAction.CREATED,
                organizationId,
                NotificationChannel.SMTP_AMQP
        );
    }

    public void notifyUserRoleRequestApproved( KeycloakId organizationId, KeycloakId userId ) {
        notifyUser(
                userId,
                Notification.ORGANIZATION_MEMBER_ROLE_APPROVED,
                null,
                PortalEvent.ROLE_REQUEST,
                EventAction.APPROVED,
                NotificationChannel.SMTP_AMQP
        );
        notifyOrganizationManagers(
                Notification.ORGANIZATION_MEMBER_ROLE_APPROVED,
                null,
                PortalEvent.ROLE_REQUEST,
                EventAction.APPROVED,
                organizationId,
                NotificationChannel.AMQP
        );
    }

    public void notifyUserRoleRequestRejected( KeycloakId organizationId, KeycloakId userId ) {
        notifyUser(
                userId,
                Notification.ORGANIZATION_MEMBER_ROLE_REJECTED,
                null,
                PortalEvent.ROLE_REQUEST,
                EventAction.REJECTED,
                NotificationChannel.SMTP_AMQP
        );
        notifyOrganizationManagers(
                Notification.ORGANIZATION_MEMBER_ROLE_REJECTED,
                null,
                PortalEvent.ROLE_REQUEST,
                EventAction.REJECTED,
                organizationId,
                NotificationChannel.AMQP
        );
    }

    // MANAGER UPDATES MEMBER ROLE

    public void notifyUserMemberRoleUpdated( KeycloakId userId ) {
        notifyUser(
                userId,
                Notification.ORGANIZATION_MANAGER_UPDATES_USER_ROLE,
                null,
                PortalEvent.ORGANIZATION,
                EventAction.UPDATED,
                NotificationChannel.SMTP_AMQP
        );
    }

    // MODERATOR DEACTIVATES ACCOUNT

    public void notifyUserAccountDeactivated( KeycloakId userId ) {
        notifyUser(
                userId,
                Notification.MODERATOR_DEACTIVATES_ACCOUNT,
                null,
                null,
                null,
                NotificationChannel.SMTP
        );
    }

    // CATEGORY CHANGE

    public void notifyModeratorsCategoryChangeRequested( KeycloakId organizationId ) {
        notifyModerators(
                Notification.MANAGER_REQUESTS_ORGANIZATION_CATEGORY_CHANGE,
                null,
                PortalEvent.CATEGORY_REQUEST,
                EventAction.CREATED,
                NotificationChannel.SMTP_AMQP
        );
        notifyOrganizationManagers(
                Notification.MANAGER_REQUESTS_ORGANIZATION_CATEGORY_CHANGE,
                null,
                PortalEvent.CATEGORY_REQUEST,
                EventAction.CREATED,
                organizationId,
                NotificationChannel.AMQP
        );
    }

    // TODO: can we obtain the creator ID of the change request somehow? requires update in change request model
    public void notifyManagerCategoryChangeApproved( KeycloakId organizationId/*, KeycloakId managerUserId*/ ) {
//        notifyUser(
//                managerUserId,
//                Notification.ORGANIZATION_CATEGORY_CHANGE_APPROVED,
//                null,
//                PortalEvent.CATEGORY_REQUEST,
//                EventAction.APPROVED,
//                NotificationChannel.SMTP_AMQP
//        );
        notifyOrganizationManagers(
                Notification.ORGANIZATION_CATEGORY_CHANGE_APPROVED,
                null,
                PortalEvent.CATEGORY_REQUEST,
                EventAction.APPROVED,
                organizationId,
                NotificationChannel.AMQP
        );
    }

    // TODO: can we obtain the creator ID of the change request somehow? requires update in change request model
    public void notifyManagerCategoryChangeRejected( KeycloakId organizationId/*, KeycloakId managerUserId*/ ) {
//        notifyUser(
//                managerUserId,
//                Notification.ORGANIZATION_CATEGORY_CHANGE_REJECTED,
//                templateArguments,
//                PortalEvent.CATEGORY_REQUEST,
//                EventAction.REJECTED,
//                NotificationChannel.SMTP_AMQP
//        );
        notifyOrganizationManagers(
                Notification.ORGANIZATION_CATEGORY_CHANGE_REJECTED,
                null,
                PortalEvent.CATEGORY_REQUEST,
                EventAction.REJECTED,
                organizationId,
                NotificationChannel.AMQP
        );
    }

    // PRIVATE MEMBERS

    private void notifyUser(
            KeycloakId userId,
            Notification notification,
            Map<String, String> templateArguments,
            PortalEvent portalEvent,
            EventAction eventAction,
            NotificationChannel notificationChannel
    ) {
        try {
            UserRepresentation user = keycloakService.getUserById( userId ).toRepresentation();

            if ( templateArguments == null ) {
                templateArguments = new HashMap<>();
            }
            templateArguments.put( "first_name", user.getFirstName() );
            templateArguments.put( "last_name", user.getLastName() );
            templateArguments.put( "portal_base_url", portalBaseUrl );

            PostShoutOut shoutOut = gatherShoutOutData(
                    user,
                    notification,
                    templateArguments,
                    notificationChannel
            );

            postShoutOut( shoutOut );

            if ( portalEvent != null && eventAction != null ) {
                triggerEvent( portalEvent, eventAction, user.getId() );
            }
        } catch ( Exception ex ) {
            log.error( "Failed to notify user: {}", ex.getMessage() );
        }
    }

    private void notifyModerators(
            Notification notification,
            Map<String, String> templateArguments,
            PortalEvent portalEvent,
            EventAction eventAction,
            NotificationChannel notificationChannel
    ) {
        Set<UserRepresentation> moderators = userServiceV2.getUsersByRole( UserRole.MODERATOR.getName() );
        if ( moderators != null ) {
            notifyUserGroup(
                    notification,
                    templateArguments,
                    portalEvent,
                    eventAction,
                    moderators,
                    notificationChannel
            );
        } else {
            log.error( "No moderators found. Request can not be processed." );
        }
    }

    private void notifyOrganizationManagers(
            Notification notification,
            Map<String, String> templateArguments,
            PortalEvent portalEvent,
            EventAction eventAction,
            KeycloakId organizationId,
            NotificationChannel notificationChannel
    ) {
        // TODO: exclude requesting user, when creator ID is added to change request models
        Set<UserRepresentation> managers = keycloakService.getOrganizationMembers( organizationId.toString() )
                .stream()
                .filter( Objects::nonNull )
                .filter( user -> getOrganizationUserRoles( user, organizationId ).contains( OrganizationUserRoleV2.MANAGER ) )
                .collect( Collectors.toSet() );
        notifyUserGroup( notification, templateArguments, portalEvent, eventAction, managers, notificationChannel );
    }

    private List<OrganizationUserRoleV2> getOrganizationUserRoles(
            UserRepresentation userRepresentation,
            KeycloakId organizationId
    ) {
        return new ArrayList<>(
                keycloakService.getOrganizationUserRoles( new KeycloakId( userRepresentation.getId() ), organizationId )
        );
    }

    private void notifyUserGroup(
            Notification notification,
            Map<String, String> templateArguments,
            PortalEvent portalEvent,
            EventAction eventAction,
            Set<UserRepresentation> userGroup,
            NotificationChannel notificationChannel
    ) {
        try {
            userGroup.forEach( user -> {
                Map<String, String> cloneTemplateArguments = templateArguments == null ?
                        new HashMap<>() :
                        new HashMap<>( templateArguments );
                // add basic template arguments
                cloneTemplateArguments.put( "first_name", user.getFirstName() );
                cloneTemplateArguments.put( "last_name", user.getLastName() );
                cloneTemplateArguments.put( "portal_base_url", portalBaseUrl );

                PostShoutOut shoutOut = gatherShoutOutData(
                        user,
                        notification,
                        cloneTemplateArguments,
                        notificationChannel
                );
                postShoutOut( shoutOut );

                if ( portalEvent != null && eventAction != null ) {
                    triggerEvent( portalEvent, eventAction, user.getId() );
                }
            } );
        } catch ( Exception ex ) {
            log.error( "Failed to notify user group: {}", ex.getMessage() );
        }
    }

    private PostShoutOut gatherShoutOutData(
            UserRepresentation user,
            Notification notification,
            Map<String, String> templateArguments,
            NotificationChannel notificationChannel
    ) {
        Map<String, List<String>> attributes = user.getAttributes();
        String languageCodeString = KeycloakUtils.getAttributeValue( attributes, "locale", "en" );
        Locale locale = new Locale( languageCodeString );

        String htmlContent = templateService.getHTMLContent( notification, locale, templateArguments );
        String textContent = templateService.getTextContent( notification, locale, templateArguments );

        PostMessageSMTP msgSMTP = null;
        if ( notificationChannel == NotificationChannel.SMTP_AMQP || notificationChannel == NotificationChannel.SMTP ) {
            msgSMTP = buildPostMessage(
                    textContent,
                    String.format( "EMPAIA | %s", notification.getNotificationSubject( locale ) ),
                    user.getEmail(),
                    htmlContent
            );
        }

        PostMessageAMQP msgAMPQ = null;
        if ( notificationChannel == NotificationChannel.SMTP_AMQP || notificationChannel == NotificationChannel.AMQP ) {
            msgAMPQ = buildPostMessage( notification.toString(), user.getId() );
        }

        return buildPostShoutOut( user.getId(), this.rabbitmqMessageTimeout, msgSMTP, msgAMPQ );
    }

    private void triggerEvent( PortalEvent event, EventAction action, String userId ) {
        AMQPData amqpData = AMQPData.builder().action( action ).build();
        AMQPPayload payload = AMQPPayload.builder().event( event ).eventData( amqpData ).build();

        PutDirectAMQP directAMQP = PutDirectAMQP.builder()
                .data( payload )
                .routingKey( userId )
                .build();
        putEvent( directAMQP );
    }

    private void putEvent( PutDirectAMQP putDirectAMQP ) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.put( this.sosUrl + "/v1/direct/amqp", putDirectAMQP );
        } catch ( Exception ex ) {
            String msg = String.format( "Could not put event %s : %s",
                    putDirectAMQP,
                    ex.getMessage()
            );
            log.error( msg );
        }
    }

    private PostMessageSMTP buildPostMessage( String content, String subject, String to, String contentHtml ) {
        return PostMessageSMTP.builder()
                .sink( Sink.SMTP )
                .content( content )
                .subject( subject )
                .to( to )
                .contentHtml( contentHtml )
                .build();
    }

    private PostMessageAMQP buildPostMessage( String content, String routing_key ) {
        return PostMessageAMQP.builder()
                .sink( Sink.AMQP )
                .content( content )
                .routingKey( routing_key )
                .build();
    }

    private PostShoutOut buildPostShoutOut(
            String recipientId,
            int timeoutInterval,
            PostMessageSMTP messageSMTP,
            PostMessageAMQP messageAMQP
    ) {
        List<PostMessage> messages = new ArrayList<>();
        if ( messageSMTP != null ) {
            messages.add( messageSMTP );
        }
        if ( messageAMQP != null ) {
            messages.add( messageAMQP );
        }
        return PostShoutOut.builder()
                .recipientId( recipientId )
                .timeoutInterval( timeoutInterval )
                .messages( messages )
                .build();
    }

    private void postShoutOut( PostShoutOut postShoutOut ) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.postForEntity( this.sosUrl + "/v1/shout-outs", postShoutOut, String.class );
        } catch ( Exception ex ) {
            String msg = String.format( "Could not send shout out %s : %s",
                    postShoutOut,
                    ex.getMessage()
            );
            log.error( msg );
        }
    }
}
