package org.empaia.services.v2;

import com.jlefebure.spring.boot.minio.MinioException;
import lombok.extern.slf4j.Slf4j;
import org.empaia.exceptions.generic.AlreadyExistException;
import org.empaia.exceptions.generic.NotFoundException;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.db.v2.DBMembershipRequest;
import org.empaia.models.dto.v2.*;
import org.empaia.models.enums.v2.LanguageCode;
import org.empaia.models.enums.v2.RequestState;
import org.empaia.repositories.v2.MembershipRequestsRepository;
import org.empaia.services.KeycloakService;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotAuthorizedException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class MyService {

    private final ConfidentialOrganizationService confidentialOrganizationService;
    private final KeycloakService keycloakService;
    private final MembershipRequestsRepository membershipRequestsRepository;
    private final MemberService memberService;
    private final ProfilePictureService profilePictureService;
    private final UserServiceV2 userServiceV2;
    private final ShoutOutService shoutOutService;

    public MyService(
            ConfidentialOrganizationService confidentialOrganizationService,
            KeycloakService keycloakService,
            MembershipRequestsRepository membershipRequestsRepository,
            MemberService memberService,
            ProfilePictureService profilePictureService,
            UserServiceV2 userServiceV2,
            ShoutOutService shoutOutService
    ) {
        this.confidentialOrganizationService = confidentialOrganizationService;
        this.keycloakService = keycloakService;
        this.membershipRequestsRepository = membershipRequestsRepository;
        this.memberService = memberService;
        this.profilePictureService = profilePictureService;
        this.userServiceV2 = userServiceV2;
        this.shoutOutService = shoutOutService;
    }

    public UserProfileEntity getProfile( KeycloakId userId ) {
        return userServiceV2.getUserProfile( userId );
    }

    public UserProfileEntity updateContactData( KeycloakId userId, PostUserContactDataEntity userContactDataEntity ) {
        return userServiceV2.updateContactData( userId, userContactDataEntity );
    }

    public UserProfileEntity updateDetails( KeycloakId userId, UserDetailsEntity userDetailsEntity ) {
        return userServiceV2.updateDetails( userId, userDetailsEntity );
    }

    public UserProfileEntity updateLanguage( KeycloakId userId, LanguageCode languageCode ) {
        return userServiceV2.updateLanguage( userId, languageCode );
    }

    public ResponseEntity<InputStreamResource> getProfilePicture( KeycloakId userId ) throws FileNotFoundException {
        UserResource userResource = keycloakService.getUserById( userId );
        UserRepresentation userRepresentation = userResource.toRepresentation();
        return profilePictureService.getProfilePicture( new KeycloakId( userRepresentation.getId() ) );
    }

    public ResponseEntity<InputStreamResource> getProfilePicture( KeycloakId userId, int width ) throws FileNotFoundException {
        UserResource userResource = keycloakService.getUserById( userId );
        UserRepresentation userRepresentation = userResource.toRepresentation();
        return profilePictureService.getProfilePicture( new KeycloakId( userRepresentation.getId() ), width );
    }

    public void setProfilePicture( KeycloakId userId, MultipartFile profilePicture ) throws Exception {
        UserResource userResource = keycloakService.getUserById( userId );
        profilePictureService.setProfilePicture( userResource, profilePicture );
    }

    public void deleteProfilePictures( KeycloakId userId ) throws MinioException {
        UserResource userResource = keycloakService.getUserById( userId );
        profilePictureService.deleteProfilePictures( userResource );
    }

    public MembershipRequestsEntity getMembershipRequests( KeycloakId userId, int page, int pageSize ) {
        PageRequest pageRequest = PageRequest.of( page, pageSize );
        Page<DBMembershipRequest> membershipRequestsPage = membershipRequestsRepository.findAllByUserId(
                userId.toString(), pageRequest
        );
        List<MembershipRequestEntity> membershipRequests = membershipRequestsPage.stream()
                .map( this::buildMembershipRequestEntity )
                .collect( Collectors.toList() );
        return MembershipRequestsEntity.builder()
                .membershipRequestEntities( membershipRequests )
                .totalMembershipRequestsCount( Long.valueOf( membershipRequestsPage.getTotalElements() ).intValue() )
                .build();
    }

    public MembershipRequestEntity revokeMembershipRequest( KeycloakId userId, Integer membershipRequestId ) {
        DBMembershipRequest dbMembershipRequest = membershipRequestsRepository.findByMembershipRequestId(
                membershipRequestId
        ).orElseThrow( () -> new NotFoundException( "Membership not requested" ) );
        if ( userId.equals( dbMembershipRequest.getUserId() ) ) {
            dbMembershipRequest.updateRequestState( RequestState.REVOKED, userId, null );
            membershipRequestsRepository.save( dbMembershipRequest );
            return this.buildMembershipRequestEntity( dbMembershipRequest );
        } else {
            throw new NotAuthorizedException( "Action not allowed." );
        }
    }

    public MembershipRequestEntity requestMembership( KeycloakId userId, PostMembershipRequestEntity membershipRequestEntity ) {
        Optional<DBMembershipRequest> membershipRequest = membershipRequestsRepository.findByOrganizationIdAndUserIdAndRequestState(
                membershipRequestEntity.getOrganizationId().toString(), userId.toString(), RequestState.REQUESTED.getValue()
        );
        if ( membershipRequest.isPresent() ) {
            throw new AlreadyExistException( "Membership already requested" );
        }
        String organizationId = membershipRequestEntity.getOrganizationId().toString();
        UserRepresentation userRepresentation;
        try {
            userRepresentation = keycloakService.getUserById( userId ).toRepresentation();
        } catch ( javax.ws.rs.NotFoundException e ) {
            log.info( "Membership requested failed because no user with this user id exists " + userId );
            throw new BadRequestException( "User not found" );
        }
        DBMembershipRequest newMembershipRequest = DBMembershipRequest.builder()
                .organizationId( organizationId )
                .organizationName( keycloakService.getOrganizationName( new KeycloakId( organizationId ) ) )
                .userId( userId.toString() )
                .userName( userRepresentation.getUsername() )
                .createdAt( new Date().getTime() )
                .requestState( RequestState.REQUESTED.getValue() )
                .build();
        membershipRequestsRepository.save( newMembershipRequest );
        shoutOutService.notifyManagersOrganizationMembership( new KeycloakId( organizationId ) );
        return this.buildMembershipRequestEntity( newMembershipRequest );
    }

    private MembershipRequestEntity buildMembershipRequestEntity( DBMembershipRequest membershipRequest ) {
        return MembershipRequestEntity.builder()
                .membershipRequestId( membershipRequest.getMembershipRequestId() )
                .organizationId( new KeycloakId( membershipRequest.getOrganizationId() ) )
                .organizationName( membershipRequest.getOrganizationName() )
                .userDetails( userServiceV2.getExtendedUserDetails( membershipRequest.getUserId() ) )
                .createdAt( membershipRequest.getCreatedAt() )
                .reviewerComment( membershipRequest.getReviewerComment() )
                .reviewerId( new KeycloakId( membershipRequest.getReviewerId() ) )
                .updatedAt( membershipRequest.getUpdatedAt() )
                .requestState( membershipRequest.getRequestState() )
                .build();
    }

    public DefaultActiveOrganizationEntity getDefaultActiveOrganization( KeycloakId userId ) {
        return DefaultActiveOrganizationEntity.builder().organizationId(
                keycloakService.getDefaultActiveOrganization( userId )
        ).build();
    }

    public DefaultActiveOrganizationEntity setDefaultActiveOrganization( KeycloakId userId, DefaultActiveOrganizationEntity defaultActiveOrganization ) {
        return keycloakService.setDefaultActiveOrganization( OrganizationMember.builder()
                .organizationId( defaultActiveOrganization.getOrganizationId() )
                .userId( userId )
                .build()
        );
    }

    public void deleteDefaultActiveOrganization( KeycloakId userId ) {
        keycloakService.deleteDefaultActiveOrganization( userId );
    }

    public PublicOrganizationProfilesEntity getActivePublicOrganizations( KeycloakId userId, int page, int pageSize ) {
        return memberService.getActivePublicOrganizationProfilesEntityForUser( page, pageSize, userId );
    }

    public ConfidentialOrganizationProfilesEntity getActiveConfidentialOrganizations( KeycloakId userId, int page, int pageSize ) {
        return memberService.getActiveConfidentialOrganizationProfilesEntityForUser( page, pageSize, userId );
    }

    public MembershipsEntity getMemberships( KeycloakId userId ) {
        var membershipRequests = getMembershipRequests( userId, 0, 9999 );
        var confidentialOrganizationProfilesEntity = getActiveConfidentialOrganizations( userId, 0, 9999 );
        List<MembershipEntity> membershipEntities = membershipRequests.getMembershipRequestEntities().stream()
                .filter( request -> request.getRequestState().equals( RequestState.REQUESTED ) )
                .map( this::createMembershipEntityFromMembershipRequestEntity )
                .filter( Objects::nonNull )
                .collect( Collectors.toList() );
        membershipEntities.addAll(
                confidentialOrganizationProfilesEntity.getOrganizations().stream()
                        .map( this::createMembershipEntityFromConfidentialOrganizationProfileEntity )
                        .filter( Objects::nonNull )
                        .toList()
        );
        return MembershipsEntity.builder()
                .memberships( membershipEntities )
                .build();
    }

    private MembershipEntity createMembershipEntityFromMembershipRequestEntity( MembershipRequestEntity membershipRequestEntity ) {
        KeycloakId organizationId = membershipRequestEntity.getOrganizationId();
        ConfidentialOrganizationProfileEntity confidentialOrganizationProfileEntity = null;
        try {
            confidentialOrganizationProfileEntity = confidentialOrganizationService.getActiveConfidentialOrganizationProfileEntity( organizationId );
        } catch ( NotFoundException e ) {
            log.error( "No active organization found for membership request: " + membershipRequestEntity.getOrganizationId() );
        }
        return (confidentialOrganizationProfileEntity == null)
                ? null
                : MembershipEntity.builder()
                .categories( confidentialOrganizationProfileEntity.getDetails().getCategories() )
                .organizationName( confidentialOrganizationProfileEntity.getOrganizationName() )
                .organizationId( organizationId )
                .logoUrl( confidentialOrganizationProfileEntity.getLogoUrl() )
                .membershipRequestId( membershipRequestEntity.getMembershipRequestId() )
                .resizedLogoUrls( confidentialOrganizationProfileEntity.getResizedLogoUrls() )
                .build();
    }

    private MembershipEntity createMembershipEntityFromConfidentialOrganizationProfileEntity(
            ConfidentialOrganizationProfileEntity confidentialOrganizationProfileEntity
    ) {
        return (confidentialOrganizationProfileEntity == null)
                ? null
                : MembershipEntity.builder()
                .categories( confidentialOrganizationProfileEntity.getDetails().getCategories() )
                .organizationName( confidentialOrganizationProfileEntity.getOrganizationName() )
                .organizationId( confidentialOrganizationProfileEntity.getOrganizationId() )
                .logoUrl( confidentialOrganizationProfileEntity.getLogoUrl() )
                .resizedLogoUrls( confidentialOrganizationProfileEntity.getResizedLogoUrls() )
                .build();
    }

    public void deleteUser( KeycloakId userId, KeycloakId deletedByUserId ) {
        userServiceV2.deleteUser( userId, deletedByUserId, true, true );
    }
}
