package org.empaia.services.v2;

import lombok.extern.slf4j.Slf4j;
import org.empaia.exceptions.generic.NotFoundException;
import org.empaia.exceptions.user.ValidationException;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.db.v2.*;
import org.empaia.models.dto.v2.*;
import org.empaia.models.enums.v2.LanguageCode;
import org.empaia.models.enums.v2.UserAccountState;
import org.empaia.models.enums.v2.UserRole;
import org.empaia.repositories.v2.*;
import org.empaia.services.KeycloakService;
import org.empaia.utils.KeycloakUtils;
import org.empaia.utils.UserUtils;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.BadRequestException;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static org.empaia.utils.KeycloakUtils.getUserAccountState;

@Slf4j
@Service
@Transactional
public class UserServiceV2 {

    private final KeycloakService keycloakService;
    private final MembershipRequestsRepository membershipRequestsRepository;
    private final OrganizationUserRoleRequestsRepository organizationUserRoleRequestsRepository;
    private final OrganizationCategoryRequestsRepository organizationCategoryRequestsRepository;
    private final OrganizationDetailsRepository organizationDetailsRepository;
    private final DeletedUserHistoryRepository deletedUserHistoryRepository;
    private final ProfilePictureService profilePictureService;

    @Autowired
    public UserServiceV2(
            KeycloakService keycloakService,
            MembershipRequestsRepository membershipRequestsRepository,
            OrganizationUserRoleRequestsRepository organizationUserRoleRequestsRepository,
            OrganizationCategoryRequestsRepository organizationCategoryRequestsRepository,
            OrganizationDetailsRepository organizationDetailsRepository,
            DeletedUserHistoryRepository deletedUserHistoryRepository,
            ProfilePictureService profilePictureService
    ) {
        this.keycloakService = keycloakService;
        this.membershipRequestsRepository = membershipRequestsRepository;
        this.organizationUserRoleRequestsRepository = organizationUserRoleRequestsRepository;
        this.organizationCategoryRequestsRepository = organizationCategoryRequestsRepository;
        this.organizationDetailsRepository = organizationDetailsRepository;
        this.deletedUserHistoryRepository = deletedUserHistoryRepository;
        this.profilePictureService = profilePictureService;
    }

    private void deleteUserUnchecked( String keycloakUserId, String deletedByUserId ) {
        // delete open requests
        PageRequest pageRequest = PageRequest.of( 0, 1000 );
        Page<DBMembershipRequest> membershipRequests = membershipRequestsRepository.findAllByUserId( keycloakUserId, pageRequest);
        membershipRequestsRepository.deleteAll( membershipRequests );
        Page<DBOrganizationUserRoleRequest> roleRequests = organizationUserRoleRequestsRepository.findAllByUserId( keycloakUserId, pageRequest );
        organizationUserRoleRequestsRepository.deleteAll( roleRequests );
        Page<DBOrganizationCategoryRequest> categoryRequests = organizationCategoryRequestsRepository.findAllByCreatorId( keycloakUserId, pageRequest );
        organizationCategoryRequestsRepository.deleteAll( categoryRequests );

        // delete draft organizations
        deleteUnapprovedOrganizationsForUser( keycloakUserId );

        // add entry to user history
        DBDeletedUserHistory dbEntry = DBDeletedUserHistory.builder()
                .userId( keycloakUserId )
                .deletedBy( deletedByUserId )
                .deletedOn( new Date().getTime() )
                .build();
        deletedUserHistoryRepository.save(dbEntry);

        keycloakService.deleteUser( keycloakUserId );
    }

    private void deleteUnapprovedOrganizationsForUser( String userId ) {
        Page<DBOrganizationDetails> organizationDetails = organizationDetailsRepository.findAllByCreatorId(
                userId, PageRequest.of(0, 1000)
        );
        organizationDetails.stream().forEach(
                orgDetail -> {
                    if ( orgDetail.isUnapproved() ) {
                        keycloakService.deleteGroup( orgDetail.getKeycloakId() );
                        organizationDetailsRepository.delete( orgDetail );
                    }
                }
        );
    }

    public void deleteUnapprovedUser( KeycloakId userId, KeycloakId deletedByUserId ) {
        UserRepresentation userRepresentation = getUserRepresentation( userId );
        if ( userRepresentation.isEmailVerified() ) {
            throw new BadRequestException( "User is already approved and cannot be deleted" );
        }
        deleteUserUnchecked( userId.toString(), deletedByUserId.toString() );
    }

    public UserProfileEntity updateDetails( KeycloakId userId, UserDetailsEntity userDetailsEntity ) {
        UserResource userResource = keycloakService.getUserById( userId.toString() );
        UserRepresentation userRepresentation = userResource.toRepresentation();
        addUserDetailsToUserRepresentation( userDetailsEntity, userRepresentation );
        return updateUserResource( userResource, userRepresentation );
    }

    public UserProfileEntity updateContactData( KeycloakId userId, PostUserContactDataEntity userContactDataEntity ) {
        UserResource userResource = keycloakService.getUserById( userId.toString() );
        UserRepresentation userRepresentation = userResource.toRepresentation();
        addContactDataToUserRepresentation( userContactDataEntity, userRepresentation );
        return updateUserResource( userResource, userRepresentation );
    }

    public UserProfileEntity updateLanguage( KeycloakId userId, LanguageCode languageCode ) {
        UserResource userResource = keycloakService.getUserById( userId.toString() );
        UserRepresentation userRepresentation = userResource.toRepresentation();
        Map<String,List<String>> attributes = getOrCreateUserRepresentationAttributes( userRepresentation );
        attributes.put( "locale", Collections.singletonList( languageCode.toString().toLowerCase() ) );
        return updateUserResource( userResource, userRepresentation );
    }

    private UserProfileEntity updateUserResource( UserResource userResource, UserRepresentation userRepresentation ) {
        userResource.update( userRepresentation );
        userResource = keycloakService.getUserById( userRepresentation.getId() );
        return createUserProfileEntityFromUserResource( userResource );
    }

    public UserProfileEntity getUserProfile( KeycloakId userId ) {
        return _getUserProfile( userId, true );
    }

    public UserProfileEntity getUserProfileOfUnapprovedUser( KeycloakId userId ) {
        return _getUserProfile( userId, false );
    }

    private UserRepresentation getUserRepresentation( KeycloakId userId ) {
        UserResource userResource = keycloakService.findUserById(userId.toString()).orElseThrow(
                () -> {
                    log.error("User not found: " + userId);
                    return new NotFoundException("User was not found");
                }
        );
        try {
            return userResource.toRepresentation();
        } catch (javax.ws.rs.NotFoundException e) {
            log.error("User not found: " + userId);
            throw new NotFoundException("User was not found");
        }
    }

    public Set<UserRepresentation> getUsersByRole( String roleName ) {
        try {
            return keycloakService.getUsersResource()
                    .list()
                    .stream()
                    .filter( user -> keycloakService.getUserRoles( user.getId() )
                            .stream()
                            .map( UserRole::getName )
                            .collect( Collectors.toSet() )
                            .contains( roleName ) )
                    .collect( Collectors.toSet() );
        } catch ( Exception e ) {
            log.error( String.format( "Error finding user with role %s", roleName ) );
            log.error( e.getMessage() );
            return Set.of();
        }
    }

    private UserProfileEntity _getUserProfile( KeycloakId userId, boolean expectedIsEmailVerified ) {
        UserRepresentation userRepresentation = getUserRepresentation( userId );
        if ( expectedIsEmailVerified != userRepresentation.isEmailVerified() ) {
            String logMsg;
            String exceptionMsg;
            if ( expectedIsEmailVerified ) {
                logMsg = "User exists, but is not approved: " + userId;
                exceptionMsg = "Approved user was not found";
            } else {
                logMsg = "User exists, but is not unapproved: " + userId;
                exceptionMsg = "Unapproved user was not found";
            }
            log.error( logMsg );
            throw new NotFoundException( exceptionMsg );
        }
        return createUserProfileEntityFromUserRepresentation( userRepresentation );
    }

    public String createNewUser( PostUserEntity userEntity ) {
        // TODO: create a python test that verifies that no second user with the same email or username can be
        // created. Keycloak should already make this check and throw an exception then.
        // TODO: verify in a test that the needs email verification flag is set

        userEntity.validate();

        Map<String, List<String>> attributes = new HashMap<>();
        attributes.put( "locale", Collections.singletonList( userEntity.getLanguageCode().toString().toLowerCase() ) );
        UserDetailsEntity details = userEntity.getDetails();
        String title = details.getTitle();
        if ( title == null || title.isBlank() ) {
            details.setTitle( "" );
        }
        attributes.put( "title", Collections.singletonList( details.getTitle() ) );
        Timestamp now = new Timestamp( new Date().getTime() );
        KeycloakUtils.setAttributeValue( attributes, "createdTimestamp", now );
        KeycloakUtils.setAttributeValue( attributes, "updatedTimestamp", now );
        UserContactDataEntity contactData = userEntity.getContactData();
        KeycloakUtils.addPostUserContactDataToAttributes( contactData, attributes );
        return keycloakService.createUser(
            contactData.getEmailAddress(),
            details.getFirstName(),
            details.getLastName(),
            contactData.getEmailAddress(),
            userEntity.getPassword(),
            attributes
        ).orElseThrow(
            () -> new ValidationException( "Unable to create the user" )
        );
    }

    private UserProfileEntity createUserProfileEntityFromUserResource( UserResource userResource ) {
        return createUserProfileEntityFromUserRepresentation( userResource.toRepresentation() );
    }

    private UserProfileEntity createUserProfileEntityFromUserRepresentation( UserRepresentation userRepresentation ) {
        Map<String,List<String>> attributes = userRepresentation.getAttributes();
        String languageCodeString = KeycloakUtils.getAttributeValue(attributes, "locale", "en");
        LanguageCode languageCode = LanguageCode.fromString( languageCodeString.toUpperCase() );
        UserAccountState accountState = getUserAccountState( userRepresentation );

        UserContactDataEntity contactData = createUserContactData( userRepresentation );
        UserDetailsEntity userDetails = createUserDetails( userRepresentation );
        List<UserRole> userRoles = keycloakService.getUserRoles( userRepresentation.getId() );

        KeycloakId userId = new KeycloakId( userRepresentation.getId() );

        UserProfileEntity userProfileEntity = UserProfileEntity.builder()
                .accountState( accountState )
                .contactData( contactData )
                .createdTimestamp( userRepresentation.getCreatedTimestamp() )
                .details( userDetails )
                .languageCode( languageCode )
                .userName( userRepresentation.getUsername() )
                .userId( userId )
                .userRoles( userRoles )
                .build();
        userProfileEntity.setProfilePictureUrl(
                profilePictureService.getProfilePictureUrl( userId )
        );
        userProfileEntity.setResizedProfilePictureUrls(
                profilePictureService.getResizedProfilePictureUrls( userId )
        );
        return userProfileEntity;
    }

    public UserContactDataEntity createUserContactData( UserRepresentation userRepresentation ) {
        UserContactDataEntity contactDataEntity = new UserContactDataEntity();
        contactDataEntity.setEmailAddress( userRepresentation.getEmail() );
        Map<String,List<String>> attributes = userRepresentation.getAttributes();
        KeycloakUtils.setUserContactDataFromAttributes( contactDataEntity, attributes );
        return contactDataEntity;
    }

    public void addContactDataToUserRepresentation( PostUserContactDataEntity userContactData, UserRepresentation userRepresentation ) {
        Map<String,List<String>> attributes = getOrCreateUserRepresentationAttributes( userRepresentation );
        KeycloakUtils.addPostUserContactDataToAttributes( userContactData, attributes );
    }


    public UserDetailsEntity createUserDetails( UserRepresentation userRepresentation ) {
        UserDetailsEntity detailsEntity = new UserDetailsEntity();
        detailsEntity.setFirstName( userRepresentation.getFirstName() );
        detailsEntity.setLastName( userRepresentation.getLastName() );
        Map<String,List<String>> attributes = getOrCreateUserRepresentationAttributes( userRepresentation );
        detailsEntity.setTitle(KeycloakUtils.getAttributeValue(attributes, "title", ""));
        return detailsEntity;
    }

    public void addUserDetailsToUserRepresentation( UserDetailsEntity userDetails, UserRepresentation userRepresentation ) {
        userRepresentation.setFirstName( userDetails.getFirstName() );
        userRepresentation.setLastName( userDetails.getLastName() );
        Map<String,List<String>> attributes = getOrCreateUserRepresentationAttributes( userRepresentation );
        KeycloakUtils.setAttributeValueIfNotNull( attributes, "title", userDetails.getTitle() );
    }

    public ExtendedUserDetailsEntity getExtendedUserDetails( String userId ) {
        UserRepresentation userRepresentation = keycloakService.getUserById( userId ).toRepresentation();
        Map<String, List<String>> attributes = getOrCreateUserRepresentationAttributes( userRepresentation );
        String title = KeycloakUtils.getAttributeValue(attributes, "title", "");
        return ExtendedUserDetailsEntity.builder()
                .userId( new KeycloakId( userRepresentation.getId() ) )
                .title( title )
                .firstName( userRepresentation.getFirstName() )
                .lastName( userRepresentation.getLastName() )
                .emailAddress( userRepresentation.getEmail() )
                .build();
    }

    private Map<String,List<String>> getOrCreateUserRepresentationAttributes( UserRepresentation userRepresentation ) {
        Map<String,List<String>> attributes = userRepresentation.getAttributes();
        if ( attributes == null ) {
            attributes = new HashMap<>();
            userRepresentation.setAttributes(attributes);
        }
        return attributes;
    }

    public void deleteUser( KeycloakId userId, KeycloakId deletedByUserId, boolean checkUserRole, boolean checkManagerInOrg ) {
        UserResource userResource = keycloakService.getUserById( userId );
        if ( checkUserRole && ( keycloakService.isAdmin( userResource ) || keycloakService.isModerator( userResource ) ) ) {
            throw new BadRequestException( "Account deletion for user with role ADMIN or MODERATOR not allowed" );
        }
        if ( checkManagerInOrg && isLastManagerOfAnyOrganization( userId )) {
            throw new BadRequestException(
                    "Account deletion not allowed. User is the last manager in charge for one of its organization"
            );
        }
        deleteUserUnchecked( userId.toString(), deletedByUserId.toString() );
    }

    public boolean isLastManagerOfAnyOrganization( KeycloakId userId ) {
        List<KeycloakId> organizationIds = keycloakService.getOrganizationIdsOfMember( userId );
        AtomicBoolean isLastManager = new AtomicBoolean( false );
        organizationIds.forEach( orgId -> {
            OrganizationMember organizationMember = UserUtils.makeOrganizationMemberEntity( orgId, userId );
            if ( keycloakService.isLastOrganizationManagerInCharge( organizationMember ) ) {
                isLastManager.set( true );
            }
        });
        return isLastManager.get();
    }

    public boolean canLeave( KeycloakId organizationId, KeycloakId userId ) {
        OrganizationMember organizationMember = UserUtils.makeOrganizationMemberEntity( organizationId, userId );
        if ( !keycloakService.isGroupMember( organizationMember ) ) {
            throw new NotFoundException( "You are not a member of the organization");
        }
        return !keycloakService.isLastOrganizationManagerInCharge( organizationMember );
    }
}

