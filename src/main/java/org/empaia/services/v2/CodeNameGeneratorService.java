package org.empaia.services.v2;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.util.List;

@Slf4j
@Service
public class CodeNameGeneratorService {

    private final String[] adjectives;
    private final String[] animalNames;


    public CodeNameGeneratorService(
            ObjectMapper objectMapper
    ) throws IOException {
        TypeReference<List<String>> typeReference = new TypeReference<>() {};
        this.animalNames = objectMapper.readValue(
                ResourceUtils.getURL("classpath:code_name_generator_data/scientific_animals.json"),
                typeReference
        ).toArray(String[]::new);
        this.adjectives = objectMapper.readValue(
                ResourceUtils.getURL("classpath:code_name_generator_data/adjectives.json"),
                typeReference
        ).toArray(String[]::new);
    }

    public String generateRandomCodeName() {
        int adjectiveIndex = (int)(Math.random() * adjectives.length);
        int animalNameIndex = (int)(Math.random() * animalNames.length);
        return this.adjectives[adjectiveIndex] + "_" + this.animalNames[animalNameIndex].replace(" ", "_");
    }
}
