package org.empaia.services.v2;

import lombok.extern.slf4j.Slf4j;
import org.empaia.configs.ConsistencyCheckConfiguration;
import org.empaia.exceptions.keycloak.KeycloakClientIdException;
import org.empaia.exceptions.keycloak.KeycloakInternalException;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.ConfidentialOrganizationEntity;
import org.empaia.models.dto.v2.ConfidentialOrganizationProfilesEntity;
import org.empaia.models.dto.v2.ConfidentialOrganizationsEntity;
import org.empaia.models.enums.v2.ClientType;
import org.empaia.models.enums.v2.UserRole;
import org.empaia.services.KeycloakService;
import org.empaia.utils.KeycloakUtils;
import org.empaia.validators.*;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.stereotype.Service;

import javax.ws.rs.BadRequestException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class KeycloakConsistencyCheckService {
    private static final List<String> DEFAULT_CLIENT_SCOPES = Arrays.asList(
            "acr", "address", "email", "microprofile-jwt", "offline_access", "organization", "phone", "profile", "role_list", "roles", "web-origins"
    );
    private static final int DELETE_UNAPPROVED_ENTITIES_TIME_PERIOD_IN_WEEKS = 4;

    private final ConsistencyCheckConfiguration configuration;
    private final KeycloakService keycloakService;
    private final KeycloakClientService keycloakClientService;
    private final ConfidentialOrganizationService confidentialOrganizationService;
    private final MyUnapprovedOrganizationsService myUnapprovedOrganizationsService;
    private final OrganizationDeploymentConfigurationService organizationDeploymentConfigurationService;

    public KeycloakConsistencyCheckService(
            ConsistencyCheckConfiguration configuration,
            KeycloakService keycloakService,
            KeycloakClientService keycloakClientService,
            ConfidentialOrganizationService confidentialOrganizationService,
            MyUnapprovedOrganizationsService myUnapprovedOrganizationsService,
            OrganizationDeploymentConfigurationService organizationDeploymentConfigurationService
    ) {
        this.configuration = configuration;
        this.keycloakService = keycloakService;
        this.keycloakClientService = keycloakClientService;
        this.confidentialOrganizationService = confidentialOrganizationService;
        this.myUnapprovedOrganizationsService = myUnapprovedOrganizationsService;
        this.organizationDeploymentConfigurationService = organizationDeploymentConfigurationService;
    }

    public void checkEmpaiaRealmExists() {
        log.info( "* Validate EMPAIA realm exists" );

        String realm = this.keycloakService.getRealm();
        if ( !realm.equals( "empaia" ) ) {
            log.warn( "   > [!] EMPAIA Realm does not exist!" );
        } else {
            log.info( "   > EMPAIA realm exists" );
        }
    }

    public void checkGlobalUserRoles() {
        log.info( "* Validate global user roles" );

        ClientRepresentation authServiceClient = getClientIfExists( ClientType.AUTH_SERVICE_CLIENT.getShortName() );
        if ( authServiceClient == null || !authServiceClient.isEnabled() ) {
            log.warn( "   > [!] Auth service client does not exist or is disabled" );
            return;
        }

        Set<String> roles = this.keycloakService.getClientsResource()
                .get( authServiceClient.getId() ).roles().list()
                .stream().map( RoleRepresentation::getName ).collect( Collectors.toSet() );

        List<String> v2UserRoleNames = Arrays.asList(
                "admin",
                UserRole.MODERATOR.getName().toLowerCase(),
                UserRole.EMPAIA_INTERNATIONAL_ASSOCIATE.getName().toLowerCase()
        );

        boolean globalRolesPresent = true;
        for ( String roleName : v2UserRoleNames ) {
            if ( !roles.contains( roleName ) ) {
                log.warn( String.format( "   > [!] Missing global user role %s! ", roleName ) );
                globalRolesPresent = false;
            }
        }
        if ( globalRolesPresent ) {
            log.info( "   > Expected global user roles found" );
        }
    }

    private ClientRepresentation getClientIfExists( String clientName ) {
        ClientRepresentation client = null;
        try {
            client = this.keycloakService.getClientByClientId( clientName );
        } catch ( KeycloakInternalException | KeycloakClientIdException ex ) {
            log.warn( String.format( "   > [!] Client %s does not exist or is disabled", clientName ) );
        }
        return client;
    }

    public void checkNonVerifiedUserAccounts( boolean runKeycloakCleanup ) {
        log.info( "* Validate non-verified user accounts" );

        LocalDateTime ldt = LocalDateTime.now().minusWeeks( DELETE_UNAPPROVED_ENTITIES_TIME_PERIOD_IN_WEEKS );
        Date validUntil = Date.from( ldt.atZone( ZoneId.systemDefault() ).toInstant() );

        List<UserRepresentation> users = this.keycloakService.getUnapprovedUsers( 0, 9999 );
        Set<UserRepresentation> nonVerifiedUsers = users.stream()
                .filter( user -> !user.isEmailVerified() )
                .collect( Collectors.toSet() );

        if ( nonVerifiedUsers.isEmpty() ) {
            log.info( "   > No non-verified users that must be removed" );
        } else {
            nonVerifiedUsers.forEach( user -> {
                Date userCreationDate = new Date( user.getCreatedTimestamp() );
                if ( userCreationDate.before( validUntil ) ) {
                    log.warn(
                            "   > [!] User " + user.getEmail() + " has not yet verified its email address within the last "
                                    + DELETE_UNAPPROVED_ENTITIES_TIME_PERIOD_IN_WEEKS + " weeks"
                    );
                    if ( runKeycloakCleanup ) {
                        this.keycloakService.deleteUser( user.getId() );
                        log.info( String.format( "        > User %s was deleted", user.getEmail() ) );
                    }
                }
            } );
        }
    }

    public void checkUnapprovedOrganizations( boolean runKeycloakCleanup ) {
        log.info( "* Validate unapproved organization entities" );

        LocalDateTime ldt = LocalDateTime.now().minusWeeks( DELETE_UNAPPROVED_ENTITIES_TIME_PERIOD_IN_WEEKS );
        Date validUntil = Date.from( ldt.atZone( ZoneId.systemDefault() ).toInstant() );

        ConfidentialOrganizationProfilesEntity organizationsProfiles = confidentialOrganizationService.getUnapprovedConfidentialOrganizationProfileEntities();

        if ( organizationsProfiles.getOrganizations().isEmpty() ) {
            log.info( "   > No unapproved organizations that must be removed" );
        } else {
            organizationsProfiles.getOrganizations().forEach( organization -> {
                Date organizationCreationDate = new Date( organization.getCreatedTimestamp() );
                if ( organizationCreationDate.before( validUntil ) ) {
                    log.warn(
                            "   > [!] Organization " + organization.getOrganizationName() + " was not yet requested for approval within the last "
                                    + DELETE_UNAPPROVED_ENTITIES_TIME_PERIOD_IN_WEEKS + " weeks"
                    );
                    if ( runKeycloakCleanup ) {
                        try {
                            myUnapprovedOrganizationsService.deleteUnapprovedOrganization( organization.getOrganizationId() );
                            log.info( String.format( "        > Organization %s was deleted", organization.getOrganizationName() ) );
                        } catch ( BadRequestException ignored ) {
                            log.info( String.format(
                                    "        > Failed to delete unapproved organization %s: needs to be approved/rejected by moderator first",
                                    organization.getOrganizationName() )
                            );
                        }

                    }
                }
            } );
        }
    }

    public void checkUnusedClientScopes( boolean runKeycloakCleanup ) {
        log.info( "* Validate unused client scopes" );

        Map<String, String> clientScopeIdsByName = keycloakService.getClientScopeIdsByClientScopeName();

        Set<String> organizationIds = confidentialOrganizationService.getApprovedConfidentialOrganizationsEntity( 0, 9999 )
                .getOrganizations().stream().map( org -> org.getOrganizationId().toString() ).collect( Collectors.toSet() );

        Map<String, String> unusedClientScopes = new HashMap<>();
        clientScopeIdsByName.forEach( ( key, value ) -> {
            if ( !DEFAULT_CLIENT_SCOPES.contains( key.toLowerCase() ) ) {
                String organizationId = key.split( "\\." )[0];
                if ( !organizationIds.contains( organizationId ) || !KeycloakUtils.isValidKeycloakInstanceName( key, "scope" ) ) {
                    unusedClientScopes.put( key, value );
                }
            }
        } );

        if ( unusedClientScopes.isEmpty() ) {
            log.info( "   > No unused client scopes found" );
        } else {
            unusedClientScopes.forEach( ( key, value ) -> {
                log.warn( String.format( "   > [!] Client scope %s does not have a valid naming schema", key ) );
                if ( runKeycloakCleanup ) {
                    keycloakService.deleteGroupScope( key );
                    log.info( String.format( "         > Deleted client scope %s", key ) );
                }
            } );
        }
    }

    public void checkUnusedClientsAndRelatedUserAccounts( boolean runKeycloakCleanup ) {
        log.info( "* Validate clients that are not related to any organization" );

        Set<String> customGlobalClients = ClientType.getCustomGlobalClients().stream().map( ClientType::getShortName ).collect( Collectors.toSet() );
        Set<String> defaultClients = new HashSet<>( ClientType.getDefaultClients() );
        Set<ClientRepresentation> unusedClients = keycloakService.getClients( 0, 9999 ).stream()
                .filter( c -> !defaultClients.contains( c.getClientId() ) )
                .filter( c -> !customGlobalClients.contains( c.getClientId() ) )
                .filter( c -> !KeycloakUtils.isValidKeycloakInstanceName( c.getClientId(), "client" ) )
                .collect( Collectors.toSet() );

        if ( unusedClients.isEmpty() ) {
            log.info( "   > No unused clients found" );
        } else {
            for ( ClientRepresentation client : unusedClients ) {
                log.warn( String.format( "   > [!] Client %s does not seem to have a valid naming schema", client.getClientId() ) );

                UserRepresentation user = null;
                try {
                    user = keycloakService.getClientServiceAccountUser( new KeycloakId( client.getId() ) );
                    log.warn( String.format( "         > Found a related service account: %s", user.getId() ) );
                } catch ( BadRequestException ignored ) {
                }

                if ( runKeycloakCleanup ) {
                    keycloakService.deleteClient( client.getId() );
                    if ( user != null ) {
                        keycloakService.deleteUser( user.getId() );
                        log.info( String.format( "         > Deleted client %s and related service account %s", client.getClientId(), user.getId() ) );
                    } else {
                        log.info( String.format( "         > Deleted client %s", client.getClientId() ) );
                    }
                }
            }
        }
    }

    public void validateAllClients() {
        log.info( "* Validate client, client scope and service account configurations" );

        List<ValidClientStrategy> clientValidators = new ArrayList<>();
        clientValidators.add( new ClientProtocolMapperValidator() );
        clientValidators.add( new ClientCapabilityConfigValidator() );
        clientValidators.add( new ClientUrisValidator( organizationDeploymentConfigurationService, confidentialOrganizationService, configuration ) );
        clientValidators.add( new ClientScopeValidator( keycloakClientService ) );
        clientValidators.add( new ClientServiceAccountValidator( keycloakClientService ) );

        int startOfBatchIndex = 0, endOfBatchIndex = 1000;
        List<ClientRepresentation> clientBatch;
        boolean validationSucceeded = true;
        do {
            clientBatch = keycloakService.getClients( startOfBatchIndex, endOfBatchIndex );
            startOfBatchIndex = endOfBatchIndex + 1;
            endOfBatchIndex = endOfBatchIndex + 1000;

            for ( ClientRepresentation client : clientBatch ) {
                // skipping validation for default and unknown clients
                try {
                    ClientType type = ClientType.fromClientId( client.getClientId() );
                    if ( type.equals( ClientType.DEFAULT ) ) {
                        continue;
                    }
                } catch ( EnumConstantNotPresentException ignored ) {
                    // these are already checked above
                    //log.warn( String.format( "   > [!] Unknown client type: %s", client.getClientId() ) );
                    continue;
                }

                for ( ValidClientStrategy clientValidator : clientValidators ) {
                    if ( !clientValidator.validate( client ) ) {
                        log.warn( String.format( "         > Client %s is not correctly set up!", client.getClientId() ) );
                        validationSucceeded = false;
                    }
                }
            }

        } while ( !clientBatch.isEmpty() );

        if ( validationSucceeded ) {
            log.info( "   > All clients, client scopes and service accounts are correctly configured" );
        }
    }

    public void migrateOrganizationClientsAndClientScopes( boolean runKeycloakClientMigration, boolean runKeycloakCleanup ) {
        log.info( String.format( "* Run client migration with settings (runKeycloakMigration=%s, runKeycloakCleanup=%s)...", runKeycloakClientMigration, runKeycloakCleanup ) );

        ConfidentialOrganizationsEntity confidentialOrganizationsEntity = confidentialOrganizationService.getApprovedConfidentialOrganizationsEntity( 0, 9999 );
        if ( confidentialOrganizationsEntity.getOrganizations().isEmpty() ) {
            log.info( "   > No organizations found." );
            return;
        }

        for ( ConfidentialOrganizationEntity confidentialOrganization : confidentialOrganizationsEntity.getOrganizations() ) {
            KeycloakId organizationId = confidentialOrganization.getOrganizationId();
            Set<String> missingClientNames = keycloakClientService.getMissingClientNamesForOrganization( organizationId, confidentialOrganization.getCategories() );
            if ( !missingClientNames.isEmpty() ) {
                log.warn(
                        "   > [!] Organization " + confidentialOrganization.getOrganizationId()
                                + " missing clients: " + String.join( ", ", missingClientNames )
                );
                if ( runKeycloakClientMigration ) {
                    log.info( String.format( ">> Setting up and configuring clients, client scopes and service accounts for %s", organizationId ) );
                    keycloakClientService.setupDefaultOrganizationClients( organizationId, confidentialOrganization.getCategories() );
                }
            }

            Set<String> invalidOrLegacyClients = keycloakClientService.getLegacyAndInvalidClientsForOrganization( organizationId );
            if ( !invalidOrLegacyClients.isEmpty() ) {
                log.warn(
                        "   > [!] Organization " + confidentialOrganization.getOrganizationId()
                                + " has invalid or legacy clients: " + String.join( ", ", invalidOrLegacyClients )
                );
                if ( runKeycloakCleanup ) {
                    for ( String clientId : invalidOrLegacyClients ) {
                        // remove all clients that do not fit the correct naming schema
                        log.info( String.format( "         > Deleting client %s ", clientId ) );
                        keycloakClientService.deleteClient( clientId );
                    }
                }
            }
        }
        log.info( "   > Client migration done." );
    }
}