package org.empaia.services.v2;

import lombok.extern.slf4j.Slf4j;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.db.v2.DBOrganizationDetails;
import org.empaia.models.dto.v2.*;
import org.empaia.models.enums.v2.LanguageCode;
import org.empaia.models.enums.v2.OrganizationAccountState;
import org.empaia.models.enums.v2.OrganizationCategory;
import org.empaia.services.KeycloakService;
import org.empaia.utils.KeycloakUtils;
import org.keycloak.representations.idm.GroupRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.management.InvalidAttributeValueException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class PublicOrganizationService {

    private final OrganizationLogoService organizationLogoService;
    private final OrganizationServiceV2 organizationService;
    private final KeycloakService keycloakService;


    @Autowired
    public PublicOrganizationService(
            OrganizationLogoService organizationLogoService,
            OrganizationServiceV2 organizationService,
            KeycloakService keycloakService
    ) {
        this.organizationLogoService = organizationLogoService;
        this.organizationService = organizationService;
        this.keycloakService = keycloakService;
    }

    public PublicOrganizationProfileEntity createPublicOrganizationProfileEntity(
            GroupRepresentation groupRepresentation, DBOrganizationDetails organizationDetails
    ) {
        PublicOrganizationProfileEntity organizationEntity = createPublicOrganizationProfileEntityFromGroupRepresentation(
                groupRepresentation, organizationDetails
        );
        KeycloakId organizationId = new KeycloakId( groupRepresentation.getId() );
        assert organizationEntity != null;
        organizationEntity.setLogoUrl(
                organizationLogoService.getLogoUrl( organizationId )
        );
        organizationEntity.setResizedLogoUrls(
                organizationLogoService.getResizedLogoUrls( organizationId )
        );
        return organizationEntity;
    }

    private PublicOrganizationProfileEntity createPublicOrganizationProfileEntityFromGroupRepresentation(
            GroupRepresentation groupRepresentation, DBOrganizationDetails dbOrganizationDetails ) {
        KeycloakId organizationId = new KeycloakId( groupRepresentation.getId() );
        PublicOrganizationProfileEntity.PublicOrganizationProfileEntityBuilder<?, ?> builder = PublicOrganizationProfileEntity
                .builder()
                .organizationId( organizationId );

        String name = null;
        Map<String, List<String>> attributes = groupRepresentation.getAttributes();
        if ( attributes == null ) {
            log.error( "Organization is missing keycloak attributes: " + groupRepresentation.getId() );
        } else {
            final List<String> defaultList = Collections.singletonList( name );
            name = attributes.getOrDefault( "name", defaultList ).get( 0 );
        }
        builder = builder.organizationName( name );

        PublicOrganizationContactDataEntity contactData;
        PublicOrganizationDetailsEntity organizationDetails;
        try {
            contactData = createPublicOrganizationContactData( groupRepresentation );
            organizationDetails = createPublicOrganizationDetails( groupRepresentation, dbOrganizationDetails );
        } catch ( InvalidAttributeValueException e ) {
            log.error( "Failed to get organization details and contact data", e );
            return null;
        }

        return builder.contactData( contactData )
                .details( organizationDetails )
                .logoUrl( organizationLogoService.getLogoUrl( organizationId ) )
                .resizedLogoUrls( organizationLogoService.getResizedLogoUrls( organizationId ) )
                .build();
    }

    public PublicOrganizationContactDataEntity createPublicOrganizationContactData(
            GroupRepresentation groupRepresentation
    ) throws InvalidAttributeValueException {
        PublicOrganizationContactDataEntity contactDataEntity = new PublicOrganizationContactDataEntity();
        Map<String, List<String>> attributes = groupRepresentation.getAttributes();
        KeycloakUtils.setAddressEntityFromAttributes( contactDataEntity, attributes );
        contactDataEntity.setDepartment( KeycloakUtils.getAttributeValue( attributes, "department", "" ) );
        contactDataEntity.setWebsite( KeycloakUtils.getAttributeValue( attributes, "website", "" ) );
        boolean isEmailAddressPublic = KeycloakUtils.getAttributeValueAsBoolean( attributes, "isEmailAddressPublic", false );
        if ( isEmailAddressPublic ) {
            contactDataEntity.setEmailAddress( KeycloakUtils.getAttributeValue( attributes, "emailAddress", null ) );
        }
        boolean isFaxNumberPublic = KeycloakUtils.getAttributeValueAsBoolean( attributes, "isFaxNumberPublic", false );
        if ( isFaxNumberPublic ) {
            contactDataEntity.setFaxNumber( KeycloakUtils.getAttributeValue( attributes, "faxNumber", null ) );
        }
        boolean isPhoneNumberPublic = KeycloakUtils.getAttributeValueAsBoolean( attributes, "isPhoneNumberPublic", false );
        if ( isPhoneNumberPublic ) {
            contactDataEntity.setPhoneNumber( KeycloakUtils.getAttributeValue( attributes, "phoneNumber", null ) );
        }
        return contactDataEntity;
    }

    public PublicOrganizationDetailsEntity createPublicOrganizationDetails(
            GroupRepresentation groupRepresentation, DBOrganizationDetails dbOrganizationDetails
    ) throws InvalidAttributeValueException, NoSuchElementException {
        List<TextTranslationEntity> descriptions = new ArrayList<>();
        descriptions.add(
                TextTranslationEntity.builder().languageCode( LanguageCode.EN ).text( dbOrganizationDetails.getDescriptionEnglish() ).build()
        );
        descriptions.add(
                TextTranslationEntity.builder().languageCode( LanguageCode.DE ).text( dbOrganizationDetails.getDescriptionGerman() ).build()
        );
        PublicOrganizationDetailsEntity.PublicOrganizationDetailsEntityBuilder<?, ?> builder = PublicOrganizationDetailsEntity.builder()
                .description( descriptions );

        Map<String, List<String>> attributes = groupRepresentation.getAttributes();
        builder.categories( getOrganizationCategories( attributes ) );

        boolean isUserCountPublic = KeycloakUtils.getAttributeValueAsBoolean( attributes, "isUserCountPublic", false );
        if ( isUserCountPublic ) {
            int numberOfMembers = this.keycloakService.getOrganizationMembers( dbOrganizationDetails.getKeycloakId() ).size();
            builder = builder.numberOfMembers( numberOfMembers );
        }
        return builder.build();
    }

    public PublicOrganizationProfileEntity getActivePublicOrganizationProfileEntity( KeycloakId organizationId ) {
        return organizationService.getOrganizationProfileEntityForMatchingAccountState(
                organizationId, OrganizationAccountState.ACTIVE, this::createPublicOrganizationProfileEntity
        );
    }

    public PublicOrganizationsEntity getActivePublicOrganizationsEntity( int page, int pageSize ) {
        return organizationService.getActiveOrganizationsEntity(
                page, pageSize,
                this::createPublicOrganizationEntity,
                this::createPublicOrganizationsEntity
        );
    }

    public PublicOrganizationsEntity getPublicOrganizationsEntity(
            int page, int pageSize, List<OrganizationAccountState> accountStates, List<KeycloakId> organizationIds
    ) {
        return organizationService.getOrganizationsEntity(
                page, pageSize, accountStates, organizationIds,
                this::createPublicOrganizationEntity,
                this::createPublicOrganizationsEntity
        );
    }

    public PublicOrganizationProfilesEntity getPublicOrganizationProfilesEntity(
            int page, int pageSize, List<OrganizationAccountState> accountStates, List<KeycloakId> organizationIds
    ) {
        return organizationService.getOrganizationProfilesEntity(
                page, pageSize, accountStates, organizationIds,
                this::createPublicOrganizationProfileEntity,
                this::createPublicOrganizationProfilesEntity
        );
    }

    private PublicOrganizationProfilesEntity createPublicOrganizationProfilesEntity(
            List<PublicOrganizationProfileEntity> organizations,
            int totalNumberOfOrganizations
    ) {
        return PublicOrganizationProfilesEntity.builder()
                .organizations( organizations )
                .totalOrganizationsCount( totalNumberOfOrganizations )
                .build();
    }

    private PublicOrganizationEntity createPublicOrganizationEntity(
            KeycloakId organizationId, String name, Map<String, List<String>> attributes
    ) {
        var builder = PublicOrganizationEntity.builder()
                .organizationName( name )
                .organizationId( organizationId );
        if ( attributes != null ) {
            builder = builder
                    .logoUrl( organizationLogoService.getLogoUrl( organizationId ) )
                    .resizedLogoUrls( organizationLogoService.getResizedLogoUrls( organizationId ) )
                    .categories( getOrganizationCategories( attributes ) );
        }
        return builder.build();
    }

    private List<OrganizationCategory> getOrganizationCategories( Map<String, List<String>> attributes ) {
        return KeycloakUtils.getAttributeValueList( attributes, "categories", Collections.emptyList() )
                .stream()
                .filter( s -> !s.isEmpty() ) // list with single empty string means empty category list
                .map( OrganizationCategory::fromName )
                .collect( Collectors.toList() );
    }

    private PublicOrganizationsEntity createPublicOrganizationsEntity(
            List<PublicOrganizationEntity> organizations,
            int totalNumberOfOrganizations
    ) {
        return PublicOrganizationsEntity.builder()
                .organizations( organizations )
                .totalOrganizationsCount( totalNumberOfOrganizations )
                .build();
    }
}

