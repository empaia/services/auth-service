package org.empaia.services.v2;

import com.jlefebure.spring.boot.minio.MinioException;
import com.jlefebure.spring.boot.minio.MinioService;
import io.minio.ErrorCode;
import io.minio.MinioClient;
import io.minio.errors.ErrorResponseException;
import io.minio.http.Method;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Map;

@Slf4j
@Service
public class FileServiceV2 {
    private final MinioService minioService;
    private final MinioClient minioClient;

    @Value( "${spring.minio.bucket}" )
    private String bucketName;
    @Value( "${spring.minio.url}" )
    private String minioInternalURL;
    @Value( "${spring.minio.external_url}" )
    private String minioExternalURL;

    public FileServiceV2( MinioService minioService, MinioClient minioClient ) {
        this.minioService = minioService;
        this.minioClient = minioClient;
    }

    public void uploadObject( String objectName, InputStream inputStream, String mimeType ) throws MinioException {
        try {
            minioService.upload( Paths.get( objectName ), inputStream, mimeType );
        } catch ( MinioException e ) {
            log.error( "An error occurred while uploading the file to minio: " + e );
            throw e;
        }
    }

    public InputStreamResource downloadObject( String objectName ) throws FileNotFoundException {
        try {
            InputStream stream = minioService.get( Paths.get( objectName ) );
            return new InputStreamResource( stream );
        } catch ( MinioException e ) {
            throw new FileNotFoundException( objectName );
        }
    }

    public void deleteObject( String objectName ) throws MinioException {
        try {
            minioService.remove( Paths.get( objectName ) );
        } catch ( MinioException e ) {
            log.debug( "An error occurred while deleting the file from minio: ", e );
            throw e;
        }
    }

    public boolean hasMinIOObject( String objectName ) throws Exception {
        boolean found;
        try {
            minioClient.statObject( bucketName, objectName );
            found = true;
        } catch ( ErrorResponseException e ) {
            if ( e.errorResponse().message().equals( "Object does not exist" ) ||
                    e.errorResponse().errorCode() == ErrorCode.NO_SUCH_OBJECT ) {
                found = false;
            } else {
                throw e;
            }
        }
        return found;
    }

    public String getPresignedUrl( String objectName, Map<String, String> reqParams ) throws Exception {
        int expireTimeInSeconds = 15 * 60;
        String url;
        if ( hasMinIOObject( objectName ) ) {
            url = this.minioClient.getPresignedObjectUrl(
                    Method.GET, bucketName, objectName, expireTimeInSeconds, reqParams
            );
            url = this.rewritePresignedUrl( url );
        } else {
            url = null;
        }
        return url;
    }

    private String rewritePresignedUrl( String url ) {
        if ( this.minioExternalURL != null && !this.minioExternalURL.isEmpty() &&
                this.minioInternalURL != null && !this.minioInternalURL.isEmpty() ) {
            url = url.replace( this.minioInternalURL, this.minioExternalURL );
        }
        return url;
    }

    public void renameObject( String oldObjectName, String newObjectName, String mimeType ) throws Exception {
        if ( hasMinIOObject( oldObjectName ) ) {
            var oldObjectPath = Paths.get( oldObjectName );
            InputStream inputStream = null;
            try {
                inputStream = minioService.get( oldObjectPath );
            } catch ( MinioException e ) {
                log.debug( "An error occurred while renaming this file in minio: " + oldObjectName + " -> " + newObjectName, e );
            }
            if ( inputStream != null ) {
                minioService.upload( Paths.get( newObjectName ), inputStream, mimeType );
                minioService.remove( oldObjectPath );
            }
        }
    }
}
