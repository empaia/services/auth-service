package org.empaia.services.v2;

import lombok.extern.slf4j.Slf4j;
import org.empaia.exceptions.generic.NotFoundException;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.db.v2.DBMembershipRequest;
import org.empaia.models.db.v2.DBOrganizationCategoryRequest;
import org.empaia.models.db.v2.DBOrganizationDetails;
import org.empaia.models.db.v2.DBOrganizationUserRoleRequest;
import org.empaia.models.dto.v2.*;
import org.empaia.models.enums.v2.OrganizationAccountState;
import org.empaia.models.enums.v2.OrganizationCategory;
import org.empaia.repositories.v2.MembershipRequestsRepository;
import org.empaia.repositories.v2.OrganizationCategoryRequestsRepository;
import org.empaia.repositories.v2.OrganizationDetailsRepository;
import org.empaia.repositories.v2.OrganizationUserRoleRequestsRepository;
import org.empaia.services.KeycloakService;
import org.empaia.utils.KeycloakUtils;
import org.empaia.utils.UserUtils;
import org.keycloak.admin.client.resource.GroupResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.management.InvalidAttributeValueException;
import java.sql.Timestamp;
import java.text.Normalizer;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class ConfidentialOrganizationService {
    private final KeycloakService keycloakService;
    private final MembershipRequestsRepository membershipRequestsRepository;
    private final CodeNameGeneratorService codeNameGeneratorService;
    private final OrganizationCategoryRequestsRepository organizationCategoryRequestsRepository;
    private final OrganizationDetailsRepository organizationDetailsRepository;
    private final OrganizationLogoService organizationLogoService;
    private final OrganizationServiceV2 organizationService;
    private final OrganizationUserRoleRequestsRepository organizationUserRoleRequestsRepository;


    @Autowired
    public ConfidentialOrganizationService(
            KeycloakService keycloakService,
            OrganizationDetailsRepository organizationDetailsRepository,
            MembershipRequestsRepository membershipRequestsRepository,
            CodeNameGeneratorService codeNameGeneratorService,
            OrganizationCategoryRequestsRepository organizationCategoryRequestsRepository,
            OrganizationLogoService organizationLogoService,
            OrganizationServiceV2 organizationService,
            OrganizationUserRoleRequestsRepository organizationUserRoleRequestsRepository
    ) {
        this.keycloakService = keycloakService;
        this.membershipRequestsRepository = membershipRequestsRepository;
        this.codeNameGeneratorService = codeNameGeneratorService;
        this.organizationCategoryRequestsRepository = organizationCategoryRequestsRepository;
        this.organizationDetailsRepository = organizationDetailsRepository;
        this.organizationLogoService = organizationLogoService;
        this.organizationService = organizationService;
        this.organizationUserRoleRequestsRepository = organizationUserRoleRequestsRepository;
    }

    public ConfidentialOrganizationProfileEntity updateDetails( KeycloakId organizationId, PostInitialConfidentialOrganizationDetailsEntity detailsEntity ) {
        organizationService.updateOrganizationGroupRepresentation( organizationId, groupRepresentation -> {
            addOrganizationDetailsToGroupRepresentation( detailsEntity, groupRepresentation );
            storeOrganizationDetailsInDB( organizationId, detailsEntity );
        } );
        return createConfidentialOrganizationProfileEntity( organizationId ).orElseThrow(
                () -> new NotFoundException( "Organization not found" )
        );
    }

    public ConfidentialOrganizationProfileEntity updateContactData( KeycloakId organizationId, ConfidentialOrganizationContactDataEntity contactData ) {
        organizationService.updateOrganizationGroupRepresentation( organizationId, groupRepresentation -> {
            addContactDataToGroupRepresentation( contactData, groupRepresentation );
        } );
        return createConfidentialOrganizationProfileEntity( organizationId ).orElseThrow(
                () -> new NotFoundException( "Organization not found" )
        );
    }

    public ConfidentialOrganizationProfileEntity updateName( KeycloakId organizationId, String name ) {
        organizationService.updateOrganizationAttributes( organizationId, attributes -> {
            attributes.put( "name", Collections.singletonList( name ) );
        } );
        return createConfidentialOrganizationProfileEntity( organizationId ).orElseThrow(
                () -> new NotFoundException( "Organization not found" )
        );
    }

    public Optional<ConfidentialOrganizationProfileEntity> createConfidentialOrganizationProfileEntity( KeycloakId organizationId ) {
        Optional<GroupResource> groupResource = keycloakService.getOptionalGroupResource( organizationId );
        Optional<ConfidentialOrganizationProfileEntity> confidentialOrganizationProfileEntity;
        if ( groupResource.isPresent() ) {
            Optional<DBOrganizationDetails> details = organizationDetailsRepository.findByKeycloakId( organizationId.toString() );
            if ( details.isEmpty() ) {
                log.error( "Organization details not found for organization: " + organizationId );
                confidentialOrganizationProfileEntity = Optional.empty();
            } else {
                confidentialOrganizationProfileEntity = Optional.ofNullable(
                        createConfidentialOrganizationProfileEntity(
                                groupResource.get().toRepresentation(), details.get()
                        )
                );
            }
        } else {
            log.error( "Organization not found: " + organizationId );
            confidentialOrganizationProfileEntity = Optional.empty();
        }
        return confidentialOrganizationProfileEntity;
    }

    public ConfidentialOrganizationProfileEntity createConfidentialOrganizationProfileEntity(
            GroupRepresentation groupRepresentation, DBOrganizationDetails organizationDetails
    ) {
        ConfidentialOrganizationProfileEntity organizationEntity = mapGroupRepresentationToConfidentialOrganizationProfileEntity(
                groupRepresentation, organizationDetails
        );
        KeycloakId organizationId = new KeycloakId( groupRepresentation.getId() );
        assert organizationEntity != null;
        organizationEntity.setLogoUrl(
                organizationLogoService.getLogoUrl( organizationId )
        );
        organizationEntity.setResizedLogoUrls(
                organizationLogoService.getResizedLogoUrls( organizationId )
        );
        return organizationEntity;
    }

    public ConfidentialOrganizationProfileEntity createNewOrganization( PostOrganizationEntity organizationEntity, KeycloakId creatorId ) {
        // TODO: create a python test that verifies that no second organization with the same email or username can be
        // created. Keycloak should already make this check and throw an exception then.

        Map<String, List<String>> attributes = new HashMap<>();
        KeycloakUtils.setAttributeValueIfNotNull( attributes, "name", organizationEntity.getName() );
        Timestamp now = new Timestamp( new Date().getTime() );
        KeycloakUtils.setAttributeValue( attributes, "createdTimestamp", now );
        KeycloakUtils.setAttributeValue( attributes, "updatedTimestamp", now );

        GroupRepresentation groupRepresentation = new GroupRepresentation();
        groupRepresentation.setName( createUniqueOrganizationGroupName( codeNameGeneratorService.generateRandomCodeName() ) );
        groupRepresentation.setAttributes( attributes );

        if ( organizationEntity.getContactData() != null ) {
            addContactDataToGroupRepresentation( organizationEntity.getContactData(), groupRepresentation );
        }
        if ( organizationEntity.getDetails() != null ) {
            addOrganizationDetailsToGroupRepresentation( organizationEntity.getDetails(), groupRepresentation );
        }

        KeycloakId organizationId = keycloakService.createOrganizationGroup( groupRepresentation );

        DBOrganizationDetails dbOrganizationDetails = new DBOrganizationDetails();
        dbOrganizationDetails.setOrganizationAccountState( OrganizationAccountState.REQUIRES_ACTIVATION );
        dbOrganizationDetails.setKeycloakId( organizationId.toString() );
        dbOrganizationDetails.setCreatorId( creatorId.toString() );
        if ( organizationEntity.getDetails() != null ) {
            dbOrganizationDetails.setDescriptionEnglish( organizationEntity.getDetails().getDescriptionEnglish() );
            dbOrganizationDetails.setDescriptionGerman( organizationEntity.getDetails().getDescriptionGerman() );
        }
        organizationDetailsRepository.save( dbOrganizationDetails );

        return createConfidentialOrganizationProfileEntity( organizationId ).orElseThrow(
                () -> new RuntimeException( "Failed to create organization" )
        );
    }

    private String createUniqueOrganizationGroupName( String organizationName ) {
        String groupName = Normalizer.normalize( organizationName, Normalizer.Form.NFD )
                .replaceAll( "\\p{InCombiningDiacriticalMarks}+", "" )
                .replaceAll( "[^a-zA-Z0-9-_]", "_" )
                .toLowerCase( Locale.ROOT );
        if ( groupName.length() >= 250 ) {
            groupName = groupName.substring( 0, 249 );
        }
        List<String> groupNames = keycloakService.getOrganizationGroupNames();
        if ( groupNames.contains( groupName ) ) {
            String baseGroupName = groupName;
            int uniqueSuffixCounter = 1;
            groupName = baseGroupName + uniqueSuffixCounter;
            while ( groupNames.contains( groupName ) ) {
                uniqueSuffixCounter++;
                groupName = baseGroupName + uniqueSuffixCounter;
            }
        }
        return groupName;
    }

    private ConfidentialOrganizationProfileEntity mapGroupRepresentationToConfidentialOrganizationProfileEntity(
            GroupRepresentation groupRepresentation, DBOrganizationDetails dbOrganizationDetails
    ) {
        ConfidentialOrganizationProfileEntity.ConfidentialOrganizationProfileEntityBuilder<?, ?> builder = ConfidentialOrganizationProfileEntity
                .builder()
                .organizationId( new KeycloakId( groupRepresentation.getId() ) )
                .normalizedName( groupRepresentation.getName() );

        Map<String, List<String>> attributes = groupRepresentation.getAttributes();
        if ( attributes == null ) {
            log.error( "Organization is missing keycloak attributes: " + groupRepresentation.getId() );
        } else {
            final List<String> defaultList = Collections.singletonList( null );
            String name = attributes.getOrDefault( "name", defaultList ).get( 0 );
            Timestamp createdTimestamp = KeycloakUtils.getAttributeValueAsTimestamp( attributes, "createdTimestamp", null );
            Timestamp updatedTimestamp = KeycloakUtils.getAttributeValueAsTimestamp( attributes, "updatedTimestamp", null );
            builder = builder
                    .createdTimestamp( createdTimestamp == null ? 0 : createdTimestamp.getTime() )
                    .updatedTimestamp( updatedTimestamp == null ? 0 : updatedTimestamp.getTime() )
                    .organizationName( name );
        }

        OrganizationAccountState accountState = dbOrganizationDetails.getOrganizationAccountState();
        builder = builder.accountState( accountState );

        ConfidentialOrganizationContactDataEntity contactData;
        ConfidentialOrganizationDetailsEntity organizationDetails;
        try {
            contactData = mapGroupRepresentationToConfidentialOrganizationContactData( groupRepresentation );
            organizationDetails = mapGroupRepresentationToConfidentialOrganizationDetails( groupRepresentation, dbOrganizationDetails );
        } catch ( InvalidAttributeValueException e ) {
            log.error( "Failed to get organization details and contact data", e );
            return null;
        }

        return builder.contactData( contactData )
                .details( organizationDetails )
                .build();
    }

    public ConfidentialOrganizationContactDataEntity mapGroupRepresentationToConfidentialOrganizationContactData( GroupRepresentation groupRepresentation ) throws InvalidAttributeValueException {
        ConfidentialOrganizationContactDataEntity contactDataEntity = new ConfidentialOrganizationContactDataEntity();
        Map<String, List<String>> attributes = groupRepresentation.getAttributes();
        KeycloakUtils.setConfidentialOrganizationContactDataFromAttributes( contactDataEntity, attributes );
        contactDataEntity.setDepartment( KeycloakUtils.getAttributeValue( attributes, "department", null ) );
        contactDataEntity.setEmailAddress( KeycloakUtils.getAttributeValue( attributes, "emailAddress", null ) );
        contactDataEntity.setFaxNumber( KeycloakUtils.getAttributeValue( attributes, "faxNumber", null ) );
        contactDataEntity.setPhoneNumber( KeycloakUtils.getAttributeValue( attributes, "phoneNumber", null ) );
        contactDataEntity.setEmailAddressPublic( KeycloakUtils.getAttributeValueAsBoolean( attributes, "isEmailAddressPublic", false ) );
        contactDataEntity.setFaxNumberPublic( KeycloakUtils.getAttributeValueAsBoolean( attributes, "isFaxNumberPublic", false ) );
        contactDataEntity.setPhoneNumberPublic( KeycloakUtils.getAttributeValueAsBoolean( attributes, "isPhoneNumberPublic", false ) );
        contactDataEntity.setWebsite( KeycloakUtils.getAttributeValue( attributes, "website", null ) );
        return contactDataEntity;
    }

    public void addContactDataToGroupRepresentation( ConfidentialOrganizationContactDataEntity organizationContactData, GroupRepresentation groupRepresentation ) {
        Map<String, List<String>> attributes = getOrCreateGroupRepresentationAttributes( groupRepresentation );
        KeycloakUtils.addOrganizationContactDataToAttributes( organizationContactData, attributes );
        KeycloakUtils.setAttributeValueIfNotNull( attributes, "department", organizationContactData.getDepartment() );
        KeycloakUtils.setAttributeValueIfNotNull( attributes, "emailAddress", organizationContactData.getEmailAddress() );
        KeycloakUtils.setAttributeValueIfNotNull( attributes, "faxNumber", organizationContactData.getFaxNumber() );
        KeycloakUtils.setAttributeValueIfNotNull( attributes, "phoneNumber", organizationContactData.getPhoneNumber() );
        KeycloakUtils.setAttributeValueIfNotNull( attributes, "isEmailAddressPublic", organizationContactData.isEmailAddressPublic() ? "true" : "false" );
        KeycloakUtils.setAttributeValueIfNotNull( attributes, "isFaxNumberPublic", organizationContactData.isFaxNumberPublic() ? "true" : "false" );
        KeycloakUtils.setAttributeValueIfNotNull( attributes, "isPhoneNumberPublic", organizationContactData.isPhoneNumberPublic() ? "true" : "false" );
        KeycloakUtils.setAttributeValueIfNotNull( attributes, "website", organizationContactData.getWebsite() );
    }

    public ConfidentialOrganizationDetailsEntity mapGroupRepresentationToConfidentialOrganizationDetails(
            GroupRepresentation groupRepresentation, DBOrganizationDetails dbOrganizationDetails
    ) throws InvalidAttributeValueException, NoSuchElementException {
        ConfidentialOrganizationDetailsEntity detailsEntity = new ConfidentialOrganizationDetailsEntity();
        Map<String, List<String>> attributes = groupRepresentation.getAttributes();
        if ( attributes != null ) {
            boolean isUserCountPublic = KeycloakUtils.getAttributeValueAsBoolean( attributes, "isUserCountPublic", false );
            detailsEntity.setUserCountPublic( isUserCountPublic );
            List<UserRepresentation> organizationMembers = keycloakService.getOrganizationMembers(
                    dbOrganizationDetails.getKeycloakId()
            );
            detailsEntity.setNumberOfMembers( organizationMembers.size() );
            List<String> categories = KeycloakUtils.getAttributeValueList(
                    attributes, "categories", new ArrayList<>()
            );
            if ( categories != null ) {
                if ( categories.size() == 1 && categories.get( 0 ).isEmpty() ) {
                    // empty category list is stored as list with exactly one empty string
                    detailsEntity.setCategories( Collections.emptyList() );
                } else {
                    detailsEntity.setCategories(
                            categories
                                    .stream()
                                    .map( OrganizationCategory::fromName )
                                    .collect( Collectors.toList() )
                    );
                }
            }
            detailsEntity.setDescriptionEnglish( dbOrganizationDetails.getDescriptionEnglish() );
            detailsEntity.setDescriptionGerman( dbOrganizationDetails.getDescriptionGerman() );
        }
        return detailsEntity;
    }

    private void storeOrganizationDetailsInDB( KeycloakId organizationId, PostConfidentialOrganizationDetailsEntity confidentialOrganizationDetailsEntity ) {
        DBOrganizationDetails dbOrganizationDetails = organizationDetailsRepository.findByKeycloakId( organizationId.toString() )
                .orElseThrow(
                        () -> new NotFoundException( "Organization not found " )
                );
        storeOrganizationDetailsInDB( organizationId, confidentialOrganizationDetailsEntity, dbOrganizationDetails );
    }

    @Transactional
    private void storeOrganizationDetailsInDB( KeycloakId organizationId, PostConfidentialOrganizationDetailsEntity confidentialOrganizationDetailsEntity, DBOrganizationDetails dbOrganizationDetails ) {
        dbOrganizationDetails.setDescriptionEnglish( confidentialOrganizationDetailsEntity.getDescriptionEnglish() );
        dbOrganizationDetails.setDescriptionGerman( confidentialOrganizationDetailsEntity.getDescriptionGerman() );
        dbOrganizationDetails.setKeycloakId( organizationId.toString() );
        organizationDetailsRepository.save( dbOrganizationDetails );
    }

    public void addOrganizationDetailsToGroupRepresentation( PostInitialConfidentialOrganizationDetailsEntity organizationDetails, GroupRepresentation groupRepresentation ) {
        Map<String, List<String>> attributes = getOrCreateGroupRepresentationAttributes( groupRepresentation );

        var categoryList = organizationDetails.getCategories();
        if ( categoryList != null ) {
            if ( categoryList.isEmpty() ) {
                // store list with empty string, because an empty list results in "null" when getting the categories later
                attributes.put( "categories", Collections.singletonList( "" ) );
            } else {
                Set<String> categories = categoryList.stream()
                        .map( OrganizationCategory::getName )
                        .collect( Collectors.toSet() );
                attributes.put( "categories", new ArrayList<>( categories ) );
            }
        } else {
            attributes.put( "categories", null );
        }
        KeycloakUtils.setAttributeValueIfNotNull( attributes, "isUserCountPublic", organizationDetails.isUserCountPublic() ? "true" : "false" );
    }

    private Map<String, List<String>> getOrCreateGroupRepresentationAttributes( GroupRepresentation groupRepresentation ) {
        Map<String, List<String>> attributes = groupRepresentation.getAttributes();
        if ( attributes == null ) {
            attributes = new HashMap<>();
            groupRepresentation.setAttributes( attributes );
        }
        return attributes;
    }

    public void deleteOrganization( KeycloakId organizationId ) {
        keycloakService.deleteAllClientsAndClientGroupsOfOrganization( organizationId ); // delete all clients and scopes related to organization
        Optional<DBOrganizationDetails> details = organizationDetailsRepository.findByKeycloakId( organizationId.toString() );
        details.ifPresent( organizationDetailsRepository::delete );
        PageRequest pageRequest = PageRequest.of( 0, 1000 );
        Page<DBMembershipRequest> membershipRequests = membershipRequestsRepository.findAllByOrganizationId( organizationId.toString(), pageRequest );
        membershipRequestsRepository.deleteAll( membershipRequests );
        Page<DBOrganizationUserRoleRequest> roleRequests = organizationUserRoleRequestsRepository.findAllByOrganizationId( organizationId.toString(), pageRequest );
        organizationUserRoleRequestsRepository.deleteAll( roleRequests );
        Page<DBOrganizationCategoryRequest> categoryRequests = organizationCategoryRequestsRepository.findAllByOrganizationId( organizationId.toString(), pageRequest );
        organizationCategoryRequestsRepository.deleteAll( categoryRequests );
        Optional<GroupResource> groupResource = keycloakService.getOptionalGroupResource( organizationId );
        if ( groupResource.isPresent() ) {
            unsetDefaultActiveOrganizationForMembers( groupResource.get(), organizationId );
            keycloakService.deleteGroup( organizationId.toString() );
        } else {
            log.warn( "Cannot remove organization group from keycloak, because it does not exist: " + organizationId );
        }
    }

    private void unsetDefaultActiveOrganizationForMembers( GroupResource groupResource, KeycloakId organizationId ) {
        groupResource.members().forEach( userRepresentation -> {
            var attributes = userRepresentation.getAttributes();
            if ( attributes != null ) {
                UserResource userResource = keycloakService.getUserById( userRepresentation.getId() );
                List<String> defaultActiveOrganization = attributes.getOrDefault( "default-active-organization", Collections.emptyList() );
                if ( !defaultActiveOrganization.isEmpty() ) {
                    if ( organizationId.equals( defaultActiveOrganization.get( 0 ) ) ) {
                        attributes.put( "default-active-organization", Collections.emptyList() );
                        userResource.update( userRepresentation );
                    }
                }
            }
        } );
    }

    public ConfidentialOrganizationProfilesEntity getConfidentialOrganizationProfilesEntity(
            int page, int pageSize, List<OrganizationAccountState> organizationAccountStates
    ) {
        if ( organizationAccountStates == null ) {
            organizationAccountStates = Arrays.asList( OrganizationAccountState.values() );
        }
        return organizationService.getOrganizationProfilesEntity(
                page, pageSize, organizationAccountStates,
                this::createConfidentialOrganizationProfileEntity,
                this::createConfidentialOrganizationProfilesEntity
        );
    }

    public ConfidentialOrganizationProfilesEntity getConfidentialOrganizationProfilesEntityByCreator(
            int page, int pageSize, List<OrganizationAccountState> organizationAccountStates, KeycloakId creatorId
    ) {
        if ( organizationAccountStates == null ) {
            organizationAccountStates = Arrays.asList( OrganizationAccountState.values() );
        }
        return organizationService.getOrganizationProfilesEntityByCreator(
                page, pageSize, organizationAccountStates, creatorId,
                this::createConfidentialOrganizationProfileEntity,
                this::createConfidentialOrganizationProfilesEntity
        );
    }

    public ConfidentialOrganizationProfilesEntity getConfidentialOrganizationProfilesEntity(
            int page, int pageSize, List<OrganizationAccountState> accountStates, List<KeycloakId> organizationIds
    ) {
        return organizationService.getOrganizationProfilesEntity(
                page, pageSize, accountStates, organizationIds,
                this::createConfidentialOrganizationProfileEntity,
                this::createConfidentialOrganizationProfilesEntity
        );
    }

    private ConfidentialOrganizationProfilesEntity createConfidentialOrganizationProfilesEntity(
            List<ConfidentialOrganizationProfileEntity> organizations,
            int totalNumberOfOrganizations
    ) {
        return ConfidentialOrganizationProfilesEntity.builder()
                .organizations( organizations )
                .totalOrganizationsCount( totalNumberOfOrganizations )
                .build();
    }

    public DBOrganizationDetails getDBOrganizationDetails( KeycloakId organizationId ) throws NotFoundException {
        return organizationDetailsRepository.findByKeycloakId( organizationId.toString() ).orElseThrow(
                () -> new NotFoundException( "Organization details not found" )
        );
    }

    public void leave( KeycloakId organizationId, KeycloakId leavingUserId ) {
        OrganizationMember organizationMember = UserUtils.makeOrganizationMemberEntity( organizationId, leavingUserId );
        if ( !keycloakService.isGroupMember( organizationMember ) ) {
            throw new NotFoundException( "You are not a member of the organization " );
        }
        keycloakService.removeUserFromOrganization( organizationMember );
    }

    public ConfidentialOrganizationProfileEntity getActiveConfidentialOrganizationProfileEntity( KeycloakId organizationId ) {
        return organizationService.getOrganizationProfileEntityForMatchingAccountState(
                organizationId, OrganizationAccountState.ACTIVE, this::createConfidentialOrganizationProfileEntity
        );
    }

    public ConfidentialOrganizationProfileEntity getConfidentialOrganizationProfileEntity( KeycloakId organizationId ) {
        return organizationService.getOrganizationProfileEntity(
                organizationId, this::createConfidentialOrganizationProfileEntity
        );
    }

    public ConfidentialOrganizationProfilesEntity getUnapprovedConfidentialOrganizationProfileEntities() {
        List<OrganizationAccountState> unapprovedAccountStates = List.of(
                OrganizationAccountState.AWAITING_ACTIVATION,
                OrganizationAccountState.REQUIRES_ACTIVATION
        );
        return organizationService.getOrganizationProfilesEntity(
                0, 999, unapprovedAccountStates,
                this::createConfidentialOrganizationProfileEntity,
                this::createConfidentialOrganizationProfilesEntity
        );
    }

    public ConfidentialOrganizationProfileEntity getUnapprovedConfidentialOrganizationProfileEntity( KeycloakId organizationId ) {
        OrganizationAccountState[] unapprovedAccountStates = { OrganizationAccountState.AWAITING_ACTIVATION, OrganizationAccountState.REQUIRES_ACTIVATION };
        return organizationService.getOrganizationProfileEntityForMatchingAccountStateIn(
                organizationId, unapprovedAccountStates, this::createConfidentialOrganizationProfileEntity
        );
    }

    public ConfidentialOrganizationsEntity getApprovedConfidentialOrganizationsEntity( Integer page, Integer pageSize ) {
        var approvedAccountStates = List.of( OrganizationAccountState.ACTIVE, OrganizationAccountState.AWAITING_DEACTIVATION, OrganizationAccountState.INACTIVE );
        return this.getConfidentialOrganizationsEntity( page, pageSize, approvedAccountStates );
    }

    public ConfidentialOrganizationsEntity getApprovedConfidentialOrganizationsEntityForUser( Integer page, Integer pageSize, KeycloakId userId ) {
        List<KeycloakId> organizationIds = keycloakService.getOrganizationIdsOfMember( userId );
        var accountStates = List.of( OrganizationAccountState.ACTIVE, OrganizationAccountState.AWAITING_DEACTIVATION, OrganizationAccountState.INACTIVE );
        return organizationService.getOrganizationsEntity(
                page, pageSize, accountStates, organizationIds,
                this::createConfidentialOrganizationEntity,
                this::createConfidentialOrganizationsEntity
        );
    }

    public ConfidentialOrganizationsEntity getConfidentialOrganizationsEntity(
            Integer page, Integer pageSize, List<OrganizationAccountState> organizationAccountStates
    ) {
        if ( organizationAccountStates == null ) {
            organizationAccountStates = Arrays.asList( OrganizationAccountState.values() );
        }
        return organizationService.getOrganizationsEntity(
                page, pageSize, organizationAccountStates,
                this::createConfidentialOrganizationEntity,
                this::createConfidentialOrganizationsEntity );
    }

    private ConfidentialOrganizationEntity createConfidentialOrganizationEntity(
            KeycloakId organizationId, String name, Map<String, List<String>> attributes
    ) {
        var builder = ConfidentialOrganizationEntity.builder()
                .organizationName( name )
                .organizationId( organizationId );
        if ( attributes != null ) {
            List<OrganizationCategory> categories = KeycloakUtils.getAttributeValueList( attributes, "categories", Collections.emptyList() )
                    .stream()
                    .filter( s -> !s.isEmpty() ) // list with single empty string means empty category list
                    .map( OrganizationCategory::fromName )
                    .collect( Collectors.toList() );
            builder = builder
                    .categories( categories );
        }
        DBOrganizationDetails dbOrganizationDetails = organizationDetailsRepository.findByKeycloakId( organizationId.toString() )
                .orElseThrow(
                        () -> new NotFoundException( "Organization not found " )
                );
        return builder
                .accountState( dbOrganizationDetails.getOrganizationAccountState() )
                .build();
    }

    private ConfidentialOrganizationsEntity createConfidentialOrganizationsEntity(
            List<ConfidentialOrganizationEntity> organizations,
            int totalNumberOfOrganizations
    ) {
        return ConfidentialOrganizationsEntity.builder()
                .organizations( organizations )
                .totalOrganizationsCount( totalNumberOfOrganizations )
                .build();
    }
}

