package org.empaia.services.v2;

import lombok.extern.slf4j.Slf4j;
import org.empaia.exceptions.generic.AlreadyExistException;
import org.empaia.exceptions.generic.NotFoundException;
import org.empaia.models.db.v2.DBOrganizationUserRoleRequest;
import org.empaia.models.dto.v2.OrganizationMember;
import org.empaia.models.dto.v2.OrganizationUserRoleRequestEntity;
import org.empaia.models.dto.v2.OrganizationUserRoleRequestsEntity;
import org.empaia.models.dto.v2.PostOrganizationUserRoleRequestEntity;
import org.empaia.models.enums.v2.OrganizationUserRoleV2;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.enums.v2.RequestState;
import org.empaia.repositories.v2.OrganizationUserRoleRequestsRepository;
import org.empaia.services.KeycloakService;
import org.empaia.utils.JwtUtils;
import org.empaia.utils.UserUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotAuthorizedException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class OrganizationUserRoleRequestService {
    private final OrganizationUserRoleRequestsRepository organizationUserRoleRequestsRepository;
    private final ConfidentialOrganizationService confidentialOrganizationService;
    private final KeycloakService keycloakService;
    private final UserServiceV2 userServiceV2;
    private final ShoutOutService shoutOutService;


    public OrganizationUserRoleRequestService(
            OrganizationUserRoleRequestsRepository organizationUserRoleRequestsRepository,
            ConfidentialOrganizationService confidentialOrganizationService,
            KeycloakService keycloakService,
            UserServiceV2 userServiceV2,
            ShoutOutService shoutOutService )
    {
        this.organizationUserRoleRequestsRepository = organizationUserRoleRequestsRepository;
        this.confidentialOrganizationService = confidentialOrganizationService;
        this.keycloakService = keycloakService;
        this.userServiceV2 = userServiceV2;
        this.shoutOutService = shoutOutService;
    }

    public OrganizationUserRoleRequestsEntity getOrganizationUserRoleRequests( int page, int pageSize, KeycloakId organizationId ) {
        Page<DBOrganizationUserRoleRequest> dbRequests = organizationUserRoleRequestsRepository.findAllByOrganizationIdAndRequestState(
                organizationId.toString(), RequestState.REQUESTED.getValue(), PageRequest.of( page, pageSize )
        );
        List<OrganizationUserRoleRequestEntity> roleRequestEntities = dbRequests.stream()
                .map( this::buildOrganizationUserRoleRequestEntity )
                .collect( Collectors.toList() );
        return OrganizationUserRoleRequestsEntity.builder()
                .organizationUserRoleRequestEntities( roleRequestEntities )
                .totalOrganizationUserRoleRequestsCount( Long.valueOf( dbRequests.getTotalElements() ).intValue() )
                .build();
    }

    public OrganizationUserRoleRequestsEntity getOrganizationUserRoleRequests( KeycloakId userId, KeycloakId organizationId, int page, int pageSize ) {
        Page<DBOrganizationUserRoleRequest> dbRequests = organizationUserRoleRequestsRepository.findAllByOrganizationIdAndUserId(
                organizationId.toString(), userId.toString(), PageRequest.of( page, pageSize )
        );
        return this.convertOrganizationUserRoleRequests( dbRequests );
    }

    public OrganizationUserRoleRequestsEntity convertOrganizationUserRoleRequests( Page<DBOrganizationUserRoleRequest> dbRequests ) {
        List<OrganizationUserRoleRequestEntity> roleRequestEntities = dbRequests.stream()
                .map( this::buildOrganizationUserRoleRequestEntity )
                .collect( Collectors.toList() );
        return OrganizationUserRoleRequestsEntity.builder()
                .organizationUserRoleRequestEntities( roleRequestEntities )
                .totalOrganizationUserRoleRequestsCount( Long.valueOf( dbRequests.getTotalElements() ).intValue() )
                .build();
    }

    public OrganizationUserRoleRequestEntity acceptOrganizationUserRoleRequest( KeycloakId userId, KeycloakId organizationId, int roleRequestId ) {
        DBOrganizationUserRoleRequest dbRequest = validateAndReturnOrganizationUserRoleRequest( roleRequestId, organizationId, userId, false );
        Set<OrganizationUserRoleV2> requestedOrganizationUserRoles = dbRequest.getRequestedUserRoles();
        OrganizationMember organizationMember = OrganizationMember.builder()
                .organizationId( new KeycloakId( dbRequest.getOrganizationId() ) )
                .userId( new KeycloakId( dbRequest.getUserId() ) )
                .build();
        List<OrganizationUserRoleV2> organizationUserRoles = keycloakService.getOrganizationUserRoles( organizationMember );
        requestedOrganizationUserRoles.forEach( role -> {
            if ( !organizationUserRoles.contains( role ) ) {
                organizationUserRoles.add( role );
            }
        } );
        keycloakService.setOrganizationUserRoles( organizationMember, organizationUserRoles );
        dbRequest.updateRequestState( RequestState.ACCEPTED, userId, null );
        organizationUserRoleRequestsRepository.save( dbRequest );
        shoutOutService.notifyUserRoleRequestApproved( organizationId, organizationMember.getUserId() );
        return buildOrganizationUserRoleRequestEntity( dbRequest );
    }

    public OrganizationUserRoleRequestEntity denyOrganizationUserRoleRequest( KeycloakId userId, KeycloakId organizationId, int roleRequestId, String reviewerComment ) {
        DBOrganizationUserRoleRequest dbRequest = validateAndReturnOrganizationUserRoleRequest( roleRequestId, organizationId, userId, false );
        dbRequest.updateRequestState( RequestState.REJECTED, userId, reviewerComment );
        organizationUserRoleRequestsRepository.save( dbRequest );
        shoutOutService.notifyUserRoleRequestRejected( organizationId, new KeycloakId(dbRequest.getUserId()) );
        return buildOrganizationUserRoleRequestEntity( dbRequest );
    }

    public OrganizationUserRoleRequestEntity requestOrganizationUserRole(
            KeycloakId userId,
            KeycloakId organizationId,
            PostOrganizationUserRoleRequestEntity organizationUserRoleRequest,
            Jwt accessToken
    ) {
        Page<DBOrganizationUserRoleRequest> existingOpenRequests = organizationUserRoleRequestsRepository.findAllByOrganizationIdAndUserIdAndRequestState(
                organizationId.toString(),
                userId.toString(),
                RequestState.REQUESTED.getValue(),
                PageRequest.of( 0, 1 )
        );
        if ( existingOpenRequests.getTotalElements() > 0 ) {
            throw new AlreadyExistException( "An open role request for this organization and user already exists. Revoke open request first" );
        }
        Set<OrganizationUserRoleV2> requestedOrganizationUserRoles = new HashSet<>(organizationUserRoleRequest.getRequestedUserRoles());
        Set<OrganizationUserRoleV2> organizationUserRoles = JwtUtils.getOrganizationUserRolesFromToken( accessToken, organizationId );
        if ( organizationUserRoles.equals( requestedOrganizationUserRoles ) ) {
            throw new AlreadyExistException( "User already has all requested user roles" );
        }
        OrganizationMember member = OrganizationMember.builder().organizationId( organizationId ).userId( userId ).build();
        if ( !keycloakService.getAllowedOrganizationUserRoles( member ).containsAll( requestedOrganizationUserRoles ) ) {
            throw new BadRequestException( "The organization categories do not allow the requested organization user roles for this organization" );
        }

        DBOrganizationUserRoleRequest dbOrganizationUserRoleRequest = DBOrganizationUserRoleRequest.builder()
                .organizationId( organizationId.toString() )
                .userId( userId.toString() )
                .createdAt( new Date().getTime() )
                .requestState( RequestState.REQUESTED.getValue() )
                .build();
        dbOrganizationUserRoleRequest.setRequestedUserRoles( requestedOrganizationUserRoles );

        organizationUserRoleRequestsRepository.save( dbOrganizationUserRoleRequest );
        shoutOutService.notifyManagersRoleChangeRequested( organizationId );
        return buildOrganizationUserRoleRequestEntity( dbOrganizationUserRoleRequest );
    }

    public OrganizationUserRoleRequestEntity revokeOrganizationUserRoleRequest( KeycloakId userId, KeycloakId organizationId, int roleRequestId ) {
        DBOrganizationUserRoleRequest dbOrganizationUserRoleRequest = validateAndReturnOrganizationUserRoleRequest( roleRequestId, organizationId, userId, true );
        dbOrganizationUserRoleRequest.updateRequestState( RequestState.REVOKED, userId, null );
        organizationUserRoleRequestsRepository.save( dbOrganizationUserRoleRequest );
        return buildOrganizationUserRoleRequestEntity( dbOrganizationUserRoleRequest );
    }

    private DBOrganizationUserRoleRequest validateAndReturnOrganizationUserRoleRequest( int roleRequestId, KeycloakId organizationId, KeycloakId userId, boolean validateRequestOwnedByUser ) {
        OrganizationMember organizationMember = UserUtils.makeOrganizationMemberEntity( organizationId, userId );
        DBOrganizationUserRoleRequest dbRequest = organizationUserRoleRequestsRepository.findById( roleRequestId ).orElseThrow(
                () -> new NotFoundException( "Organization user role request not found" )
        );
        if ( validateRequestOwnedByUser && !organizationMember.getUserId().equals( dbRequest.getUserId() ) ) {
            log.error("Role request " + dbRequest.getId() + " belongs to user " + dbRequest.getUserId() + ", not to user " + organizationMember.getUserId() );
            throw new NotAuthorizedException( "Action not allowed. ");
        } else if ( !organizationMember.getOrganizationId().equals( dbRequest.getOrganizationId() ) ) {
            log.error("Role request " + dbRequest.getId() + " belongs to organization " + dbRequest.getOrganizationId() + ", not to organization " + organizationMember.getOrganizationId() );
            throw new NotAuthorizedException( "Action not allowed. ");
        }
        return dbRequest;
    }

    private OrganizationUserRoleRequestEntity buildOrganizationUserRoleRequestEntity( DBOrganizationUserRoleRequest dbRequest ) {
        Set<OrganizationUserRoleV2> requestedRoles = dbRequest.getRequestedUserRoles();
        Set<OrganizationUserRoleV2> activeOrganizationUserRoles = keycloakService.getOrganizationUserRoles(
                new KeycloakId( dbRequest.getUserId() ),
                new KeycloakId( dbRequest.getOrganizationId() )
        );

        Set<OrganizationUserRoleV2> newlyRequestedRoles = new HashSet<>( requestedRoles );
        newlyRequestedRoles.removeAll( activeOrganizationUserRoles );

        Set<OrganizationUserRoleV2> retractedRoles = new HashSet<>( activeOrganizationUserRoles );
        retractedRoles.removeAll( requestedRoles );

        return OrganizationUserRoleRequestEntity.builder()
                .organizationUserRoleRequestId( dbRequest.getId() )
                .organizationId( new KeycloakId( dbRequest.getOrganizationId() ) )
                .organizationName(
                        confidentialOrganizationService.getConfidentialOrganizationProfileEntity(
                                new KeycloakId( dbRequest.getOrganizationId() )
                        ).getOrganizationName()
                )
                .existingRoles( new ArrayList<>( activeOrganizationUserRoles ) )
                .requestedRoles( new ArrayList<>( newlyRequestedRoles ) )
                .retractedRoles( new ArrayList<>( retractedRoles ) )
                .userDetails( userServiceV2.getExtendedUserDetails( dbRequest.getUserId() ) )
                .createdAt( dbRequest.getCreatedAt() )
                .reviewerComment( dbRequest.getReviewerComment() )
                .reviewerId( new KeycloakId( dbRequest.getReviewerId() ) )
                .updatedAt( dbRequest.getUpdatedAt() )
                .requestState( dbRequest.getRequestState() )
                .build();
    }
}
