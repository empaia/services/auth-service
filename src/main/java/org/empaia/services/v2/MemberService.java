package org.empaia.services.v2;

import lombok.extern.slf4j.Slf4j;
import org.empaia.exceptions.generic.NotFoundException;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.*;
import org.empaia.models.enums.v2.OrganizationAccountState;
import org.empaia.models.enums.v2.OrganizationUserRoleV2;
import org.empaia.models.enums.v2.UserAccountState;
import org.empaia.repositories.v2.OrganizationDetailsRepository;
import org.empaia.services.KeycloakService;
import org.empaia.utils.KeycloakUtils;
import org.empaia.utils.UserUtils;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static org.empaia.utils.KeycloakUtils.getUserAccountState;

@Slf4j
@Service
@Transactional
public class MemberService {

    private final ConfidentialOrganizationService confidentialOrganizationService;
    private final KeycloakService keycloakService;
    private final OrganizationDetailsRepository organizationDetailsRepository;
    private final PublicOrganizationService publicOrganizationService;
    private final UserServiceV2 userServiceV2;
    private final OrganizationUserRoleRequestService organizationUserRoleRequestService;

    @Autowired
    public MemberService(
            ConfidentialOrganizationService confidentialOrganizationService,
            KeycloakService keycloakService,
            OrganizationDetailsRepository organizationDetailsRepository,
            PublicOrganizationService publicOrganizationService,
            UserServiceV2 userServiceV2,
            OrganizationUserRoleRequestService organizationUserRoleRequestService ) {
        this.confidentialOrganizationService = confidentialOrganizationService;
        this.keycloakService = keycloakService;
        this.organizationDetailsRepository = organizationDetailsRepository;
        this.publicOrganizationService = publicOrganizationService;
        this.userServiceV2 = userServiceV2;
        this.organizationUserRoleRequestService = organizationUserRoleRequestService;
    }

    public PublicOrganizationsEntity getActiveOrganizationsEntityForUser( int page, int pageSize, KeycloakId userId ) {
        List<KeycloakId> organizationIds = keycloakService.getOrganizationIdsOfMember( userId );
        return publicOrganizationService.getPublicOrganizationsEntity( page, pageSize, List.of( OrganizationAccountState.ACTIVE ), organizationIds );
    }

    public PublicOrganizationProfilesEntity getActivePublicOrganizationProfilesEntityForUser( int page, int pageSize, KeycloakId userId ) {
        List<KeycloakId> organizationIds = keycloakService.getOrganizationIdsOfMember(userId);
        return publicOrganizationService.getPublicOrganizationProfilesEntity( page, pageSize, List.of(OrganizationAccountState.ACTIVE), organizationIds );
    }

    public ConfidentialOrganizationProfilesEntity getActiveConfidentialOrganizationProfilesEntityForUser( int page, int pageSize, KeycloakId userId ) {
        List<KeycloakId> organizationIds = keycloakService.getOrganizationIdsOfMember(userId);
        return confidentialOrganizationService.getConfidentialOrganizationProfilesEntity( page, pageSize, List.of(OrganizationAccountState.ACTIVE), organizationIds );
    }

    public MemberProfilesEntity getMembersOnlyInThisOrganization( KeycloakId organizationId ) {
        List<MemberProfileEntity> memberEntities = keycloakService.getOrganizationMembersOnlyInThisOrganization( organizationId.toString() )
                .stream()
                .map( userRepresentation -> createMemberProfileEntityFromUserRepresentation( userRepresentation, organizationId) )
                .filter( Objects::nonNull )
                .collect( Collectors.toList() );
        return MemberProfilesEntity.builder().users( memberEntities ).build();
    }

    public MemberProfilesEntity getMemberProfiles( KeycloakId organizationId ) {
        List<MemberProfileEntity> memberEntities = keycloakService.getOrganizationMembers( organizationId.toString() ).stream()
                .map( userRepresentation -> createMemberProfileEntityFromUserRepresentation( userRepresentation, organizationId) )
                .filter( Objects::nonNull )
                .collect( Collectors.toList() );
        return MemberProfilesEntity.builder().users( memberEntities ).build();
    }

    public MemberProfileEntity getMemberProfile( OrganizationMember organizationMember ) {
        // TODO: why does keycloakService.isGroupMember return false?
        if ( !keycloakService.isGroupMember( organizationMember ) )  {
            throw new NotFoundException( "User " + organizationMember.getUserId().toString() +
                                        " is not a member of organization  " + organizationMember.getOrganizationId().toString() );
        }
        return createMemberProfileEntityFromUserRepresentation(
                keycloakService.getUserById( organizationMember.getUserId() ).toRepresentation(), organizationMember.getOrganizationId()
        );
    }

    private MemberProfileEntity createMemberProfileEntityFromUserRepresentation( UserRepresentation userRepresentation, KeycloakId organizationId ) {
        UserAccountState accountState = getUserAccountState( userRepresentation );
        if ( accountState == UserAccountState.DISABLED ) {
            return null;
        }

        List<OrganizationUserRoleV2> organizationUserRoles = new ArrayList<>(
                keycloakService.getOrganizationUserRoles(
                    new KeycloakId(userRepresentation.getId()), new KeycloakId(organizationId.toString())
                )
        );

        String title = null;
        Map<String, List<String>> attributes = userRepresentation.getAttributes();
        if (attributes != null) {
            title = KeycloakUtils.getAttributeValue( attributes, "title", null);
        }

        return MemberProfileEntity.builder()
                .firstName( userRepresentation.getFirstName() )
                .lastName( userRepresentation.getLastName() )
                .title( title )
                .organizationUserRoles( organizationUserRoles )
                .userId( new KeycloakId( userRepresentation.getId() ) )
                .build();
    }

    public boolean canLeave( KeycloakId organizationId, KeycloakId userId ) {
        return userServiceV2.canLeave( organizationId, userId );
    }

    public void leave( KeycloakId organizationId, KeycloakId leavingUserId ) {
        OrganizationMember organizationMember = UserUtils.makeOrganizationMemberEntity( organizationId, leavingUserId );
        if ( !keycloakService.isGroupMember( organizationMember ) ) {
            throw new NotFoundException( "You are not a member of the organization");
        }
        keycloakService.removeUserFromOrganization( organizationMember );
    }

    public OrganizationUserRoleRequestEntity requestOrganizationUserRole( KeycloakId userId, KeycloakId organizationId, PostOrganizationUserRoleRequestEntity organizationUserRoleRequest, Jwt accessToken ) {
        return organizationUserRoleRequestService.requestOrganizationUserRole( userId, organizationId, organizationUserRoleRequest, accessToken );
    }

    public OrganizationUserRoleRequestEntity revokeOrganizationUserRoleRequest( KeycloakId userId, KeycloakId organizationId, int roleRequestId ) {
        return organizationUserRoleRequestService.revokeOrganizationUserRoleRequest( userId, organizationId, roleRequestId );
    }

    public OrganizationUserRoleRequestsEntity getOrganizationUserRoleRequests( KeycloakId userId, KeycloakId organizationId, int page, int pageSize ) {
        return organizationUserRoleRequestService.getOrganizationUserRoleRequests( userId, organizationId, page, pageSize );
    }

    public boolean isCreatorOfUnapprovedOrganization( OrganizationMember organizationMember ) {
        String organizationId = organizationMember.getOrganizationId().toString();
        String userId = organizationMember.getUserId().toString();
        boolean isCreatorOfUnapprovedOrganization = false;
        Optional<GroupRepresentation> groupRepresentation = keycloakService.getGroup( organizationId );
        if ( groupRepresentation.isPresent() ) {
            Map<String, List<String>> attributes = groupRepresentation.get().getAttributes();
            if (attributes != null) {
                List<Integer> unapprovedAccountStates = Arrays.asList(
                        OrganizationAccountState.AWAITING_ACTIVATION.getValue(),
                        OrganizationAccountState.REQUIRES_ACTIVATION.getValue()
                );
                isCreatorOfUnapprovedOrganization = organizationDetailsRepository
                        .findByKeycloakIdAndCreatorIdAndAccountStateIn(
                                organizationId, userId, unapprovedAccountStates
                        ).isPresent();
            }
        }
        return isCreatorOfUnapprovedOrganization;
    }
}

