package org.empaia.services.v2;

import org.empaia.models.dto.v2.CountryEntityV2;
import org.empaia.models.dto.v2.TextTranslationEntity;
import org.empaia.models.enums.v2.CountryCode;
import org.empaia.models.enums.v2.LanguageCode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.BadRequestException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service
@Transactional
public class DomainServiceV2 {

    public DomainServiceV2() {
    }

    public CountryEntityV2 getCountry( String iso3166Alpha2Code ) {
        CountryCode countryCode;
        try {
            countryCode = CountryCode.valueOf( iso3166Alpha2Code.toUpperCase(Locale.ROOT) );
        } catch ( IllegalArgumentException e ) {
            throw new BadRequestException(
                    String.format( "'%s' is not a valid ISO 3166 alpha 2 country code", iso3166Alpha2Code )
            );
        }
        return createCountryEntityV2( countryCode );
    }

    public List<CountryEntityV2> getCountries() {
        return Arrays.stream( CountryCode.values() )
                .map( this::createCountryEntityV2 )
                .collect( Collectors.toList() );
    }

    private CountryEntityV2 createCountryEntityV2( CountryCode countryCode ) {
        return CountryEntityV2.builder()
                .countryCode( countryCode )
                .translatedNames(
                        Arrays.asList(
                                createTranslatedCountryName( countryCode, LanguageCode.DE),
                                createTranslatedCountryName( countryCode, LanguageCode.EN)
                        )
                )
                .build();
    }

    private TextTranslationEntity createTranslatedCountryName( CountryCode countryCode, LanguageCode languageCode ) {
        Locale locale = new Locale( languageCode.name().toLowerCase(Locale.ROOT), countryCode.name() );
        return TextTranslationEntity.builder()
                        .text( locale.getDisplayCountry( locale ) )
                        .languageCode( languageCode )
                        .build();
    }
}

