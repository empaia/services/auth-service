package org.empaia.services.v2;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.weaver.ast.Or;
import org.empaia.exceptions.generic.NotFoundException;
import org.empaia.exceptions.keycloak.KeycloakInternalException;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.PostOrganizationDeploymentConfiguration;
import org.empaia.models.enums.v2.ClientServiceRole;
import org.empaia.models.enums.v2.ClientType;
import org.empaia.models.enums.v2.OrganizationCategory;
import org.empaia.models.enums.v2.Protocol;
import org.empaia.services.KeycloakService;
import org.empaia.utils.KeycloakUtils;
import org.keycloak.admin.client.resource.ClientResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.ClientScopeRepresentation;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class KeycloakClientService {
    public static final String CLIENT_GROUP_TYPE_ATTRIBUTE = "empaia_client_group_type";

    private final KeycloakService keycloakService;

    public KeycloakClientService( KeycloakService keycloakService ) {
        this.keycloakService = keycloakService;
    }

    public String createClient( KeycloakId organizationId, ClientType clientType ) {
        String organizationScopeId = keycloakService.getScopeId( "organization" )
                .orElseThrow( () -> new NotFoundException( "Unable to find organization scope" ) );

        ClientRepresentation clientRepresentation = keycloakService.createClient(
                createClientRepresentation( organizationId, clientType.getShortName(), ClientType.isOrganizationFrontendClient( clientType ) ), organizationId
        );

        // add organization scope to client
        keycloakService.addScopeToClient( clientRepresentation.getId(), organizationScopeId );
        createAndAssignClientGroupAndServiceRoles( clientRepresentation, organizationId, clientType.getShortName(), ClientType.isOrganizationFrontendClient( clientType ) );

        return clientRepresentation.getClientId();
    }

    public String updateOrCreateClient( KeycloakId organizationId, ClientType clientType ) {
        String clientId = KeycloakUtils.createClientName( organizationId, clientType.getShortName() );
        if ( !keycloakService.hasClient( clientId ) ) {
            return createClient( organizationId, clientType );
        }

        ClientRepresentation clientRepresentation = keycloakService.getClientByClientId( clientId );
        createAndAssignClientGroupAndServiceRoles( clientRepresentation, organizationId, clientType.getShortName(), ClientType.isOrganizationFrontendClient( clientType ) );

        if ( !clientRepresentation.isEnabled() ) {
            clientRepresentation.setEnabled( true );
            keycloakService.getClientsResource().get( clientRepresentation.getId() ).update( clientRepresentation );
        }

        return clientRepresentation.getClientId();
    }

    public void deleteClient( String clientId ) {
        ClientRepresentation clientRepresentation = keycloakService.getClientByClientId( clientId );
        keycloakService.deleteClient( clientRepresentation.getId() );
    }

    private void createAndAssignClientGroupAndServiceRoles( ClientRepresentation clientRepresentation, KeycloakId organizationId, String clientScopeSuffix, boolean isFrontendClient ) {
        String clientGroupName = KeycloakUtils.createClientScopeName( organizationId, clientScopeSuffix );
        createClientGroupIfNotExist( clientGroupName );
        if ( !isFrontendClient ) {
            setClientServiceRoles( clientRepresentation, organizationId );
        }
    }

    private void setClientServiceRoles( ClientRepresentation clientRepresentation, KeycloakId organizationId ) {
        ClientResource clientResource = keycloakService.getClientsResource().get( clientRepresentation.getId() );
        List<ClientServiceRole> roles = getClientServiceRoles( clientRepresentation );
        setClientServiceRoles( clientResource, organizationId, roles );
    }

    public List<ClientServiceRole> getClientServiceRoles( ClientRepresentation clientRepresentation ) {
        var roles = new ArrayList<ClientServiceRole>();
        if ( ClientType.isServiceAppReader( clientRepresentation.getName() ) ) {
            roles.add( ClientServiceRole.SERVICE_APP_READER );
        }
        if ( clientRepresentation.getName().equalsIgnoreCase( ClientType.APP_SERVICE.getShortName() ) ) {
            roles.add( ClientServiceRole.SERVICE_APP_CONFIG_READER );
        }
        if ( clientRepresentation.getName().equalsIgnoreCase( ClientType.COMPUTE_PROVIDER_JOB_EXECUTION_SERVICE.getShortName() ) ) {
            roles.add( ClientServiceRole.SERVICE_COMPUTE_PROVIDER );
        }
        return roles;
    }

    private void setClientServiceRoles( ClientResource clientResource, KeycloakId organizationId, List<ClientServiceRole> roles ) {
        ensureClientServiceRoleGroupsExist( organizationId );
        setServiceAccountUserRoles( clientResource, organizationId, roles );
    }

    private void ensureClientServiceRoleGroupsExist( KeycloakId organizationId ) {
        for ( ClientServiceRole role : ClientServiceRole.values() ) {
            GroupRepresentation subGroup = new GroupRepresentation();
            subGroup.setName( role.getName() );
            keycloakService.createSubGroup( organizationId.toString(), subGroup, true );
            setAllowedAudiencesForClientServiceRole( organizationId, role );
        }
    }

    private void setAllowedAudiencesForClientServiceRole( KeycloakId organizationId, ClientServiceRole role ) {
        var groupRepresentation = keycloakService.getOrganizationSubGroup( organizationId.toString(), role.getName() );
        Map<String, List<String>> attributes = groupRepresentation.getAttributes();
        if ( attributes == null ) {
            attributes = new HashMap<>();
        }
        KeycloakUtils.setAllowedAudience( attributes, role.getAllowedAudiences() );
        keycloakService.getGroupResource( groupRepresentation.getId() ).update( groupRepresentation );
    }

    private void setServiceAccountUserRoles(
            ClientResource clientResource,
            KeycloakId organizationId,
            List<ClientServiceRole> roles
    ) {
        UserRepresentation userRepresentation = clientResource.getServiceAccountUser();
        var roleNamesToSet = roles.stream().map( ClientServiceRole::getName ).collect( Collectors.toSet() );
        keycloakService.setOrganizationUserRoles(
                userRepresentation.getId(),
                organizationId.toString(),
                roleNamesToSet,
                false
        );
    }

    private void addClientToClientGroup( String clientKeycloakId, Set<String> clientGroups ) {
        Map<String, String> clientScopeIdsByName = keycloakService.getClientScopeIdsByClientScopeName();
        // create a set with already added client scopes to add only new client scopes to the client:
        Set<String> addedClientScopes = keycloakService.getClientScopes( clientKeycloakId ).stream()
                .map( ClientScopeRepresentation::getName )
                .collect( Collectors.toUnmodifiableSet() );
        clientGroups.stream()
                .filter( clientScope -> !addedClientScopes.contains( clientScope ) )
                .forEach(
                        clientScope -> keycloakService.addScopeToClient( clientKeycloakId, clientScopeIdsByName.get( clientScope )
                        )
                );
    }

    private ClientRepresentation createClientRepresentation( KeycloakId organizationId, String clientSuffix, boolean isFrontendClient ) {
        String clientId = KeycloakUtils.createClientName( organizationId, clientSuffix );

        HashMap<String, String> attributes = new HashMap<>();
        attributes.put( "display.on.consent.screen", "false" );
        attributes.put( "id.token.signed.response.alg", "RS256" );
        attributes.put( CLIENT_GROUP_TYPE_ATTRIBUTE, ClientType.fromShortName( clientSuffix ).toString() );

        ClientRepresentation clientRepresentation = new ClientRepresentation();
        clientRepresentation.setAttributes( attributes );
        clientRepresentation.setClientId( clientId );
        clientRepresentation.setDescription( String.format( "%s client for organization %s", clientSuffix.toUpperCase(), organizationId ) );
        clientRepresentation.setFullScopeAllowed( true );
        clientRepresentation.setName( clientSuffix );
        clientRepresentation.setProtocol( Protocol.OPENID_CONNECT );
        clientRepresentation.setProtocolMappers( null );

        if ( isFrontendClient ) {
            configureFrontendClient( clientRepresentation );
        } else {
            configureBackendClient( clientRepresentation );
        }

        clientRepresentation.setEnabled( true );
        return clientRepresentation;
    }

    private void configureBackendClient( ClientRepresentation r ) {
        r.setBearerOnly( false );
        r.setPublicClient( false );
        r.setFrontchannelLogout( false );
        r.setConsentRequired( false );
        r.setStandardFlowEnabled( false );
        r.setImplicitFlowEnabled( false );
        r.setDirectAccessGrantsEnabled( false );
        r.setServiceAccountsEnabled( true );
        r.setAuthorizationServicesEnabled( true );
    }

    private void configureFrontendClient( ClientRepresentation r ) {
        r.setBearerOnly( false );
        r.setPublicClient( true );
        r.setStandardFlowEnabled( true );
        r.setImplicitFlowEnabled( false );
        r.setDirectAccessGrantsEnabled( false );
        r.setServiceAccountsEnabled( false );
        r.setAuthorizationServicesEnabled( false );
    }

    public void deleteClientsAndClientsScopesForOrganizationCategory( KeycloakId organizationId, OrganizationCategory organizationCategory ) {
        if ( organizationCategory.equals( OrganizationCategory.APP_CUSTOMER ) ) {
            for ( ClientType type : ClientType.getCustomerClients() ) {
                deleteClientAndClientScope( organizationId, type );
            }
        }
        if ( organizationCategory.equals( OrganizationCategory.COMPUTE_PROVIDER ) ) {
            for ( ClientType type : ClientType.getComputeProviderClients() ) {
                deleteClientAndClientScope( organizationId, type );
            }
        }
    }

    private void deleteClientAndClientScope( KeycloakId organizationId, ClientType type ) {
        keycloakService.deleteGroupScope( KeycloakUtils.createClientScopeName( organizationId, type.getShortName() ) );
        keycloakService.deleteClient( KeycloakUtils.createClientName( organizationId, type.getShortName() ) );
    }

    public void setupDefaultOrganizationClients( KeycloakId organizationId, List<OrganizationCategory> organizationCategories ) {
        if ( organizationCategories.contains( OrganizationCategory.APP_CUSTOMER ) ) {
            setupAppCustomerClients( organizationId );
        }
        if ( organizationCategories.contains( OrganizationCategory.COMPUTE_PROVIDER ) ) {
            setupComputeProviderClients( organizationId );
        }
    }

    public void setupComputeProviderClients( KeycloakId organizationId ) {
        for ( ClientType clientType : ClientType.getComputeProviderClients() ) {
            updateOrCreateClientAndGrantAccess( organizationId, clientType );
        }
    }

    public void setupAppCustomerClients( KeycloakId organizationId ) {
        for ( ClientType clientType : ClientType.getCustomerClients() ) {
            updateOrCreateClientAndGrantAccess( organizationId, clientType );
        }
    }

    private void updateOrCreateClientAndGrantAccess( KeycloakId organizationId, ClientType clientType ) {
        String accessingClientId = updateOrCreateClient( organizationId, clientType );
        for ( String grantAccessTo : clientType.getGrantedAccess() ) {
            String targetClientScopeName = KeycloakUtils.createClientScopeName( organizationId, grantAccessTo );
            grantAccess( accessingClientId, targetClientScopeName );
        }
    }

    public Set<String> getMissingClientNamesForOrganization( KeycloakId organizationId, List<OrganizationCategory> categories ) {
        Set<String> clientNames = keycloakService.getAllClientsOfOrganization( organizationId )
                .stream()
                .map( ClientRepresentation::getClientId )
                .collect( Collectors.toSet() );
        Set<String> missingClientNames = new HashSet<>();
        for ( ClientType clientType : ClientType.getOrganizationClientsForCategories( categories ) ) {
            String clientName = KeycloakUtils.createClientName( organizationId, clientType.getShortName() );
            if ( !clientNames.contains( clientName ) ) {
                missingClientNames.add( clientName );
            }
        }
        return missingClientNames;
    }

    public Set<String> getLegacyAndInvalidClientsForOrganization( KeycloakId organizationId ) {
        Set<String> validClientNames = new HashSet<>();
        for ( ClientType clientType : ClientType.values() ) {
            validClientNames.add( KeycloakUtils.createClientName( organizationId, clientType.getShortName() ) );
        }
        Set<String> clientNames = keycloakService.getAllClientsOfOrganization( organizationId )
                .stream()
                .map( ClientRepresentation::getClientId )
                .collect( Collectors.toSet() );
        clientNames.removeAll( validClientNames );
        return clientNames;
    }

    public Set<ClientScopeRepresentation> getScopesOfClient( String clientId ) {
        String clientKeycloakId = keycloakService.getClientByClientId( clientId ).getId();
        return keycloakService.getClientScopes( clientKeycloakId );
    }

    public void grantAccess( String accessingClientId, String targetClientScopeName ) throws KeycloakInternalException {
        String accessingClientKeycloakId = keycloakService.getClientByClientId( accessingClientId ).getId();
        createClientGroupIfNotExist( targetClientScopeName );
        String targetScope = keycloakService.getClientScopeIdsByClientScopeName().get( targetClientScopeName );
        // TODO: what happens if already granted?
        keycloakService.addScopeToClient( accessingClientKeycloakId, targetScope );
    }

    public void revokeAccess( String accessingClientId, String targetClientScopeName ) {
        String accessingClientKeycloakId = keycloakService.getClientByClientId( accessingClientId ).getId();
        String targetScope = keycloakService.getClientScopeIdsByClientScopeName().get( targetClientScopeName );
        // TODO: what happens if scope not present?
        keycloakService.removeScopeFromClient( accessingClientKeycloakId, targetScope );
    }

    private void createClientGroupIfNotExist( String clientScopeName ) throws KeycloakInternalException {
        if ( !keycloakService.hasClientScope( clientScopeName ) ) {
            keycloakService.createGroupScope( clientScopeName );
        }
    }

    public void updateFrontendClient( ClientRepresentation clientRepresentation, PostOrganizationDeploymentConfiguration configuration ) {
        ClientResource clientResource = keycloakService.getClientsResource().get( clientRepresentation.getId() );
        if ( ClientType.fromShortName( clientRepresentation.getName() ).equals( ClientType.WORKBENCH_CLIENT ) ) {
            if ( !configuration.getWorkbenchRedirectUris().isEmpty() ) {
                clientRepresentation.setRedirectUris( configuration.getWorkbenchRedirectUris() );
            }
            if ( !configuration.getWorkbenchWebOrigins().isEmpty() ) {
                clientRepresentation.setWebOrigins( configuration.getWorkbenchWebOrigins() );
            }
        }
        if ( ClientType.fromShortName( clientRepresentation.getName() ).equals( ClientType.DATA_MANAGEMENT_CLIENT ) ) {
            if ( !configuration.getDataManagerRedirectUris().isEmpty() ) {
                clientRepresentation.setRedirectUris( configuration.getDataManagerRedirectUris() );
            }
            if ( !configuration.getDataManagerWebOrigins().isEmpty() ) {
                clientRepresentation.setWebOrigins( configuration.getDataManagerWebOrigins() );
            }
        }
        clientResource.update( clientRepresentation );
    }

    public void setEnabledFlagForOrganizationClients( KeycloakId organizationId, boolean enabled ) {
        Set<ClientRepresentation> clients = keycloakService.getAllClientsOfOrganization( organizationId );
        for ( ClientRepresentation clientRepresentation : clients ) {
            ClientResource clientResource = keycloakService.getClientsResource().get( clientRepresentation.getId() );
            clientRepresentation.setEnabled( enabled );
            clientResource.update( clientRepresentation );
        }
    }

    public boolean checkIfClientHasServiceAccountInOrganization( ClientRepresentation clientRepresentation, String organizationId ) {
        return keycloakService.isClientServiceAccountUserMemberOfOrganizationGroup(
                new KeycloakId( clientRepresentation.getId() ),
                new KeycloakId( organizationId )
        );
    }
}
