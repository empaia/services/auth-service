package org.empaia.services.v2;

import lombok.extern.slf4j.Slf4j;
import org.empaia.configs.TemplateConfiguration;
import org.empaia.models.enums.v2.Notification;
import org.empaia.models.enums.v2.TemplateType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Slf4j
@Service
public class TemplateService {

    private static final Map<Notification, String> templateFiles = new HashMap<>() {{
        put( Notification.USER_CREATES_ORGANIZATION, "user-creates-org" );
        put( Notification.ORGANIZATION_IS_APPROVED, "org-approved" );
        put( Notification.ORGANIZATION_IS_REJECTED, "org-rejected" );
        put( Notification.USER_REQUESTS_ORGANIZATION_MEMBERSHIP, "user-requests-org-membership" );
        put( Notification.ORGANIZATION_MEMBERSHIP_IS_APPROVED, "org-membership-approved" );
        put( Notification.ORGANIZATION_MEMBERSHIP_IS_REJECTED, "org-membership-rejected" );
        put( Notification.ORGANIZATION_MEMBER_REQUESTS_ROLE, "org-member-requests-role" );
        put( Notification.ORGANIZATION_MEMBER_ROLE_APPROVED, "org-member-role-approved" );
        put( Notification.ORGANIZATION_MEMBER_ROLE_REJECTED, "org-member-role-rejected" );
        put( Notification.ORGANIZATION_MANAGER_UPDATES_USER_ROLE, "org-manager-updates-role" );
        put( Notification.MODERATOR_DEACTIVATES_ACCOUNT, "moderator-deactivates-acc" );
        put( Notification.MANAGER_REQUESTS_ORGANIZATION_CATEGORY_CHANGE, "manager-requests-category-change" );
        put( Notification.ORGANIZATION_CATEGORY_CHANGE_APPROVED, "org-category-change-approved" );
        put( Notification.ORGANIZATION_CATEGORY_CHANGE_REJECTED, "org-category-change-rejected" );
    }};

    private final TemplateEngine templateEngine;

    @Autowired
    public TemplateService( TemplateConfiguration templateConfiguration ) {
        this.templateEngine = templateConfiguration.emailTemplateEngine();
    }

    public String getHTMLContent( Notification notification, Locale locale, Map<String, String> templateArguments ) {
        String filename = templateFiles.get( notification );
        return processTemplate( locale, templateArguments, filename, TemplateType.HTML );
    }

    public String getTextContent( Notification notification, Locale locale, Map<String, String> templateArguments ) {
        String filename = templateFiles.get( notification );
        return processTemplate( locale, templateArguments, filename, TemplateType.TXT );
    }

    private String processTemplate(
            Locale locale,
            Map<String, String> args,
            String templateFileName,
            TemplateType templateType
    ) {
        final Context ctx = new Context( locale );
        args.forEach( ctx::setVariable );

        String content = null;
        try {
            content = templateEngine.process(
                    String.format(
                            "/%s-%s.%s",
                            templateFileName,
                            locale.toString().toUpperCase(),
                            templateType.getType()
                    ),
                    ctx
            );
        } catch ( Exception ex ) {
            log.error( "Failed processing template" );
            log.error( ex.getMessage() );
        }
        return content;
    }
}
