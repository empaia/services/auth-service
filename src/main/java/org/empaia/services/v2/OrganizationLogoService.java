package org.empaia.services.v2;

import com.jlefebure.spring.boot.minio.MinioException;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.ResizedPictureUrlsEntity;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.representations.idm.GroupRepresentation;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

@Service
@Slf4j
@Transactional
public class OrganizationLogoService extends ImageFileService {
    private static final String ORIGINAL_LOGO_NAME = "organizations/%s/logo/original.jpeg";
    private static final String RESIZED_LOGO_NAME = "organizations/%s/logo/%d.jpeg";


    public OrganizationLogoService( FileServiceV2 fileService ) {
        super( fileService );
    }

    public String getLogoUrl( KeycloakId organizationId ) {
        return getImageFileUrl( organizationId, ORIGINAL_LOGO_NAME, -1 );
    }

    public ResizedPictureUrlsEntity getResizedLogoUrls( KeycloakId organizationId ) {
        return getResizedImageFileUrls( organizationId, RESIZED_LOGO_NAME );
    }

    public ResponseEntity<InputStreamResource> getLogo( KeycloakId organizationId ) throws FileNotFoundException {
        return getImageFile( organizationId, ORIGINAL_LOGO_NAME );
    }

    public ResponseEntity<InputStreamResource> getLogo( KeycloakId organizationId, int width ) throws FileNotFoundException {
        return getImageFile( organizationId, RESIZED_LOGO_NAME, width );
    }

    public void setLogo( GroupRepresentation groupRepresentation, MultipartFile logoFile ) throws Exception {
        KeycloakId organizationId = new KeycloakId( groupRepresentation.getId() );
        setImageFile( organizationId, ORIGINAL_LOGO_NAME, RESIZED_LOGO_NAME, logoFile );
    }
}
