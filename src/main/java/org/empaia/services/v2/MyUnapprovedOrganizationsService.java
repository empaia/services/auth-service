package org.empaia.services.v2;

import lombok.extern.slf4j.Slf4j;
import org.empaia.exceptions.generic.NotFoundException;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.db.v2.DBOrganizationDetails;
import org.empaia.models.dto.v2.*;
import org.empaia.models.enums.v2.OrganizationAccountState;
import org.empaia.repositories.v2.OrganizationDetailsRepository;
import org.empaia.services.KeycloakService;
import org.keycloak.representations.idm.GroupRepresentation;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import jakarta.validation.*;
import javax.ws.rs.BadRequestException;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
@Transactional
public class MyUnapprovedOrganizationsService {

    private final ConfidentialOrganizationService confidentialOrganizationService;
    private final KeycloakService keycloakService;
    private final ShoutOutService shoutOutService;
    private final OrganizationDetailsRepository organizationDetailsRepository;
    private final OrganizationLogoService organizationLogoService;
    private final ModelMapper modelMapper;

    public MyUnapprovedOrganizationsService(
            ConfidentialOrganizationService confidentialOrganizationService,
            KeycloakService keycloakService,
            ShoutOutService shoutOutService, OrganizationDetailsRepository organizationDetailsRepository,
            OrganizationLogoService organizationLogoService,
            ModelMapper modelMapper
    ) {
        this.confidentialOrganizationService = confidentialOrganizationService;
        this.keycloakService = keycloakService;
        this.shoutOutService = shoutOutService;
        this.organizationDetailsRepository = organizationDetailsRepository;
        this.organizationLogoService = organizationLogoService;
        this.modelMapper = modelMapper;
    }

    public UnapprovedConfidentialOrganizationProfilesEntity getOrganizations(
            int page, int pageSize, List<OrganizationAccountState> organizationAccountStates, KeycloakId creatorId
    ) {
        ConfidentialOrganizationProfilesEntity organizationProfilesEntity = confidentialOrganizationService.getConfidentialOrganizationProfilesEntityByCreator(
                page, pageSize, organizationAccountStates, creatorId
        );
        return modelMapper.map(organizationProfilesEntity, UnapprovedConfidentialOrganizationProfilesEntity.class);
    }

    public UnapprovedConfidentialOrganizationProfileEntity getUnapprovedOrganization( KeycloakId unapprovedOrgId ) {
        return modelMapper.map(
                confidentialOrganizationService.getUnapprovedConfidentialOrganizationProfileEntity( unapprovedOrgId ),
                UnapprovedConfidentialOrganizationProfileEntity.class
        );
    }

    public UnapprovedConfidentialOrganizationProfileEntity createUnapprovedOrganization( PostOrganizationEntity organization, KeycloakId creatorId ) {
        return modelMapper.map(
                confidentialOrganizationService.createNewOrganization( organization, creatorId ),
                UnapprovedConfidentialOrganizationProfileEntity.class
        );
    }

    public UnapprovedConfidentialOrganizationProfileEntity updateUnapprovedOrganizationContactData( KeycloakId organizationId, ConfidentialOrganizationContactDataEntity contactData ) {
        verifyUnapprovedOrganizationCanBeModified( organizationId );
        return modelMapper.map(
                confidentialOrganizationService.updateContactData( organizationId, contactData ),
                UnapprovedConfidentialOrganizationProfileEntity.class
        );
    }

    public UnapprovedConfidentialOrganizationProfileEntity updateUnapprovedOrganizationDetails( KeycloakId organizationId, PostInitialConfidentialOrganizationDetailsEntity detailsEntity ) {
        verifyUnapprovedOrganizationCanBeModified( organizationId );
        return modelMapper.map(
                confidentialOrganizationService.updateDetails( organizationId, detailsEntity ),
                UnapprovedConfidentialOrganizationProfileEntity.class
        );
    }
    
    public UnapprovedConfidentialOrganizationProfileEntity updateUnapprovedOrganizationName( KeycloakId organizationId, PostOrganizationNameEntity organizationNameEntity ) {
        verifyUnapprovedOrganizationCanBeModified( organizationId );
        return modelMapper.map(
                confidentialOrganizationService.updateName( organizationId, organizationNameEntity.getName() ),
                UnapprovedConfidentialOrganizationProfileEntity.class
        );
    }

    public void setUnapprovedOrganizationLogo( KeycloakId organizationId, MultipartFile logo ) throws Exception {
        verifyUnapprovedOrganizationCanBeModified( organizationId );
        GroupRepresentation groupRepresentation = getOrganizationGroupRepresentation( organizationId );
        organizationLogoService.setLogo( groupRepresentation, logo );
    }

    public boolean canRequestApproval( KeycloakId organizationId ) {
        boolean isOrganizationDataValid = true;
        try {
            validateConfidentialOrganizationProfileData( organizationId );
        } catch ( ConstraintViolationException e ) {
            isOrganizationDataValid = false;
        }

        boolean canRequestApproval = false;
        if ( isOrganizationDataValid ) {
            DBOrganizationDetails organizationDetails = confidentialOrganizationService.getDBOrganizationDetails(organizationId);
            OrganizationAccountState accountState = organizationDetails.getOrganizationAccountState();
            if (accountState == OrganizationAccountState.REQUIRES_ACTIVATION) {
                canRequestApproval = true;
            }
        }
        return canRequestApproval;
    }

    public UnapprovedConfidentialOrganizationProfileEntity requestUnapprovedOrganizationApproval( KeycloakId organizationId ) {
        validateConfidentialOrganizationProfileData( organizationId );
        setOrganizationApprovalRequested( organizationId, true );
        UnapprovedConfidentialOrganizationProfileEntity unapprovedOrgProfile = getUnapprovedOrganization( organizationId );
        shoutOutService.notifyModeratorsNewOrganizationCreated( organizationId );
        return unapprovedOrgProfile;
    }

    private void validateConfidentialOrganizationProfileData( KeycloakId organizationId ) {
        ConfidentialOrganizationProfileEntity confidentialOrganizationProfileEntity =
            confidentialOrganizationService.getConfidentialOrganizationProfileEntity( organizationId );
        try (ValidatorFactory factory = Validation.buildDefaultValidatorFactory()) {
            Validator validator = factory.getValidator();
            validateEntity(validator, confidentialOrganizationProfileEntity);
            validateEntity(validator, confidentialOrganizationProfileEntity.getContactData());
            validateEntity(validator, confidentialOrganizationProfileEntity.getDetails());
        }
    }

    private <E> void validateEntity(Validator validator, E entity) {
        Set<ConstraintViolation<E>> violations = validator.validate( entity );
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }

    public UnapprovedConfidentialOrganizationProfileEntity revokeUnapprovedOrganizationApprovalRequest( KeycloakId organizationId ) {
        setOrganizationApprovalRequested( organizationId, false );
        return getUnapprovedOrganization( organizationId );
    }

    private void setOrganizationApprovalRequested( KeycloakId organizationId, boolean isApprovalRequested ) throws BadRequestException, NotFoundException {
        DBOrganizationDetails organizationDetails = confidentialOrganizationService.getDBOrganizationDetails( organizationId );
        organizationDetails.verifyOrganizationIsNotApproved();
        OrganizationAccountState accountState = organizationDetails.getOrganizationAccountState();
        if ( isApprovalRequested ) {
            if ( accountState == OrganizationAccountState.REQUIRES_ACTIVATION ) {
                organizationDetails.setOrganizationAccountState( OrganizationAccountState.AWAITING_ACTIVATION );
            } else {
                throw new BadRequestException( "Invalid organization account state " + accountState.name() );
            }
        } else {
            if ( accountState == OrganizationAccountState.AWAITING_ACTIVATION ) {
                organizationDetails.setOrganizationAccountState( OrganizationAccountState.REQUIRES_ACTIVATION );
            } else {
                throw new BadRequestException( "Invalid organization account state " + accountState.name() );
            }
        }
        organizationDetailsRepository.save( organizationDetails );
    }

    public void deleteUnapprovedOrganization( KeycloakId organizationId ) throws BadRequestException, NotFoundException {
        DBOrganizationDetails organizationDetails = confidentialOrganizationService.getDBOrganizationDetails( organizationId );
        verifyUnapprovedOrganizationCanBeModified( organizationDetails );
        keycloakService.deleteGroup( organizationId.toString() );
        organizationDetailsRepository.delete( organizationDetails );
    }

    private GroupRepresentation getOrganizationGroupRepresentation( KeycloakId organizationId ) throws NotFoundException {
        return keycloakService.getGroup( organizationId.toString() ).orElseThrow(
            () -> new NotFoundException( "Organization not found" )
        );
    }

    private boolean isUnapprovedOrganizationLocked( KeycloakId organizationId ) {
        DBOrganizationDetails organizationDetails = confidentialOrganizationService.getDBOrganizationDetails( organizationId );
        return isUnapprovedOrganizationLocked( organizationDetails );
    }

    private boolean isUnapprovedOrganizationLocked( DBOrganizationDetails organizationDetails ) {
        OrganizationAccountState accountState = organizationDetails.getOrganizationAccountState();
        return accountState != OrganizationAccountState.REQUIRES_ACTIVATION;
    }

    private void verifyUnapprovedOrganizationCanBeModified( KeycloakId organizationId ) throws BadRequestException {
        if ( isUnapprovedOrganizationLocked( organizationId ) ) {
            throw new BadRequestException( "Organization cannot be modified" );
        }
    }

    private void verifyUnapprovedOrganizationCanBeModified( DBOrganizationDetails organizationDetails ) throws BadRequestException {
        if ( isUnapprovedOrganizationLocked( organizationDetails ) ) {
            throw new BadRequestException( "Organization cannot be modified" );
        }
    }
}
