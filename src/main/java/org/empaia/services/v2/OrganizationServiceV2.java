package org.empaia.services.v2;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.empaia.exceptions.generic.NotFoundException;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.db.v2.DBOrganizationDetails;
import org.empaia.models.dto.v2.BuildEntitiesFunction;
import org.empaia.models.dto.v2.BuildEntityFunction;
import org.empaia.models.enums.v2.OrganizationAccountState;
import org.empaia.repositories.v2.OrganizationDetailsRepository;
import org.empaia.services.KeycloakService;
import org.keycloak.admin.client.resource.GroupResource;
import org.keycloak.representations.idm.GroupRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;


@Slf4j
@Service
@Transactional
public class OrganizationServiceV2 {

    @FunctionalInterface
    public interface ThrowingVisitor<T, E extends Throwable> {
        void visit(T t) throws E;
    }

    @FunctionalInterface
    public interface ThrowingVisitor2<T, U, E extends Throwable> {
        void visit(T t, U u) throws E;
    }

    @FunctionalInterface
    public interface ThrowingFunction<T, R, E extends Throwable> {
        R apply(T t) throws E;
    }

    @FunctionalInterface
    public interface ThrowingFunction2<T, U, R, E extends Throwable> {
        R apply(T t, U u) throws E;
    }

    private final KeycloakService keycloakService;
    private final OrganizationDetailsRepository organizationDetailsRepository;

    @Autowired
    public OrganizationServiceV2(
            KeycloakService keycloakService,
            OrganizationDetailsRepository organizationDetailsRepository
    ) {
        this.keycloakService = keycloakService;
        this.organizationDetailsRepository = organizationDetailsRepository;
    }

    public <E> E getOrganizationEntity(
            GroupRepresentation groupRepresentation, BuildEntityFunction<E> buildEntityFunction
    ) {
        return createOrganizationEntityFromGroupRepresentation( groupRepresentation, buildEntityFunction );
    }

    private <E> E createOrganizationEntityFromGroupRepresentation(
            GroupRepresentation groupRepresentation, BuildEntityFunction<E> buildEntityFunction
    ) {
        String name = null;
        KeycloakId organizationId = new KeycloakId( groupRepresentation.getId() );
        Map<String,List<String>> attributes = groupRepresentation.getAttributes();
        if ( attributes == null ) {
            log.error("Organization is missing keycloak attributes: " + groupRepresentation.getId());
        } else {
            final List<String> defaultList = Collections.singletonList(null);
            name = attributes.getOrDefault("name", defaultList).get(0);
        }
        return buildEntityFunction.apply( organizationId, name, attributes );
    }

    public <ES, E> ES getActiveOrganizationsEntity( int page, int pageSize,
                                                    BuildEntityFunction<E> buildEntityFunction,
                                                    BuildEntitiesFunction<ES, E> buildEntitiesFunction ) {
        return getOrganizationsEntity( page, pageSize, OrganizationAccountState.ACTIVE,
                buildEntityFunction, buildEntitiesFunction );
    }

    public <ES, E> ES getOrganizationsEntity(
            int page, int pageSize, List<OrganizationAccountState> accountStates, List<KeycloakId> organizationIds,
            BuildEntityFunction<E> buildEntityFunction,
            BuildEntitiesFunction<ES, E> buildEntitiesFunction
    ) {
        List<Integer> accountStateValues = accountStates.stream().map(OrganizationAccountState::getValue).collect(Collectors.toList());
        Page<DBOrganizationDetails> detailsPage;
        if ( organizationIds == null ) {
            detailsPage = organizationDetailsRepository.findAllByAccountStateIn(accountStateValues, PageRequest.of(page, pageSize));
        } else {
            detailsPage = organizationDetailsRepository.findAllByKeycloakIdInAndAccountStateIn(
                    organizationIds.stream().map(KeycloakId::toString).collect(Collectors.toList()),
                    accountStateValues, PageRequest.of(page, pageSize)
            );
        }
        return getOrganizationsEntity( detailsPage, buildEntityFunction, buildEntitiesFunction );
    }

    public <ES, E> ES getOrganizationsEntity(
            int page, int pageSize, List<OrganizationAccountState> accountStates,
            BuildEntityFunction<E> buildEntityFunction,
            BuildEntitiesFunction<ES, E> buildEntitiesFunction
    ) {
        return getOrganizationsEntity( page, pageSize, accountStates, null, buildEntityFunction, buildEntitiesFunction );
    }

    public <ES, E> ES getOrganizationsEntity(
            int page, int pageSize, OrganizationAccountState accountState,
            BuildEntityFunction<E> buildEntityFunction,
            BuildEntitiesFunction<ES, E> buildEntitiesFunction ) {
        Page<DBOrganizationDetails> detailsPage = organizationDetailsRepository
                .findAllByAccountState( accountState.getValue(), PageRequest.of( page, pageSize ) );
        return getOrganizationsEntity( detailsPage, buildEntityFunction, buildEntitiesFunction );
    }

    private <ES, E> ES getOrganizationsEntity(
            Page<DBOrganizationDetails> detailsPage,
            BuildEntityFunction<E> buildEntityFunction,
            BuildEntitiesFunction<ES, E> buildEntitiesFunction) {
        var organizations = detailsPage.stream()
                .map( organizationDetails -> keycloakService.getGroup(organizationDetails.getKeycloakId()) )
                .filter( Optional::isPresent )
                .map( Optional::get )
                .map( groupRepresentation -> getOrganizationEntity( groupRepresentation, buildEntityFunction ) )
                .collect( Collectors.toList() );
        return buildEntitiesFunction.apply( organizations, Long.valueOf( detailsPage.getTotalElements() ).intValue() );
    }


    public <ES, E> ES getOrganizationProfilesEntity(
            int page, int pageSize, List<OrganizationAccountState> accountStates,
            BiFunction<GroupRepresentation, DBOrganizationDetails, E> createOrganizationProfileEntityFunction,
            BiFunction<List<E>, Integer, ES> createOrganizationProfilesEntityFunction
    ) {
       return getOrganizationProfilesEntity(
               page, pageSize, accountStates, null,
               createOrganizationProfileEntityFunction, createOrganizationProfilesEntityFunction
       );
    }

    public <ES, E> ES getOrganizationProfilesEntityByCreator(
            int page, int pageSize, List<OrganizationAccountState> accountStates, KeycloakId creatorId,
            BiFunction<GroupRepresentation, DBOrganizationDetails, E> createOrganizationProfileEntityFunction,
            BiFunction<List<E>, Integer, ES> createOrganizationProfilesEntityFunction
    ) {
        if ( creatorId == null ) {
            throw new IllegalArgumentException("Creator ID must not be null");
        }
        List<Integer> accountStateValues = accountStates.stream().map( OrganizationAccountState::getValue ).collect( Collectors.toList() );
        Page<DBOrganizationDetails> detailsPage = organizationDetailsRepository.findAllByCreatorIdAndAccountStateIn(
                creatorId.toString(), accountStateValues, PageRequest.of( page, pageSize )
        );
        return getOrganizationProfilesEntity(
                detailsPage, createOrganizationProfileEntityFunction, createOrganizationProfilesEntityFunction
        );
    }

    public <ES, E> ES getOrganizationProfilesEntity(
            int page, int pageSize, List<OrganizationAccountState> accountStates, List<KeycloakId> organizationIds,
            BiFunction<GroupRepresentation, DBOrganizationDetails, E> createOrganizationProfileEntityFunction,
            BiFunction<List<E>, Integer, ES> createOrganizationProfilesEntityFunction
    ) {
        List<Integer> accountStateValues = accountStates.stream().map(OrganizationAccountState::getValue).collect(Collectors.toList());
        Page<DBOrganizationDetails> detailsPage;
        if ( organizationIds != null ) {
            detailsPage = organizationDetailsRepository.findAllByKeycloakIdInAndAccountStateIn(
                    organizationIds.stream().map( KeycloakId::toString).collect(Collectors.toList()),
                    accountStateValues, PageRequest.of(page, pageSize)
            );
        } else {
            detailsPage = organizationDetailsRepository.findAllByAccountStateIn(
                    accountStateValues, PageRequest.of(page, pageSize)
            );
        }
        return getOrganizationProfilesEntity(
                detailsPage, createOrganizationProfileEntityFunction, createOrganizationProfilesEntityFunction
        );
    }

    private <ES, E> ES getOrganizationProfilesEntity(
            Page<DBOrganizationDetails> detailsPage,
            BiFunction<GroupRepresentation, DBOrganizationDetails, E> createOrganizationProfileEntityFunction,
            BiFunction<List<E>, Integer, ES> createOrganizationProfilesEntityFunction
    ) {
        var organizations = detailsPage.stream()
                .map( organizationDetails -> {
                    var groupRepresentation = keycloakService.getGroup(organizationDetails.getKeycloakId());
                    return new ImmutablePair<>(organizationDetails, groupRepresentation);
                } )
                .filter( pair -> (((Optional<GroupRepresentation>) pair.right).isPresent()) )
                .map( pair -> createOrganizationProfileEntityFunction.apply( pair.right.get(), pair.left ) )
                .collect( Collectors.toList() );
        return createOrganizationProfilesEntityFunction.apply( organizations, Long.valueOf(detailsPage.getTotalElements()).intValue() );
    }

    public <T> T getOrganizationProfileEntity(
            KeycloakId organizationId,
            BiFunction<GroupRepresentation, DBOrganizationDetails, T> createOrganizationProfileEntityFunction) {
        DBOrganizationDetails organizationDetails = organizationDetailsRepository.findByKeycloakId(
                organizationId.toString()
        ).orElseThrow( () -> new NotFoundException( "Organization not found" ) );
        GroupResource groupResource = keycloakService.getOptionalGroupResource( organizationId ).orElseThrow(
                () -> new NotFoundException( "Organization group not found ")
        );
        return createOrganizationProfileEntityFunction.apply( groupResource.toRepresentation(), organizationDetails );
    }

    public <T> T getOrganizationProfileEntityForMatchingAccountState(
            KeycloakId organizationId,
            OrganizationAccountState accountState,
            BiFunction<GroupRepresentation, DBOrganizationDetails, T> createOrganizationProfileEntityFunction) {
        return getOrganizationProfileEntityForMatchingAccountStateIn(
                organizationId, new OrganizationAccountState[]{accountState}, createOrganizationProfileEntityFunction);
    }

    public <T> T getOrganizationProfileEntityForMatchingAccountStateIn(
            KeycloakId organizationId,
            OrganizationAccountState[] accountStates,
            BiFunction<GroupRepresentation, DBOrganizationDetails, T> createOrganizationProfileEntityFunction) {
        List<Integer> accountStateValues = Arrays.stream(accountStates)
                .map( OrganizationAccountState::getValue )
                .collect( Collectors.toList() );
        DBOrganizationDetails organizationDetails = organizationDetailsRepository.findByKeycloakIdAndAccountStateIn(
                organizationId.toString(), accountStateValues
        ).orElseThrow( () -> new NotFoundException( "Organization not found" ) );
        GroupResource groupResource = keycloakService.getOptionalGroupResource( organizationId ).orElseThrow(
                () -> new NotFoundException( "Organization group not found ")
        );
        return createOrganizationProfileEntityFunction.apply( groupResource.toRepresentation(), organizationDetails );
    }

    public <E extends Throwable> void visitOrganizationGroupResource(
            KeycloakId organizationId, ThrowingVisitor2<GroupResource, GroupRepresentation, E> visitGroupResource
    ) throws E {
        visitOrganizationGroupResource( organizationId, ( groupResource, groupRepresentation ) -> {
            visitGroupResource.visit( groupResource, groupRepresentation );
            return null;
        });
    }

    public <E extends Throwable> void updateOrganizationGroupRepresentation(
            KeycloakId organizationId, ThrowingVisitor<GroupRepresentation, E> updateGroupRepresentation
    ) throws E {
        updateOrganizationGroupRepresentation( organizationId, groupRepresentation -> {
            updateGroupRepresentation.visit( groupRepresentation );
            return null;
        });
    }

    public <E extends Throwable> void visitOrganizationGroupRepresentation(
            KeycloakId organizationId, ThrowingVisitor<GroupRepresentation, E> visitGroupRepresentation
    ) throws E {
        visitOrganizationGroupRepresentation( organizationId, groupRepresentation -> {
            visitGroupRepresentation.visit( groupRepresentation );
            return null;
        });
    }

    public <E extends Throwable> void visitOrganizationAttributes(
            KeycloakId organizationId, ThrowingVisitor<Map<String,List<String>>, E> visitAttributesFunction
    ) throws E {
        visitOrganizationAttributes( organizationId, attributes -> {
            visitAttributesFunction.visit( attributes );
            return null;
        });
    }

    public <E extends Throwable> void updateOrganizationAttributes(
            KeycloakId organizationId, ThrowingVisitor<Map<String,List<String>>, E> updateAttributesFunction
    ) throws E {
        updateOrganizationAttributes( organizationId, attributes -> {
            updateAttributesFunction.visit( attributes );
            return null;
        });
    }

    public <E extends Throwable, R> R visitOrganizationGroupResource(
            KeycloakId organizationId, ThrowingFunction2<GroupResource, GroupRepresentation, R, E> visitGroupResource
    ) throws E {
        GroupResource groupResource;
        GroupRepresentation groupRepresentation;
        try {
            groupResource = keycloakService.getGroupResource(organizationId.toString());
            groupRepresentation = groupResource.toRepresentation();
        } catch (javax.ws.rs.NotFoundException e) {
            log.error("Organization " + organizationId + " not found");
            throw new NotFoundException("Organization not found");
        }
        return visitGroupResource.apply(groupResource, groupRepresentation);
    }

    public <E extends Throwable, R> R updateOrganizationGroupRepresentation(
            KeycloakId organizationId, ThrowingFunction<GroupRepresentation, R, E> updateGroupRepresentation
    ) throws E {
        return visitOrganizationGroupResource( organizationId, (groupResource, groupRepresentation) -> {
            R result = updateGroupRepresentation.apply( groupRepresentation );
            groupResource.update( groupRepresentation );
            return result;
        });
    }

    public <E extends Throwable, R> R visitOrganizationGroupRepresentation(
            KeycloakId organizationId, ThrowingFunction<GroupRepresentation, R, E> visitGroupRepresentation
    ) throws E {
        return visitOrganizationGroupResource( organizationId, (groupResource, groupRepresentation) -> {
            return visitGroupRepresentation.apply( groupRepresentation );
        });
    }

    public <E extends Throwable, R> R visitOrganizationAttributes(
            KeycloakId organizationId, ThrowingFunction<Map<String,List<String>>, R, E> visitAttributesFunction
    ) throws E {
        return visitOrganizationGroupRepresentation( organizationId, groupRepresentation -> {
            Map<String, List<String> > attributes = groupRepresentation.getAttributes();
            return visitAttributesFunction.apply( attributes );
        });
    }

    public <E extends Throwable, R> R updateOrganizationAttributes(
            KeycloakId organizationId, ThrowingFunction<Map<String,List<String>>, R, E> updateAttributesFunction
    ) throws E {
        return updateOrganizationGroupRepresentation( organizationId, groupRepresentation -> {
            Map<String, List<String> > attributes = groupRepresentation.getAttributes();
            R result = updateAttributesFunction.apply( attributes );
            // TODO: if attributes is a reference, then setting it should not be necessary and this can be removed.
            //       visitOrganizationAttributes() and updateOrganizationAttributes() can be the same function then
            groupRepresentation.setAttributes( attributes );
            return result;
        });
    }
}

