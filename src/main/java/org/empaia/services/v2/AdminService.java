package org.empaia.services.v2;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.empaia.configs.EmpaiaConfiguration;
import org.empaia.exceptions.generic.NotFoundException;
import org.empaia.exceptions.keycloak.KeycloakClientIdException;
import org.empaia.exceptions.keycloak.KeycloakInternalException;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.*;
import org.empaia.models.enums.v2.*;
import org.empaia.services.KeycloakService;
import org.empaia.utils.KeycloakUtils;
import org.keycloak.admin.client.resource.ClientResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.ClientScopeRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.BadRequestException;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AdminService {

    public static final String CLIENT_GROUP_TYPE_ATTRIBUTE = "empaia_client_group_type";

    private final EmpaiaConfiguration empaiaConfiguration;
    private final ConfidentialOrganizationService confidentialOrganizationService;
    private final OrganizationDeploymentConfigurationService organizationDeploymentConfigurationService;
    private final KeycloakService keycloakService;
    private final KeycloakClientService keycloakClientService;
    private final MemberService memberService;
    private final UserServiceV2 userService;

    public AdminService(
            EmpaiaConfiguration empaiaConfiguration,
            ConfidentialOrganizationService confidentialOrganizationService,
            OrganizationDeploymentConfigurationService organizationDeploymentConfigurationService,
            KeycloakService keycloakService,
            KeycloakClientService keycloakClientService,
            MemberService memberService,
            UserServiceV2 userService
    ) {
        this.empaiaConfiguration = empaiaConfiguration;
        this.confidentialOrganizationService = confidentialOrganizationService;
        this.organizationDeploymentConfigurationService = organizationDeploymentConfigurationService;
        this.keycloakService = keycloakService;
        this.keycloakClientService = keycloakClientService;
        this.memberService = memberService;
        this.userService = userService;
    }

    public String generateEmailVerificationToken( String emailAddress, int tokenLifetimeMS ) {
        UserRepresentation userRepresentation = keycloakService.getUserByEmail( emailAddress ).orElseThrow(
                () -> new NotFoundException( "No user with this email address found" )
        );
        UserAccountState accountState = KeycloakUtils.getUserAccountState( userRepresentation );
        if ( accountState != UserAccountState.REQUIRES_EMAIL_VERIFICATION ) {
            throw new BadRequestException( "User with the given email address does not require email verification" );
        }
        String userId = userRepresentation.getId();
        Date now = new Date( System.currentTimeMillis() );
        Date expiresOn = new Date( System.currentTimeMillis() + tokenLifetimeMS );
        var algorithm = SignatureAlgorithm.HS256;
        return Jwts.builder()
                .setHeaderParam( "alg", algorithm.toString() )
                .setHeaderParam( "typ", "JWT" )
                .setIssuer( empaiaConfiguration.getServerUrl() )
                .setAudience( empaiaConfiguration.getServerUrl() )
                .setExpiration( expiresOn )
                .setIssuedAt( now )
                .setSubject( userId )
                .setNotBefore( now )
                .setId( null )
                .claim( "nonce", null )
                .claim( "typ", "VERIFY_EMAIL" )
                .claim( "asid", null )
                .signWith( algorithm, empaiaConfiguration.getTokenSecret() )
                .compact();
    }


    public void deleteUser( KeycloakId userId, KeycloakId deletedByUserId ) {
        userService.deleteUser( userId, deletedByUserId, false, false );
    }

    public void grantAdminPrivileges( KeycloakId userId ) {
        keycloakService.grantAdminPrivileges( userId );
    }

    public void revokeAdminPrivileges( KeycloakId userId ) {
        keycloakService.revokeAdminPrivileges( userId );
    }

    public void deleteOrganization( KeycloakId organizationId ) {
        confidentialOrganizationService.deleteOrganization( organizationId );
    }

    @Transactional
    public void deleteMembersOnlyInThisOrganization( KeycloakId organizationId, KeycloakId deletedByUserId ) {
        MemberProfilesEntity members = memberService.getMembersOnlyInThisOrganization( organizationId );
        members.getUsers().forEach(
                member -> userService.deleteUser( member.getUserId(), deletedByUserId, false, false )
        );
    }

    public ConfidentialOrganizationProfilesEntity getOrganizationByAccountState( List<String> accountStates, int page, int pageSize ) {
        List<OrganizationAccountState> l = accountStates.stream().map( OrganizationAccountState::fromName ).collect( Collectors.toList() );
        return confidentialOrganizationService.getConfidentialOrganizationProfilesEntity( page, pageSize, l );
    }

    public OrganizationDeploymentConfiguration setOrganizationDeploymentConfiguration(
            KeycloakId organizationId, PostOrganizationDeploymentConfiguration organizationDeploymentConfiguration, KeycloakId userId
    ) {
        confidentialOrganizationService.getConfidentialOrganizationProfileEntity( organizationId );
        return organizationDeploymentConfigurationService.setOrganizationDeploymentConfiguration( organizationId, organizationDeploymentConfiguration, userId );
    }

    public OrganizationDeploymentConfiguration getOrganizationDeploymentConfiguration( KeycloakId organizationId ) {
        confidentialOrganizationService.getConfidentialOrganizationProfileEntity( organizationId );
        return organizationDeploymentConfigurationService.getOrganizationDeploymentConfiguration( organizationId );
    }

    public void removeComputerProviderJesForOrganization( KeycloakId organizationId ) {
        ConfidentialOrganizationProfileEntity organizationProfile = confidentialOrganizationService.getConfidentialOrganizationProfileEntity( organizationId );
        if ( !organizationProfile.getDetails().getCategories().contains( OrganizationCategory.APP_CUSTOMER ) ) {
            throw new BadRequestException( "Only organizations of category APP_CUSTOMER can use their own JES" );
        }
        String jesClientScope = KeycloakUtils.createClientScopeName( organizationProfile.getOrganizationId(), ClientType.JOB_EXECUTION_SERVICE.getShortName() );
        if ( !keycloakService.hasClientScope( jesClientScope ) ) {
            throw new NotFoundException( String.format( "No JES client scope for organization %s found", organizationProfile.getOrganizationId() ) );
        }
        // make sure JES client is enabled
        setOrganizationClientEnableFlag( organizationProfile.getOrganizationId(), ClientType.JOB_EXECUTION_SERVICE, true );
        // get wbs client and remove all external jes scopes
        ClientRepresentation wbsClient = keycloakService.getClientByClientId(
                KeycloakUtils.createClientName( organizationProfile.getOrganizationId(), ClientType.WORKBENCH_SERVICE.getShortName() )
        );
        removeComputeProviderJesClientScopeFromClient( organizationProfile.getOrganizationId(), wbsClient );
        // set own jes scope for wbs client if not yet done
        String jesScopeId = keycloakService.getClientScopeIdsByClientScopeName().get( jesClientScope );
        if ( !keycloakService.getClientByClientId( wbsClient.getClientId() ).getDefaultClientScopes().contains( jesScopeId ) ) {
            keycloakService.addScopeToClient( wbsClient.getId(), jesScopeId );
        }
    }

    public void setComputeProviderForOrganization( KeycloakId organizationId, PostComputeProvider postComputeProvider ) {
        ConfidentialOrganizationProfileEntity jesOrganizationProfile = confidentialOrganizationService.getConfidentialOrganizationProfileEntity(
                new KeycloakId( postComputeProvider.getComputeProviderOrganizationId() )
        );
        if ( !jesOrganizationProfile.getDetails().getCategories().contains( OrganizationCategory.COMPUTE_PROVIDER ) ) {
            throw new BadRequestException(
                    String.format(
                            "JES of organization %s is not eligible to serve as an external JES (missing COMPUTE_PROVIDER category)",
                            postComputeProvider.getComputeProviderOrganizationId()
                    )
            );
        }
        ConfidentialOrganizationProfileEntity organizationProfile = confidentialOrganizationService.getConfidentialOrganizationProfileEntity( organizationId );
        if ( !organizationProfile.getDetails().getCategories().contains( OrganizationCategory.APP_CUSTOMER ) ) {
            throw new BadRequestException( "Organization must have APP_CUSTOMER category to set compute provider JES" );
        }
        KeycloakId cpOrganizationId = new KeycloakId( postComputeProvider.getComputeProviderOrganizationId() );
        setComputeProviderJesForOrganization( organizationId, cpOrganizationId );
    }

    private void setComputeProviderJesForOrganization( KeycloakId organizationId, KeycloakId computeProviderOrganizationId ) {
        String cpJesClientScopeName = KeycloakUtils.createClientScopeName(
                computeProviderOrganizationId, ClientType.COMPUTE_PROVIDER_JOB_EXECUTION_SERVICE.getShortName()
        );
        if ( !keycloakService.hasClientScope( cpJesClientScopeName ) ) {
            throw new NotFoundException( String.format( "JES client scope for compute provider organization %s does not exist", organizationId ) );
        }
        // get wbs client and remove all jes scopes (e.g. own other compute provider jes)
        ClientRepresentation wbsClient = keycloakService.getClientByClientId(
                KeycloakUtils.createClientName( organizationId, ClientType.WORKBENCH_SERVICE.getShortName() )
        );
        removeComputeProviderJesClientScopeFromClient( organizationId, wbsClient );
        // set compute provider jes scope for wbs client
        String cpJesScopeId = keycloakService.getClientScopeIdsByClientScopeName().get( cpJesClientScopeName );
        keycloakService.addScopeToClient( wbsClient.getId(), cpJesScopeId );
    }

    private void removeComputeProviderJesClientScopeFromClient( KeycloakId organizationId, ClientRepresentation client ) {
        Set<ClientScopeRepresentation> wbsClientScopes = keycloakService.getClientScopes( client.getId() );
        if ( !wbsClientScopes.isEmpty() ) {
            Set<ClientScopeRepresentation> jesScopes = wbsClientScopes
                    .stream()
                    // not a client scope of the own organization
                    .filter( scope -> !scope.getName().startsWith( String.valueOf( organizationId ) ) )
                    // has JES suffix
                    .filter( scope -> scope.getName().endsWith( ClientType.COMPUTE_PROVIDER_JOB_EXECUTION_SERVICE.getShortName() ) )
                    .collect( Collectors.toSet() );
            jesScopes.forEach( scope -> keycloakService.removeScopeFromClient( client.getId(), scope.getId() ) );
        }
    }

    private void setOrganizationClientEnableFlag( KeycloakId organizationId, ClientType clientType, boolean enabled ) {
        try {
            ClientRepresentation clientRepresentation = keycloakService.getClientByClientId(
                    KeycloakUtils.createClientName( organizationId, clientType.getShortName() )
            );
            ClientResource clientResource = keycloakService.getClientsResource().get( clientRepresentation.getId() );
            clientRepresentation.setEnabled( enabled );
            clientResource.update( clientRepresentation );
        } catch ( KeycloakInternalException | KeycloakClientIdException ignored ) {
            log.warn( String.format( "Client %s for organization %s does not exist.", clientType.getShortName(), organizationId ) );
        }
    }

    public OrganizationDeploymentCredentials getOrganizationDeploymentCredentials( KeycloakId organizationId ) {
        confidentialOrganizationService.getConfidentialOrganizationProfileEntity( organizationId );
        Set<ClientRepresentation> clientRepresentations = keycloakService.getAllClientsOfOrganization( organizationId );
        OrganizationDeploymentCredentials result = OrganizationDeploymentCredentials.builder().build();
        for ( ClientRepresentation clientRepresentation : clientRepresentations ) {
            ClientCredentials credentials = ClientCredentials.builder()
                    .id( clientRepresentation.getId() )
                    .clientId( clientRepresentation.getClientId() )
                    .secret( clientRepresentation.getSecret() )
                    .build();
            if ( clientRepresentation.getName().equalsIgnoreCase( ClientType.WORKBENCH_SERVICE.getShortName() ) ) {
                result.setWorkbenchService( credentials );
            }
            if ( clientRepresentation.getName().equalsIgnoreCase( ClientType.WORKBENCH_CLIENT.getShortName() ) ) {
                result.setWorkbenchClient( credentials );
            }
            if ( clientRepresentation.getName().equalsIgnoreCase( ClientType.MEDICAL_DATA_SERVICE.getShortName() ) ) {
                result.setMedicalDataService( credentials );
            }
            if ( clientRepresentation.getName().equalsIgnoreCase( ClientType.DATA_MANAGEMENT_CLIENT.getShortName() ) ) {
                result.setDataManagementClient( credentials );
            }
            if ( clientRepresentation.getName().equalsIgnoreCase( ClientType.APP_SERVICE.getShortName() ) ) {
                result.setAppService( credentials );
            }
            if ( clientRepresentation.getName().equalsIgnoreCase( ClientType.UPLOAD_SERVICE.getShortName() ) ) {
                result.setUploadService( credentials );
            }
            if ( clientRepresentation.getName().equalsIgnoreCase( ClientType.ID_MAPPER_SERVICE.getShortName() ) ) {
                result.setIdMapperService( credentials );
            }
            if ( clientRepresentation.getName().equalsIgnoreCase( ClientType.JOB_EXECUTION_SERVICE.getShortName() ) ) {
                result.setJobExecutionService( credentials );
            }
            if ( clientRepresentation.getName().equalsIgnoreCase( ClientType.COMPUTE_PROVIDER_JOB_EXECUTION_SERVICE.getShortName() ) ) {
                result.setComputeProviderJobExecutionService( credentials );
            }
        }
        return result;
    }

    public ClientsEntity getAllClients( int page, int pageSize ) {
        List<ClientEntity> clients = keycloakService.getClients( page, pageSize ).stream()
                .filter( this::isManagedClient )
                .map( this::clientRepresentationToClientEntity )
                .collect( Collectors.toList() );
        return ClientsEntity.builder()
                .clients( clients )
                .totalClientsCount( clients.size() )
                .build();
    }

    public ClientsEntity getClientsEntityForOrganization( KeycloakId organizationId, int page, int pageSize ) {
        List<ClientRepresentation> clientRepresentations = keycloakService.findClientsByClientIdPrefix( String.valueOf( organizationId ), page, pageSize );
        List<ClientEntity> clientEntities = clientRepresentations.stream().map( this::clientRepresentationToClientEntity ).collect( Collectors.toList() );
        return ClientsEntity.builder()
                .clients( clientEntities )
                .totalClientsCount( clientEntities.size() )
                .build();
    }

    private boolean isManagedClient( ClientRepresentation clientRepresentation ) {
        String clientGroupTypeName = clientRepresentation.getAttributes().getOrDefault( CLIENT_GROUP_TYPE_ATTRIBUTE, null );
        return clientGroupTypeName != null;
    }

    private ClientEntity clientRepresentationToClientEntity( ClientRepresentation clientRepresentation ) {
        List<ClientServiceRole> clientServiceRoles = keycloakClientService.getClientServiceRoles( clientRepresentation );
        return ClientEntity.builder()
                .clientId( clientRepresentation.getClientId() )
                .keycloakId( clientRepresentation.getId() )
                .type( clientRepresentation.getName() )
                .name( clientRepresentation.getName() )
                .description( clientRepresentation.getDescription() )
                .redirectUris( clientRepresentation.getRedirectUris() )
                .webOrigins( clientRepresentation.getWebOrigins() )
                .clientServiceRoles( clientServiceRoles )
                .build();
    }
}
