package org.empaia.services.v2;

import lombok.extern.slf4j.Slf4j;
import org.empaia.exceptions.generic.AlreadyExistException;
import org.empaia.exceptions.generic.NotFoundException;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.db.v2.DBOrganizationCategoryRequest;
import org.empaia.models.db.v2.DBOrganizationDetails;
import org.empaia.models.dto.v2.OrganizationCategoryRequestEntity;
import org.empaia.models.dto.v2.OrganizationCategoryRequestsEntity;
import org.empaia.models.dto.v2.PostOrganizationCategoryRequestEntity;
import org.empaia.models.enums.v2.OrganizationCategory;
import org.empaia.models.enums.v2.RequestState;
import org.empaia.repositories.v2.OrganizationCategoryRequestsRepository;
import org.empaia.utils.KeycloakUtils;
import org.keycloak.representations.idm.GroupRepresentation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.NotAllowedException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class OrganizationCategoryRequestService {
    private final ConfidentialOrganizationService confidentialOrganizationService;
    private final OrganizationCategoryRequestsRepository organizationCategoryRequestsRepository;
    private final OrganizationServiceV2 organizationService;
    private final UserServiceV2 userServiceV2;
    private final ShoutOutService shoutOutService;

    public OrganizationCategoryRequestService(
            ConfidentialOrganizationService confidentialOrganizationService,
            OrganizationCategoryRequestsRepository organizationCategoryRequestsRepository,
            OrganizationServiceV2 organizationService,
            UserServiceV2 userServiceV2,
            ShoutOutService shoutOutService )
    {
        this.confidentialOrganizationService = confidentialOrganizationService;
        this.organizationCategoryRequestsRepository = organizationCategoryRequestsRepository;
        this.organizationService = organizationService;
        this.userServiceV2 = userServiceV2;
        this.shoutOutService = shoutOutService;
    }

    public OrganizationCategoryRequestEntity getOrganizationCategoryRequest( int categoryRequestId ) {
        DBOrganizationCategoryRequest dbRequest = organizationCategoryRequestsRepository.findByOrganizationCategoryRequestId( categoryRequestId )
                .orElseThrow(
                        () -> new NotFoundException( "Organization category request not found" )
                );
        return buildOrganizationCategoryRequestEntity(dbRequest);
    }

    public OrganizationCategoryRequestEntity acceptOrganizationCategoryRequest( KeycloakId userId, int categoryRequestId ) {
        DBOrganizationCategoryRequest dbRequest = organizationCategoryRequestsRepository.findByOrganizationCategoryRequestId( categoryRequestId )
                .orElseThrow(
                        () -> new NotFoundException( "Organization category request not found" )
                );
        Set<String> organizationCategories = dbRequest.getRequestedOrganizationCategories()
                .stream()
                .map( Enum::toString )
                .collect( Collectors.toSet());
        organizationService.updateOrganizationAttributes( new KeycloakId( dbRequest.getOrganizationId() ), attributes -> {
            attributes.put( "categories", List.copyOf( organizationCategories ) );
        });
        dbRequest.updateRequestState( RequestState.ACCEPTED, userId, null );
        dbRequest = organizationCategoryRequestsRepository.save( dbRequest );
        shoutOutService.notifyManagerCategoryChangeApproved( new KeycloakId( dbRequest.getOrganizationId() ) );
        return buildOrganizationCategoryRequestEntity(dbRequest);
    }

    public OrganizationCategoryRequestEntity denyOrganizationCategoryRequest( KeycloakId userId, int categoryRequestId, String reviewerComment ) {
        DBOrganizationCategoryRequest dbRequest = organizationCategoryRequestsRepository.findByOrganizationCategoryRequestId( categoryRequestId )
                .orElseThrow(
                        () -> new NotFoundException( "Organization category request not found" )
                );
        dbRequest.updateRequestState( RequestState.REJECTED, userId, reviewerComment );
        dbRequest = organizationCategoryRequestsRepository.save( dbRequest );
        shoutOutService.notifyManagerCategoryChangeRejected( new KeycloakId( dbRequest.getOrganizationId() ) );
        return buildOrganizationCategoryRequestEntity( dbRequest );
    }

    public OrganizationCategoryRequestEntity revokeOrganizationCategoryRequest(  KeycloakId userId, KeycloakId organizationId,  int categoryRequestId ) {
        DBOrganizationCategoryRequest dbRequest = organizationCategoryRequestsRepository.findByOrganizationCategoryRequestId(
                categoryRequestId
        ).orElseThrow(
                () -> new NotFoundException( "Organization category request does not exist" )
        );
        if ( organizationId.equals( dbRequest.getOrganizationId() ) ) {
            dbRequest.updateRequestState( RequestState.REVOKED, userId, null );
            dbRequest = organizationCategoryRequestsRepository.save(dbRequest);
            return buildOrganizationCategoryRequestEntity( dbRequest );
        } else {
            throw new NotAllowedException( "You are not allowed to delete this organization category request" );
        }
    }

    public OrganizationCategoryRequestsEntity getOrganizationCategoryRequests(KeycloakId organizationId, int page, int pageSize ) {
        PageRequest pageRequest = PageRequest.of( page, pageSize );
        Page<DBOrganizationCategoryRequest> dbRequests = organizationCategoryRequestsRepository.findAllByOrganizationId(
                organizationId.toString(), pageRequest
        );

        List<OrganizationCategoryRequestEntity> categoryRequestEntities = dbRequests.stream()
                .map( this::buildOrganizationCategoryRequestEntity )
                .collect( Collectors.toList() );
        return OrganizationCategoryRequestsEntity.builder()
                .organizationCategoryRequestEntities( categoryRequestEntities )
                .totalOrganizationCategoryRequestsCount( Long.valueOf( dbRequests.getTotalElements() ).intValue() )
                .build();
    }

    public OrganizationCategoryRequestsEntity getOrganizationCategoryRequests(int page, int pageSize ) {
        PageRequest pageRequest = PageRequest.of( page, pageSize );
        Page<DBOrganizationCategoryRequest> dbRequests = organizationCategoryRequestsRepository.findAllByRequestState( RequestState.REQUESTED.getValue(), pageRequest );
        List<OrganizationCategoryRequestEntity> categoryRequestEntities = dbRequests.stream()
                .map( this::buildOrganizationCategoryRequestEntity )
                .collect( Collectors.toList() );
        return OrganizationCategoryRequestsEntity.builder()
                .organizationCategoryRequestEntities( categoryRequestEntities )
                .totalOrganizationCategoryRequestsCount( Long.valueOf( dbRequests.getTotalElements() ).intValue() )
                .build();
    }

    public OrganizationCategoryRequestEntity requestOrganizationCategory( KeycloakId userId, KeycloakId organizationId, PostOrganizationCategoryRequestEntity organizationCategoryRequest ) {
        if ( organizationCategoryRequestsRepository.existsByOrganizationIdAndRequestState(
                organizationId.toString(), RequestState.REQUESTED.getValue()
        ) ) {
            throw new AlreadyExistException( "Open organization category request already exists" );
        }

        Set<String> organizationCategoryRequestNames = organizationCategoryRequest.getRequestedOrganizationCategories()
                .stream()
                .map(Enum::toString)
                .collect( Collectors.toSet());
        organizationService.visitOrganizationAttributes( organizationId, attributes -> {
            Set<String> categoryList = Set.copyOf( KeycloakUtils.getAttributeValueList(attributes, "categories", new ArrayList<>() ) );
            if ( categoryList != null && categoryList.equals( organizationCategoryRequestNames ) ) {
                throw new AlreadyExistException( "Requested organization categories already set" );
            }
        });

        DBOrganizationCategoryRequest dbRequest = DBOrganizationCategoryRequest.builder()
                .appCustomerCategoryRequested(
                        organizationCategoryRequest.getRequestedOrganizationCategories().contains( OrganizationCategory.APP_CUSTOMER )
                )
                .appVendorCategoryRequested(
                        organizationCategoryRequest.getRequestedOrganizationCategories().contains( OrganizationCategory.APP_VENDOR )
                )
                .computeProviderCategoryRequested(
                        organizationCategoryRequest.getRequestedOrganizationCategories().contains( OrganizationCategory.COMPUTE_PROVIDER )
                )
                .productProviderCategoryRequested(
                        organizationCategoryRequest.getRequestedOrganizationCategories().contains( OrganizationCategory.PRODUCT_PROVIDER )
                )
                .organizationId( organizationId.toString() )
                .creatorId( userId.toString() )
                .createdAt( new Date().getTime() )
                .requestState( RequestState.REQUESTED.getValue() )
                .build();
        dbRequest = organizationCategoryRequestsRepository.save( dbRequest );
        OrganizationCategoryRequestEntity organizationCategoryRequestEntity = buildOrganizationCategoryRequestEntity( dbRequest );
        shoutOutService.notifyModeratorsCategoryChangeRequested( organizationId );
        return organizationCategoryRequestEntity;
    }

    private List<OrganizationCategory> createActiveOrganizationCategoryList(GroupRepresentation groupRepresentation, DBOrganizationDetails dbOrganizationDetails) {
        Map<String,List<String>> attributes = groupRepresentation.getAttributes();
        if ( attributes != null ) {
            List<String> categories = KeycloakUtils.getAttributeValueList(
                    attributes, "categories", new ArrayList<>()
            );
            if ( !(categories.size() == 1 && categories.get(0).isEmpty()) ) {
                return categories
                        .stream()
                        .map(OrganizationCategory::fromName)
                        .collect(Collectors.toList());
            }
        }
        return Collections.emptyList();
    }

    private OrganizationCategoryRequestEntity buildOrganizationCategoryRequestEntity( DBOrganizationCategoryRequest dbRequest ) {
        KeycloakId organizationId = new KeycloakId( dbRequest.getOrganizationId() );
        Set<OrganizationCategory> requestedCategories = new HashSet<>( dbRequest.getRequestedOrganizationCategories() );
        Set<OrganizationCategory> activeOrganizationCategories = new HashSet<>(
                organizationService.getOrganizationProfileEntity(organizationId, this::createActiveOrganizationCategoryList)
        );

        Set<OrganizationCategory> newlyRequestedCategories = new HashSet<>( requestedCategories );
        newlyRequestedCategories.removeAll( activeOrganizationCategories );

        Set<OrganizationCategory> retractedCategories = new HashSet<>( activeOrganizationCategories );
        retractedCategories.removeAll( requestedCategories );

        return OrganizationCategoryRequestEntity.builder()
                .organizationCategoryRequestId( dbRequest.getOrganizationCategoryRequestId() )
                .organizationId( organizationId )
                .organizationName(
                        confidentialOrganizationService.getConfidentialOrganizationProfileEntity(
                                new KeycloakId( dbRequest.getOrganizationId() )
                        ).getOrganizationName()
                )
                .existingCategories( new ArrayList<>( activeOrganizationCategories ) )
                .requestedCategories( new ArrayList<>( newlyRequestedCategories ) )
                .retractedCategories( new ArrayList<>( retractedCategories ) )
                .userDetails( userServiceV2.getExtendedUserDetails( dbRequest.getCreatorId() ) )
                .createdAt( dbRequest.getCreatedAt() )
                .reviewerComment( dbRequest.getReviewerComment() )
                .reviewerId( new KeycloakId( dbRequest.getReviewerId() ) )
                .updatedAt( dbRequest.getUpdatedAt() )
                .requestState( dbRequest.getRequestState() )
                .build();
    }
}
