package org.empaia.services.v2;

import lombok.extern.slf4j.Slf4j;
import org.empaia.exceptions.generic.NotFoundException;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.db.v2.DBMembershipRequest;
import org.empaia.models.dto.v2.*;
import org.empaia.models.enums.v2.OrganizationUserRoleV2;
import org.empaia.models.enums.v2.RequestState;
import org.empaia.models.enums.v2.UserAccountState;
import org.empaia.repositories.v2.MembershipRequestsRepository;
import org.empaia.services.KeycloakService;
import org.empaia.utils.KeycloakUtils;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.ws.rs.BadRequestException;
import java.util.*;
import java.util.stream.Collectors;

import static org.empaia.utils.KeycloakUtils.getUserAccountState;

@Slf4j
@Service
public class ManagerService {

    private final ConfidentialOrganizationService confidentialOrganizationService;
    private final KeycloakService keycloakService;
    private final MembershipRequestsRepository membershipRequestsRepository;
    private final OrganizationLogoService organizationLogoService;
    private final ShoutOutService shoutOutService;
    private final UserServiceV2 userServiceV2;
    private final OrganizationUserRoleRequestService organizationUserRoleRequestService;
    private final OrganizationCategoryRequestService organizationCategoryRequestService;

    public ManagerService(
            ConfidentialOrganizationService confidentialOrganizationService,
            KeycloakService keycloakService,
            MembershipRequestsRepository membershipRequestsRepository,
            OrganizationLogoService organizationLogoService,
            ShoutOutService shoutOutService,
            UserServiceV2 userServiceV2,
            OrganizationUserRoleRequestService organizationUserRoleRequestService,
            OrganizationCategoryRequestService organizationCategoryRequestService ) {
        this.confidentialOrganizationService = confidentialOrganizationService;
        this.keycloakService = keycloakService;
        this.membershipRequestsRepository = membershipRequestsRepository;
        this.organizationLogoService = organizationLogoService;
        this.shoutOutService = shoutOutService;
        this.userServiceV2 = userServiceV2;
        this.organizationUserRoleRequestService = organizationUserRoleRequestService;
        this.organizationCategoryRequestService = organizationCategoryRequestService;
    }

    public MembershipRequestsEntity getMembershipRequests( KeycloakId organizationId, int page, int size ) {

        Page<DBMembershipRequest> membershipRequestPage = membershipRequestsRepository.findAllByOrganizationIdAndRequestState(
                organizationId.toString(), RequestState.REQUESTED.getValue(), PageRequest.of( page, size )
        );
        List<MembershipRequestEntity> membershipRequests = membershipRequestPage.stream()
                .map( this::buildMembershipRequestEntity )
                .collect( Collectors.toList() );

        return MembershipRequestsEntity.builder()
                .membershipRequestEntities( membershipRequests )
                .totalMembershipRequestsCount( Long.valueOf( membershipRequestPage.getTotalElements() ).intValue() )
                .build();
    }

    public MembershipRequestEntity acceptMembershipRequest( KeycloakId userId, KeycloakId organizationId, int membershipRequestId ) {
        DBMembershipRequest membershipRequest = membershipRequestsRepository.findByMembershipRequestId( membershipRequestId )
                .orElseThrow( () -> new NotFoundException( "No such membership request found " ) );
        if ( !organizationId.equals( membershipRequest.getOrganizationId() ) ) {
            throw new BadRequestException( "Wrong organization-id header" );
        }
        OrganizationMember organizationMember = OrganizationMember.builder()
                .organizationId( organizationId )
                .userId( new KeycloakId( membershipRequest.getUserId() ) )
                .build();
        keycloakService.addUserToOrganization( organizationMember );
        membershipRequest.updateRequestState( RequestState.ACCEPTED, userId, null );
        membershipRequestsRepository.save( membershipRequest );
        shoutOutService.notifyUserOrganizationMembershipApproved( organizationId, organizationMember.getUserId() );
        return this.buildMembershipRequestEntity( membershipRequest );
    }

    private MembershipRequestEntity buildMembershipRequestEntity( DBMembershipRequest membershipRequest ) {
        return MembershipRequestEntity.builder()
                .membershipRequestId( membershipRequest.getMembershipRequestId() )
                .organizationId( new KeycloakId( membershipRequest.getOrganizationId() ) )
                .organizationName( membershipRequest.getOrganizationName() )
                .userDetails( userServiceV2.getExtendedUserDetails( membershipRequest.getUserId() ) )
                .createdAt( membershipRequest.getCreatedAt() )
                .reviewerComment( membershipRequest.getReviewerComment() )
                .reviewerId( new KeycloakId( membershipRequest.getReviewerId() ) )
                .updatedAt( membershipRequest.getUpdatedAt() )
                .requestState( membershipRequest.getRequestState() )
                .build();
    }

    public MembershipRequestEntity denyMembershipRequest(
            KeycloakId userId,
            KeycloakId organizationId,
            int membershipRequestId,
            String reviewerComment
    ) {
        DBMembershipRequest membershipRequest = membershipRequestsRepository.findByMembershipRequestId( membershipRequestId ).orElseThrow(
                () -> new NotFoundException( "No such membership request found " )
        );
        // TODO: write pytest that tries to deny the request twice, it must expect the 404 error. This
        // TODO: will verify that this SQL deletion is correctly executed:
        if ( !organizationId.equals( membershipRequest.getOrganizationId() ) ) {
            throw new BadRequestException( "Wrong organization-id header" );
        }
        membershipRequest.updateRequestState( RequestState.REJECTED, userId, reviewerComment );
        membershipRequestsRepository.save( membershipRequest );
        shoutOutService.notifyUserOrganizationMembershipRejected( organizationId, new KeycloakId( membershipRequest.getUserId() ) );
        return buildMembershipRequestEntity( membershipRequest );
    }

    public ConfidentialMemberProfilesEntity getMemberProfiles( KeycloakId organizationId ) {
        List<ConfidentialMemberProfileEntity> memberEntities = keycloakService.getOrganizationMembers( organizationId.toString() ).stream()
                .map( userRepresentation -> createMemberProfileEntityFromUserRepresentation( userRepresentation, organizationId ) )
                .filter( Objects::nonNull )
                .collect( Collectors.toList() );
        return ConfidentialMemberProfilesEntity.builder().users( memberEntities ).build();
    }

    public ConfidentialMemberProfileEntity getMemberProfile( OrganizationMember organizationMember ) {
        // TODO: why does keycloakService.isGroupMember return false?
        if ( !keycloakService.isGroupMember( organizationMember ) ) {
            throw new NotFoundException( "User " + organizationMember.getUserId().toString() +
                    " is not a member of organization  " + organizationMember.getOrganizationId().toString() );
        }
        return createMemberProfileEntityFromUserRepresentation(
                keycloakService.getUserById( organizationMember.getUserId() ).toRepresentation(), organizationMember.getOrganizationId()
        );
    }

    private ConfidentialMemberProfileEntity createMemberProfileEntityFromUserRepresentation( UserRepresentation userRepresentation, KeycloakId organizationId ) {
        UserAccountState accountState = getUserAccountState( userRepresentation );
        if ( accountState == UserAccountState.DISABLED ) {
            return null;
        }

        List<OrganizationUserRoleV2> organizationUserRoles = keycloakService
                .getUserOrganisationSubGroups( userRepresentation.getId(), organizationId.toString() )
                .stream()
                .map( groupRepresentation -> OrganizationUserRoleV2.fromName( groupRepresentation.getName() ) )
                .collect( Collectors.toList() );

        String title = null;
        Map<String, List<String>> attributes = userRepresentation.getAttributes();
        if ( attributes != null ) {
            title = KeycloakUtils.getAttributeValue( attributes, "title", null );
        }

        return ConfidentialMemberProfileEntity.builder()
                .firstName( userRepresentation.getFirstName() )
                .lastName( userRepresentation.getLastName() )
                .title( title )
                .emailAddress( userRepresentation.getEmail() )
                .organizationUserRoles( organizationUserRoles )
                .userId( new KeycloakId( userRepresentation.getId() ) )
                .build();
    }

    public void removeUserFromOrganization( OrganizationMember organizationMember ) {
        keycloakService.removeUserFromOrganization( organizationMember );
    }

    public OrganizationUserRolesEntity setOrganizationUserRoles( OrganizationMember organizationMember, PostOrganizationUserRolesEntity requestedRoles ) {
        Set<OrganizationUserRoleV2> allowedUserRoles = keycloakService.getAllowedOrganizationUserRoles( organizationMember );
        Set<OrganizationUserRoleV2> currentRoles = new HashSet<>( keycloakService.getOrganizationUserRoles( organizationMember ) );
        // if MANAGER role was revoked in request, we need to ensure that user is not the last manager in charge
        if ( currentRoles.contains( OrganizationUserRoleV2.MANAGER  ) && !requestedRoles.getRoles().contains( OrganizationUserRoleV2.MANAGER ) ) {
            allowedUserRoles.removeIf( role -> keycloakService.isLastOrganizationManagerInCharge( organizationMember ) );
        }
        for ( OrganizationUserRoleV2 role : requestedRoles.getRoles() ) {
            if ( !allowedUserRoles.contains( role ) ) {
                throw new BadRequestException( "The organization categories do not allow the organization user role " + role.getName() + " for this organization" );
            }
        }

        keycloakService.setOrganizationUserRoles( organizationMember, requestedRoles.getRoles() );
        shoutOutService.notifyUserMemberRoleUpdated( organizationMember.getUserId() );
        return OrganizationUserRolesEntity.builder()
                .ownedRoles( requestedRoles.getRoles() )
                .selectableRoles( new ArrayList<>( allowedUserRoles ) )
                .build();
    }

    public OrganizationUserRoleRequestsEntity getOrganizationUserRoleRequests( int page, int pageSize, KeycloakId organizationId ) {
        return organizationUserRoleRequestService.getOrganizationUserRoleRequests( page, pageSize, organizationId );
    }

    public OrganizationUserRoleRequestEntity acceptOrganizationUserRoleRequest( KeycloakId userId, KeycloakId organizationId, int roleRequestId ) {
        return organizationUserRoleRequestService.acceptOrganizationUserRoleRequest( userId, organizationId, roleRequestId );
    }

    public OrganizationUserRoleRequestEntity denyOrganizationUserRoleRequest( KeycloakId userId, KeycloakId organizationId, int roleRequestId, String reviewerComment ) {
        return organizationUserRoleRequestService.denyOrganizationUserRoleRequest( userId, organizationId, roleRequestId, reviewerComment );
    }

    public ConfidentialOrganizationProfileEntity getOrganizationProfile( KeycloakId organizationId ) {
        return confidentialOrganizationService.getActiveConfidentialOrganizationProfileEntity( organizationId );
    }

    public ConfidentialOrganizationProfileEntity updateOrganizationName( KeycloakId organizationId, PostOrganizationNameEntity organizationNameEntity ) {
        return confidentialOrganizationService.updateName( organizationId, organizationNameEntity.getName() );
    }

    public ConfidentialOrganizationProfileEntity updateOrganizationContactData( KeycloakId organizationId, ConfidentialOrganizationContactDataEntity contactData ) {
        return confidentialOrganizationService.updateContactData( organizationId, contactData );
    }

    public ConfidentialOrganizationProfileEntity updateOrganizationDetails( KeycloakId organizationId, PostConfidentialOrganizationDetailsEntity detailsEntity ) {
        ConfidentialOrganizationProfileEntity profileEntity = confidentialOrganizationService.getConfidentialOrganizationProfileEntity( organizationId );
        PostInitialConfidentialOrganizationDetailsEntity initialDetailsEntity = PostInitialConfidentialOrganizationDetailsEntity
                .builder()
                .categories( profileEntity.getDetails().getCategories() )
                .descriptionEnglish( detailsEntity.getDescriptionEnglish() )
                .descriptionGerman( detailsEntity.getDescriptionGerman() )
                .isUserCountPublic( detailsEntity.isUserCountPublic() )
                .build();
        return confidentialOrganizationService.updateDetails( organizationId, initialDetailsEntity );
    }

    public ConfidentialOrganizationProfileEntity setOrganizationLogo( KeycloakId organizationId, MultipartFile logo ) throws Exception {
        GroupRepresentation groupRepresentation = keycloakService.getGroup( organizationId.toString() ).orElseThrow(
                () -> new NotFoundException( "Organization not found" )
        );
        organizationLogoService.setLogo( groupRepresentation, logo );
        return confidentialOrganizationService.getConfidentialOrganizationProfileEntity( organizationId );
    }

    public OrganizationCategoryRequestEntity revokeOrganizationCategoryRequest( KeycloakId userId, KeycloakId organizationId, int categoryRequestId ) {
        return organizationCategoryRequestService.revokeOrganizationCategoryRequest( userId, organizationId, categoryRequestId );
    }

    public OrganizationCategoryRequestsEntity getOrganizationCategoryRequests( KeycloakId organizationId, int page, int pageSize ) {
        return organizationCategoryRequestService.getOrganizationCategoryRequests( organizationId, page, pageSize );
    }

    public OrganizationCategoryRequestEntity requestOrganizationCategory( KeycloakId userId, KeycloakId organizationId, PostOrganizationCategoryRequestEntity organizationCategoryRequest ) {
        return organizationCategoryRequestService.requestOrganizationCategory( userId, organizationId, organizationCategoryRequest );
    }
}
