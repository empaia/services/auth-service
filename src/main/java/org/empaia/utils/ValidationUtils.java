package org.empaia.utils;

import org.empaia.exceptions.user.ValidationException;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

public class ValidationUtils {
    public static void validateUriList( List<String> uriList, String baseDomain, boolean allowLocalhost, boolean allowHttp ) throws ValidationException {
        for ( String uri : uriList ) {
            if ( uri == null || uri.isEmpty() || uri.equals( "*" ) ) {
                continue;
            }
            if ( !allowHttp && uri.toLowerCase().startsWith( "http://" ) ) {
                throw new ValidationException( String.format( "'%s' starts with http:// but is not allowed.", uri ) );
            }
            if ( uri.toLowerCase().contains( "localhost" ) ) {
                if ( !allowLocalhost ) {
                    throw new ValidationException( String.format( "'%s' contains localhost but is not allowed.", uri ) );
                }
            } else {
                if ( baseDomain != null && !baseDomain.isEmpty() && !uri.contains( baseDomain ) ) {
                    throw new ValidationException( String.format( "'%s' is not a subdomain of '%s'.", uri, baseDomain ) );
                }
            }
            try {
                new URL( uri ).toURI();
            } catch ( MalformedURLException | URISyntaxException e ) {
                throw new ValidationException( String.format( "'%s' is not a valid URL.", uri ) );
            }
        }
    }
}
