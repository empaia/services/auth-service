package org.empaia.utils;

import lombok.extern.slf4j.Slf4j;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.AddressEntity;
import org.empaia.models.dto.v2.ConfidentialOrganizationContactDataEntity;
import org.empaia.models.dto.v2.PostUserContactDataEntity;
import org.empaia.models.dto.v2.UserContactDataEntity;
import org.empaia.models.enums.v2.ClientType;
import org.empaia.models.enums.v2.CountryCode;
import org.empaia.models.enums.v2.UserAccountState;
import org.keycloak.representations.idm.UserRepresentation;

import javax.management.InvalidAttributeValueException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Slf4j
public class KeycloakUtils {

    public static void setAttributeValue( Map<String, List<String>> attributes, String name, Timestamp value ) {
        DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
        attributes.put( name, Collections.singletonList( formatter.format( value.toLocalDateTime() ) ) );
    }

    public static Timestamp getAttributeValueAsTimestamp( Map<String, List<String>> attributes, String name, Timestamp defaultValue ) {
        Timestamp timestamp = defaultValue;
        if ( attributes != null ) {
            final List<String> defaultList = Collections.singletonList( null );
            String valueString = attributes.getOrDefault( name, defaultList ).get( 0 );
            if ( valueString != null ) {
                try {
                    timestamp = Timestamp.valueOf( LocalDateTime.parse( valueString, DateTimeFormatter.ISO_LOCAL_DATE_TIME ) );
                } catch ( Exception e ) {
                    log.error( String.format( "Failed to parse %s: %s", name, valueString ), e );
                }
            }
        }
        return timestamp;
    }

    public static void setConfidentialOrganizationContactDataFromAttributes( ConfidentialOrganizationContactDataEntity contactData, Map<String, List<String>> attributes ) {
        contactData.setPhoneNumber( getAttributeValue( attributes, "phoneNumber", "" ) );
        setAddressEntityFromAttributes( contactData, attributes );
    }

    public static void setUserContactDataFromAttributes( UserContactDataEntity contactData, Map<String, List<String>> attributes ) {
        contactData.setPhoneNumber( getAttributeValue( attributes, "phoneNumber", "" ) );
        setAddressEntityFromAttributes( contactData, attributes );
    }

    public static void setAddressEntityFromAttributes( AddressEntity addressEntity, Map<String, List<String>> attributes ) {
        String countryCodeString = getAttributeValue( attributes, "countryCode", "DE" );
        if ( countryCodeString.isEmpty() ) {
            countryCodeString = "DE";
        }
        addressEntity.setCountryCode( CountryCode.valueOf( countryCodeString ) );
        addressEntity.setPlaceName( getAttributeValue( attributes, "placeName", "" ) );
        addressEntity.setStreetName( getAttributeValue( attributes, "streetName", "" ) );
        addressEntity.setStreetNumber( getAttributeValue( attributes, "streetNumber", "" ) );
        addressEntity.setZipCode( getAttributeValue( attributes, "zipCode", "" ) );
    }

    public static void addPostUserContactDataToAttributes( PostUserContactDataEntity contactData, Map<String, List<String>> attributes ) {
        String countryCodeString = contactData.getCountryCode() == null ? null : contactData.getCountryCode().name();
        setAttributeValueIfNotNull( attributes, "countryCode", countryCodeString );
        setAttributeValueIfNotNull( attributes, "phoneNumber", contactData.getPhoneNumber() );
        setAttributeValueIfNotNull( attributes, "placeName", contactData.getPlaceName() );
        setAttributeValueIfNotNull( attributes, "streetName", contactData.getStreetName() );
        setAttributeValueIfNotNull( attributes, "streetNumber", contactData.getStreetNumber() );
        setAttributeValueIfNotNull( attributes, "zipCode", contactData.getZipCode() );
    }

    public static void addOrganizationContactDataToAttributes( ConfidentialOrganizationContactDataEntity contactData, Map<String, List<String>> attributes ) {
        String countryCodeString = contactData.getCountryCode() == null ? null : contactData.getCountryCode().name();
        setAttributeValueIfNotNull( attributes, "countryCode", countryCodeString );
        setAttributeValueIfNotNull( attributes, "phoneNumber", contactData.getPhoneNumber() );
        setAttributeValueIfNotNull( attributes, "placeName", contactData.getPlaceName() );
        setAttributeValueIfNotNull( attributes, "streetName", contactData.getStreetName() );
        setAttributeValueIfNotNull( attributes, "streetNumber", contactData.getStreetNumber() );
        setAttributeValueIfNotNull( attributes, "zipCode", contactData.getZipCode() );
    }

    public static void setAttributeValueIfNotNull( Map<String, List<String>> attributes, String name, String value ) {
        if ( value != null ) {
            attributes.put( name, Collections.singletonList( value ) );
        }
    }

    public static String getAttributeValue( Map<String, List<String>> attributes, String name, String defaultValue ) {
        return getAttributeValueList( attributes, name, Collections.singletonList( defaultValue ) ).get( 0 );
    }

    public static List<String> getAttributeValueList( Map<String, List<String>> attributes, String name, List<String> defaultValueList ) {
        List<String> valueList;
        if ( attributes == null ) {
            valueList = defaultValueList;
        } else {
            valueList = attributes.getOrDefault( name, defaultValueList );
        }
        return valueList;
    }

    public static boolean getAttributeValueAsBoolean( Map<String, List<String>> attributes, String name, boolean defaultValue ) throws InvalidAttributeValueException {
        boolean value = defaultValue;
        if ( attributes != null ) {
            String valueString = attributes.getOrDefault( name, Collections.singletonList( null ) ).get( 0 );
            if ( valueString != null ) {
                value = switch ( valueString ) {
                    case "true" -> true;
                    case "false" -> false;
                    default ->
                            throw new InvalidAttributeValueException( "Attribute '" + name + "' has an invalid value: '" + valueString + "'" );
                };
            }
        }
        return value;
    }

    public static UserAccountState getUserAccountState( UserRepresentation userRepresentation ) {
        UserAccountState accountState = UserAccountState.DISABLED;
        if ( userRepresentation.isEnabled() ) {
            if ( userRepresentation.isEmailVerified() ) {
                if ( userRepresentation.getRequiredActions().contains( "UPDATE_PROFILE" ) ) {
                    accountState = UserAccountState.REQUIRES_PROFILE_UPDATE;
                } else {
                    accountState = UserAccountState.ENABLED_AND_NO_ACTIONS_REQUIRED;
                }
            } else {
                accountState = UserAccountState.REQUIRES_EMAIL_VERIFICATION;
            }
        }
        return accountState;
    }

    public static HashSet<String> getAllowedAudience( Map<String, List<String>> attributes ) {
        var allowedAudience = new HashSet<String>();
        var allowedAudienceCount = Integer.parseInt(
                attributes.getOrDefault( "allowed_audience_count", List.of( "0" ) ).get( 0 )
        );
        if ( allowedAudienceCount > 0 ) {
            for ( int i = 0; i < allowedAudienceCount; i++ ) {
                var aud = attributes.getOrDefault(
                        String.format( "allowed_audience_%d", i ), List.of( "" )
                ).get( 0 );
                if ( !aud.isBlank() ) {
                    allowedAudience.add( aud );
                }
            }
        }
        return allowedAudience;
    }

    public static void clearAllowedAudience( Map<String, List<String>> attributes ) {

        var allowedAudienceCount = Integer.parseInt(
                attributes.getOrDefault( "allowed_audience_count", List.of( "0" ) ).get( 0 )
        );
        if ( allowedAudienceCount > 0 ) {
            for ( int i = 0; i < allowedAudienceCount; i++ ) {
                var key = String.format( "allowed_audience_%d", i );
                var aud = attributes.getOrDefault( key, List.of( "" ) ).get( 0 );
                if ( !aud.isBlank() ) {
                    attributes.remove( key );
                }
            }
            attributes.put( "allowed_audience_count", List.of( "0" ) );
        }
    }

    public static void setAllowedAudience( Map<String, List<String>> attributes, HashSet<String> allowedAudience ) {
        clearAllowedAudience( attributes );
        allowedAudience.remove( "" );
        attributes.put( "allowed_audience_count", List.of( String.valueOf( allowedAudience.size() ) ) );
        var audienceArray = allowedAudience.toArray( String[]::new );
        for ( int i = 0; i < allowedAudience.size(); i++ ) {
            attributes.put( String.format( "allowed_audience_%d", i ), List.of( audienceArray[i] ) );
        }
    }

    public static String createClientScopeName( KeycloakId organizationId, String clientScopeSuffix ) {
        return String.format( "%s.scope.%s", organizationId, clientScopeSuffix ).toLowerCase();
    }

    public static String createClientName( KeycloakId organizationId, String clientSuffix ) {
        return String.format( "%s.client.%s", organizationId.toString(), clientSuffix ).toLowerCase();
    }

    public static boolean isValidKeycloakInstanceName( String instanceId, String instanceType ) {
        String[] clientIdParts = instanceId.split( "\\." );
        if ( clientIdParts.length != 3 || !clientIdParts[1].equals( instanceType ) ) {
            return false;
        }
        try {
            ClientType.fromShortName( clientIdParts[2] );
        } catch ( EnumConstantNotPresentException ignored ) {
            return false;
        }
        return true;
    }

}
