package org.empaia.utils;

import jakarta.servlet.http.HttpServletRequest;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.empaia.configs.EmpaiaConfiguration;
import org.empaia.models.auth.KeycloakId;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.servlet.HandlerMapping;

import javax.ws.rs.BadRequestException;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;

public class AuthorizationUtils {
    public static Jwt getAccessToken(HttpServletRequest request ) throws AccessDeniedException {
        if ( request.getUserPrincipal() == null ) {
            throw new AccessDeniedException( "This endpoint requires authentication" );
        }

        Jwt token = ((JwtAuthenticationToken)request.getUserPrincipal()).getToken();

        if ( token == null ) { throw new AccessDeniedException( "This endpoint requires an authentication token" ); }
        return token;
    }

    public static boolean hasGlobalRole( Jwt accessToken, String roleName ) {
        if ( !accessToken.hasClaim( "resource_access" ) ) {
            return false;
        }
        String authServieClient = null;
        if ( accessToken.getClaimAsMap("resource_access" ).containsKey( "auth_service_client" ) ) {
            authServieClient = "auth_service_client";
        } else if ( accessToken.getClaimAsMap( "resource_access" ).containsKey( "org.empaia.prod.aaa" ) ) {
            authServieClient = "org.empaia.prod.aaa"; // legacy
        }

        if ( authServieClient == null ) {
            return false;
        }

        @SuppressWarnings("unchecked")
        Map<String, List<String>> authClientRoles = (Map<String,List<String>>) accessToken.getClaimAsMap( "resource_access" ).get( authServieClient );
        return authClientRoles.containsKey("roles") && authClientRoles.get("roles").contains( roleName );
    }

    public static <T extends Annotation> T getAuthorizeAnnotation(JoinPoint joinPoint, Class<T> clazz ) throws NoSuchMethodException {
        MethodSignature signature      = ( MethodSignature ) joinPoint.getSignature();
        String          methodName     = signature.getMethod().getName();
        Class<?>[]      parameterTypes = signature.getMethod().getParameterTypes();
        return joinPoint
                .getTarget()
                .getClass()
                .getMethod( methodName, parameterTypes )
                .getAnnotation( clazz );
    }

    public static KeycloakId getOrganizationKeycloakIdFromHeader(HttpServletRequest request) {
        return new KeycloakId( getParameterFromHeader( request, EmpaiaConfiguration.ORGANIZATION_HEADER_V2), true );
    }

    public static KeycloakId getUserKeycloakIdFromHeader(HttpServletRequest request ) {
        return new KeycloakId( getParameterFromHeader( request, EmpaiaConfiguration.USER_HEADER_V2), true );
    }

    private static String getParameterFromHeader( HttpServletRequest request, String parameterName ) {
        String parameter = request.getHeader( parameterName );
        if ( parameter == null ) {
            throw new BadRequestException( String.format( "Invalid parameter %s in header", parameterName ) );
        }
        return parameter;
    }

    public static KeycloakId getOrganizationIdFromPathParameters( HttpServletRequest request ) {
        var pathParameters = (Map<String,String>) request.getAttribute( HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE );
        if ( pathParameters.containsKey("organization_id") ) {
            return new KeycloakId( pathParameters.get("organization_id"), true );
        } else {
            throw new AccessDeniedException( "Organization ID not found in path parameters" );
        }
    }
}
