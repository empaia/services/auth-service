package org.empaia.utils;

public final class RegularExpressions {
    public static final String ONLY_CHARS            = "^$|^[a-zA-Z ]+$";
    public static final String PHONE_NUMBER          = "^$|^[0-9\\- +()]+$";
    public static final String NO_SPECIAL_CHARS      = "^(?!.*[<>&'\"\\]#\\[;]).*$";
    // RegExp based on Wikipedia information: https://en.wikipedia.org/wiki/Postal_code
    public static final String POSTAL_CODE           = "^$|^[a-zA-Z0-9 \\-]+$";
    public static final String VALID_KEYCLOAK_ID     = "^[a-zA-Z0-9\\-]+$";

    private RegularExpressions() {
        throw new IllegalStateException( "Utility class" );
    }
}
