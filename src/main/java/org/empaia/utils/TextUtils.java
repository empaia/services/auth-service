package org.empaia.utils;

import java.text.Normalizer;

public class TextUtils {

    private TextUtils() {

    }

    public static String normalize( String in ) {
        if ( in == null ) {
            return null;
        }

        return Normalizer.normalize( in, Normalizer.Form.NFD )
                         .replaceAll( "\\p{InCombiningDiacriticalMarks}+", "" )
                         .replaceAll( "[^a-zA-Z0-9-_]", "" );
    }
}
