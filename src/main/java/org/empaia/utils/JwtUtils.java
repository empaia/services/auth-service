package org.empaia.utils;

import org.empaia.models.auth.KeycloakId;
import org.empaia.models.enums.v2.OrganizationUserRoleV2;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.*;
import java.util.stream.Collectors;

@Configuration
public class JwtUtils {

    public static Set<OrganizationUserRoleV2> getOrganizationUserRolesFromToken( Jwt token, KeycloakId organizationId ) {
        Set<OrganizationUserRoleV2> roles = new HashSet<>();
        Map<String, Object> claims = token.getClaims();
        if ( claims.containsKey( "organizations" ) ) {
            @SuppressWarnings( "unchecked" )
            Map<String, Map<String, List<String>>> organizations = ( Map<String, Map<String, List<String>>> ) claims.get( "organizations" );
            if ( organizations.containsKey( organizationId.toString() ) ) {
                roles = organizations.get( organizationId.toString() )
                        .getOrDefault( "roles", Collections.emptyList() )
                        .stream()
                        .map( OrganizationUserRoleV2::fromName )
                        .collect( Collectors.toSet() );
            }
        }
        return roles;
    }
}