package org.empaia.utils;

import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.OrganizationMember;
import org.springframework.context.annotation.Configuration;


@Configuration
public class UserUtils {
    public static OrganizationMember makeOrganizationMemberEntity( KeycloakId organizationId, KeycloakId userId ) {
        return OrganizationMember.builder()
                .organizationId( organizationId )
                .userId( userId )
                .build();
    }
}
