package org.empaia.runners;

import lombok.extern.slf4j.Slf4j;
import org.empaia.configs.ConsistencyCheckConfiguration;
import org.empaia.services.v2.KeycloakConsistencyCheckService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class KeycloakMigrationRunner implements CommandLineRunner {
    private final KeycloakConsistencyCheckService keycloakConsistencyCheckService;
    private final ConsistencyCheckConfiguration configuration;


    public KeycloakMigrationRunner(
            KeycloakConsistencyCheckService keycloakConsistencyCheckService, ConsistencyCheckConfiguration configuration
    ) {
        this.keycloakConsistencyCheckService = keycloakConsistencyCheckService;
        this.configuration = configuration;
    }

    @Override
    public void run( String... args ) throws Exception {
        if ( !configuration.isRunKeycloakClientMigration() && !configuration.isRunKeycloakCleanup() ) {
            log.info( "Keycloak migration disabled. Set 'runKeycloakClientMigration' or 'runKeycloakCleanup' to trigger migration" );
            return;
        }

        log.info( "Running keycloak migration..." );
        keycloakConsistencyCheckService.migrateOrganizationClientsAndClientScopes(
                configuration.isRunKeycloakClientMigration(), configuration.isRunKeycloakCleanup()
        );
    }
}
