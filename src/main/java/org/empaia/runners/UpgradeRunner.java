package org.empaia.runners;

import lombok.extern.slf4j.Slf4j;
import org.empaia.configs.AuthServiceVersion;
import org.empaia.exceptions.generic.NotFoundException;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.enums.v2.OrganizationUserRoleV2;
import org.empaia.services.KeycloakService;
import org.empaia.services.v2.ModeratorService;
import org.empaia.utils.KeycloakUtils;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;


@Component
@Slf4j
public class UpgradeRunner implements CommandLineRunner {
    private final AuthServiceVersion authServiceVersion;
    private final KeycloakService keycloakService;
    private final ModeratorService moderatorService;

    public UpgradeRunner(
            AuthServiceVersion authServiceVersion,
            KeycloakService keycloakService,
            ModeratorService moderatorService
    ) {
        this.authServiceVersion = authServiceVersion;
        this.keycloakService = keycloakService;
        this.moderatorService = moderatorService;
    }

    @Override
    public void run( String... args ) throws Exception {
        UserRepresentation serviceAccountUser = keycloakService.getAuthServiceClientServiceAccountUser();
        var attributes = serviceAccountUser.getAttributes();
        if ( attributes == null ) {
            attributes = new HashMap<>();
        }
        String authServiceVersionKey = "auth_service_version";
        var storedVersion = KeycloakUtils.getAttributeValue( attributes, authServiceVersionKey, null );
        if ( storedVersion == null || authServiceVersion.isPreviousVersion( storedVersion ) ) {
            log.info( "Auth service version was incremented: " +
                    storedVersion + " -> " + authServiceVersion.getVersion() + ". Running upgrade..." );
            runUpgrades( storedVersion );
            attributes.put( authServiceVersionKey, Collections.singletonList( authServiceVersion.getVersion() ) );
            serviceAccountUser.setAttributes( attributes );
            UserResource userResource = keycloakService.getUserById( serviceAccountUser.getId() );
            userResource.update( serviceAccountUser );
        } else {
            log.info( "Auth service version is up to date: " +
                    storedVersion + " == " + authServiceVersion.getVersion() + "." );
        }
    }

    private void runUpgrades( String storedVersion ) {
        var versions = authServiceVersion.getVersionUntilCurrentVersion( storedVersion == null ? "0.0.0" : storedVersion );
        if ( versions.contains( "0.1.3" ) ) {
            log.info( " - v0.1.3: keycloak data: adding 'marketplace_service_client' as allowed audience to all " +
                    "APP_MAINTAINER sub groups of all organization groups." );
            addAudienceToUserRoleGroupOfAllOrganizations( OrganizationUserRoleV2.APP_MAINTAINER, "marketplace_service_client" );
        }
        if ( versions.contains( "0.1.5" ) ) {
            log.info( " - v0.1.5:" );
            log.info( "    keycloak data: adding all organization client service accounts as members to their organization keycloak groups" );
            log.info( "    removing organization group membership protocol mapper" );
            addExistingServiceClientAccountsToOrganizationGroups();
            removeOrganizationGroupMembershipProtocolMapper();
        }
        if ( versions.contains( "0.1.8" ) ) {
            log.info( " - v0.1.8: keycloak data: adding 'marketplace_service_client' as allowed audience to all " +
                    "CLEARANCE_MAINTAINER sub groups of all organization groups." );
            addAudienceToUserRoleGroupOfAllOrganizations( OrganizationUserRoleV2.CLEARANCE_MAINTAINER, "marketplace_service_client" );
        }
        if ( versions.contains( "0.1.11" ) ) {
            log.info( " - v0.1.11: upgrade group names and CLEARANCE_MAINTAINER again, in case there was no previous migration (name has changed)" );
            upgradeUserGroupNames();
            addAudienceToUserRoleGroupOfAllOrganizations( OrganizationUserRoleV2.CLEARANCE_MAINTAINER, "marketplace_service_client" );
        }
    }

    private void addAudienceToUserRoleGroupOfAllOrganizations( OrganizationUserRoleV2 userRole, String audience ) {
        var groups = keycloakService.getGroups();
        for ( GroupRepresentation group : groups ) {
            try {
                moderatorService.addAllowedAudienceFromOrganizationUserRoleGroup(
                        new KeycloakId( group.getId() ), userRole, new HashSet<>( Collections.singletonList( audience ) )
                );
            } catch ( NotFoundException ex ) {
                // skip groups missing user roles; migration should not fail
            }
        }
    }

    private void upgradeUserGroupNames() {
        var groups = keycloakService.getGroups();
        for ( GroupRepresentation group : groups ) {
            keycloakService.renameSubGroupName( group.getId(), "Manager", "manager" );
            keycloakService.renameSubGroupName( group.getId(), "Data Manager", "data-manager" );
            keycloakService.renameSubGroupName( group.getId(), "App Maintainer", "app-maintainer" );
            keycloakService.renameSubGroupName( group.getId(), "Pathologist", "pathologist" );
            keycloakService.renameSubGroupName( group.getId(), "Clearance Maintainer", "clearance-maintainer" );
        }
    }

    private void addExistingServiceClientAccountsToOrganizationGroups() {
        var clientsResource = keycloakService.getClientsResource();
        var groups = keycloakService.getGroups();
        for ( GroupRepresentation groupRepresentation : groups ) {
            var groupResource = keycloakService.getGroupResource( groupRepresentation.getId() );
            var memberIds = groupResource.members().stream().map( UserRepresentation::getId ).collect( Collectors.toSet() );
            KeycloakId organizationId = new KeycloakId( groupRepresentation.getId() );

            Set<ClientRepresentation> clients = keycloakService.getAllClientsOfOrganization( organizationId );
            for ( ClientRepresentation client : clients ) {
                UserRepresentation user = keycloakService.getClientServiceAccountUser( new KeycloakId( client.getId() ) );
                addServiceClientAccountToOrganizationGroup( client, user, memberIds, organizationId );
            }
        }
    }

    private void addServiceClientAccountToOrganizationGroup(
            ClientRepresentation clientRepresentation, UserRepresentation serviceAccount, Set<String> groupMemberIds, KeycloakId organizationId
    ) {
        if ( clientRepresentation.isServiceAccountsEnabled() ) {
            if ( !groupMemberIds.contains( serviceAccount.getId() ) ) {
                UserResource userResource = keycloakService.getUserById( serviceAccount.getId() );
                userResource.joinGroup( organizationId.toString() );
            }
        }
    }

    private void removeOrganizationGroupMembershipProtocolMapper() {
        keycloakService.removeProtocolMapperFromClientScope( "organization", "Organisation Mapper" );
    }
}

