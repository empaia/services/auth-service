package org.empaia.runners;

import lombok.extern.slf4j.Slf4j;
import org.empaia.configs.ConsistencyCheckConfiguration;
import org.empaia.services.v2.KeycloakConsistencyCheckService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class KeycloakConsistencyCheckRunner implements CommandLineRunner {
    private final KeycloakConsistencyCheckService keycloakConsistencyCheckService;
    private final ConsistencyCheckConfiguration configuration;

    public KeycloakConsistencyCheckRunner(
            KeycloakConsistencyCheckService keycloakConsistencyCheckService,
            ConsistencyCheckConfiguration configuration
    ) {
        this.keycloakConsistencyCheckService = keycloakConsistencyCheckService;
        this.configuration = configuration;
    }

    @Override
    public void run( String... args ) throws Exception {
        log.info( "####################################################################" );
        log.info( "#                  Keycloak Consistency Check                      #" );
        log.info( "####################################################################" );

        log.info( "Running Keycloak consistency checks..." );

        // check global configurations
        keycloakConsistencyCheckService.checkEmpaiaRealmExists();
        keycloakConsistencyCheckService.checkGlobalUserRoles();

        // check unapproved users and organizations and removes old, dangling instances
        keycloakConsistencyCheckService.checkNonVerifiedUserAccounts( configuration.isRunKeycloakCleanup() );
        keycloakConsistencyCheckService.checkUnapprovedOrganizations( configuration.isRunKeycloakCleanup() );

        // check unused (legacy) clients, scopes and service accounts
        // cleans up unused clients, scopes and service accounts
        keycloakConsistencyCheckService.checkUnusedClientScopes( configuration.isRunKeycloakCleanup() );
        keycloakConsistencyCheckService.checkUnusedClientsAndRelatedUserAccounts( configuration.isRunKeycloakCleanup() );

        // check client and client scope configurations
        keycloakConsistencyCheckService.validateAllClients();

        log.info( "####################################################################" );
    }
}
