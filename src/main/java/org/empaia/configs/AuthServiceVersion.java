package org.empaia.configs;


import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.*;
import java.util.stream.Collectors;

public class AuthServiceVersion {

    private final String fullVersion;

    public AuthServiceVersion( ApplicationContext context ) {
        fullVersion = context.getBeansWithAnnotation(SpringBootApplication.class)
                .entrySet().stream().findFirst().flatMap(es -> {
                    final String implementationVersion = es.getValue().getClass().getPackage().getImplementationVersion();
                    return Optional.ofNullable(implementationVersion);
                }).orElse("unknown");
    }

    public boolean isPreviousVersion( String version ) {
        return isPreviousVersion( version, fullVersion );
    }

    public static boolean isPreviousVersion( String version, String currentVersion ) {
        var actualVersion = splitVersion(currentVersion);
        var givenVersion = splitVersion(version);
        assert actualVersion.size() == 3;
        assert givenVersion.size() == 3;
        boolean isMajorLess = givenVersion.get(0) < actualVersion.get(0);
        boolean isMinorLess = givenVersion.get(1) < actualVersion.get(1);
        boolean isPatchLess = givenVersion.get(2) < actualVersion.get(2);
        boolean isMajorEqual = givenVersion.get(0).equals(actualVersion.get(0));
        boolean isMinorEqual = givenVersion.get(1).equals(actualVersion.get(1));
        return isMajorLess || isMajorEqual && (isMinorLess || isMinorEqual && isPatchLess);
    }

    public static List<Integer> splitVersion( String version ) {
        return Arrays.stream(version.split("\\.")).map(
                Integer::parseInt
        ).collect(Collectors.toList());
    }

    public String getVersion() {
        return fullVersion;
    }

    public List<String> getVersionUntilCurrentVersion(String previousVersion ) {
        return getVersionUntilCurrentVersion( previousVersion, fullVersion );
    }

    public static List<String> getVersionUntilCurrentVersion(String previousVersion, String currentVersion ) {
        var actualVersion = splitVersion( currentVersion );
        var givenVersion = splitVersion(previousVersion);
        assert actualVersion.size() == 3;
        assert givenVersion.size() == 3;
        List<String> versions = new ArrayList<>();
        if ( isPreviousVersion( previousVersion, currentVersion ) ) {
            int majorStart = givenVersion.get(0);
            int minorStart = givenVersion.get(1);
            int patchStart = givenVersion.get(2);
            int majorEnd = actualVersion.get(0);
            int minorEnd = actualVersion.get(1);
            int patchEnd = actualVersion.get(2);

            // Temporary fix to include minor/patch versions up to 99
            for (int i = majorStart; i <= majorEnd; i++) {
                int minStart = (i == majorStart) ? minorStart : 0;
                int minEnd = (i == majorEnd) ? minorEnd : 99;
                for (int j = minStart; j <= minEnd; j++) {
                    int patStart = (i == majorStart && j == minorStart) ? patchStart + 1 : 0;
                    int patEnd = (i == majorEnd && j == minorEnd) ? patchEnd : 99;
                    for (int k = patStart; k <= patEnd; k++) {
                        versions.add(i + "." + j + "." + k);
                    }
                }
            }
        }
        return versions;
    }
}
