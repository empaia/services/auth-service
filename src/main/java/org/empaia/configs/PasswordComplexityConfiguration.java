package org.empaia.configs;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Data
@Component
@ConfigurationProperties( prefix = "password-complexity" )
public class PasswordComplexityConfiguration {
    private int minimumLength;
    private int maximumLength;
    private int minimumSpecialChars;
    private int minimumDigits;
    private int minimumUppercase;
    private int minimumLowercase;
}
