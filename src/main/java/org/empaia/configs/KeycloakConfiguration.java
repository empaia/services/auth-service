package org.empaia.configs;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class KeycloakConfiguration {

    public final KeycloakInstanceConfiguration configuration;

    @Autowired
    public KeycloakConfiguration( KeycloakInstanceConfiguration configuration ) {
        this.configuration = configuration;
    }

    @Bean
    public KeycloakBuilder keycloakBuilder() {
        return KeycloakBuilder.builder();
    }

    @Bean
    public Keycloak keycloak() {
        return KeycloakBuilder.builder()
                .serverUrl( configuration.getServerUrl() )
                .realm( configuration.getRealm() )
                .grantType( OAuth2Constants.CLIENT_CREDENTIALS )
                .clientId( configuration.getClientId() )
                .clientSecret( configuration.getClientSecret() )
                .resteasyClient( (( ResteasyClientBuilder ) ResteasyClientBuilder.newBuilder()).connectionPoolSize( 10 ).build() )
                .build();
    }
}
