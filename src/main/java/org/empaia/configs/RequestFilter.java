package org.empaia.configs;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.util.ContentCachingRequestWrapper;


import java.io.IOException;

public class RequestFilter implements Filter {

    @Override
    public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException, ServletException {
        request = new ContentCachingRequestWrapper( (HttpServletRequest) request );
        chain.doFilter( request, response );
    }
}
