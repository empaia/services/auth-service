package org.empaia.configs;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.*;
import io.swagger.v3.oas.models.servers.Server;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class SwaggerConfiguration {
    static {
        // By default, springdoc inlines enums in schemas. For the frontend data generator we want separate
        // enum schemas that are referenced by other schemas. This enables that.
        // (see also https://springdoc.org/faq.html#_how_can_i_add_reusable_enums)
        io.swagger.v3.core.jackson.ModelResolver.enumsAsRef = true;
    }

    final KeycloakInstanceConfiguration keycloakConfiguration;

    @Value( "${empaia.swagger-auth-url}" )
    String authUrl;

    @Value( "${empaia.swagger-server-url}" )
    String serverUrl;

    @Value( "${empaia.swagger-title-suffix}" )
    String titleSuffix;

    @Autowired
    public SwaggerConfiguration( KeycloakInstanceConfiguration configuration ) {
        this.keycloakConfiguration = configuration;
    }

    @Bean
    public GroupedOpenApi v2Api() {
        String[] paths = {"/api/v2/**"};
        String[] packagesToScan = {"org.empaia"};
        return GroupedOpenApi.builder()
                .group( "v2" )
                .pathsToMatch( paths )
                .packagesToScan( packagesToScan )
                .build();
    }

    public OpenApiCustomizer buildOpenApiCustomizer(String apiVersion) {
        return openApi -> openApi
                .info( new Info().title( "Empaia Portal Backend - " + titleSuffix + " Version: " + apiVersion )
                        .description( "The Empaia portal backend OpenAPI definition for " + apiVersion )
                        .version( "v0.0.3" ) )
                .servers( getServers() )
                .addSecurityItem( new SecurityRequirement().addList( "bearerAuth" ) )
                .components( getScheme() );
    }

    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI().info( new Info().title( "Empaia Portal Backend - " + titleSuffix )
                        .description( "The Empaia portal backend OpenAPI definition" )
                        .version( "v0.0.3" ) )
                .servers( getServers() )
                .addSecurityItem( new SecurityRequirement().addList( "bearerAuth" ) )
                .components( getScheme() );
    }

    private List<Server> getServers() {
        ArrayList<Server> servers = new ArrayList<>();

        servers.add( new Server().url( serverUrl ).description( titleSuffix ) );

        return servers;
    }

    private Components getScheme() {
        return new Components().addSecuritySchemes( "bearerAuth",
                new SecurityScheme().type( SecurityScheme.Type.OAUTH2 )
                        .scheme( "oauth2" )
                        .in( SecurityScheme.In.HEADER )
                        .name( "Authorization" )
                        .bearerFormat( "JWT" )
                        .flows( getFlows() )
        );
    }

    private OAuthFlows getFlows() {
        try {
            String authServerUrl = authUrl;
            if ( !authServerUrl.endsWith("/") ) { authServerUrl += "/"; }
            String realm = keycloakConfiguration.getRealm();

            return new OAuthFlows()
                    .authorizationCode(
                            new OAuthFlow()
                                    .authorizationUrl( authServerUrl + "realms/" + realm + "/protocol/openid-connect/auth" )
                                    .scopes( getScopes() )
                                    .tokenUrl( authServerUrl + "realms/" + realm + "/protocol/openid-connect/token" )
                    );
        } catch ( Exception e ) {
            return null;
        }
    }

    private Scopes getScopes() {
        return new Scopes()
                .addString( "address", "OpenID Connect built-in scope: address" )
                .addString( "email", "OpenID Connect built-in scope: email" )
                .addString( "microprofile-jwt", "Microprofile - JWT built-in scope" )
                .addString( "organization", "Organization mapping scope" )
                .addString( "offline_access", "OpenID Connect built-in scope: offline_access" )
                .addString( "phone", "OpenID Connect built-in scope: phone" )
                .addString( "profile", "OpenID Connect built-in scope: profile" )
                .addString( "roles", "OpenID Connect scope for add user roles to the access token" );
    }
}
