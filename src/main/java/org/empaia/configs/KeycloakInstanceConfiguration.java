package org.empaia.configs;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Data
@Component
@ConfigurationProperties( prefix = "keycloak-instance" )
public class KeycloakInstanceConfiguration {

    private String serverUrl;

    private String realm;

    private String clientId;

    private String clientSecret;
}
