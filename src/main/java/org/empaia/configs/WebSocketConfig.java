package org.empaia.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@CrossOrigin
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    // EP for client registration
    @Override
    public void registerStompEndpoints( StompEndpointRegistry registry ) {
        registry.addEndpoint( "/api/websockets" ).setAllowedOrigins( "*" );
    }

    // EP for client topic subscription
    @Override
    public void configureMessageBroker( MessageBrokerRegistry registry ) {
        registry.enableSimpleBroker( "/topic/", "/queue/" );
        registry.setApplicationDestinationPrefixes( "/app" );
        // Add more channels if needed
    }
}
