package org.empaia.configs;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

@ControllerAdvice
public class FileUploadExceptionAdvice {

    @ExceptionHandler( MaxUploadSizeExceededException.class )
    public ResponseEntity<String> handleMaxUploadSizeExceededException( Exception ex ) {
        return ResponseEntity.status( HttpStatus.PAYLOAD_TOO_LARGE )
                             .contentType( MediaType.TEXT_PLAIN )
                             .body( "File too big" );
    }
}