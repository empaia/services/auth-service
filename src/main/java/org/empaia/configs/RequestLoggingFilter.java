package org.empaia.configs;

import jakarta.servlet.*;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Arrays;

@Slf4j
public class RequestLoggingFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        log.info("--------------------------------------------------------------------");
        log.info("Request scheme:{}", servletRequest.getScheme());
        log.info("Request server name and port: {}:{}", servletRequest.getServerName(), servletRequest.getServerPort());
        StringBuilder paramMap = new StringBuilder();
        servletRequest.getParameterMap()
                .forEach((key, value) -> paramMap.append("[Key: ").append(key).append(" | Value: ").append(Arrays.toString(value)).append("] "));
        log.info("Request parameter map:{}", paramMap);
        log.info("--------------------------------------------------------------------");
        filterChain.doFilter(servletRequest, servletResponse);
    }
}