package org.empaia.configs;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfiguration {
// might be useful for debugging
//    @Bean
//    public FilterRegistrationBean<RequestLoggingFilter> requestLoggingFilter(){
//        FilterRegistrationBean<RequestLoggingFilter> registrationBean = new FilterRegistrationBean<>();
//        registrationBean.setFilter( new RequestLoggingFilter() );
//        registrationBean.setName( "requestLoggingFilter" );
//        registrationBean.addUrlPatterns( "/api/*" );
//        registrationBean.setOrder( 2 );
//
//        return registrationBean;
//    }

    @Bean
    public FilterRegistrationBean<RequestFilter> requestFilter(){
        FilterRegistrationBean<RequestFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter( new RequestFilter() );
        registrationBean.setName( "requestFilter" );
        registrationBean.addUrlPatterns( "/api/*" );
        registrationBean.setOrder( 1 );

        return registrationBean;
    }
}
