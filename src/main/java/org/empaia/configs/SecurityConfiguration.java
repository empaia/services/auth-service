package org.empaia.configs;

import org.empaia.security.AdminUserRoleAuthorizationManager;
import org.empaia.security.ModeratorUserRoleAuthorizationManager;
import org.empaia.security.UserIdAndAudienceAuthorizationManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;


@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class SecurityConfiguration {

    @Value( "${spring.security.oauth2.resourceserver.jwt.jwk-set-uri}" )
    private String jwkSetUri;

    @Bean
    public SecurityFilterChain securityFilterChain( HttpSecurity http ) throws Exception {
        http
                .csrf( AbstractHttpConfigurer::disable )
                .cors( cors -> cors.configurationSource( corsConfigurationSource() ) )
                .authorizeHttpRequests( authorize -> authorize
                        .requestMatchers(
                                "/api/v2/domain/**",
                                "/api/v2/public/**",
                                "/docs/**",
                                "/favicon.ico",
                                "/protocol/**",
                                "/v3/api-docs/**",
                                "/api/websockets/**"
                        ).permitAll()
                        .requestMatchers(
                                "/api/v2/admin/**"
                        ).access( new AdminUserRoleAuthorizationManager() )
                        .requestMatchers(
                                "/api/v2/moderator/**"
                        ).access( new ModeratorUserRoleAuthorizationManager() )
                        .requestMatchers(
                                "/api/v2/admin/**",
                                "/api/v2/moderator/**",
                                "/api/v2/manager/**",
                                "/api/v2/my/**",
                                "/api/v2/organization/**"
                        ).access( new UserIdAndAudienceAuthorizationManager() )
                        .anyRequest().authenticated()
                )
                .sessionManagement( session -> session.sessionCreationPolicy( SessionCreationPolicy.STATELESS ) )
                .oauth2ResourceServer( ( oauth2 ) -> oauth2.jwt( jwt -> jwt.jwkSetUri( jwkSetUri ) ) );
        return http.build();
    }

    CorsConfigurationSource corsConfigurationSource() {
        final var configuration = new CorsConfiguration();
        configuration.setAllowedOriginPatterns( List.of( "*" ) );
        configuration.setAllowedMethods( List.of( "*" ) );
        configuration.setAllowedHeaders( List.of( "*" ) );
        configuration.setExposedHeaders( List.of( "Location", "location" ) );
        configuration.setAllowCredentials( false );

        final var source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration( "/**", configuration );

        return source;
    }
}