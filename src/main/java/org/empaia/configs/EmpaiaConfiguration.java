package org.empaia.configs;

import com.jlefebure.spring.boot.minio.MinioConfigurationProperties;
import com.jlefebure.spring.boot.minio.MinioService;
import io.minio.MinioClient;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidPortException;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Data
@Component
@ConfigurationProperties( prefix = "empaia" )
public class EmpaiaConfiguration {
    public static final String ORGANIZATION_HEADER_V2 = "organization-id";
    public static final String USER_HEADER_V2 = "user-id";

    private String environment;
    private String serverUrl;
    private String frontendAuthUri;
    private String tokenSecret;

    @Value( "${spring.minio.bucket}" )
    private String bucketName;
    @Value( "${spring.minio.url}" )
    private String minioInternalURL;
    @Value( "${spring.minio.access-key}" )
    private String minioAccessKey;
    @Value( "${spring.minio.secret-key}" )
    private String minioSecretKey;

    @Bean
    AuthServiceVersion getAuthServiceVersion( ApplicationContext context ){
        return new AuthServiceVersion( context );
    }

    @Bean
    MinioConfigurationProperties getMinioConfiguration() {
        MinioConfigurationProperties properties = new MinioConfigurationProperties();
        properties.setUrl(minioInternalURL);
        properties.setAccessKey(minioAccessKey);
        properties.setSecretKey(minioSecretKey);
        properties.setBucket(bucketName);
        return properties;
    }

    @Bean
    MinioService getMinioService() {
        return new MinioService();
    }

    @Bean
    MinioClient getMinioClient() throws InvalidPortException, InvalidEndpointException {
        return new MinioClient( minioInternalURL, minioAccessKey, minioSecretKey );
    }
}
