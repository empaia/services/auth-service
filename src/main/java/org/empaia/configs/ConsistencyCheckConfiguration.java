package org.empaia.configs;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.List;

@Primary
@Data
@Component
public class ConsistencyCheckConfiguration {
    @Value( "${consistency.run-keycloak-client-migration}" )
    private boolean runKeycloakClientMigration;
    @Value( "${consistency.run-keycloak-cleanup}" )
    private boolean runKeycloakCleanup;
    @Value( "${consistency.portal-client-valid-redirect-uris}" )
    private List<String> portalClientValidRedirectUris;
    @Value( "${consistency.portal-client-valid-web-origins}" )
    private List<String> portalClientValidWebOrigins;
}
