package org.empaia.security.organization;

import org.empaia.models.enums.v2.OrganizationUserRoleV2;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention( RetentionPolicy.RUNTIME )
@Target( { ElementType.METHOD } )
public @interface AuthorizeOrganizationV2 {

    boolean readOrganizationIdFromPath() default true; // if false, the organization id is read from the header

    OrganizationUserRoleV2[] organizationUserRoles() default {};

    boolean allowCreatorOfUnapprovedOrganization() default false;

    boolean allowAnyMember() default false;
}