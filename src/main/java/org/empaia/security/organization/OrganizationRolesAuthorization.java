package org.empaia.security.organization;

import lombok.extern.slf4j.Slf4j;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.OrganizationMember;
import org.empaia.models.enums.v2.OrganizationUserRoleV2;
import org.empaia.services.KeycloakService;
import org.empaia.services.v2.MemberService;
import org.keycloak.representations.idm.GroupRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@Slf4j
public class OrganizationRolesAuthorization {

    private final MemberService memberService;
    private final KeycloakService keycloakService;

    @Autowired
    public OrganizationRolesAuthorization( MemberService memberService, KeycloakService keycloakService ) {
        this.memberService = memberService;
        this.keycloakService = keycloakService;
    }

    public void authorizeV2API(
            Jwt token, KeycloakId organizationId, OrganizationUserRoleV2[] organizationUserRoles,
            boolean allowCreatorOfUnapprovedOrganization, boolean allowAnyMember
    ) {
        boolean isAuthorized = false;
        if ( allowAnyMember ) {
            isAuthorized = isOrganizationMemberV2API( token, organizationId );
        }
        if ( !isAuthorized && organizationUserRoles.length > 0 ) {
            isAuthorized = hasAnyOrganizationUserRoleV2API( token, organizationId, organizationUserRoles );
        }
        if ( !isAuthorized && allowCreatorOfUnapprovedOrganization ) {
            isAuthorized = memberService.isCreatorOfUnapprovedOrganization(
                    OrganizationMember.builder()
                            .organizationId( organizationId )
                            .userId( new KeycloakId( token.getSubject(), true ) )
                            .build()
            );
        }
        if ( !isAuthorized ) {
            throw new AccessDeniedException( "Your user account is not allowed to do this" );
        }
    }

    private boolean isOrganizationMemberV2API( Jwt token, KeycloakId organizationId ) {
        boolean isMember = false;
        Map<String, Object> claims = token.getClaims();
        if ( claims.containsKey( "organizations" ) ) {
            Optional<GroupRepresentation> organizationGroup = keycloakService.getGroup( organizationId.toString() );
            if ( organizationGroup.isPresent() ) {
                @SuppressWarnings( "unchecked" )
                Map<String, Map<String, List<String>>> organizations = ( Map<String, Map<String, List<String>>> ) claims.get( "organizations" );
                if ( organizations.containsKey( organizationId.toString() ) ) {
                    isMember = true;
                }
            }
        }
        return isMember;
    }

    private boolean hasAnyOrganizationUserRoleV2API( Jwt token, KeycloakId organizationId, OrganizationUserRoleV2[] organizationUserRoles ) {
        boolean hasAnyOrganizationUserRole = false;
        Map<String, Object> claims = token.getClaims();
        if ( claims.containsKey( "organizations" ) ) {
            hasAnyOrganizationUserRole = hasAnyOrganizationUserRoleV2API( claims, organizationId.toString(), organizationUserRoles );
        }
        return hasAnyOrganizationUserRole;
    }

    private boolean hasAnyOrganizationUserRoleV2API(
            Map<String, Object> claims, String organizationId, OrganizationUserRoleV2[] organizationUserRoles ) {
        List<String> allowedOrganizationUserRolesNames = Arrays.stream( organizationUserRoles )
                .map( OrganizationUserRoleV2::getName ).toList();
        boolean hasAnyOrganizationUserRole = false;
        Optional<GroupRepresentation> organizationGroup = keycloakService.getGroup( organizationId );
        if ( organizationGroup.isPresent() ) {
            @SuppressWarnings( "unchecked" )
            Map<String, Map<String, List<String>>> organizations = ( Map<String, Map<String, List<String>>> ) claims.get( "organizations" );
            Map<String, List<String>> organizationRolesMap = organizations.getOrDefault( organizationId, Collections.emptyMap() );
            List<String> actualRoles = organizationRolesMap.getOrDefault( "roles", Collections.emptyList() );
            for ( String actualRole : actualRoles ) {
                if ( allowedOrganizationUserRolesNames.contains( actualRole ) ) {
                    hasAnyOrganizationUserRole = true;
                    break;
                }
            }
        }
        return hasAnyOrganizationUserRole;
    }
}
