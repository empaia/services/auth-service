package org.empaia.security.organization;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.enums.v2.OrganizationUserRoleV2;
import org.empaia.utils.AuthorizationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Component;

import jakarta.servlet.http.HttpServletRequest;


@Aspect
@Component
@Configuration
@Slf4j
public class OrganizationRolesAllowedAspect {

    @Autowired
    private HttpServletRequest request;

    private final OrganizationRolesAuthorization organizationRolesAuthorization;

    public OrganizationRolesAllowedAspect( OrganizationRolesAuthorization organizationRolesAuthorization ) {
        this.organizationRolesAuthorization = organizationRolesAuthorization;
    }

    @Before( "@annotation(org.empaia.security.organization.AuthorizeOrganizationV2)" )
    public void authorizeOrganizationV2( JoinPoint joinPoint ) throws Throwable {
        AuthorizeOrganizationV2 annotation = AuthorizationUtils.getAuthorizeAnnotation(joinPoint,  AuthorizeOrganizationV2.class);

        Jwt token = AuthorizationUtils.getAccessToken(request);

        OrganizationUserRoleV2[] organizationUserRoles = annotation.organizationUserRoles();
        KeycloakId organizationId;
        if (annotation.readOrganizationIdFromPath()) {
            organizationId = AuthorizationUtils.getOrganizationIdFromPathParameters(request);
        } else {
            organizationId = AuthorizationUtils.getOrganizationKeycloakIdFromHeader(request);
        }
        organizationRolesAuthorization.authorizeV2API(
                token, organizationId, organizationUserRoles,
                annotation.allowCreatorOfUnapprovedOrganization(),
                annotation.allowAnyMember()
        );
    }


}
