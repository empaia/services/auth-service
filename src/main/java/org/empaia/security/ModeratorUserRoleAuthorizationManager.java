package org.empaia.security;

import org.empaia.utils.AuthorizationUtils;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.AuthorizationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.web.access.intercept.RequestAuthorizationContext;
import org.springframework.stereotype.Component;

import java.util.function.Supplier;

@Component
public final class ModeratorUserRoleAuthorizationManager implements AuthorizationManager<RequestAuthorizationContext> {

    @Override
    public AuthorizationDecision check(Supplier<Authentication> authentication, RequestAuthorizationContext context) throws AccessDeniedException {
        Jwt accessToken = AuthorizationUtils.getAccessToken( context.getRequest() );
        return new AuthorizationDecision( AuthorizationUtils.hasGlobalRole( accessToken, "moderator" ) );
    }
}
