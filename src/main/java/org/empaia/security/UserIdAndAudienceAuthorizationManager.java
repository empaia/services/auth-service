package org.empaia.security;

import org.empaia.utils.AuthorizationUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.AuthorizationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.web.access.intercept.RequestAuthorizationContext;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Supplier;

@Component
public final class UserIdAndAudienceAuthorizationManager implements AuthorizationManager<RequestAuthorizationContext> {

    @Value( "${empaia.auth-audience}" )
    private String authAudience;

    @Override
    public AuthorizationDecision check( Supplier<Authentication> authentication, RequestAuthorizationContext context ) throws AccessDeniedException {
        String userId = AuthorizationUtils.getUserKeycloakIdFromHeader( context.getRequest() ).toString();
        Jwt accessToken = AuthorizationUtils.getAccessToken( context.getRequest() );

        boolean isAuthenticated = true;

        List<String> audiences = accessToken.getClaimAsStringList( "aud" );
        if ( authAudience != null && !authAudience.isEmpty() && !audiences.contains( authAudience ) ) {
            isAuthenticated = false;
        }

        if ( !userId.equalsIgnoreCase( accessToken.getClaimAsString( "sub" ) ) ) {
            isAuthenticated = false;
        }

        return new AuthorizationDecision( isAuthenticated );
    }
}
