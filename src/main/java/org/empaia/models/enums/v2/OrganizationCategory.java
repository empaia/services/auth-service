package org.empaia.models.enums.v2;

import lombok.Getter;

public enum OrganizationCategory {
    APP_CUSTOMER( "APP_CUSTOMER" ),
    APP_VENDOR( "APP_VENDOR" ),
    // category does not have any side effects for now; is used to indicate an organization is a compute provider
    COMPUTE_PROVIDER( "COMPUTE_PROVIDER" ),
    PRODUCT_PROVIDER( "PRODUCT_PROVIDER" );

    @Getter
    private final String name;

    OrganizationCategory( String name ) { this.name  = name; }

    public static OrganizationCategory fromName( String name ) {
        for ( OrganizationCategory category : OrganizationCategory.values() ) {
            if ( name.equals( category.getName() ) ) {
                return category;
            }
        }
        throw new EnumConstantNotPresentException( OrganizationCategory.class, name );
    }
}
