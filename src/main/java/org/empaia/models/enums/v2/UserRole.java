package org.empaia.models.enums.v2;

import lombok.Getter;

import java.lang.EnumConstantNotPresentException;


public enum UserRole {
    MODERATOR( "MODERATOR" ),
    EMPAIA_INTERNATIONAL_ASSOCIATE ( "EMPAIA-INTERNATIONAL-ASSOCIATE" );

    @Getter
    private final String name;

    UserRole( String name ) { this.name = name; }

    public static UserRole fromValue( String value ) {
        for ( UserRole role : UserRole.values() ) {
            if ( value.equalsIgnoreCase( role.getName() ) ) {
                return role;
            }
        }
        throw new EnumConstantNotPresentException( UserRole.class, value );
    }
}
