package org.empaia.models.enums.v2;

import lombok.Getter;

import java.util.Locale;

public enum Notification {
    USER_CREATES_ORGANIZATION(
            "Action required – new organization",
            "Aktion erforderlich – neue Organisation"
    ),
    ORGANIZATION_IS_APPROVED(
            "Approved – new organization",
            "Bestätigt – neue Organisation"
    ),
    ORGANIZATION_IS_REJECTED(
            "Rejected – new organization",
            "Abgelehnt – neue Organisation"
    ),
    USER_REQUESTS_ORGANIZATION_MEMBERSHIP(
            "Action required – organization membership",
            "Aktion erforderlich – Mitgliedschaft in Organisation"
    ),
    ORGANIZATION_MEMBERSHIP_IS_APPROVED(
            "Approved – organization membership",
            "Bestätigt – Mitgliedschaft in Organisation"
    ),
    ORGANIZATION_MEMBERSHIP_IS_REJECTED(
            "Rejected – organization membership",
            "Abgelehnt – Mitgliedschaft in Organisation"
    ),
    ORGANIZATION_MEMBER_REQUESTS_ROLE(
            "Action required – organization member role",
            "Aktion erforderlich – Rolle in Organisation"
    ),
    ORGANIZATION_MEMBER_ROLE_APPROVED(
            "Approved – organization member role",
            "Bestätigt – Rolle in Organisation"
    ),
    ORGANIZATION_MEMBER_ROLE_REJECTED(
            "Rejected – organization member role",
            "Abgelehnt – Rolle in Organisation"
    ),
    ORGANIZATION_MANAGER_UPDATES_USER_ROLE(
            "Organization membership role has been altered",
            "Rolle in Organisation wurde geändert"
    ),
    MODERATOR_DEACTIVATES_ACCOUNT(
            "Account deactivated",
            "Benutzerkonto deaktiviert"
    ),
    MANAGER_REQUESTS_ORGANIZATION_CATEGORY_CHANGE(
            "Action required – organization category",
            "Aktion erforderlich – Kategorie einer Organisation"
    ),
    ORGANIZATION_CATEGORY_CHANGE_APPROVED(
            "Approved – organization category",
            "Bestätigt – neue Kategorie einer Organisation"
    ),
    ORGANIZATION_CATEGORY_CHANGE_REJECTED(
            "Rejected – organization category",
            "Abgelehnt – Kategorie einer Organisation"
    );

    @Getter
    private final String subjectEN;
    @Getter
    private final String subjectDE;

    Notification( String subjectEN, String subjectDE ) {
        this.subjectEN = subjectEN;
        this.subjectDE = subjectDE;
    }

    public String getNotificationSubject(Locale locale) {
        if ( locale.equals( Locale.ENGLISH ) ) {
            return subjectEN;
        }
        if ( locale.equals( Locale.GERMAN ) ) {
            return subjectDE;
        }
        return subjectEN;
    }
}
