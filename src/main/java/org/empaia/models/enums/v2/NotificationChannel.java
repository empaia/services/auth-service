package org.empaia.models.enums.v2;

public enum NotificationChannel {
    SMTP_AMQP,
    SMTP,
    AMQP
}
