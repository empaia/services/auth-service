package org.empaia.models.enums.v2;

public enum RequestState {
    REQUESTED( 1 ),
    REVOKED( 2 ),
    ACCEPTED( 3 ),
    REJECTED( 4 );

    private final int value;

    RequestState( int value ) {
        this.value = value;
    }

    public static RequestState value( String value ) {
        return RequestState.value( Integer.parseInt( value ) );
    }

    public static RequestState value( int value ) {
        return switch ( value ) {
            case 1 -> REQUESTED;
            case 2 -> REVOKED;
            case 3 -> ACCEPTED;
            case 4 -> REJECTED;
            default ->
                    throw new EnumConstantNotPresentException( RequestState.class, Integer.valueOf( value ).toString() );
        };
    }

    public static RequestState fromName( String name ) {
        return switch ( name ) {
            case "REQUESTED" -> REQUESTED;
            case "REVOKED" -> REVOKED;
            case "ACCEPTED" -> ACCEPTED;
            case "REJECTED" -> REJECTED;
            default -> throw new EnumConstantNotPresentException( RequestState.class, name );
        };
    }

    public static boolean isNotFinal( RequestState state ) {
        return !state.equals( RequestState.ACCEPTED ) && !state.equals( RequestState.REJECTED ) && !state.equals( RequestState.REVOKED );
    }

    public int getValue() {
        return value;
    }
}
