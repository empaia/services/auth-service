package org.empaia.models.enums.v2;

public enum LanguageCode {
    DE, EN;

    public static LanguageCode fromString( String string ) {
        for ( LanguageCode languageCode : LanguageCode.values() ) {
            if ( string.equalsIgnoreCase( languageCode.toString() ) ) {
                return languageCode;
            }
        }
        throw new EnumConstantNotPresentException( LanguageCode.class, string );
    }
}
