package org.empaia.models.enums.v2;

import com.google.common.collect.ImmutableList;
import lombok.Getter;

import java.util.*;
import java.util.stream.Collectors;

@Getter
public enum ClientType {
    // CLIENT_NAME ( client short name, [ grant access to clients / audiences ] )

    // GLOBAL CLIENTS
    PORTAL_CLIENT( "portal_client", new String[]{ "event_service_client", "auth_service_client" } ),
    AUTH_SERVICE_CLIENT( "auth_service_client", new String[]{ "shout_out_service_client" } ),
    EVENT_SERVICE_CLIENT( "event_service_client", new String[]{} ),
    SHOUT_OUT_SERVICE_CLIENT( "shout_out_service_client", new String[]{} ),
    MARKETPLACE_SERVICE_CLIENT( "marketplace_service_client", new String[]{ "shout_out_service_client" } ),
    MEDICAL_DEVICE_COMPANION_CLIENT( "medical_device_companion_client", new String[]{ "medical_device_companion_client" } ),
    DEFAULT( "", new String[]{} ),

    // FRONTEND CLIENTS
    WORKBENCH_CLIENT( "wbc", new String[]{} ),
    DATA_MANAGEMENT_CLIENT( "dmc", new String[]{} ),

    // BACKEND CLIENTS
    WORKBENCH_SERVICE( "wbs", new String[]{ "mds", "idms", "jes" } ),
    MEDICAL_DATA_SERVICE( "mds", new String[]{} ),
    APP_SERVICE( "as", new String[]{ "mds" } ),
    UPLOAD_SERVICE( "us", new String[]{} ),
    ID_MAPPER_SERVICE( "idms", new String[]{} ),
    JOB_EXECUTION_SERVICE( "jes", new String[]{} ),
    COMPUTE_PROVIDER_JOB_EXECUTION_SERVICE( "cpjes", new String[]{} );

    private final String shortName;
    private final String[] grantedAccess;

    ClientType( String shortName, String[] grantedAccess ) {
        this.shortName = shortName;
        this.grantedAccess = grantedAccess;
    }

    // CLIENT GROUPS BASED ON TYPE

    @Getter
    private static final List<ClientType> frontendClients = ImmutableList.of(
            PORTAL_CLIENT,
            WORKBENCH_CLIENT,
            DATA_MANAGEMENT_CLIENT,
            MEDICAL_DEVICE_COMPANION_CLIENT
    );

    @Getter
    private static final List<ClientType> customGlobalClients = ImmutableList.of(
            PORTAL_CLIENT,
            AUTH_SERVICE_CLIENT,
            EVENT_SERVICE_CLIENT,
            SHOUT_OUT_SERVICE_CLIENT,
            MARKETPLACE_SERVICE_CLIENT,
            MEDICAL_DEVICE_COMPANION_CLIENT
    );


    // CLIENT GROUPS BASED ON CATEGORY

    @Getter
    private static final List<String> defaultClients = ImmutableList.of(
            "account", "account-console", "admin-cli", "broker", "realm-management", "security-admin-console", "swagger", "swagger_client"
    );

    @Getter
    private static final List<ClientType> customerClients = ImmutableList.of(
            WORKBENCH_SERVICE,
            WORKBENCH_CLIENT,
            MEDICAL_DATA_SERVICE,
            DATA_MANAGEMENT_CLIENT,
            APP_SERVICE,
            UPLOAD_SERVICE,
            ID_MAPPER_SERVICE,
            JOB_EXECUTION_SERVICE
    );
    @Getter
    private static final List<ClientType> computeProviderClients = ImmutableList.of(
            COMPUTE_PROVIDER_JOB_EXECUTION_SERVICE
    );
    @Getter
    private static final List<ClientType> vendorClients = ImmutableList.of();
    @Getter
    private static final List<ClientType> productProviderClients = ImmutableList.of();

    // CLIENT GROUPS BASED ON CONFIGURATIONS

    @Getter
    private static final List<ClientType> serviceAppReaderClients = ImmutableList.of(
            APP_SERVICE,
            WORKBENCH_SERVICE,
            JOB_EXECUTION_SERVICE,
            COMPUTE_PROVIDER_JOB_EXECUTION_SERVICE
    );
    @Getter
    private static final List<ClientType> enabledDirectAccessGrantClients = ImmutableList.of(
            AUTH_SERVICE_CLIENT
    );
    @Getter
    private static final List<ClientType> enabledStandardFlowClients = ImmutableList.of(
            PORTAL_CLIENT,
            WORKBENCH_CLIENT,
            DATA_MANAGEMENT_CLIENT,
            MEDICAL_DEVICE_COMPANION_CLIENT
    );
    @Getter
    private static final List<ClientType> enabledImplicitFlowClients = ImmutableList.of();
    @Getter
    private static final List<ClientType> enabledServiceAccountClients = ImmutableList.of(
            WORKBENCH_SERVICE,
            MEDICAL_DATA_SERVICE,
            APP_SERVICE,
            UPLOAD_SERVICE,
            ID_MAPPER_SERVICE,
            JOB_EXECUTION_SERVICE,
            COMPUTE_PROVIDER_JOB_EXECUTION_SERVICE,
            AUTH_SERVICE_CLIENT,
            EVENT_SERVICE_CLIENT,
            MARKETPLACE_SERVICE_CLIENT,
            SHOUT_OUT_SERVICE_CLIENT
    );

    public static ClientType fromShortName( String shortName ) {
        for ( ClientType type : ClientType.values() ) {
            if ( shortName.equals( type.getShortName() ) ) {
                return type;
            }
        }
        throw new EnumConstantNotPresentException( ClientType.class, shortName );
    }

    public static ClientType fromClientId( String clientId ) {
        if ( defaultClients.contains( clientId ) ) {
            return DEFAULT;
        }
        String[] clientIdParts = clientId.split( "\\." );
        if ( clientIdParts.length == 3 ) {
            return fromShortName( clientIdParts[2] );
        }
        if ( getCustomGlobalClients().stream().map( ClientType::getShortName ).collect( Collectors.toSet() ).contains( clientId ) ) {
            return fromShortName( clientId );
        }
        throw new EnumConstantNotPresentException( ClientType.class, clientId );
    }

    public static boolean isGlobalServiceClient( ClientType clientType ) {
        return customGlobalClients.contains( clientType ) && !frontendClients.contains( clientType );
    }

    public static boolean isOrganizationClient( ClientType type ) {
        return !customGlobalClients.contains( type ) && !type.equals( DEFAULT );
    }

    public static boolean isOrganizationFrontendClient( ClientType clientType ) {
        return frontendClients.contains( clientType ) && !customGlobalClients.contains( clientType ) && !clientType.equals( DEFAULT );
    }

    public static boolean isOrganizationServiceClient( ClientType clientType ) {
        return !frontendClients.contains( clientType ) && !customGlobalClients.contains( clientType ) && !clientType.equals( DEFAULT );
    }

    public static boolean isServiceAppReader( String client ) {
        return serviceAppReaderClients.stream().map( ClientType::getShortName ).collect( Collectors.toSet() ).contains( client );
    }

    public static List<String> getExpectedProtocolMappers( ClientType clientType ) {
        if ( isGlobalServiceClient( clientType ) ) {
            return Arrays.asList( clientType.grantedAccess );
        }
        return List.of();
    }

    public static Set<ClientType> getOrganizationClientsForCategories( List<OrganizationCategory> categories ) {
        Set<ClientType> clients = new HashSet<>();
        if ( categories.contains( OrganizationCategory.APP_CUSTOMER ) ) {
            clients.addAll( customerClients );
        }
        if ( categories.contains( OrganizationCategory.APP_VENDOR ) ) {
            clients.addAll( vendorClients );
        }
        if ( categories.contains( OrganizationCategory.COMPUTE_PROVIDER ) ) {
            clients.addAll( computeProviderClients );
        }
        if ( categories.contains( OrganizationCategory.PRODUCT_PROVIDER ) ) {
            clients.addAll( productProviderClients );
        }
        return clients;
    }
}