package org.empaia.models.enums.v2;

import lombok.Getter;

public enum PortalEvent {
    ORGANIZATION( "ORGANIZATION" ),
    UNAPPROVED_ORGANIZATION( "UNAPPROVED_ORGANIZATION" ),
    ROLE_REQUEST( "ROLE_REQUEST" ),
    MEMBERSHIP_REQUEST( "MEMBERSHIP_REQUEST" ),
    CATEGORY_REQUEST( "CATEGORY_REQUEST" );

    @Getter
    private final String eventName;

    PortalEvent( String eventName ) { this.eventName = eventName; }

    public static PortalEvent fromName( String eventName ) {
        for ( PortalEvent event : PortalEvent.values() ) {
            if ( eventName.equals( event.getEventName() ) ) {
                return event;
            }
        }
        throw new EnumConstantNotPresentException( PortalEvent.class, eventName );
    }
}
