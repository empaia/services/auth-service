package org.empaia.models.enums.v2;

import lombok.Getter;

public enum EventAction {
    CREATED( "CREATED" ),
    UPDATED( "UPDATED" ),
    REJECTED( "REJECTED" ),
    APPROVED( "APPROVED" );

    @Getter
    private final String eventAction;

    EventAction(String eventAction ) { this.eventAction = eventAction; }

    public static EventAction fromName(String eventReason ) {
        for ( EventAction action : EventAction.values() ) {
            if ( eventReason.equals( action.getEventAction() ) ) {
                return action;
            }
        }
        throw new EnumConstantNotPresentException( EventAction.class, eventReason );
    }
}
