package org.empaia.models.enums.v2;

import lombok.Getter;

@Getter
public enum OrganizationUserRoleV2 {
    MANAGER( "manager" ),
    PATHOLOGIST( "pathologist" ),
    APP_MAINTAINER( "app-maintainer" ),
    DATA_MANAGER( "data-manager" ),
    CLEARANCE_MAINTAINER( "clearance-maintainer" );

    private final String name;


    OrganizationUserRoleV2( String name ) {
        this.name  = name;
    }

    public static OrganizationUserRoleV2 fromName( String name ) {
        for ( OrganizationUserRoleV2 role : OrganizationUserRoleV2.values() ) {
            if ( name.equals( role.getName() ) ) {
                return role;
            }
        }
        throw new EnumConstantNotPresentException( OrganizationUserRoleV2.class, name );
    }
}
