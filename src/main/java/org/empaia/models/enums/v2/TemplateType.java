package org.empaia.models.enums.v2;

import lombok.Getter;

public enum TemplateType {
    HTML( "html"),
    TXT( "txt");

    @Getter
    final String type;

    TemplateType( String type ) {
        this.type = type;
    }
}
