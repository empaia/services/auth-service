package org.empaia.models.enums.v2;

public enum Sink {
    SMTP,
    AMQP
}
