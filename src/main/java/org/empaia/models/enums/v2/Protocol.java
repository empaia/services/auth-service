package org.empaia.models.enums.v2;

public class Protocol {

    public static final String MAPPER_GROUP_MEMBERSHIP = "oidc-group-membership-mapper";
    public static final String MAPPER_AUDIENCE = "oidc-audience-mapper";

    public static final String OPENID_CONNECT = "openid-connect";

    private Protocol() {

    }
}
