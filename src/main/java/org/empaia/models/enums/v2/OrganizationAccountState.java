package org.empaia.models.enums.v2;

import lombok.Getter;

public enum OrganizationAccountState {
    ACTIVE( 1 ),
    INACTIVE( 2 ),
    REQUIRES_ACTIVATION( 4 ),
    AWAITING_DEACTIVATION( 6 ),
    AWAITING_ACTIVATION( 7 );

    @Getter
    private final int value;

    OrganizationAccountState( int value ) {
        this.value = value;
    }

    public static OrganizationAccountState fromValue( int value ) {
        for ( OrganizationAccountState state : OrganizationAccountState.values() ) {
            if ( value == state.getValue() ) {
                return state;
            }
        }
        throw new EnumConstantNotPresentException( OrganizationAccountState.class, Integer.valueOf( value ).toString() );
    }

    public static OrganizationAccountState fromName( String name ) {
        return switch ( name ) {
            case "ACTIVE" -> ACTIVE;
            case "INACTIVE" -> INACTIVE;
            case "REQUIRES_ACTIVATION" -> REQUIRES_ACTIVATION;
            case "AWAITING_DEACTIVATION" -> AWAITING_DEACTIVATION;
            case "AWAITING_ACTIVATION" -> AWAITING_ACTIVATION;
            default -> throw new EnumConstantNotPresentException( OrganizationAccountState.class, name );
        };
    }

    public String toName() {
        return switch ( this ) {
            case ACTIVE -> "ACTIVE";
            case INACTIVE -> "INACTIVE";
            case REQUIRES_ACTIVATION -> "REQUIRES_ACTIVATION";
            case AWAITING_DEACTIVATION -> "AWAITING_DEACTIVATION";
            case AWAITING_ACTIVATION -> "AWAITING_ACTIVATION";
        };
    }
}
