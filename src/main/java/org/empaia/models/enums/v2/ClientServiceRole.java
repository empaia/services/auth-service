package org.empaia.models.enums.v2;

import lombok.Getter;

import java.util.HashSet;
import java.util.List;

@Getter
public enum ClientServiceRole {
    SERVICE_APP_READER( "service-app-reader", new String[]{ "marketplace_service_client" } ),
    SERVICE_APP_CONFIG_READER( "service-app-config-reader", new String[]{ "marketplace_service_client" } ),
    SERVICE_COMPUTE_PROVIDER( "service-compute-provider", new String[]{ "marketplace_service_client" } );

    private final String name;

    @Getter
    final HashSet<String> allowedAudiences;


    ClientServiceRole( String name, String[] allowedAudiences ) {
        this.name = name;
        this.allowedAudiences = new HashSet<>( List.of( allowedAudiences ) );
    }

    public static ClientServiceRole fromName( String name ) {
        for ( ClientServiceRole role : ClientServiceRole.values() ) {
            if ( name.equals( role.getName() ) ) {
                return role;
            }
        }
        throw new EnumConstantNotPresentException( ClientServiceRole.class, name );
    }

    public static boolean isClientServiceRole( String name ) {
        for ( ClientServiceRole role : ClientServiceRole.values() ) {
            if ( name.equals( role.getName() ) ) {
                return true;
            }
        }
        return false;
    }
}
