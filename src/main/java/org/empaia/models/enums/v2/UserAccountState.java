package org.empaia.models.enums.v2;

import lombok.Getter;

public enum UserAccountState {
    DISABLED( "DISABLED" ),
    ENABLED_AND_NO_ACTIONS_REQUIRED( "ENABLED_AND_NO_ACTIONS_REQUIRED" ),
    REQUIRES_EMAIL_VERIFICATION( "REQUIRES_EMAIL_VERIFICATION" ),
    REQUIRES_PROFILE_UPDATE( "REQUIRES_PROFILE_UPDATE" );

    @Getter
    private final String name;

    UserAccountState( String name ) {
        this.name = name;

    }

    public static UserAccountState fromName( String name ) {
        for ( UserAccountState state : UserAccountState.values() ) {
            if ( name.equals( state.getName() ) ) {
                return state;
            }
        }
        throw new EnumConstantNotPresentException( UserAccountState.class, name );
    }
}
