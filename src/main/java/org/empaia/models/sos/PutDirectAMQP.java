package org.empaia.models.sos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import jakarta.validation.constraints.NotNull;

@Data
@SuperBuilder
public class PutDirectAMQP {
    @NotNull
    private AMQPPayload data;

    @JsonProperty( "routing_key" )
    @NotNull
    private String routingKey;
}
