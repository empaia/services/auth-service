package org.empaia.models.sos;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import org.empaia.models.enums.v2.EventAction;

import jakarta.validation.constraints.NotNull;

@Data
@SuperBuilder
public class AMQPData {
    @NotNull
    EventAction action;
}
