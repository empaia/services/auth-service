package org.empaia.models.sos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.util.List;

@Data
@SuperBuilder
public class PostShoutOut {
    @JsonProperty( "recipient_id" )
    @NotNull
    private String recipientId;

    @JsonProperty( "timeout_interval" )
    @NotNull
    private int timeoutInterval;

    @NotNull
    @NotEmpty
    private List<PostMessage> messages;
}
