package org.empaia.models.sos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@EqualsAndHashCode( callSuper = true )
public class PostMessageAMQP extends PostMessage{
    @JsonProperty( "routing_key" )
    private String routingKey;
}
