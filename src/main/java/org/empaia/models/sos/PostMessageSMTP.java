package org.empaia.models.sos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import jakarta.validation.constraints.NotNull;

@Data
@SuperBuilder
@EqualsAndHashCode( callSuper = true )
public class PostMessageSMTP extends PostMessage {
    @NotNull
    private String to;

    @NotNull
    private String subject;

    @JsonProperty( "content_html" )
    private String contentHtml;
}
