package org.empaia.models.sos;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.models.enums.v2.Sink;

import jakarta.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@SuperBuilder
public class PostMessage {
    @NotNull
    private Sink sink;

    @NotNull
    private String content;
}
