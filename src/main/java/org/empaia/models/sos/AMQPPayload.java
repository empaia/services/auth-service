package org.empaia.models.sos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.SuperBuilder;
import org.empaia.models.enums.v2.PortalEvent;

import jakarta.validation.constraints.NotNull;

@Data
@SuperBuilder
public class AMQPPayload {
    @NotNull
    PortalEvent event;

    @NotNull
    @JsonProperty( "event_data" )
    AMQPData eventData;
}
