package org.empaia.models.dto.v2;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import org.empaia.models.auth.KeycloakId;

import jakarta.validation.constraints.NotNull;

@Data
@SuperBuilder
public class OrganizationMember {
    @NotNull
    KeycloakId organizationId;
    @NotNull
    KeycloakId userId;
}
