package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.enums.v2.OrganizationCategory;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.util.List;

import static org.empaia.utils.EntityLimits.MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH;

@Data
@NoArgsConstructor
@SuperBuilder
public class UnapprovedCommonOrganizationDataEntity {

    @JsonProperty( "organization_id" )
    @NotNull
    private KeycloakId organizationId;

    @JsonProperty( "organization_name" )
    @Size( max= MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH )
    private String organizationName;

    @JsonProperty( "logo_url" )
    private String logoUrl;

    @JsonProperty( "resized_logo_urls" )
    private ResizedPictureUrlsEntity resizedLogoUrls;


    private List<OrganizationCategory> categories;
}
