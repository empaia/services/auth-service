package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import jakarta.validation.constraints.NotNull;
import java.util.List;

@Data
@SuperBuilder
public class PublicOrganizationsEntity {

    @JsonProperty( "organizations" )
    @NotNull
    private List<PublicOrganizationEntity> organizations;

    @JsonProperty( "total_organizations_count" )
    @NotNull
    private int totalOrganizationsCount;
}
