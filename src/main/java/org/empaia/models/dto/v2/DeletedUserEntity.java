package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@SuperBuilder
public class DeletedUserEntity {
    @JsonProperty( "user_id" )
    @NotBlank
    private String userId;

    @JsonProperty( "deleted_by" )
    @NotBlank
    private String deleted_by;

    @JsonProperty( "deleted_on" )
    @NotBlank
    private Long deletedOn;
}
