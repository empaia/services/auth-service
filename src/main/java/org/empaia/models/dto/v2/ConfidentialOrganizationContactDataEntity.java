package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import org.empaia.utils.RegularExpressions;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.validators.ValidEmail;
import org.empaia.validators.ValidURL;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import static org.empaia.utils.EntityLimits.MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH;

@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode( callSuper = true )
public class ConfidentialOrganizationContactDataEntity extends AddressEntity {

    @JsonProperty( "email_address" )
    @NotNull
    @ValidEmail
    @Size( max = MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH )
    private String emailAddress;

    @JsonProperty( "phone_number" )
    @NotNull
    @Pattern( regexp = RegularExpressions.PHONE_NUMBER, message = "Invalid phone number" )
    @Size( max = MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH )
    private String phoneNumber;

    @JsonProperty( "department" )
    @Size( max = MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH  )
    private String department;

    @JsonProperty( "fax_number" )
    @Pattern( regexp = RegularExpressions.PHONE_NUMBER, message = "Invalid fax number" )
    @Size( max = MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH )
    private String faxNumber;

    @JsonProperty( "website" )
    @NotNull
    @ValidURL( allowEmpty = true )
    @Size( max = MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH  )
    private String website;

    @JsonProperty( "is_phone_number_public" )
    @NotNull
    private boolean isPhoneNumberPublic;

    @JsonProperty( "is_fax_number_public" )
    @NotNull
    private boolean isFaxNumberPublic;

    @JsonProperty( "is_email_address_public" )
    @NotNull
    private boolean isEmailAddressPublic;
}
