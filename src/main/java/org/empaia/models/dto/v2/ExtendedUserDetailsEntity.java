package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.models.auth.KeycloakId;
import org.empaia.validators.ValidEmail;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import static org.empaia.utils.EntityLimits.MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH;

@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode( callSuper = true )
public class ExtendedUserDetailsEntity extends UserDetailsEntity {

    @JsonProperty( "user_id" )
    @NotNull
    private KeycloakId userId;

    @JsonProperty( "email_address" )
    @NotNull( message = "email_address cannot be empty" )
    @ValidEmail
    @Size( max = MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH )
    private String emailAddress;
}
