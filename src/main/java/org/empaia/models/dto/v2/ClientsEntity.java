package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
public class ClientsEntity {
    @JsonProperty( "clients" )
    @NotNull
    private List<ClientEntity> clients;

    @JsonProperty( "total_clients_count" )
    @NotNull
    private int totalClientsCount;
}
