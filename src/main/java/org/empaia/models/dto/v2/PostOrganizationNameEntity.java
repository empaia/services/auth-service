package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

import static org.empaia.utils.EntityLimits.MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH;

@Data
@NoArgsConstructor
@SuperBuilder
public class PostOrganizationNameEntity {

    @JsonProperty( "organization_name" )
    @NotBlank( message = "The organization name is required" )
    @Size( max= MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH )
    private String name;
}
