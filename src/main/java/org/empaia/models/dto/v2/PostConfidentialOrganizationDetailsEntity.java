package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Data
@NoArgsConstructor
@SuperBuilder
public class PostConfidentialOrganizationDetailsEntity {

    @JsonProperty( "description_en" )
    @NotNull
    @Size( max = 750 )
    private String descriptionEnglish;

    @JsonProperty( "description_de" )
    @NotNull
    @Size( max = 750 )
    private String descriptionGerman;

    @JsonProperty( "is_user_count_public" )
    @NotNull
    private boolean isUserCountPublic;

}
