package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
public class RegisteredUsersEntity {

    @JsonProperty( "users" )
    @NotNull
    private List<RegisteredUserEntity> users;

    @JsonProperty( "total_users_count" )
    @NotNull
    private int totalUserCount;
}

