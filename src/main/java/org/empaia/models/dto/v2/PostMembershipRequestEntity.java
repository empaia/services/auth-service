package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.models.auth.KeycloakId;

import jakarta.validation.constraints.*;

@Data
@NoArgsConstructor
@SuperBuilder
public class PostMembershipRequestEntity {

    @JsonProperty( "organization_id" )
    @NotNull
    private KeycloakId organizationId;
}
