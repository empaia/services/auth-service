package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.empaia.models.auth.KeycloakId;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@SuperBuilder
public class DefaultActiveOrganizationEntity {

    @JsonProperty("organization_id")
    private KeycloakId organizationId;
}