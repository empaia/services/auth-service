package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.empaia.exceptions.user.ValidationException;
import org.empaia.models.enums.v2.ClientServiceRole;
import org.empaia.utils.RegularExpressions;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class ClientEntity {
    @JsonProperty( "client_id" )
    @Size( min = 3, max = 128 )
    private String clientId;

    @JsonProperty( "keycloak_id" )
    private String keycloakId;

    @NotNull
    @JsonProperty( "name" )
    @Size( min = 3, max = 512 )
    @Pattern( regexp = RegularExpressions.ONLY_CHARS, message = ValidationException.SPECIAL_CHARACTERS_NOT_ALLOWED_MSG )
    private String name;

    @NotNull
    @JsonProperty( "description" )
    @Size( min = 3, max = 4096 )
    @Pattern( regexp = RegularExpressions.NO_SPECIAL_CHARS, message = ValidationException.SPECIAL_CHARACTERS_NOT_ALLOWED_MSG )
    private String description;

    @NotNull
    @JsonProperty( "type" )
    private String type;

    @JsonProperty( "web_origins" )
    private List<String> webOrigins;

    @JsonProperty( "redirect_uris" )
    private List<String> redirectUris;

    @JsonProperty( "client_service_roles" )
    private List<ClientServiceRole> clientServiceRoles;
}
