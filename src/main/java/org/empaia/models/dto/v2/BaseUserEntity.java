package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.exceptions.user.ValidationException;
import org.empaia.models.enums.v2.LanguageCode;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.Locale;

@Data
@NoArgsConstructor
@SuperBuilder
public class BaseUserEntity {

    @JsonProperty( "language_code" )
    @NotNull( message = "language_code cannot be empty" )
    private LanguageCode languageCode;

    @JsonProperty( "contact_data" )
    @NotNull
    @Valid
    private UserContactDataEntity contactData;

    @JsonProperty( "details" )
    @NotNull
    @Valid
    private UserDetailsEntity details;

    @Hidden
    public Locale getLocale() { return new Locale( languageCode.toString() ); }

    public void validate() throws ValidationException {
        Locale locale = getLocale();
        if ( locale.getISO3Language() == null || locale.getISO3Language().isEmpty() ) {
            throw new ValidationException(
                String.format("Language '%s' is invalid, ist must be given in ISO 639-1 or ISO 639-2 format.", languageCode)
            );
        }
    }
}
