package org.empaia.models.dto.v2;

import org.empaia.models.auth.KeycloakId;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

@FunctionalInterface
public interface BuildEntityFunction<E> {

    E apply(KeycloakId organizationId, String name, Map<String, List<String>> attributes);
/*
    default <K> BuildEntityFunction<K> andThen(Function<? super E, ? extends K> after) {
        Objects.requireNonNull(after);
        return (KeycloakId organizationId, String name, Map<String, List<String>> attributes)
                -> after.apply(apply(organizationId, name, attributes));
    }*/
}