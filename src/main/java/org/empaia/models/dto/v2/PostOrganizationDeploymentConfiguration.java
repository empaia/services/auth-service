package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
public class PostOrganizationDeploymentConfiguration {
    @JsonProperty( "allow_localhost_web_clients" )
    @NotNull
    private boolean allowLocalhostWebClients;

    @JsonProperty( "allow_http" )
    @NotNull
    private boolean allowHttp;

    @JsonProperty( "workbench_deployment_domain" )
    @NotNull
    private String workbenchDeploymentDomain;

    @JsonProperty( "data_manager_deployment_domain" )
    @NotNull
    private String dataManagerDeploymentDomain;

    @JsonProperty( "workbench_redirect_uris" )
    @NotNull
    private List<String> workbenchRedirectUris;

    @JsonProperty( "data_manager_redirect_uris" )
    @NotNull
    private List<String> dataManagerRedirectUris;

    @JsonProperty( "workbench_web_origins" )
    @NotNull
    private List<String> workbenchWebOrigins;

    @JsonProperty( "data_manager_web_origins" )
    @NotNull
    private List<String> dataManagerWebOrigins;
}
