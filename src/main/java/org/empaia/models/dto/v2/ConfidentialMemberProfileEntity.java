package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode( callSuper = true )
public class ConfidentialMemberProfileEntity extends MemberProfileEntity {

    @JsonProperty( "email_address" )
    @NotBlank
    private String emailAddress;

}
