package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
public class ResizedPictureUrlsEntity {

    public static final List<Integer> RESIZED_IMAGE_WIDTHS = Arrays.asList( 60, 400, 800, 1200 );

    @JsonProperty( "w60" )
    private String w60;

    @JsonProperty( "w400" )
    private String w400;

    @JsonProperty( "w800" )
    private String w800;

    @JsonProperty( "w1200" )
    private String w1200;
}
