package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.enums.v2.OrganizationUserRoleV2;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@SuperBuilder
public class AllowedAudiencesByOrganizationUserRoleEntity {

    @JsonProperty("organization_id")
    private KeycloakId organizationId;

    @JsonProperty("allowed_audiences_by_organization_user_role")
    private Map<OrganizationUserRoleV2, List<String>> allowedAudiencesByOrganizationUserRole;
}