package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.ResizedPictureUrlsEntity;
import org.empaia.models.enums.v2.UserAccountState;
import org.empaia.models.enums.v2.UserRole;

import jakarta.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode( callSuper = true )
public class UserProfileEntity extends BaseUserEntity {

    @JsonProperty( "account_state" )
    @NotNull
    private UserAccountState accountState;

    @JsonProperty( "created_timestamp" )
    @NotNull
    private Long createdTimestamp;

    @JsonProperty( "profile_picture_url" )
    private String profilePictureUrl;

    @JsonProperty ( "resized_profile_picture_urls" )
    private ResizedPictureUrlsEntity resizedProfilePictureUrls;

    @JsonProperty( "user_id" )
    @NotNull
    private KeycloakId userId;

    @JsonProperty( "user_roles" )
    @NotNull
    private List<UserRole> userRoles;

    @JsonProperty( "username" )
    @NotNull
    private String userName;
}

