package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.models.enums.v2.CountryCode;
import org.empaia.utils.RegularExpressions;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import static org.empaia.utils.EntityLimits.MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH;

@Data
@NoArgsConstructor
@SuperBuilder
public class AddressEntity {

    @JsonProperty( "country_code" )
    @NotNull
    private CountryCode countryCode;

    @JsonProperty( "place_name" )
    @NotBlank
    @Size( max = MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH )
    private String placeName;

    @JsonProperty( "street_name" )
    @NotBlank
    @Size( max = MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH )
    private String streetName;

    @JsonProperty( "street_number" )
    @NotBlank
    @Size( max = MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH )
    private String streetNumber;

    @JsonProperty( "zip_code" )
    @NotBlank
    @Pattern( regexp = RegularExpressions.POSTAL_CODE, message = "Invalid postal code" )
    @Size( max = MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH )
    private String zipCode;
}
