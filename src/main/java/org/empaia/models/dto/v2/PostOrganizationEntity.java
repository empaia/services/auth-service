package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.validation.Valid;
import jakarta.validation.constraints.*;

import static org.empaia.utils.EntityLimits.MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH;

@Data
@NoArgsConstructor
@SuperBuilder
public class PostOrganizationEntity {

    @JsonProperty( "contact_data" )
    @Valid
    ConfidentialOrganizationContactDataEntity contactData;

    @JsonProperty( "details" )
    @Valid
    PostInitialConfidentialOrganizationDetailsEntity details;

    @JsonProperty( "organization_name" )
    @Size( max= MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH )
    private String name;
}
