package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode( callSuper = true )
public class MembershipEntity extends CommonOrganizationDataEntity {

    @JsonProperty( "membership_request_id" )
    private Integer membershipRequestId;
}
