package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class PostComputeProvider {
    @JsonProperty( "compute_provider_organization_id" )
    @NotNull
    private String computeProviderOrganizationId;
}
