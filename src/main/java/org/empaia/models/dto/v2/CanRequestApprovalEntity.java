package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import jakarta.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class CanRequestApprovalEntity {
    @JsonProperty( "can_request_approval" )
    @NotNull
    private boolean canRequestApproval;
}
