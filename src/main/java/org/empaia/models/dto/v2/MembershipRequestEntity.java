package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.enums.v2.RequestState;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@SuperBuilder
public class MembershipRequestEntity {

    @JsonProperty( "membership_request_id" )
    @NotNull
    private int membershipRequestId;

    @JsonProperty( "organization_id" )
    @NotNull
    private KeycloakId organizationId;

    @JsonProperty( "organization_name" )
    @NotBlank
    private String organizationName;

    @JsonProperty( "user_details" )
    @NotNull
    private ExtendedUserDetailsEntity userDetails;

    @JsonProperty( "created_at" )
    @NotNull
    private Long createdAt;

    @JsonProperty("reviewer_id")
    private KeycloakId reviewerId;

    @JsonProperty("reviewer_comment")
    private String reviewerComment;

    @JsonProperty("updated_at")
    private Long updatedAt;

    @JsonProperty( "request_state" )
    @NotNull
    private RequestState requestState;
}
