package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.empaia.models.enums.v2.UserRole;
import lombok.Data;

import jakarta.validation.constraints.NotNull;
import java.util.List;

@Data
public class UserRolesEntity {
    @JsonProperty( "roles" )
    @NotNull
    private List<UserRole> userRoles;
}
