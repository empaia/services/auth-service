package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode( callSuper = true )
public class UnapprovedConfidentialOrganizationProfileEntity extends UnapprovedConfidentialOrganizationEntity {

    @JsonProperty( "created_timestamp" )
    @NotNull
    private Long createdTimestamp;

    @JsonProperty( "updated_timestamp" )
    @NotNull
    private Long updatedTimestamp;

    @JsonProperty( "normalized_name" )
    private String normalizedName;

    @JsonProperty( "contact_data" )
    ConfidentialOrganizationContactDataEntity contactData;

    @JsonProperty( "details" )
    ConfidentialOrganizationDetailsEntity details;
}
