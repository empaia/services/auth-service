package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.models.enums.v2.OrganizationAccountState;

import jakarta.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode( callSuper = true )
public class ConfidentialOrganizationProfileEntity extends CommonOrganizationProfileDataEntity {

    @JsonProperty( "account_state" )
    @NotNull
    private OrganizationAccountState accountState;

    @JsonProperty( "created_timestamp" )
    @NotNull
    private Long createdTimestamp;

    @JsonProperty( "updated_timestamp" )
    @NotNull
    private Long updatedTimestamp;

    @JsonProperty( "normalized_name" )
    @NotNull
    private String normalizedName;

    @JsonProperty( "contact_data" )
    @NotNull
    ConfidentialOrganizationContactDataEntity contactData;

    @JsonProperty( "details" )
    @NotNull
    ConfidentialOrganizationDetailsEntity details;
}
