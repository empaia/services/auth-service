package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@SuperBuilder
public class ClientCredentials {
    @JsonProperty( "id" )
    @NotNull
    private String id;

    @JsonProperty( "client_id" )
    @NotNull
    private String clientId;

    @JsonProperty( "secret" )
    private String secret;
}
