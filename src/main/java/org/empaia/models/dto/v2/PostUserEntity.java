package org.empaia.models.dto.v2;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.validators.ValidPassword;


@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode( callSuper = true )
public class PostUserEntity extends BaseUserEntity {

    @ValidPassword
    private String password;

}
