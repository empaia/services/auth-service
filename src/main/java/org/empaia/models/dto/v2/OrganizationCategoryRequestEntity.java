package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.enums.v2.OrganizationCategory;
import org.empaia.models.enums.v2.RequestState;

import jakarta.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
public class OrganizationCategoryRequestEntity {

    @JsonProperty( "organization_category_request_id" )
    @NotNull
    private int organizationCategoryRequestId;

    @JsonProperty( "existing_categories" )
    @NotNull
    private List<OrganizationCategory> existingCategories;

    @JsonProperty( "requested_categories" )
    @NotNull
    private List<OrganizationCategory> requestedCategories;

    @JsonProperty( "retracted_categories" )
    @NotNull
    private List<OrganizationCategory> retractedCategories;

    @JsonProperty( "organization_id" )
    @NotNull
    private KeycloakId organizationId;

    @JsonProperty( "organization_name" )
    @NotNull
    private String organizationName;

    @JsonProperty( "user_details" )
    @NotNull
    private ExtendedUserDetailsEntity userDetails;

    @JsonProperty( "created_at" )
    @NotNull
    private Long createdAt;

    @JsonProperty("reviewer_id")
    private KeycloakId reviewerId;

    @JsonProperty("reviewer_comment")
    private String reviewerComment;

    @JsonProperty("updated_at")
    private Long updatedAt;

    @JsonProperty( "request_state" )
    @NotNull
    private RequestState requestState;
}
