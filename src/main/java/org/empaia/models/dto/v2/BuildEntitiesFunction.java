package org.empaia.models.dto.v2;

import java.util.List;

@FunctionalInterface
public interface BuildEntitiesFunction<ES, E> {

    ES apply(List<E> organizations, int totalNumberOfOrganizations );
/*
    default <K, E> BuildEntityFunction<K> andThen(Function<? super ES, ? extends K> after) {
        Objects.requireNonNull(after);
        return ( List<E> organizations, int totalNumberOfOrganizations )
                -> after.apply(apply( organizations, totalNumberOfOrganizations ));
    }*/
}