package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.enums.v2.OrganizationUserRoleV2;
import org.empaia.models.enums.v2.RequestState;

import jakarta.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
public class OrganizationUserRoleRequestEntity {

    @JsonProperty( "organization_user_role_request_id" )
    @NotNull
    private int organizationUserRoleRequestId;

    @JsonProperty( "organization_id" )
    @NotNull
    private KeycloakId organizationId;

    @JsonProperty( "organization_name" )
    @NotNull
    private String organizationName;

    @JsonProperty( "user_details" )
    @NotNull
    private ExtendedUserDetailsEntity userDetails;

    @JsonProperty( "existing_roles")
    @NotNull
    private List<OrganizationUserRoleV2> existingRoles;

    @JsonProperty( "requested_roles")
    @NotNull
    private List<OrganizationUserRoleV2> requestedRoles;

    @JsonProperty( "retracted_roles")
    @NotNull
    private List<OrganizationUserRoleV2> retractedRoles;

    @JsonProperty( "created_at" )
    @NotNull
    private Long createdAt;

    @JsonProperty("reviewer_id")
    private KeycloakId reviewerId;

    @JsonProperty("reviewer_comment")
    private String reviewerComment;

    @JsonProperty("updated_at")
    private Long updatedAt;

    @JsonProperty( "request_state" )
    @NotNull
    private RequestState requestState;
}
