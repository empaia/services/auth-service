package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@SuperBuilder
public class OrganizationDeploymentCredentials {
    @JsonProperty( "workbench_service" )
    private ClientCredentials workbenchService;

    @JsonProperty( "workbench_client" )
    private ClientCredentials workbenchClient;

    @JsonProperty( "medical_data_service" )
    private ClientCredentials medicalDataService;

    @JsonProperty( "data_management_client" )
    private ClientCredentials dataManagementClient;

    @JsonProperty( "id_mapper_service" )
    private ClientCredentials idMapperService;

    @JsonProperty( "upload_service" )
    private ClientCredentials uploadService;

    @JsonProperty( "app_service" )
    private ClientCredentials appService;

    @JsonProperty( "job_execution_service" )
    private ClientCredentials jobExecutionService;

    @JsonProperty( "compute_provider_job_execution_service" )
    private ClientCredentials computeProviderJobExecutionService;
}
