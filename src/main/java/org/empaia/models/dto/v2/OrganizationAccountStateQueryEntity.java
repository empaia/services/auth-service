package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.models.enums.v2.OrganizationAccountState;

import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
public class OrganizationAccountStateQueryEntity {
    @JsonProperty( "organization_account_states" )
    List<OrganizationAccountState> organizationAccountStates;
}
