package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
public class ConfidentialOrganizationProfilesEntity {

    @JsonProperty( "organizations" )
    @NotNull
    private List<ConfidentialOrganizationProfileEntity> organizations;

    @JsonProperty( "total_organizations_count" )
    @NotNull
    private int totalOrganizationsCount;
}

