package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
public class MembershipRequestsEntity {
    @JsonProperty( "membership_requests" )
    @NotNull
    private List<MembershipRequestEntity> membershipRequestEntities;

    @JsonProperty( "total_membership_requests_count" )
    @NotNull
    private int totalMembershipRequestsCount;
}
