package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.models.enums.v2.OrganizationCategory;

import jakarta.validation.constraints.NotNull;

import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
public class PublicOrganizationDetailsEntity {

    @JsonProperty( "categories" )
    @NotNull
    List<OrganizationCategory> categories;

    @JsonProperty( "description" )
    @NotNull
    private List<TextTranslationEntity> description;

    @JsonProperty( "number_of_members" )
    private Integer numberOfMembers;
}
