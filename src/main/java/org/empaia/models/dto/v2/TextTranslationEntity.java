package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.models.enums.v2.LanguageCode;

@Data
@NoArgsConstructor
@SuperBuilder
public class TextTranslationEntity {

    @JsonProperty( "language_code" )
    private LanguageCode languageCode;

    @JsonProperty( "text" )
    private String text;
}
