package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode( callSuper = true )
public class PublicOrganizationProfileEntity extends CommonOrganizationProfileDataEntity {

    @JsonProperty( "contact_data" )
    @NotNull
    PublicOrganizationContactDataEntity contactData;

    @JsonProperty( "details" )
    @NotNull
    PublicOrganizationDetailsEntity details;
}
