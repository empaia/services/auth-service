package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;


@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode( callSuper = true )
public class PublicOrganizationContactDataEntity extends AddressEntity {

    @JsonProperty( "email_address" )
    private String emailAddress;

    @JsonProperty( "phone_number" )
    private String phoneNumber;

    @JsonProperty( "department" )
    private String department;

    @JsonProperty( "fax_number" )
    private String faxNumber;

    @JsonProperty( "website" )
    private String website;
}
