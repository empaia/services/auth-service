package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
public class OrganizationUserRoleRequestsEntity {
    @JsonProperty( "organization_user_role_requests" )
    @NotNull
    private List<OrganizationUserRoleRequestEntity> organizationUserRoleRequestEntities;

    @JsonProperty( "total_organization_user_role_requests_count" )
    @NotNull
    private int totalOrganizationUserRoleRequestsCount;
}
