package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.models.enums.v2.CountryCode;

import jakarta.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
public class CountryEntityV2 {

    @JsonProperty( "country_code" )
    @NotNull
    private CountryCode countryCode;

    @JsonProperty( "translated_names" )
    @NotNull
    private List<TextTranslationEntity> translatedNames;
}
