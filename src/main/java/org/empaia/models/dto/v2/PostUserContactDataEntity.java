package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.utils.RegularExpressions;

import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import static org.empaia.utils.EntityLimits.MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH;

@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode( callSuper = true )
public class PostUserContactDataEntity extends AddressEntity {

    @JsonProperty("phone_number")
    @Pattern(regexp = RegularExpressions.PHONE_NUMBER, message = "Invalid phone number")
    @Size(max = MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH)
    private String phoneNumber;
}