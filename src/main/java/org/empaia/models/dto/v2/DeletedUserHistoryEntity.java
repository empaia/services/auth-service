package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
public class DeletedUserHistoryEntity {
    @JsonProperty( "deleted_users" )
    @NotNull
    private List<DeletedUserEntity> deletedUsers;
}
