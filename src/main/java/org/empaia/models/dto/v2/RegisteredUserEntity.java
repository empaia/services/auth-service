package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.enums.v2.UserRole;
import org.springframework.data.rest.core.annotation.Description;

import jakarta.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
public class RegisteredUserEntity {

    @JsonProperty( "user_id" )
    @NotNull
    private KeycloakId userId;

    @JsonProperty( "user_roles" )
    @NotNull
    private List<UserRole> userRoles;

    @JsonProperty( "username" )
    @NotNull
    private String userName;

    @JsonProperty( "member_of" )
    @Description( "List of organization names where the user is a member" )
    @NotNull
    private List<String> memberOf;

    @JsonProperty("is_active")
    @NotNull
    private boolean isActive;
}

