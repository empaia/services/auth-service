package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.empaia.models.enums.v2.LanguageCode;

import jakarta.validation.constraints.NotNull;

@Data
public class LanguageEntity {
    @JsonProperty( "language_code" )
    @NotNull
    LanguageCode languageCode;
}
