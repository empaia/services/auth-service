package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode( callSuper = true )
public class ConfidentialOrganizationDetailsEntity extends PostInitialConfidentialOrganizationDetailsEntity {
    @JsonProperty( "number_of_members" )
    @NotNull
    private int numberOfMembers;
}
