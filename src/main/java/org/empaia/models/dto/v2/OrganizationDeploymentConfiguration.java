package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.models.auth.KeycloakId;

@EqualsAndHashCode( callSuper = true )
@Data
@NoArgsConstructor
@SuperBuilder
public class OrganizationDeploymentConfiguration extends PostOrganizationDeploymentConfiguration {
    @JsonProperty( "organization_id" )
    @NotNull
    private KeycloakId organizationId;
}
