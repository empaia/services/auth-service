package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.empaia.validators.ValidEmailList;

import jakarta.validation.constraints.NotNull;
import java.util.List;

@Data
public class EmailAddressListEntity {

    @JsonProperty( "email_addresses" )
    @ValidEmailList
    @NotNull
    List<String> emailAddresses;
}
