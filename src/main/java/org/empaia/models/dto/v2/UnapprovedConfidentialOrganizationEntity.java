package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.models.enums.v2.OrganizationAccountState;

import jakarta.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode( callSuper = true )
public class UnapprovedConfidentialOrganizationEntity extends UnapprovedCommonOrganizationDataEntity {

    @JsonProperty( "account_state" )
    @NotNull
    private OrganizationAccountState accountState;
}
