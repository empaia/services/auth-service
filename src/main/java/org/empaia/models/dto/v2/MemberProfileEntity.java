package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.enums.v2.OrganizationUserRoleV2;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
public class MemberProfileEntity {

    @JsonProperty( "first_name" )
    @NotBlank
    private String firstName;

    @JsonProperty( "last_name" )
    @NotBlank
    private String lastName;

    @JsonProperty( "title" )
    @NotNull
    private String title;

    @JsonProperty( "user_id" )
    @NotNull
    private KeycloakId userId;

    @JsonProperty( "organization_user_roles" )
    @NotNull
    private List<OrganizationUserRoleV2> organizationUserRoles;
}
