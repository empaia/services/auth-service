package org.empaia.models.dto.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

import static org.empaia.utils.EntityLimits.MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH;

@Data
@NoArgsConstructor
@SuperBuilder
public class UserDetailsEntity {

    @JsonProperty( "first_name" )
    @NotBlank( message = "first_name cannot be empty" )
    @Size( max = MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH )
    private String firstName;

    @JsonProperty( "last_name" )
    @NotBlank( message = "last_name cannot be empty" )
    @Size( max = MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH )
    private String lastName;

    @JsonProperty( "title" )
    @Size( max = MAX_KEYCLOAK_ATTRIBUTE_VALUE_LENGTH )
    private String title;
}
