package org.empaia.models.db.v2;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.enums.v2.OrganizationCategory;
import org.empaia.models.enums.v2.RequestState;

import jakarta.persistence.*;
import javax.ws.rs.BadRequestException;
import java.util.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder
@Table( name = "organization_category_requests" )
public class DBOrganizationCategoryRequest implements DBRequest {
    @Id
    @Column(name = "organization_category_request_id", nullable = false, unique = true)
    @SequenceGenerator(name = "cat_seq",sequenceName = "organization_category_requests_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "cat_seq")
    private int organizationCategoryRequestId;

    @Column(name = "organization_id", nullable = false)
    private String organizationId;

    @Column(name = "app_customer_category_requested", nullable = false)
    private boolean appCustomerCategoryRequested;

    @Column(name = "app_vendor_category_requested", nullable = false)
    private boolean appVendorCategoryRequested;

    @Column(name = "compute_provider_category_requested", nullable = false)
    private boolean computeProviderCategoryRequested;

    @Column(name = "product_provider_category_requested", nullable = false)
    private boolean productProviderCategoryRequested;

    @Column(name = "creator_id", nullable = false)
    private String creatorId;

    @Column(name = "created_at", nullable = false)
    @Builder.Default
    private Long createdAt = new Date().getTime();

    @Column(name = "reviewer_id")
    private String reviewerId;

    @Column(name = "reviewer_comment")
    private String reviewerComment;

    @Column(name = "updated_at")
    private Long updatedAt;

    @Basic
    @Column( name = "request_state", nullable = false )
    private int requestState;

    public void setRequestState( RequestState requestState )
    {
        this.requestState = requestState.getValue();
    }

    public RequestState getRequestState() {
        return RequestState.value( this.requestState );
    }

    @Override
    public void updateRequestState( RequestState requestState, KeycloakId reviewerId, String reviewComment ) {
        if ( RequestState.isNotFinal(requestState) ) {
            throw new BadRequestException( String.format( "Request state is not in a final state (%s)", requestState ) );
        }
        this.requestState = requestState.getValue();
        this.reviewerId = reviewerId.toString();
        this.reviewerComment = reviewComment;
        this.updatedAt = new Date().getTime();
    }

    public List<OrganizationCategory> getRequestedOrganizationCategories() {
        List<OrganizationCategory> result = new ArrayList<>();
        if ( this.appCustomerCategoryRequested ) {
            result.add( OrganizationCategory.APP_CUSTOMER );
        }
        if ( this.appVendorCategoryRequested ) {
            result.add( OrganizationCategory.APP_VENDOR );
        }
        if ( this.computeProviderCategoryRequested ) {
            result.add( OrganizationCategory.COMPUTE_PROVIDER );
        }
        if ( this.productProviderCategoryRequested ) {
            result.add( OrganizationCategory.PRODUCT_PROVIDER );
        }
        return result;
    }
}
