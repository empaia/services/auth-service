package org.empaia.models.db.v2;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.enums.v2.RequestState;

import jakarta.persistence.*;
import javax.ws.rs.BadRequestException;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder
@Table( name = "membership_requests" )
public class DBMembershipRequest implements DBRequest {
    @Id
    @Column(name = "membership_request_id", nullable = false, unique = true)
    @SequenceGenerator(name = "mem_seq",sequenceName = "membership_requests_membership_request_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "mem_seq")
    private int membershipRequestId;

    @Column(name = "organization_id", nullable = false)
    private String organizationId;

    @Column(name = "organization_name", nullable = false)
    private String organizationName;

    @Column(name = "user_id", nullable = false)
    private String userId;

    @Column(name = "user_name", nullable = false)
    private String userName;

    @Column(name = "created_at", nullable = false)
    @Builder.Default
    private Long createdAt = new Date().getTime();

    @Column(name = "reviewer_id")
    private String reviewerId;

    @Column(name = "reviewer_comment")
    private String reviewerComment;

    @Column(name = "updated_at")
    private Long updatedAt;

    @Basic
    @Column( name = "request_state", nullable = false )
    private int requestState;

    public void setRequestState( RequestState requestState )
    {
        this.requestState = requestState.getValue();
    }

    public RequestState getRequestState() {
        return RequestState.value( this.requestState );
    }

    @Override
    public void updateRequestState( RequestState requestState, KeycloakId reviewerId, String reviewComment ) {
        if ( RequestState.isNotFinal(requestState) ) {
            throw new BadRequestException( String.format( "Request state is not in a final state (%s)", requestState ) );
        }
        this.requestState = requestState.getValue();
        this.reviewerId = reviewerId.toString();
        this.reviewerComment = reviewComment;
        this.updatedAt = new Date().getTime();
    }
}
