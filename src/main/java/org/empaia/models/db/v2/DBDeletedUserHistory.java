package org.empaia.models.db.v2;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;
import java.io.Serializable;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder
@Table( name = "deleted_user_history" )
public class DBDeletedUserHistory implements Serializable  {
    @Id
    @Column(name = "deleted_user_history_id", nullable = false, unique = true)
    @SequenceGenerator(name = "del_user_seq",sequenceName = "deleted_user_history_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "del_user_seq")
    private int id;

    @Column(name = "user_id", nullable = false, unique = true)
    private String userId;

    @Column(name = "deleted_by", nullable = false)
    private String deletedBy;

    @Column(name = "deleted_on", nullable = false)
    private Long deletedOn;
}
