package org.empaia.models.db.v2;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.empaia.models.enums.v2.OrganizationAccountState;

import jakarta.persistence.*;
import javax.ws.rs.BadRequestException;
import java.io.Serializable;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder
@Table( name = "organization_details" )
public class DBOrganizationDetails implements Serializable {
    @Id
    @Column( name = "organization_details_id", nullable = false, unique = true )
    @SequenceGenerator( name = "det_seq",sequenceName = "organization_details_organization_details_id_seq", allocationSize = 1 )
    @GeneratedValue( strategy = GenerationType.AUTO, generator = "det_seq" )
    private int organizationDetailsId;

    @Column( name = "keycloak_id", nullable = false )
    private String keycloakId;

    // The creator id is not stored in keycloak, because the keycloak API does not provide a query
    // over group attributes.
    @Column( name = "creator_id", nullable = false )
    private String creatorId;


    // The description is too large for keycloak group attributes, so it needs to be stored in the DB:
    @Basic
    @Column( name = "description_de", length=750 )
    @Builder.Default
    private String descriptionGerman = "";

    // The description is too large for keycloak group attributes, so it needs to be stored in the DB:
    @Basic
    @Column( name = "description_en", length=750)
    @Builder.Default
    private String descriptionEnglish = "";

    // The account state is not stored in keycloak, because the keycloak API does not provide a query
    // over group attributes.
    @Basic
    @Column( name = "account_state", nullable = false )
    private int accountState;

    public void setOrganizationAccountState( OrganizationAccountState accountState ) {
        this.accountState = accountState.getValue();
    }

    public OrganizationAccountState getOrganizationAccountState() {
        return OrganizationAccountState.fromValue( this.accountState );
    }

    public boolean isUnapproved() {
        OrganizationAccountState accountState = getOrganizationAccountState();
        return accountState == OrganizationAccountState.REQUIRES_ACTIVATION ||
               accountState == OrganizationAccountState.AWAITING_ACTIVATION;
    }

    public void verifyOrganizationIsApproved() throws BadRequestException {
        if ( isUnapproved() ) {
            throw new BadRequestException( "Organization is unapproved" );
        }
    }

    public void verifyOrganizationIsNotApproved() throws BadRequestException {
        if ( !isUnapproved() ) {
            throw new BadRequestException( "Organization is not unapproved" );
        }
    }
}
