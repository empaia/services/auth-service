package org.empaia.models.db.v2;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder
@Table( name = "organization_deployment_configurations" )
public class DBOrganizationDeploymentConfiguration {
    @Id
    @Column( name = "organization_deployment_configuration_id", nullable = false, unique = true )
    @SequenceGenerator( name = "odc_seq", sequenceName = "organization_deployment_configuration_id_seq", allocationSize = 1 )
    @GeneratedValue( strategy = GenerationType.AUTO, generator = "odc_seq" )
    private int organizationDeploymentConfigurationId;

    @Column( name = "organization_id", nullable = false )
    private String organizationId;

    @Basic
    @Column( name = "allow_localhost_web_clients", nullable = false )
    @Builder.Default
    private boolean allowLocalhostWebClients = false;

    @Basic
    @Column( name = "allow_http", nullable = false )
    @Builder.Default
    private boolean allowHttp = false;

    @Basic
    @Column( name = "workbench_deployment_domain", nullable = false )
    @Builder.Default
    private String workbenchDeploymentDomain = "";

    @Basic
    @Column( name = "data_manager_deployment_domain", nullable = false )
    @Builder.Default
    private String dataManagerDeploymentDomain = "";

    @Basic
    @Column( name = "updated_by", nullable = false )
    @Builder.Default
    private String updatedBy = "";

    @Basic
    @Column( name = "updated_at" )
    private Long updatedAt;
}
