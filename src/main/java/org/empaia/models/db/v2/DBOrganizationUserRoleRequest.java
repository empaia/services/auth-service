package org.empaia.models.db.v2;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.enums.v2.OrganizationUserRoleV2;
import org.empaia.models.enums.v2.RequestState;

import jakarta.persistence.*;
import javax.ws.rs.BadRequestException;
import java.util.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder
@Table( name = "organization_user_role_requests" )
public class DBOrganizationUserRoleRequest implements DBRequest {
    @Id
    @Column(name = "organization_user_role_requests_id", nullable = false, unique = true)
    @SequenceGenerator(name = "role_seq",sequenceName = "organization_user_role_requests_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "role_seq")
    private int id;

    @Column(name = "organization_id", nullable = false)
    private String organizationId;

    @Column(name = "user_id", nullable = false)
    private String userId;

    @Column(name = "manager_role_requested", nullable = false)
    private boolean managerRoleRequested;

    @Column(name = "app_maintainer_role_requested", nullable = false)
    private boolean appMaintainerRoleRequested;

    @Column(name = "pathologist_role_requested", nullable = false)
    private boolean pathologistRoleRequested;

    @Column(name = "data_manager_role_requested", nullable = false)
    private boolean dataManagerRoleRequested;

    @Column(name = "clearance_maintainer_role_requested", nullable = false)
    private boolean clearanceMaintainerRoleRequested;

    @Column(name = "created_at", nullable = false)
    @Builder.Default
    private Long createdAt = new Date().getTime();

    @Column(name = "reviewer_id")
    private String reviewerId;

    @Column(name = "reviewer_comment")
    private String reviewerComment;

    @Column(name = "updated_at")
    private Long updatedAt;

    @Basic
    @Column( name = "request_state", nullable = false )
    private int requestState;

    public void setRequestState( RequestState requestState )
    {
        this.requestState = requestState.getValue();
    }

    public RequestState getRequestState() {
        return RequestState.value( this.requestState );
    }

    @Override
    public void updateRequestState( RequestState requestState, KeycloakId reviewerId, String reviewComment ) {
        if ( RequestState.isNotFinal(requestState) ) {
            throw new BadRequestException( String.format( "Request state is not in a final state (%s)", requestState ) );
        }
        this.requestState = requestState.getValue();
        this.reviewerId = reviewerId.toString();
        this.reviewerComment = reviewComment;
        this.updatedAt = new Date().getTime();
    }

    public Set<OrganizationUserRoleV2> getRequestedUserRoles() {
        Set<OrganizationUserRoleV2> result = new HashSet<>();
        if ( this.managerRoleRequested ) {
            result.add( OrganizationUserRoleV2.MANAGER );
        }
        if ( this.appMaintainerRoleRequested ) {
            result.add( OrganizationUserRoleV2.APP_MAINTAINER );
        }
        if ( this.pathologistRoleRequested ) {
            result.add( OrganizationUserRoleV2.PATHOLOGIST );
        }
        if ( this.dataManagerRoleRequested ) {
            result.add( OrganizationUserRoleV2.DATA_MANAGER );
        }
        if ( this.clearanceMaintainerRoleRequested ) {
            result.add( OrganizationUserRoleV2.CLEARANCE_MAINTAINER );
        }
        return result;
    }

    public void setRequestedUserRoles( Set<OrganizationUserRoleV2> requestedUserRoles ) {
        if ( requestedUserRoles.contains( OrganizationUserRoleV2.MANAGER ) ) {
            this.managerRoleRequested = true;
        }
        if ( requestedUserRoles.contains( OrganizationUserRoleV2.APP_MAINTAINER ) ) {
            this.appMaintainerRoleRequested = true;
        }
        if ( requestedUserRoles.contains( OrganizationUserRoleV2.PATHOLOGIST ) ) {
            this.pathologistRoleRequested = true;
        }
        if ( requestedUserRoles.contains( OrganizationUserRoleV2.DATA_MANAGER ) ) {
            this.dataManagerRoleRequested = true;
        }
        if ( requestedUserRoles.contains( OrganizationUserRoleV2.CLEARANCE_MAINTAINER ) ) {
            this.clearanceMaintainerRoleRequested = true;
        }
    }
}
