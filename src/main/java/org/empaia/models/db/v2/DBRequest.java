package org.empaia.models.db.v2;

import org.empaia.models.auth.KeycloakId;
import org.empaia.models.enums.v2.RequestState;

public interface DBRequest {
    void updateRequestState( RequestState requestState, KeycloakId reviewerId, String reviewComment );
}
