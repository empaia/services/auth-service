package org.empaia.models.auth;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.extern.slf4j.Slf4j;
import org.empaia.utils.RegularExpressions;
import org.empaia.validators.ValidKeycloakId;

import javax.ws.rs.BadRequestException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Slf4j
@ValidKeycloakId
public class KeycloakId extends Object {

    private static final Pattern validIdPattern = Pattern.compile(RegularExpressions.VALID_KEYCLOAK_ID);

    private String id;

    public KeycloakId( String id ) {
        this( id, false );
    }

    public KeycloakId( String id, boolean validateId ) {
        this.id = id;
        if ( validateId ) {
            if ( !isValid() ) {
                throw new BadRequestException("Invalid keycloak id: " + id);
            }
        }
    }

    public boolean isValid() {
        boolean isValid = false;
        if ( id != null ) {
            Matcher matcher = validIdPattern.matcher(id);
            isValid = matcher.matches();
        }
        return isValid;
    }

    @Override
    @JsonValue
    public String toString() {
        return this.id;
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    @Override
    public boolean equals( Object object ) {
        boolean isEqual = false;
        if (object instanceof String) {
            isEqual = this.equals((String)object);
        } else if (object instanceof KeycloakId) {
            isEqual = this.equals((KeycloakId)object);
        }
        return isEqual;
    }

    public boolean equals( KeycloakId other ) {
        return this.id.equals(other.id);
    }

    public boolean equals( String other ) {
        return this.id.equals(other);
    }
}