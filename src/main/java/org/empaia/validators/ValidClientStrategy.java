package org.empaia.validators;

import org.keycloak.representations.idm.ClientRepresentation;

public interface ValidClientStrategy {
    boolean validate( ClientRepresentation client );
}
