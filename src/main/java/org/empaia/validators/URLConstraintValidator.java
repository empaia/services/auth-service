package org.empaia.validators;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.stereotype.Component;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;


@Component
@Slf4j
public class URLConstraintValidator implements ConstraintValidator<ValidURL, String> {

    private ValidURL constraintAnnotation;

    @Override
    public void initialize( ValidURL constraintAnnotation ) {
        this.constraintAnnotation = constraintAnnotation;
    }

    @Override
    public boolean isValid( String url, ConstraintValidatorContext constraintValidatorContext ) {
        String[] schemes = { "http", "https" };
        var validator = new UrlValidator( schemes );
        boolean isValid;
        if ( url == null ) {
            isValid = constraintAnnotation.allowNull();
        } else {
            if ( url.trim().isEmpty() ) {
                isValid = constraintAnnotation.allowEmpty();
            } else {
                isValid = validator.isValid( url );
            }
        }
        if ( !isValid ) {
            constraintValidatorContext.buildConstraintViolationWithTemplate( "Invalid URL" )
                    .addConstraintViolation()
                    .disableDefaultConstraintViolation();
        }
        return isValid;
    }
}

