package org.empaia.validators;

import lombok.extern.slf4j.Slf4j;
import org.empaia.models.enums.v2.ClientType;
import org.empaia.services.v2.KeycloakClientService;
import org.keycloak.representations.idm.ClientRepresentation;

@Slf4j
public class ClientServiceAccountValidator implements ValidClientStrategy {
    private final KeycloakClientService keycloakClientService;

    public ClientServiceAccountValidator( KeycloakClientService keycloakClientService ) {
        this.keycloakClientService = keycloakClientService;
    }

    @Override
    public boolean validate( ClientRepresentation client ) {
        ClientType clientType = ClientType.fromClientId( client.getClientId() );

        boolean isServiceAccountInOrganization = true;
        if ( ClientType.isOrganizationServiceClient( clientType ) ) {
            String organizationId = client.getClientId().split( "\\." )[0];
            isServiceAccountInOrganization = keycloakClientService.checkIfClientHasServiceAccountInOrganization( client, organizationId );
            if ( !isServiceAccountInOrganization ) {
                log.warn( String.format( "   > [!] Service account of client %s is not member of organization %s", client.getClientId(), organizationId ) );
            }
        }
        return isServiceAccountInOrganization;
    }
}
