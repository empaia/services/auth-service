package org.empaia.validators;


import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Component;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.List;


@Component
public class EmailListConstraintValidator implements ConstraintValidator<ValidEmailList, List<String> > {

    @Override
    public boolean isValid( List<String> emailAddresses, ConstraintValidatorContext constraintValidatorContext ) {
        var validator = EmailValidator.getInstance(false);
        boolean isValid = true;
        for ( var emailAddress : emailAddresses ) {
            isValid = validator.isValid(emailAddress);
            if (!isValid) {
                constraintValidatorContext.buildConstraintViolationWithTemplate("At least one email address is invalid")
                        .addConstraintViolation()
                        .disableDefaultConstraintViolation();
            }
        }
        return isValid;
    }
}

