package org.empaia.validators;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;

@Retention( RetentionPolicy.RUNTIME )
@Target( { FIELD, PARAMETER } )
@Constraint( validatedBy = EmailListConstraintValidator.class )
public @interface ValidEmailList {
    String message() default "At least one email address is invalid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

