package org.empaia.validators;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;

@Retention( RetentionPolicy.RUNTIME )
@Target( { FIELD, PARAMETER } )
@Constraint( validatedBy = URLConstraintValidator.class )
public @interface ValidURL {
    String message() default "Invalid URL";

    boolean allowNull() default false;

    boolean allowEmpty() default false;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

