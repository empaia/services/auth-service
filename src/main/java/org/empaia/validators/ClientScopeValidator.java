package org.empaia.validators;

import lombok.extern.slf4j.Slf4j;
import org.empaia.models.enums.v2.ClientType;
import org.empaia.services.v2.KeycloakClientService;
import org.empaia.utils.KeycloakUtils;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.ClientScopeRepresentation;
import org.keycloak.representations.idm.ProtocolMapperRepresentation;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public class ClientScopeValidator implements ValidClientStrategy {
    private final KeycloakClientService keycloakClientService;

    public ClientScopeValidator( KeycloakClientService keycloakClientService ) {
        this.keycloakClientService = keycloakClientService;
    }

    @Override
    public boolean validate( ClientRepresentation client ) {
        Set<ClientScopeRepresentation> clientScopes = keycloakClientService.getScopesOfClient( client.getClientId() );
        boolean isValid = validateGrantedAccesses( client, clientScopes );
        for ( ClientScopeRepresentation scope : clientScopes ) {
            if ( KeycloakUtils.isValidKeycloakInstanceName( scope.getName(), "scope" ) ) {
                isValid = validateProtocolMappers( scope ) && isValid;
            }
        }
        return isValid;
    }

    private boolean validateGrantedAccesses( ClientRepresentation client, Set<ClientScopeRepresentation> clientScopes ) {
        ClientType type = ClientType.fromClientId( client.getClientId() );
        boolean isValid = true;

        if ( ClientType.isOrganizationClient( type ) ) {
            for ( String grantedAccess : type.getGrantedAccess() ) {
                Optional<ClientScopeRepresentation> scope = clientScopes.stream()
                        .filter( s -> s.getName().toLowerCase().endsWith( grantedAccess ) )
                        .findFirst();
                if ( scope.isEmpty() ) {
                    log.warn( String.format( "   > [!] Client scope %s not present for client %s", grantedAccess, client.getClientId() ) );
                    isValid = false;
                }
            }
        }
        return isValid;
    }

    private boolean validateProtocolMappers( ClientScopeRepresentation clientScope ) {
        List<ProtocolMapperRepresentation> mappers = clientScope.getProtocolMappers();
        boolean isValid = true;
        if ( mappers == null || mappers.isEmpty() ) {
            log.warn( String.format( "   > [!] No protocol mapper configured for client scope %s", clientScope.getName() ) );
            isValid = false;
        } else {
            Set<String> mapperCustomAudiences = mappers.stream().map( mapper -> mapper.getConfig().get( "included.custom.audience" ) ).collect( Collectors.toSet() );
            String audienceName = clientScope.getName().replace( ".scope", "" );

            if ( !mapperCustomAudiences.contains( audienceName ) ) {
                log.warn( String.format( "   > [!] Client %s is missing protocol mapper %s", clientScope.getName(), audienceName ) );
                isValid = false;
            }
        }
        return isValid;
    }
}
