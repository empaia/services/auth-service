package org.empaia.validators;

import lombok.extern.slf4j.Slf4j;
import org.empaia.models.enums.v2.ClientType;
import org.keycloak.representations.idm.ClientRepresentation;

import javax.ws.rs.client.Client;

@Slf4j
public class ClientCapabilityConfigValidator implements ValidClientStrategy {
    @Override
    public boolean validate( ClientRepresentation client ) {
        ClientType clientType = ClientType.fromClientId( client.getClientId() );

        boolean isValid = validateDirectAccessGrant( client, clientType );
        isValid = validateStandardFlow( client, clientType ) && isValid;
        isValid = validateImplicitFlow( client, clientType ) && isValid;
        isValid = validatePublicClient( client, clientType ) && isValid;
        isValid = validateServiceAccountsEnabled( client, clientType ) && isValid;

        return isValid;
    }

    private boolean validateDirectAccessGrant( ClientRepresentation client, ClientType clientType ) {
        boolean isValid = true;
        if ( ClientType.getEnabledDirectAccessGrantClients().contains( clientType ) ) {
            if ( !client.isDirectAccessGrantsEnabled() ) {
                // direct grant should ONLY be enabled for auth_service_client
                log.warn( String.format( "   > [!] Direct access grant should be enabled for client %s", client.getClientId() ) );
                isValid = false;
            }
        } else {
            if ( client.isDirectAccessGrantsEnabled() ) {
                log.warn( String.format( "   > [!] Direct access grant should not be enabled for client %s", client.getClientId() ) );
                isValid = false;
            }
        }
        return isValid;
    }

    private boolean validateStandardFlow( ClientRepresentation client, ClientType clientType ) {
        boolean isValid = true;
        if ( ClientType.getEnabledStandardFlowClients().contains( clientType ) ) {
            if ( !client.isStandardFlowEnabled() ) {
                log.warn( String.format( "   > [!] Standard flow should be enabled for client %s", client.getClientId() ) );
                isValid = false;
            }
        } else {
            if ( client.isStandardFlowEnabled() ) {
                log.warn( String.format( "   > [!] Standard flow should not be enabled for client %s", client.getClientId() ) );
                isValid = false;
            }
        }
        return isValid;
    }

    private boolean validateImplicitFlow( ClientRepresentation client, ClientType clientType ) {
        boolean isValid = true;
        if ( ClientType.getEnabledImplicitFlowClients().contains( clientType ) ) {
            if ( !client.isImplicitFlowEnabled() ) {
                log.warn( String.format( "   > [!] Implicit flow should be enabled for client %s", client.getClientId() ) );
                isValid = false;
            }
        } else {
            if ( client.isImplicitFlowEnabled() ) {
                log.warn( String.format( "   > [!] Implicit flow should not be enabled for client %s", client.getClientId() ) );
                isValid = false;
            }
        }
        return isValid;
    }

    private boolean validatePublicClient( ClientRepresentation client, ClientType clientType ) {
        boolean isValid = true;
        if ( ClientType.getFrontendClients().contains( clientType ) ) {
            if ( !client.isPublicClient() ) {
                log.warn( String.format( "   > [!] Frontend client %s of organization should be a public client", client.getClientId() ) );
                isValid = false;
            }
        } else {
            if ( client.isPublicClient() ) {
                log.warn( String.format( "   > [!] Service client %s of organization should not be a public client", client.getClientId() ) );
                isValid = false;
            }
        }
        return isValid;
    }

    private boolean validateServiceAccountsEnabled( ClientRepresentation client, ClientType clientType ) {
        boolean isValid = true;
        if ( ClientType.getEnabledServiceAccountClients().contains( clientType ) ) {
            if ( !client.isServiceAccountsEnabled() ) {
                log.warn( String.format( "   > [!] Service accounts should be enabled for service client %s", client.getClientId() ) );
                isValid = false;
            }
        } else {
            if ( client.isServiceAccountsEnabled() ) {
                log.warn( String.format( "   > [!] Service accounts should not be enabled for frontend client %s", client.getClientId() ) );
                isValid = false;
            }
        }
        return isValid;
    }
}
