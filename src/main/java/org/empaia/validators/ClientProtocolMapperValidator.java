package org.empaia.validators;

import lombok.extern.slf4j.Slf4j;
import org.empaia.models.enums.v2.ClientType;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.ProtocolMapperRepresentation;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public class ClientProtocolMapperValidator implements ValidClientStrategy {

    private static final String INCLUDE_CLIENT_AUDIENCE_CLAIM = "included.client.audience";

    @Override
    public boolean validate( ClientRepresentation client ) {
        List<ProtocolMapperRepresentation> mappers = client.getProtocolMappers();
        ClientType clientType = ClientType.fromClientId( client.getClientId() );
        List<String> expectedProtocolMappers = ClientType.getExpectedProtocolMappers( clientType );

        boolean isValid = true;
        if ( !expectedProtocolMappers.isEmpty() ) {
            if ( mappers.isEmpty() ) {
                log.warn( String.format( "   > [!] Protocol mappers for client %s expected but not configured", client.getClientId() ) );
                isValid = false;
            } else {
                Set<String> customAudiences = mappers.stream()
                        .filter( mapper -> mapper.getConfig().containsKey( INCLUDE_CLIENT_AUDIENCE_CLAIM ) )
                        .filter( mapper -> !mapper.getConfig().get( INCLUDE_CLIENT_AUDIENCE_CLAIM ).isEmpty() )
                        .map( mapper -> mapper.getConfig().get( INCLUDE_CLIENT_AUDIENCE_CLAIM ) )
                        .collect( Collectors.toSet() );
                for ( String expProtocolMapper : expectedProtocolMappers ) {
                    if ( !customAudiences.contains( expProtocolMapper ) ) {
                        log.warn( String.format( "   > [!] Expected protocol mapper %s not found for %s", expProtocolMapper, client.getClientId() ) );
                        isValid = false;
                    }
                }
            }
        }
        return isValid;
    }
}
