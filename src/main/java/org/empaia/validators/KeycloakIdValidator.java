package org.empaia.validators;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import org.empaia.models.auth.KeycloakId;


public class KeycloakIdValidator implements ConstraintValidator<ValidKeycloakId, KeycloakId> {

    @Override
    public void initialize( ValidKeycloakId constraintAnnotation ) {
        ConstraintValidator.super.initialize( constraintAnnotation );
    }

    @Override
    public boolean isValid( KeycloakId value, ConstraintValidatorContext context ) {
        return value.isValid();
    }
}