package org.empaia.validators;

import org.empaia.configs.PasswordComplexityConfiguration;
import org.passay.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;


@Component
public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {
    @Autowired
    private final PasswordComplexityConfiguration passwordConfiguration;

    public PasswordConstraintValidator( PasswordComplexityConfiguration passwordConfiguration ) {
        this.passwordConfiguration = passwordConfiguration;
    }

    @Override
    public void initialize( ValidPassword constraintAnnotation ) {
        // There is nothing to init
    }

    @Override
    public boolean isValid( String s, ConstraintValidatorContext constraintValidatorContext ) {
        PasswordValidator validator = new PasswordValidator( Arrays.asList(
                new LengthRule( passwordConfiguration.getMinimumLength(), passwordConfiguration.getMaximumLength() ),
                new CharacterRule( EnglishCharacterData.UpperCase, passwordConfiguration.getMinimumUppercase() ),
                new CharacterRule( EnglishCharacterData.LowerCase, passwordConfiguration.getMinimumLowercase() ),
                new CharacterRule( EnglishCharacterData.Digit, passwordConfiguration.getMinimumDigits() ),
                new CharacterRule( EnglishCharacterData.Special, passwordConfiguration.getMinimumSpecialChars() ),
                new WhitespaceRule()
        ) );

        RuleResult result = validator.validate( new PasswordData( s ) );

        if ( result.isValid() ) {
            return true;
        }

        List<String> messages        = validator.getMessages( result );
        String       messageTemplate = String.join( "\n", messages );

        constraintValidatorContext.buildConstraintViolationWithTemplate( messageTemplate )
                                  .addConstraintViolation()
                                  .disableDefaultConstraintViolation();

        return false;
    }
}
