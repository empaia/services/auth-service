package org.empaia.validators;

import lombok.extern.slf4j.Slf4j;
import org.empaia.configs.ConsistencyCheckConfiguration;
import org.empaia.exceptions.generic.NotFoundException;
import org.empaia.exceptions.user.ValidationException;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.OrganizationDeploymentConfiguration;
import org.empaia.models.enums.v2.ClientType;
import org.empaia.models.enums.v2.OrganizationAccountState;
import org.empaia.services.v2.ConfidentialOrganizationService;
import org.empaia.services.v2.OrganizationDeploymentConfigurationService;
import org.empaia.utils.ValidationUtils;
import org.keycloak.representations.idm.ClientRepresentation;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
public class ClientUrisValidator implements ValidClientStrategy {
    private final OrganizationDeploymentConfigurationService organizationDeploymentConfigurationService;
    private final ConfidentialOrganizationService confidentialOrganizationService;
    private final ConsistencyCheckConfiguration configuration;

    public ClientUrisValidator(
            OrganizationDeploymentConfigurationService organizationDeploymentConfigurationService,
            ConfidentialOrganizationService confidentialOrganizationService,
            ConsistencyCheckConfiguration configuration
    ) {
        this.organizationDeploymentConfigurationService = organizationDeploymentConfigurationService;
        this.confidentialOrganizationService = confidentialOrganizationService;
        this.configuration = configuration;
    }

    @Override
    public boolean validate( ClientRepresentation client ) {
        boolean isValid = true;

        ClientType type = ClientType.fromClientId( client.getClientId() );
        if ( type.equals( ClientType.PORTAL_CLIENT ) ) {
            isValid = validatePortalClientUris( client, configuration );
        }
        if ( ClientType.isOrganizationFrontendClient( type ) ) {
            KeycloakId organizationId = null;
            try {
                organizationId = new KeycloakId( client.getClientId().split( "\\." )[0] );
                OrganizationDeploymentConfiguration config = organizationDeploymentConfigurationService.getOrganizationDeploymentConfiguration( organizationId );
                boolean allowLocalhost = config.isAllowLocalhostWebClients();
                boolean allowHttp = config.isAllowHttp();

                String baseDomain = null;
                if ( type.equals( ClientType.WORKBENCH_CLIENT ) ) {
                    baseDomain = config.getWorkbenchDeploymentDomain();
                }
                if ( type.equals( ClientType.DATA_MANAGEMENT_CLIENT ) ) {
                    baseDomain = config.getDataManagerDeploymentDomain();
                }
                client.getRedirectUris().forEach( redirectUri -> {
                    if ( !redirectUri.contains( "localhost" ) && !redirectUri.endsWith( "/" ) && !redirectUri.endsWith( "*" ) ) {
                        log.warn( String.format(
                                "   > [!] Redirect URI %s of client %s does not end with a trailing slash '/' or asterisk '*'", redirectUri, client.getClientId() )
                        );
                    }
                } );
                isValid = validateUris( client.getRedirectUris(), baseDomain, allowLocalhost, allowHttp );
                isValid = validateUris( client.getWebOrigins(), baseDomain, allowLocalhost, allowHttp ) && isValid;
            } catch ( NotFoundException nfe ) {
                assert organizationId != null;
                if ( !confidentialOrganizationService.getConfidentialOrganizationProfileEntity( organizationId ).getAccountState().equals( OrganizationAccountState.INACTIVE ) ) {
                    log.warn(
                            String.format( "   > [!] No deployment configuration found for organization %s while validating client %s.",
                                    organizationId, client.getClientId()
                            )
                    );
                }
            } catch ( Exception ex ) {
                log.error( "Failed to query organization deployment configuration: " + ex.getMessage() );
                isValid = false;
            }
        }
        return isValid;
    }

    private boolean validatePortalClientUris( ClientRepresentation portalClient, ConsistencyCheckConfiguration configuration ) {
        boolean isValid = true;
        if ( configuration.getPortalClientValidRedirectUris() != null ) {
            Set<String> validRedirectUris = new HashSet<>( configuration.getPortalClientValidRedirectUris() );
            Set<String> urisToValidate = new HashSet<>( portalClient.getRedirectUris() );
            if ( !urisToValidate.equals( validRedirectUris ) ) {
                log.warn( "    > [!] Redirect URIs of portal clients do not match URIs from configuration!" );
                log.warn( String.format( "         - Configured redirect URIs in KCL: %s", urisToValidate ) );
                log.warn( String.format( "         - Allowed redirect URIs from ENV: %s", validRedirectUris ) );
            }
        }
        if ( configuration.getPortalClientValidWebOrigins() != null ) {
            Set<String> validWebOrigins = new HashSet<>( configuration.getPortalClientValidWebOrigins() );
            Set<String> urisToValidate = new HashSet<>( portalClient.getWebOrigins() );
            if ( !urisToValidate.equals( validWebOrigins ) ) {
                log.warn( "    > [!] Web origins of portal clients do not match URIs from configuration!" );
                log.warn( String.format( "          - Configured web origins in KCL: %s", urisToValidate ) );
                log.warn( String.format( "          - Allowed web origins from ENV: %s", validWebOrigins ) );
            }
        }
        return isValid;
    }

    private boolean validateUris( List<String> uriList, String baseDomain, boolean allowLocalhost, boolean allowHttp ) {
        try {
            ValidationUtils.validateUriList( uriList, baseDomain, allowLocalhost, allowHttp );
            return true;
        } catch ( ValidationException ex ) {
            log.warn( String.format( "    > [!] Uri validation failed: %s", ex.getMessage() ) );
            return false;
        }
    }
}
