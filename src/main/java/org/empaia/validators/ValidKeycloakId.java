package org.empaia.validators;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import jakarta.validation.Constraint;

@Documented
@Constraint(validatedBy = KeycloakIdValidator.class)
@Target({TYPE})
@Retention(RUNTIME)
public @interface ValidKeycloakId {

    String message() default "Invalid Keycloak ID";

    Class[] groups() default {};

    Class[] payload() default {};
}
