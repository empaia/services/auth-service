package org.empaia.validators;


import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Component;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;


@Component
public class EmailConstraintValidator implements ConstraintValidator<ValidEmail, String> {

    @Override
    public void initialize( ValidEmail constraintAnnotation ) {
        // There is nothing to init
    }

    @Override
    public boolean isValid( String emailAddress, ConstraintValidatorContext constraintValidatorContext ) {
        var validator = EmailValidator.getInstance(false);

        // if emailAddress is null, then it cannot be validated. Entities should use @NotNull annotation to disallow
        // null.
        boolean isValid = emailAddress == null || validator.isValid( emailAddress );
        if ( !isValid ) {
            constraintValidatorContext.buildConstraintViolationWithTemplate("Invalid email address")
                    .addConstraintViolation()
                    .disableDefaultConstraintViolation();
        }

        return isValid;
    }
}

