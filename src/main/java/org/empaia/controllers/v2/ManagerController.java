package org.empaia.controllers.v2;

import lombok.extern.slf4j.Slf4j;
import org.empaia.configs.EmpaiaConfiguration;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.*;
import org.empaia.models.enums.v2.OrganizationUserRoleV2;
import org.empaia.security.organization.AuthorizeOrganizationV2;
import org.empaia.services.v2.ManagerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.ContentCachingRequestWrapper;

import jakarta.validation.Valid;


@Validated
@Slf4j
@RestController
@RequestMapping( "/api/v2/manager/organization" )
public class ManagerController {

    private final ManagerService managerService;

    public ManagerController( ManagerService managerService ) {
        this.managerService = managerService;
    }

    @GetMapping( path = "/membership-requests", produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( organizationUserRoles = { OrganizationUserRoleV2.MANAGER }, readOrganizationIdFromPath = false )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<MembershipRequestsEntity> getMembershipRequestsOfOrganization(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) @Valid KeycloakId organizationId,
            @RequestParam( "page" ) int page, @RequestParam( "page_size" ) int pageSize
    ) {
        return ResponseEntity.ok( managerService.getMembershipRequests( organizationId, page, pageSize ) );
    }

    @PutMapping( path = "/membership-requests/{membership_request_id}/accept", produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( organizationUserRoles = { OrganizationUserRoleV2.MANAGER }, readOrganizationIdFromPath = false )
    @Transactional
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<MembershipRequestEntity> acceptMembershipRequest(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) @Valid KeycloakId organizationId,
            @PathVariable( "membership_request_id" ) int membershipRequestId
    ) {
        return ResponseEntity.ok( managerService.acceptMembershipRequest( userId, organizationId, membershipRequestId ) );
    }

    @PutMapping( path = "/membership-requests/{membership_request_id}/deny", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( organizationUserRoles = { OrganizationUserRoleV2.MANAGER }, readOrganizationIdFromPath = false )
    @Transactional
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<MembershipRequestEntity> denyMembershipRequest(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) @Valid KeycloakId organizationId,
            @PathVariable( "membership_request_id" ) int membershipRequestId,
            @Valid @RequestBody PostReviewerComment postReviewerComment
    ) {
        return ResponseEntity.ok( managerService.denyMembershipRequest(
                userId, organizationId, membershipRequestId, postReviewerComment.getReviewerComment()
        ) );
    }

    @GetMapping( path = "/users", produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( organizationUserRoles = { OrganizationUserRoleV2.MANAGER }, readOrganizationIdFromPath = false )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<ConfidentialMemberProfilesEntity> getConfidentialMemberProfiles(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) KeycloakId organizationId
    ) {
        return ResponseEntity.ok( managerService.getMemberProfiles( organizationId ) );
    }

    @GetMapping( path = "/users/{target_user_id}/profile", produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( organizationUserRoles = { OrganizationUserRoleV2.MANAGER }, readOrganizationIdFromPath = false )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<ConfidentialMemberProfileEntity> getConfidentialMemberProfile(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) KeycloakId organizationId,
            @PathVariable( "target_user_id" ) KeycloakId targetUserId
    ) {
        OrganizationMember organizationMember = OrganizationMember.builder()
                .organizationId( organizationId )
                .userId( targetUserId )
                .build();
        return ResponseEntity.ok( managerService.getMemberProfile( organizationMember ) );
    }

    @PutMapping( path = "/users/{target_user_id}/remove" )
    @AuthorizeOrganizationV2( organizationUserRoles = { OrganizationUserRoleV2.MANAGER }, readOrganizationIdFromPath = false )
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> removeUserFromOrganization(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) @Valid KeycloakId organizationId,
            @PathVariable( "target_user_id" ) @Valid KeycloakId targetUserId
    ) {
        OrganizationMember organizationMember = OrganizationMember.builder()
                .organizationId( organizationId )
                .userId( targetUserId )
                .build();
        managerService.removeUserFromOrganization( organizationMember );
        return ResponseEntity.noContent().build();
    }

    @PutMapping( path = "/users/{target_user_id}/roles", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( organizationUserRoles = { OrganizationUserRoleV2.MANAGER }, readOrganizationIdFromPath = false )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<OrganizationUserRolesEntity> setOrganizationUserRoles(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) @Valid KeycloakId organizationId,
            @PathVariable( "target_user_id" ) @Valid KeycloakId targetUserId,
            @Valid @RequestBody PostOrganizationUserRolesEntity roles
    ) {
        OrganizationMember organizationMember = OrganizationMember.builder()
                .organizationId( organizationId )
                .userId( targetUserId )
                .build();
        return ResponseEntity.ok( managerService.setOrganizationUserRoles( organizationMember, roles ) );
    }

    @GetMapping( path = "/role-requests", produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( organizationUserRoles = { OrganizationUserRoleV2.MANAGER }, readOrganizationIdFromPath = false )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<OrganizationUserRoleRequestsEntity> getOrganizationUserRoleRequestsOfOrganization(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) @Valid KeycloakId organizationId,
            @RequestParam( "page" ) int page, @RequestParam( "page_size" ) int pageSize
    ) {
        return ResponseEntity.ok( managerService.getOrganizationUserRoleRequests( page, pageSize, organizationId ) );
    }

    @PutMapping( path = "/role-requests/{role_request_id}/accept", produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( organizationUserRoles = { OrganizationUserRoleV2.MANAGER }, readOrganizationIdFromPath = false )
    @Transactional
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<OrganizationUserRoleRequestEntity> acceptOrganizationUserRoleRequest(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) @Valid KeycloakId organizationId,
            @PathVariable( "role_request_id" ) int roleRequestId
    ) {
        return ResponseEntity.ok( managerService.acceptOrganizationUserRoleRequest( userId, organizationId, roleRequestId ) );
    }

    @PutMapping( path = "/role-requests/{role_request_id}/deny", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( organizationUserRoles = { OrganizationUserRoleV2.MANAGER }, readOrganizationIdFromPath = false )
    @Transactional
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<OrganizationUserRoleRequestEntity> denyOrganizationUserRoleRequest(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) @Valid KeycloakId organizationId,
            @PathVariable( "role_request_id" ) int roleRequestId,
            @Valid @RequestBody PostReviewerComment postReviewerComment
    ) {
        return ResponseEntity.ok( managerService.denyOrganizationUserRoleRequest(
                userId, organizationId, roleRequestId, postReviewerComment.getReviewerComment()
        ) );
    }

    @GetMapping( path = "/profile", produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( organizationUserRoles = { OrganizationUserRoleV2.MANAGER }, readOrganizationIdFromPath = false )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<ConfidentialOrganizationProfileEntity> getOrganizationProfile(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) @Valid KeycloakId organizationId
    ) {
        return ResponseEntity.ok( managerService.getOrganizationProfile( organizationId ) );
    }

    @PutMapping( path = "/profile/name", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( organizationUserRoles = { OrganizationUserRoleV2.MANAGER }, readOrganizationIdFromPath = false )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<ConfidentialOrganizationProfileEntity> setOrganizationName(
            ContentCachingRequestWrapper request,
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) @Valid KeycloakId organizationId,
            @RequestBody PostOrganizationNameEntity organizationNameEntity
    ) {
        return ResponseEntity.ok( managerService.updateOrganizationName( organizationId, organizationNameEntity ) );
    }

    @PutMapping( path = "/profile/contact", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( organizationUserRoles = { OrganizationUserRoleV2.MANAGER }, readOrganizationIdFromPath = false )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<ConfidentialOrganizationProfileEntity> setOrganizationContactData(
            ContentCachingRequestWrapper request,
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) @Valid KeycloakId organizationId,
            @RequestBody ConfidentialOrganizationContactDataEntity contactData
    ) {
        return ResponseEntity.ok( managerService.updateOrganizationContactData( organizationId, contactData ) );
    }

    @PutMapping( path = "/profile/details", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( organizationUserRoles = { OrganizationUserRoleV2.MANAGER }, readOrganizationIdFromPath = false )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<ConfidentialOrganizationProfileEntity> setOrganizationDetails(
            ContentCachingRequestWrapper request,
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) @Valid KeycloakId organizationId,
            @RequestBody PostConfidentialOrganizationDetailsEntity details
    ) {
        return ResponseEntity.ok( managerService.updateOrganizationDetails( organizationId, details ) );
    }

    @PutMapping( path = "/profile/logo", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( organizationUserRoles = { OrganizationUserRoleV2.MANAGER }, readOrganizationIdFromPath = false )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<ConfidentialOrganizationProfileEntity> setOrganizationLogo(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) @Valid KeycloakId organizationId,
            @RequestPart( "logo" ) MultipartFile logo
    ) throws Exception {
        return ResponseEntity.ok( managerService.setOrganizationLogo( organizationId, logo ) );
    }

    @GetMapping( path = "/category-requests", produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( organizationUserRoles = { OrganizationUserRoleV2.MANAGER }, readOrganizationIdFromPath = false )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<OrganizationCategoryRequestsEntity> getOrganizationCategoryRequests(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) @Valid KeycloakId organizationId,
            @RequestParam( "page" ) int page,
            @RequestParam( "page_size" ) int pageSize
    ) {
        return ResponseEntity.ok( managerService.getOrganizationCategoryRequests( organizationId, page, pageSize ) );
    }

    @PutMapping( path = "/category-requests", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( organizationUserRoles = { OrganizationUserRoleV2.MANAGER }, readOrganizationIdFromPath = false )
    @Transactional
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<OrganizationCategoryRequestEntity> requestOrganizationCategory(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) @Valid KeycloakId organizationId,
            @Valid @RequestBody PostOrganizationCategoryRequestEntity organizationCategoryRequest
    ) {
        return ResponseEntity.ok( managerService.requestOrganizationCategory( userId, organizationId, organizationCategoryRequest ) );
    }

    @PutMapping( path = "/category-requests/{category_request_id}/revoke", produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( organizationUserRoles = { OrganizationUserRoleV2.MANAGER }, readOrganizationIdFromPath = false )
    @Transactional
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<OrganizationCategoryRequestEntity> revokeOrganizationCategoryRequest(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) @Valid KeycloakId organizationId,
            @PathVariable( "category_request_id" ) int categoryRequestId
    ) {
        return ResponseEntity.ok( managerService.revokeOrganizationCategoryRequest( userId, organizationId, categoryRequestId ) );
    }
}
