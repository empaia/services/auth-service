package org.empaia.controllers.v2;

import org.empaia.configs.EmpaiaConfiguration;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.*;
import org.empaia.models.enums.v2.UserRole;
import org.empaia.services.v2.ModeratorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;

import java.util.List;


@Validated
@Slf4j
@RestController
@RequestMapping( value = "/api/v2/moderator" )
public class ModeratorController {

    private final ModeratorService moderatorService;

    ModeratorController( ModeratorService moderatorService ) {
        this.moderatorService = moderatorService;
    }

    @GetMapping( path = "/users", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<RegisteredUsersEntity> getUsers(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestParam int page,
            @RequestParam( "page_size" ) int pageSize
    ) {
        return ResponseEntity.ok( moderatorService.getUsers( page, pageSize ) );
    }

    @GetMapping( path = "/users/{target_user_id}/profile", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<UserProfileEntity> getUserProfile(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "target_user_id" ) @Valid KeycloakId targetUserId ) {
        return ResponseEntity.ok( moderatorService.getUserProfile( targetUserId ) );
    }

    @GetMapping( path = "/users/{target_user_id}/organizations", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<ConfidentialOrganizationsEntity> getOrganizationsOfUser(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "target_user_id" ) @Valid KeycloakId targetUserId,
            @RequestParam int page,
            @RequestParam( "page_size" ) int pageSize
    ) {
        return ResponseEntity.ok( moderatorService.getConfidentialOrganizationsOfUser( page, pageSize, targetUserId ) );
    }

    @GetMapping( path = "/unapproved-users/{unapproved_user_id}/profile", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<UserProfileEntity> getUserProfileOfUnapprovedUser(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "unapproved_user_id" ) @Valid KeycloakId unapprovedUserId
    ) {
        return ResponseEntity.ok( moderatorService.getUserProfileOfUnapprovedUser( unapprovedUserId ) );
    }

    @DeleteMapping( path = "/unapproved-users/{unapproved_user_id}/account" )
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<Void> deleteUnapprovedUser(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "unapproved_user_id" ) @Valid KeycloakId unapprovedUserId
    ) {
        moderatorService.deleteUnapprovedUser( unapprovedUserId, userId );
        return ResponseEntity.noContent().build();
    }

    @PutMapping( path = "/users/{target_user_id}/activate", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<UserProfileEntity> activateUser(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "target_user_id" ) @Valid KeycloakId targetUserId
    ) {
        return ResponseEntity.ok( moderatorService.activateUser( targetUserId ) );
    }

    @PutMapping( path = "/users/{target_user_id}/deactivate", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public UserProfileEntity deactivateUser(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "target_user_id" ) @Valid KeycloakId targetUserId
    ) {
        return moderatorService.deactivateUser( targetUserId, userId );
    }

    @DeleteMapping( path = "/users/{target_user_id}/account" )
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> deleteUserAccount(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "target_user_id" ) @Valid KeycloakId targetUserId
    ) {
        moderatorService.deleteUser( targetUserId, userId, false, true );
        return ResponseEntity.noContent().build();
    }

    @PutMapping( path = "/users/{target_user_id}/roles", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<List<UserRole>> setUserRoles(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "target_user_id" ) @Valid KeycloakId targetUserId,
            @Valid @RequestBody UserRolesEntity userRoles
    ) {
        return ResponseEntity.ok( moderatorService.setUserRoles( targetUserId, userRoles ) );
    }

    @GetMapping( path = "/unapproved-users", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<RegisteredUsersEntity> getUnapprovedUsers(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestParam int page,
            @RequestParam( "page_size" ) int pageSize
    ) {
        return ResponseEntity.ok( moderatorService.getUnapprovedUsers( page, pageSize ) );
    }

    @PutMapping( path = "/unapproved-organizations/{organization_id}/accept" )
    @Transactional
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> acceptOrganization(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId
    ) {
        moderatorService.acceptOrganization( organizationId );
        return ResponseEntity.noContent().build();
    }

    @PutMapping( path = "/unapproved-organizations/{organization_id}/deny", consumes = MediaType.APPLICATION_JSON_VALUE )
    @Transactional
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> denyOrganization(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId,
            @Valid @RequestBody PostReviewerComment reviewerComment
    ) {
        moderatorService.denyOrganization( organizationId, reviewerComment );
        return ResponseEntity.noContent().build();
    }

    @GetMapping( path = "/unapproved-organizations/{organization_id}/profile", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<ConfidentialOrganizationProfileEntity> getUnapprovedOrganizationProfile(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId
    ) {
        return ResponseEntity.ok( moderatorService.getUnapprovedOrganizationProfileEntity( organizationId ) );
    }

    @PutMapping( path = "/unapproved-organizations/query", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<ConfidentialOrganizationsEntity> getUnapprovedOrganizationsEntity(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @Valid @RequestBody OrganizationAccountStateQueryEntity organizationAccountStateQueryEntity,
            @RequestParam int page,
            @RequestParam( "page_size" ) int pageSize
    ) {
        return ResponseEntity.ok( moderatorService.getOrganizationsEntity( page, pageSize, organizationAccountStateQueryEntity.getOrganizationAccountStates() ) );
    }

    @GetMapping( path = "/organizations", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<ConfidentialOrganizationsEntity> getApprovedOrganizationsEntity(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestParam int page,
            @RequestParam( "page_size" ) int pageSize
    ) {
        return ResponseEntity.ok( moderatorService.getApprovedOrganizationsEntity( page, pageSize ) );
    }

    @GetMapping( path = "/organizations/{organization_id}", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<ConfidentialOrganizationProfileEntity> getConfidentialOrganizationProfileEntity(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId
    ) {
        return ResponseEntity.ok( moderatorService.getConfidentialOrganizationProfileEntity( organizationId ) );
    }

    @PutMapping( path = "/organizations/{organization_id}/activate" )
    @Transactional
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> activateOrganization(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId
    ) {
        moderatorService.activateOrganization( organizationId );
        return ResponseEntity.noContent().build();
    }

    @PutMapping( path = "/organizations/{organization_id}/deactivate" )
    @Transactional
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> deactivateOrganization(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId
    ) {
        moderatorService.deactivateOrganization( organizationId );
        return ResponseEntity.noContent().build();
    }

    @PutMapping( path = "/organizations/{organization_id}/categories", consumes = MediaType.APPLICATION_JSON_VALUE )
    @Transactional
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> setOrganizationCategories(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId,
            @Valid @RequestBody OrganizationCategoriesEntity organizationCategoriesEntity
    ) {
        moderatorService.setOrganizationCategories( organizationId, userId, organizationCategoriesEntity );
        return ResponseEntity.noContent().build();
    }

    @GetMapping( path = "/category-requests", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<OrganizationCategoryRequestsEntity> getAllOrganizationCategoryRequests(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestParam( "page" ) int page,
            @RequestParam( "page_size" ) int pageSize
    ) {
        return ResponseEntity.ok( moderatorService.getOrganizationCategoryRequests( page, pageSize ) );
    }

    @PutMapping( path = "/category-requests/{category_request_id}/accept", produces = MediaType.APPLICATION_JSON_VALUE )
    @Transactional
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<OrganizationCategoryRequestEntity> acceptOrganizationCategoryRequest(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "category_request_id" ) int categoryRequestId
    ) {
        return ResponseEntity.ok( moderatorService.acceptOrganizationCategoryRequest( userId, categoryRequestId ) );
    }

    @PutMapping( path = "/category-requests/{category_request_id}/deny", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @Transactional
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<OrganizationCategoryRequestEntity> denyOrganizationCategoryRequest(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "category_request_id" ) int categoryRequestId,
            @Valid @RequestBody PostReviewerComment reviewerComment
    ) {
        return ResponseEntity.ok( moderatorService.denyOrganizationCategoryRequest( userId, categoryRequestId, reviewerComment.getReviewerComment() ) );
    }
}
