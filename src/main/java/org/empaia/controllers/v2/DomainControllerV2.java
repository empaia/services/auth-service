package org.empaia.controllers.v2;

import org.empaia.models.dto.v2.CountryEntityV2;
import org.empaia.services.v2.DomainServiceV2;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping( value = "/api/v2/domain" )
public class DomainControllerV2 {
    public final DomainServiceV2 domainService;

    public DomainControllerV2( DomainServiceV2 domainService ) {
        this.domainService = domainService;
    }

    @GetMapping( path = "/countries", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<List<CountryEntityV2>> getCountries() {
        return ResponseEntity.ok( domainService.getCountries() );
    }

    @GetMapping( path = "/countries/{iso_3166_alpha_2_code}", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<CountryEntityV2> getCountry( @PathVariable( "iso_3166_alpha_2_code" ) String iso3166Alpha2Code ) {
        return ResponseEntity.ok( domainService.getCountry( iso3166Alpha2Code ) );
    }
}

