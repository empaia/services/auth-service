package org.empaia.controllers.v2;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.empaia.configs.EmpaiaConfiguration;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.*;
import org.empaia.services.v2.AdminService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.BadRequestException;
import java.util.List;


@Validated
@Slf4j
@RestController
@RequestMapping( value = "/api/v2/admin" )
public class AdminController {

    private final AdminService adminService;

    public AdminController( AdminService adminService ) {
        this.adminService = adminService;
    }

    @GetMapping( "/generate-email-verification-token" )
    @ResponseStatus( code = HttpStatus.OK )
    public ResponseEntity<?> generateEmailVerificationToken(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestParam( "email_address" ) String emailAddress,
            @RequestParam( "token_lifetime_ms" ) int tokenLifetimeMS
    ) {
        return ResponseEntity.ok( adminService.generateEmailVerificationToken( emailAddress, tokenLifetimeMS ) );
    }

    @DeleteMapping( "/users/{target_user_id}/account" )
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> deleteUser(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "target_user_id" ) @Valid KeycloakId targetUserId
    ) {
        if ( userId.equals( targetUserId ) ) {
            throw new BadRequestException( "Admin cannot delete own user account" );
        }
        adminService.deleteUser( targetUserId, userId );
        return ResponseEntity.noContent().build();
    }

    @PutMapping( "/users/{target_user_id}/grant_admin_privileges" )
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> grantAdminPrivileges(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "target_user_id" ) @Valid KeycloakId targetUserId
    ) {
        adminService.grantAdminPrivileges( targetUserId );
        return ResponseEntity.noContent().build();
    }

    @PutMapping( "/users/{target_user_id}/revoke_admin_privileges" )
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> revokeAdminPrivileges(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "target_user_id" ) @Valid KeycloakId targetUserId
    ) {
        adminService.revokeAdminPrivileges( targetUserId );
        return ResponseEntity.noContent().build();
    }

    @GetMapping( path = "/organizations", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( code = HttpStatus.OK )
    public ResponseEntity<ConfidentialOrganizationProfilesEntity> getOrganizationByAccountState(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestParam( "account_states" ) List<String> accountStates,
            @RequestParam( "page" ) int page,
            @RequestParam( "page_size" ) int pageSize ) {
        return ResponseEntity.ok( adminService.getOrganizationByAccountState( accountStates, page, pageSize ) );
    }

    @DeleteMapping( "/organizations/{organization_id}" )
    @Transactional
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> deleteOrganization(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId
    ) {
        adminService.deleteOrganization( organizationId );
        return ResponseEntity.noContent().build();
    }

    @GetMapping( value = "/clients" )
    @ResponseStatus( code = HttpStatus.OK )
    public ResponseEntity<ClientsEntity> getClients(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestParam( "page" ) int page,
            @RequestParam( "page_size" ) int pageSize
    ) {
        return ResponseEntity.ok( adminService.getAllClients( page, pageSize ) );
    }

    @GetMapping( value = "/organizations/{organization_id}/clients" )
    @ResponseStatus( code = HttpStatus.OK )
    public ResponseEntity<ClientsEntity> getClientsForOrganizationId(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId,
            @RequestParam( "page" ) int page,
            @RequestParam( "page_size" ) int pageSize
    ) {
        return ResponseEntity.ok( adminService.getClientsEntityForOrganization( organizationId, page, pageSize ) );
    }

    @DeleteMapping( "/organizations/{organization_id}/members_only_in_this_organization" )
    @Transactional
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> deleteMembersOnlyInThisOrganization(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId
    ) {
        adminService.deleteMembersOnlyInThisOrganization( organizationId, userId );
        return ResponseEntity.noContent().build();
    }

    @PutMapping( path = "/organizations/{organization_id}/deployment/config", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @Transactional
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<OrganizationDeploymentConfiguration> setOrganizationDeploymentConfiguration(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId,
            @RequestBody PostOrganizationDeploymentConfiguration postOrganizationDeploymentConfiguration
    ) {
        return ResponseEntity.ok( adminService.setOrganizationDeploymentConfiguration( organizationId, postOrganizationDeploymentConfiguration, userId ) );
    }

    @GetMapping( path = "/organizations/{organization_id}/deployment/config", produces = MediaType.APPLICATION_JSON_VALUE )
    @Transactional
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<OrganizationDeploymentConfiguration> getOrganizationDeploymentConfiguration(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId
    ) {
        return ResponseEntity.ok( adminService.getOrganizationDeploymentConfiguration( organizationId ) );
    }

    @PutMapping( path = "/organizations/{organization_id}/deployment/compute-provider-jes" )
    @Transactional
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> setComputeProviderJes(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId,
            @RequestBody @NotNull PostComputeProvider postComputeProvider
    ) {
        adminService.setComputeProviderForOrganization( organizationId, postComputeProvider );
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping( path = "/organizations/{organization_id}/deployment/compute-provider-jes" )
    @Transactional
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> deleteComputeProviderJes(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId
    ) {
        adminService.removeComputerProviderJesForOrganization( organizationId );
        return ResponseEntity.noContent().build();
    }

    @GetMapping( path = "/organizations/{organization_id}/deployment/client-credentials", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<OrganizationDeploymentCredentials> getOrganizationDeploymentCredentials(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId
    ) {
        return ResponseEntity.ok( adminService.getOrganizationDeploymentCredentials( organizationId ) );
    }
}
