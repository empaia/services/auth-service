package org.empaia.controllers.v2;

import jakarta.transaction.Transactional;
import org.empaia.configs.EmpaiaConfiguration;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.*;
import org.empaia.security.organization.AuthorizeOrganizationV2;
import org.empaia.services.v2.MemberService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;


@RestController
@RequestMapping( value = "/api/v2/organization" )
public class OrganizationControllerV2 {
    public final MemberService memberService;

    public OrganizationControllerV2( MemberService memberService ) {
        this.memberService = memberService;
    }

    @GetMapping( path = "/users", produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( allowAnyMember = true, readOrganizationIdFromPath = false )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<MemberProfilesEntity> getMemberProfiles(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) KeycloakId organizationId
    ) {
        return ResponseEntity.ok( memberService.getMemberProfiles( organizationId ) );
    }

    @GetMapping( path = "/users/{target_user_id}/profile", produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( allowAnyMember = true, readOrganizationIdFromPath = false )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<MemberProfileEntity> getMemberProfile(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) KeycloakId organizationId,
            @PathVariable( "target_user_id" ) KeycloakId targetUserId
    ) {
        OrganizationMember organizationMember = OrganizationMember.builder()
                .organizationId( organizationId )
                .userId( targetUserId )
                .build();
        return ResponseEntity.ok( memberService.getMemberProfile( organizationMember ) );
    }

    @GetMapping( path = "/can-leave", produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( allowAnyMember = true, readOrganizationIdFromPath = false )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<CanLeaveEntity> canLeaveOrganization(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) KeycloakId organizationId
    ) {
        return ResponseEntity.ok( CanLeaveEntity.builder().canLeave( memberService.canLeave( organizationId, userId ) ).build() );
    }

    @PutMapping( path = "/leave" )
    @AuthorizeOrganizationV2( allowAnyMember = true, readOrganizationIdFromPath = false )
    @ResponseStatus( HttpStatus.NO_CONTENT )
    @Transactional
    public ResponseEntity<?> leaveOrganization(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) KeycloakId organizationId
    ) {
        memberService.leave( organizationId, userId );
        return ResponseEntity.noContent().build();
    }

    @GetMapping( path = "/role-requests", produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( allowAnyMember = true, readOrganizationIdFromPath = false )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<OrganizationUserRoleRequestsEntity> getOrganizationUserRoleRequests(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) KeycloakId organizationId,
            @RequestParam( "page" ) int page,
            @RequestParam( "page_size" ) int pageSize
    ) {
        return ResponseEntity.ok( memberService.getOrganizationUserRoleRequests( userId, organizationId, page, pageSize ) );
    }

    @PutMapping( path = "/role-requests", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( allowAnyMember = true, readOrganizationIdFromPath = false )
    @ResponseStatus( HttpStatus.OK )
    @Transactional
    public ResponseEntity<OrganizationUserRoleRequestEntity> requestOrganizationUserRole(
            @AuthenticationPrincipal Jwt accessToken,
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) KeycloakId organizationId,
            @Valid @RequestBody PostOrganizationUserRoleRequestEntity organizationUserRoleRequest
    ) {
        return ResponseEntity.ok( memberService.requestOrganizationUserRole( userId, organizationId, organizationUserRoleRequest, accessToken ) );
    }

    @PutMapping( path = "/role-requests/{role_request_id}/revoke", produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( allowAnyMember = true, readOrganizationIdFromPath = false )
    @ResponseStatus( HttpStatus.OK )
    @Transactional
    public ResponseEntity<OrganizationUserRoleRequestEntity> revokeOrganizationUserRoleRequest(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestHeader( EmpaiaConfiguration.ORGANIZATION_HEADER_V2 ) KeycloakId organizationId,
            @PathVariable( "role_request_id" ) int roleRequestId
    ) {
        return ResponseEntity.ok( memberService.revokeOrganizationUserRoleRequest( userId, organizationId, roleRequestId ) );
    }
}

