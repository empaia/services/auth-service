package org.empaia.controllers.v2;

import lombok.extern.slf4j.Slf4j;
import org.empaia.configs.EmpaiaConfiguration;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.*;
import org.empaia.security.organization.AuthorizeOrganizationV2;
import org.empaia.services.v2.MyUnapprovedOrganizationsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.ContentCachingRequestWrapper;

import jakarta.validation.Valid;


@Validated
@Slf4j
@RestController
@RequestMapping( value = "/api/v2/my/unapproved-organizations" )
public class MyUnapprovedOrganizationsController {

    private final MyUnapprovedOrganizationsService myUnapprovedOrganizationsService;

    public MyUnapprovedOrganizationsController(
            MyUnapprovedOrganizationsService myUnapprovedOrganizationsService
    ) {
        this.myUnapprovedOrganizationsService = myUnapprovedOrganizationsService;
    }

    @PutMapping( path = "/query", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<UnapprovedConfidentialOrganizationProfilesEntity> queryUnapprovedOrganizations(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @Valid @RequestBody OrganizationAccountStateQueryEntity organizationAccountStateQueryEntity,
            @RequestParam( "page" ) int page,
            @RequestParam( "page_size" ) int pageSize
    ) {
        return ResponseEntity.ok( myUnapprovedOrganizationsService.getOrganizations(
                page, pageSize, organizationAccountStateQueryEntity.getOrganizationAccountStates(), userId
        ) );
    }

    @GetMapping( path = "/{organization_id}", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<UnapprovedConfidentialOrganizationProfileEntity> getUnapprovedOrganization(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organization_id
    ) {
        return ResponseEntity.ok( myUnapprovedOrganizationsService.getUnapprovedOrganization( organization_id ) );
    }

    @PostMapping( consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.CREATED )
    @Transactional
    public ResponseEntity<UnapprovedConfidentialOrganizationProfileEntity> createUnapprovedOrganization(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @Valid @RequestBody PostOrganizationEntity organization
    ) {
        return ResponseEntity.status( HttpStatus.CREATED ).body( myUnapprovedOrganizationsService.createUnapprovedOrganization( organization, userId ) );
    }

    @PutMapping( path = "/{organization_id}/contact", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( allowCreatorOfUnapprovedOrganization = true )
    @Transactional
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<UnapprovedConfidentialOrganizationProfileEntity> updateUnapprovedOrganizationContactData(
            ContentCachingRequestWrapper request,
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId,
            @RequestBody ConfidentialOrganizationContactDataEntity contactData
    ) {
        return ResponseEntity.ok( myUnapprovedOrganizationsService.updateUnapprovedOrganizationContactData( organizationId, contactData ) );
    }

    @PutMapping( path = "/{organization_id}/details", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( allowCreatorOfUnapprovedOrganization = true )
    @Transactional
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<UnapprovedConfidentialOrganizationProfileEntity> updateUnapprovedOrganizationDetails(
            ContentCachingRequestWrapper request,
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId,
            @RequestBody PostInitialConfidentialOrganizationDetailsEntity details
    ) {
        return ResponseEntity.ok( myUnapprovedOrganizationsService.updateUnapprovedOrganizationDetails( organizationId, details ) );
    }

    @PutMapping( path = "/{organization_id}/name", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( allowCreatorOfUnapprovedOrganization = true )
    @Transactional
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<UnapprovedConfidentialOrganizationProfileEntity> updateUnapprovedOrganizationName(
            ContentCachingRequestWrapper request,
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId,
            @RequestBody PostOrganizationNameEntity organizationNameEntity
    ) {
        return ResponseEntity.ok(
                myUnapprovedOrganizationsService.updateUnapprovedOrganizationName( organizationId, organizationNameEntity )
        );
    }

    @PutMapping( path = "/{organization_id}/logo", consumes = MediaType.MULTIPART_FORM_DATA_VALUE )
    @AuthorizeOrganizationV2( allowCreatorOfUnapprovedOrganization = true )
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> setUnapprovedOrganizationLogo(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId,
            @RequestPart( "logo" ) MultipartFile logo
    ) throws Exception {
        myUnapprovedOrganizationsService.setUnapprovedOrganizationLogo( organizationId, logo );
        return ResponseEntity.noContent().build();
    }

    @GetMapping( path = "/{organization_id}/can-request-approval", produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( allowCreatorOfUnapprovedOrganization = true )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<CanRequestApprovalEntity> canRequestApproval(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId
    ) {
        return ResponseEntity.ok( CanRequestApprovalEntity.builder()
                .canRequestApproval( myUnapprovedOrganizationsService.canRequestApproval( organizationId ) )
                .build() );
    }

    @PutMapping( path = "/{organization_id}/request-approval", produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( allowCreatorOfUnapprovedOrganization = true )
    @Transactional
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<UnapprovedConfidentialOrganizationProfileEntity> requestUnapprovedOrganizationApproval(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId
    ) {
        return ResponseEntity.ok( myUnapprovedOrganizationsService.requestUnapprovedOrganizationApproval( organizationId ) );
    }

    @PutMapping( path = "/{organization_id}/revoke-approval-request", produces = MediaType.APPLICATION_JSON_VALUE )
    @AuthorizeOrganizationV2( allowCreatorOfUnapprovedOrganization = true )
    @Transactional
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<UnapprovedConfidentialOrganizationProfileEntity> revokeUnapprovedOrganizationApprovalRequest(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId
    ) {
        return ResponseEntity.ok( myUnapprovedOrganizationsService.revokeUnapprovedOrganizationApprovalRequest( organizationId ) );
    }

    @DeleteMapping( "/{organization_id}" )
    @AuthorizeOrganizationV2( allowCreatorOfUnapprovedOrganization = true )
    @Transactional
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> deleteUnapprovedOrganization(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "organization_id" ) @Valid KeycloakId organizationId
    ) {
        myUnapprovedOrganizationsService.deleteUnapprovedOrganization( organizationId );
        return ResponseEntity.noContent().build();
    }
}
