package org.empaia.controllers.v2;

import lombok.extern.slf4j.Slf4j;
import org.empaia.exceptions.generic.AlreadyExistException;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.PublicOrganizationsEntity;
import org.empaia.models.dto.v2.PostUserEntity;
import org.empaia.models.dto.v2.PublicOrganizationProfileEntity;
import org.empaia.services.v2.PublicService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;


@Validated
@Slf4j
@RestController
@RequestMapping( "/api/v2/public" )
public class PublicController {

    private final PublicService publicService;

    public PublicController( PublicService publicService ) {
        this.publicService = publicService;
    }

    @GetMapping( path = "/organizations", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<PublicOrganizationsEntity> getActiveOrganizations(
            @RequestParam( value = "page", defaultValue = "0", required = false ) int page,
            @RequestParam( value = "page_size", defaultValue = "10", required = false ) int pageSize
    ) {
        return ResponseEntity.ok( publicService.getActiveOrganizations( page, pageSize ) );
    }

    @GetMapping( path = "/organizations/{organization_id}", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<PublicOrganizationProfileEntity> getActiveOrganization( @PathVariable( "organization_id" ) KeycloakId organizationId ) {
        return ResponseEntity.ok( publicService.getActiveOrganization( organizationId ) );
    }

    @PostMapping( path = "/register-new-user", consumes = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> registerNewUser( @Valid @RequestBody PostUserEntity userEntity ) {
        try {
            publicService.createNewUser( userEntity );
        } catch ( AlreadyExistException ex ) {
            // ignore already existing user for privacy reasons
        }
        return ResponseEntity.noContent().build();
    }

    @PostMapping( "/verify-registration" )
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> verifyAccount(
            @Valid @NotNull @RequestParam( "email_address" ) String emailAddress,
            @Valid @NotNull @RequestParam( "token" ) String tokenString
    ) {
        publicService.verifyRegistration( emailAddress, tokenString );
        return ResponseEntity.noContent().build();
    }

}
