package org.empaia.controllers.v2;

import com.jlefebure.spring.boot.minio.MinioException;
import lombok.extern.slf4j.Slf4j;
import org.empaia.configs.EmpaiaConfiguration;
import org.empaia.models.auth.KeycloakId;
import org.empaia.models.dto.v2.*;
import org.empaia.services.v2.MyService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.ContentCachingRequestWrapper;

import jakarta.validation.Valid;

import java.io.IOException;


@Validated
@Slf4j
@RestController
@RequestMapping( value = "/api/v2/my" )
public class MyController {

    private final MyService myService;

    public MyController( MyService myService ) {
        this.myService = myService;
    }

    @GetMapping( path = "/profile", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<UserProfileEntity> getProfile(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId
    ) {
        return ResponseEntity.ok( myService.getProfile( userId ) );
    }

    @PutMapping( path = "/profile/language", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<UserProfileEntity> updateLanguage(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @Valid @RequestBody LanguageEntity languageEntity
    ) {
        return ResponseEntity.ok( myService.updateLanguage( userId, languageEntity.getLanguageCode() ) );
    }

    @PutMapping( path = "/profile/details", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public UserProfileEntity updateDetails(
            ContentCachingRequestWrapper request,
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @Valid @RequestBody UserDetailsEntity userDetails
    ) {
        return myService.updateDetails( userId, userDetails );
    }

    @PutMapping( path = "/profile/contact", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<UserProfileEntity> updateContactData(
            ContentCachingRequestWrapper request,
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @Valid @RequestBody PostUserContactDataEntity userContactData
    ) {
        return ResponseEntity.ok( myService.updateContactData( userId, userContactData ) );
    }

    @DeleteMapping( path = "/profile/picture" )
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> deleteProfilePictures(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId
    ) throws IOException, MinioException {
        myService.deleteProfilePictures( userId );
        return ResponseEntity.noContent().build();
    }

    @PutMapping( path = "/profile/picture", consumes = MediaType.MULTIPART_FORM_DATA_VALUE )
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> setProfilePicture(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestPart( "picture" ) MultipartFile picture
    ) throws Exception {
        myService.setProfilePicture( userId, picture );
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping( "/account" )
    @Transactional
    @ResponseStatus( HttpStatus.NO_CONTENT )
    public ResponseEntity<?> deleteMyUserAccount(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId
    ) {
        myService.deleteUser( userId, userId );
        return ResponseEntity.noContent().build();
    }

    @GetMapping( path = "/membership-requests", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<MembershipRequestsEntity> getMembershipRequests(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestParam( "page" ) int page,
            @RequestParam( "page_size" ) int pageSize
    ) {
        return ResponseEntity.ok( myService.getMembershipRequests( userId, page, pageSize ) );
    }

    @PostMapping( path = "/membership-requests", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.CREATED )
    @Transactional
    public ResponseEntity<MembershipRequestEntity> requestMembership(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @Valid @RequestBody PostMembershipRequestEntity membershipRequest
    ) {
        return ResponseEntity.status( HttpStatus.CREATED ).body( myService.requestMembership( userId, membershipRequest ) );
    }

    @PutMapping( path = "/membership-requests/{membership_request_id}/revoke", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    @Transactional
    public MembershipRequestEntity revokeMembershipRequest(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @PathVariable( "membership_request_id" ) Integer membershipRequestId
    ) {
        return myService.revokeMembershipRequest( userId, membershipRequestId );
    }

    @GetMapping( path = "/default-active-organization", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<DefaultActiveOrganizationEntity> getDefaultActiveOrganization(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId
    ) {
        return ResponseEntity.ok( myService.getDefaultActiveOrganization( userId ) );
    }

    @PutMapping( path = "/default-active-organization", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    @Transactional
    public ResponseEntity<DefaultActiveOrganizationEntity> setDefaultActiveOrganization(
            ContentCachingRequestWrapper request,
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @Valid @RequestBody DefaultActiveOrganizationEntity defaultActiveOrganization
    ) {
        return ResponseEntity.ok( myService.setDefaultActiveOrganization( userId, defaultActiveOrganization ) );
    }

    @DeleteMapping( path = "/default-active-organization" )
    @ResponseStatus( HttpStatus.NO_CONTENT )
    @Transactional
    public ResponseEntity<?> deleteDefaultActiveOrganization(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId
    ) {
        myService.deleteDefaultActiveOrganization( userId );
        return ResponseEntity.noContent().build();
    }

    @GetMapping( path = "/organizations", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<PublicOrganizationProfilesEntity> getOrganizations(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId,
            @RequestParam( value = "page", defaultValue = "0", required = false ) int page,
            @RequestParam( value = "page_size", defaultValue = "10", required = false ) int pageSize
    ) {
        return ResponseEntity.ok( myService.getActivePublicOrganizations( userId, page, pageSize ) );
    }

    @GetMapping( path = "/memberships", produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<MembershipsEntity> getMemberships(
            @RequestHeader( EmpaiaConfiguration.USER_HEADER_V2 ) @Valid KeycloakId userId
    ) {
        return ResponseEntity.ok( myService.getMemberships( userId ) );
    }
}
