package org.empaia.repositories.v2;

import org.empaia.models.db.v2.DBDeletedUserHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DeletedUserHistoryRepository extends JpaRepository<DBDeletedUserHistory, Integer> {
    Optional<DBDeletedUserHistory> findByUserId( String userId );
}
