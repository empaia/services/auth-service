package org.empaia.repositories.v2;

import org.empaia.models.db.v2.DBOrganizationUserRoleRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganizationUserRoleRequestsRepository extends JpaRepository<DBOrganizationUserRoleRequest, Integer> {

    Page<DBOrganizationUserRoleRequest> findAllByOrganizationId( String organizationId, Pageable pageable );

    Page<DBOrganizationUserRoleRequest> findAllByOrganizationIdAndRequestState(
            String organizationId, int requestState, Pageable pageable
    );

    Page<DBOrganizationUserRoleRequest> findAllByOrganizationIdAndUserId( String organizationId, String userId, Pageable pageable );

    Page<DBOrganizationUserRoleRequest> findAllByOrganizationIdAndUserIdAndRequestState(
            String organizationId, String userId, int requestState, Pageable pageable
    );

    Page<DBOrganizationUserRoleRequest> findAllByUserId( String userId, Pageable pageable );
}
