package org.empaia.repositories.v2;

import org.empaia.models.db.v2.DBOrganizationCategoryRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrganizationCategoryRequestsRepository extends JpaRepository<DBOrganizationCategoryRequest, Integer> {

    Page<DBOrganizationCategoryRequest> findAllByOrganizationId( String organizationId, Pageable pageable );

    Page<DBOrganizationCategoryRequest> findAllByRequestState( int requestState, Pageable pageable );

    Optional<DBOrganizationCategoryRequest> findByOrganizationCategoryRequestId( int organizationCategoryRequestId );

    boolean existsByOrganizationIdAndRequestState( String organizationId, int requestState );

    Page<DBOrganizationCategoryRequest> findAllByCreatorId( String creatorId, Pageable pageable );
}
