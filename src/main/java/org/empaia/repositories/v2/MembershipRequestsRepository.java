package org.empaia.repositories.v2;

import org.empaia.models.db.v2.DBMembershipRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MembershipRequestsRepository extends JpaRepository<DBMembershipRequest, Integer> {

    Page<DBMembershipRequest> findAllByOrganizationId( String organizationId, Pageable pageable );

    Page<DBMembershipRequest> findAllByOrganizationIdAndRequestState(
            String organizationId, int requestState, Pageable pageable
    );

    Page<DBMembershipRequest> findAllByUserId( String userId, Pageable pageable );

    Page<DBMembershipRequest> findAllByUserIdAndRequestState(
            String userId, int requestState, Pageable pageable
    );

    Optional<DBMembershipRequest> findByOrganizationIdAndUserId( String organizationId, String userId );

    Optional<DBMembershipRequest> findByOrganizationIdAndUserIdAndRequestState(
            String organizationId, String userId, int requestState
    );

    Optional<DBMembershipRequest> findByMembershipRequestId( int membershipRequestId );
}
