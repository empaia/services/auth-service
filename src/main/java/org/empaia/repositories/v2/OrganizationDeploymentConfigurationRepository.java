package org.empaia.repositories.v2;

import org.empaia.models.db.v2.DBOrganizationDeploymentConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository

public interface OrganizationDeploymentConfigurationRepository extends JpaRepository<DBOrganizationDeploymentConfiguration, Integer> {
    Optional<DBOrganizationDeploymentConfiguration> findByOrganizationId(String organizationId );
}
