package org.empaia.repositories.v2;

import org.empaia.models.db.v2.DBOrganizationDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrganizationDetailsRepository extends JpaRepository<DBOrganizationDetails, Integer> {
    Optional<DBOrganizationDetails> findByKeycloakId( String keycloakId );

    Optional<DBOrganizationDetails> findByKeycloakIdAndCreatorIdAndAccountStateIn( String keycloakId, String creatorId, List<Integer> accountState );

    Optional<DBOrganizationDetails> findByKeycloakIdAndAccountState( String keycloakId, Integer accountState );

    Optional<DBOrganizationDetails> findByKeycloakIdAndAccountStateIn( String keycloakId, List<Integer> accountState );

    Page<DBOrganizationDetails> findAllByAccountState( Integer accountState, Pageable pageable );

    Page<DBOrganizationDetails> findAllByAccountStateIn( List<Integer> accountState, Pageable pageable );

    Page<DBOrganizationDetails> findAllByKeycloakIdInAndAccountStateIn( List<String> keycloakId, List<Integer> accountState, Pageable pageable );

    Page<DBOrganizationDetails> findAllByCreatorIdAndAccountStateIn( String creatorId, List<Integer> accountState, Pageable pageable );

    Page<DBOrganizationDetails> findAllByCreatorId( String creatorId, Pageable pageable );
}
