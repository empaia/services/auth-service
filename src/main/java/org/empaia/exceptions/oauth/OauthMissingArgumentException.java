package org.empaia.exceptions.oauth;

public class OauthMissingArgumentException extends RuntimeException {

    public OauthMissingArgumentException( String error ) {
        super( error );
    }
}
