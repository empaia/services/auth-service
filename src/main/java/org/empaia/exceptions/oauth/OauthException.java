package org.empaia.exceptions.oauth;

public class OauthException extends RuntimeException {

    public OauthException( String error ) {
        super( error );
    }
}
