package org.empaia.exceptions.jwt;

public class JwtGenericException extends RuntimeException {

    public JwtGenericException( String error ) {
        super( error );
    }
}

