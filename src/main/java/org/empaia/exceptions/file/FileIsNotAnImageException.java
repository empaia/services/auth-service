package org.empaia.exceptions.file;

public class FileIsNotAnImageException extends RuntimeException {

    public FileIsNotAnImageException( String filename ) {
        super( String.format( "The file '%s' is not an image", filename ) );
    }
}
