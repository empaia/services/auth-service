package org.empaia.exceptions.generic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotFoundException extends RuntimeException {
    private String title;

    public NotFoundException( String message ) {
        super( message );
    }

    public NotFoundException( String title, String message ) {
        super( message );
        this.title = title;
    }

    public NotFoundException( String title, int message ) {
        super( String.valueOf( message ) );
        this.title = title;
    }

    public NotFoundException( String title, String message, Throwable cause ) {
        super( message, cause );
        this.title = title;
    }

    public NotFoundException( String title, int message, Throwable cause ) {
        super( String.valueOf( message ), cause );
        this.title = title;
    }

    public NotFoundException( String title, Throwable cause ) {
        super( cause );
        this.title = title;
    }

    public NotFoundException(
            String title, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace
    )
    {
        super( message, cause, enableSuppression, writableStackTrace );
        this.title = title;
    }
}
