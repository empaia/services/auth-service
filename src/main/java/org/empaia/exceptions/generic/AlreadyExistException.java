package org.empaia.exceptions.generic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AlreadyExistException extends RuntimeException {
    private final String title;

    public AlreadyExistException( String message ) {
        super( message );
        this.title = null;
    }

    public AlreadyExistException( String title, String message ) {
        super( message );
        this.title = title;
    }

    public AlreadyExistException( String title, int message ) {
        super( String.valueOf( message ) );
        this.title = title;
    }

    public AlreadyExistException( String title, String message, Throwable cause ) {
        super( message, cause );
        this.title = title;
    }

    public AlreadyExistException( String title, int message, Throwable cause ) {
        super( String.valueOf( message ), cause );
        this.title = title;
    }

    public AlreadyExistException( String title, Throwable cause ) {
        super( cause );
        this.title = title;
    }

    public AlreadyExistException(
            String title, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace
    )
    {
        super( message, cause, enableSuppression, writableStackTrace );
        this.title = title;
    }
}