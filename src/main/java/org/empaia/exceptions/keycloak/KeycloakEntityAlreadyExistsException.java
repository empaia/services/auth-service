package org.empaia.exceptions.keycloak;

public class KeycloakEntityAlreadyExistsException extends RuntimeException {

    public KeycloakEntityAlreadyExistsException( String error ) {
        super( error );
    }
}
