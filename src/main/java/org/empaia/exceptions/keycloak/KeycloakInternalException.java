package org.empaia.exceptions.keycloak;

public class KeycloakInternalException extends RuntimeException {

    public KeycloakInternalException( String error ) {
        super( error );
    }
}
