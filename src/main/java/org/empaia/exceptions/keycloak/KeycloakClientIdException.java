package org.empaia.exceptions.keycloak;

public class KeycloakClientIdException extends RuntimeException {

    public KeycloakClientIdException( String error ) {
        super( error );
    }
}
