package org.empaia.exceptions;

import org.apache.commons.lang3.NotImplementedException;
import org.empaia.exceptions.file.FileIsNotAnImageException;
import org.empaia.exceptions.generic.AlreadyExistException;
import org.empaia.exceptions.generic.NotFoundException;
import org.empaia.exceptions.jwt.JwtGenericException;
import org.empaia.exceptions.jwt.JwtInvalidException;
import org.empaia.exceptions.keycloak.KeycloakClientIdException;
import org.empaia.exceptions.keycloak.KeycloakInternalException;
import org.empaia.exceptions.keycloak.KeycloakEntityAlreadyExistsException;
import org.empaia.exceptions.oauth.OauthException;
import org.empaia.exceptions.oauth.OauthMissingArgumentException;
import org.empaia.exceptions.user.ValidationException;
import org.empaia.models.exception.AdvisorError;
import io.jsonwebtoken.*;
import io.minio.errors.MinioException;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import jakarta.validation.ConstraintViolationException;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotAllowedException;
import javax.ws.rs.NotAuthorizedException;
import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
@ResponseBody
@Slf4j
public class GenericControllerAdvisor extends ResponseEntityExceptionHandler {

    private static final String JWT_ERROR = "JWT Validation error";

    @Value( "${empaia.log-exceptions}" )
    private boolean logExceptions;

    //region User
    @ExceptionHandler( BadRequestException.class )
    public ResponseEntity<Object> handleBadRequestException( WebRequest request, BadRequestException e ) {
        return handler( request, e, "Bad Request", e.getMessage(), HttpStatus.BAD_REQUEST, logExceptions );
    }

    @ExceptionHandler( ValidationException.class )
    public ResponseEntity<Object> handlePasswordMatchException( WebRequest request, ValidationException e ) {
        return handler( request, e, "Validation error", e.getMessage(), HttpStatus.BAD_REQUEST, logExceptions );
    }

    @ExceptionHandler( KeycloakEntityAlreadyExistsException.class )
    public ResponseEntity<Object> handleKeycloakUserAlreadyExistsException(
            WebRequest request, KeycloakEntityAlreadyExistsException e
    ) {
        return handler( request, e, "Creation error", e.getMessage(), HttpStatus.BAD_REQUEST, logExceptions );
    }

    //endregion

    //region OAuth

    @ExceptionHandler( OauthException.class )
    public ResponseEntity<Object> handleOauthException( WebRequest request, OauthException e ) {
        return handler( request,
                e,
                "There was an error with your request",
                e.getMessage(),
                HttpStatus.BAD_REQUEST,
                logExceptions
        );
    }

    @ExceptionHandler( OauthMissingArgumentException.class )
    public ResponseEntity<Object> handleOauthException( WebRequest request, OauthMissingArgumentException e ) {
        return handler( request, e, "Missing argument", e.getMessage(), HttpStatus.BAD_REQUEST, logExceptions );
    }

    //endregion

    //region Keycloak

    @ExceptionHandler( KeycloakClientIdException.class )
    public ResponseEntity<Object> handleKeycloakClientIdException( WebRequest request, KeycloakClientIdException e ) {
        return handler( request, e, "Keycloak not found", e.getMessage(), HttpStatus.NOT_FOUND, logExceptions );
    }

    @ExceptionHandler( KeycloakInternalException.class )
    public ResponseEntity<Object> handleKeycloakInternalException( WebRequest request, KeycloakInternalException e ) {
        return handler( request, e, "Internal server error", e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, true );
    }

    //endregion

    //region JWT

    @ExceptionHandler( SignatureException.class )
    public ResponseEntity<Object> handleSignatureException( WebRequest request, SignatureException e ) {
        return handler( request,
                e,
                JWT_ERROR,
                "Unable to verify the validity of your token",
                HttpStatus.FORBIDDEN,
                logExceptions
        );
    }

    @ExceptionHandler( IncorrectClaimException.class )
    public ResponseEntity<Object> handleIncorrectClaimException( WebRequest request, IncorrectClaimException e ) {
        return handler( request, e, JWT_ERROR, e.getMessage(), HttpStatus.FORBIDDEN, logExceptions );
    }

    @ExceptionHandler( PrematureJwtException.class )
    public ResponseEntity<Object> handlePrematureJwtException( WebRequest request, PrematureJwtException e ) {
        return handler( request, e, "Too early", e.getMessage().split( "\\." )[0], HttpStatus.FORBIDDEN, logExceptions );
    }

    @ExceptionHandler( MalformedJwtException.class )
    public ResponseEntity<Object> handleMalformedJwtException( WebRequest request, MalformedJwtException e ) {
        return handler( request, e, JWT_ERROR, e.getMessage(), HttpStatus.FORBIDDEN, logExceptions );
    }

    @ExceptionHandler( ExpiredJwtException.class )
    public ResponseEntity<Object> handleExpiredJwtException( WebRequest request, ExpiredJwtException e ) {
        return handler( request, e, "Too late", e.getMessage().split( "\\." )[0], HttpStatus.FORBIDDEN, logExceptions );
    }

    @ExceptionHandler( JwtGenericException.class )
    public ResponseEntity<Object> handleJwtGenericException( WebRequest request, JwtGenericException e ) {
        return handler( request, e, "Server error", e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, true );
    }

    @ExceptionHandler( JwtInvalidException.class )
    public ResponseEntity<Object> handleJwtInvalidException( WebRequest request, JwtInvalidException e ) {
        return handler( request, e, JWT_ERROR, e.getMessage(), HttpStatus.FORBIDDEN, logExceptions );
    }

    //endregion

    //region Files

    @ExceptionHandler( FileIsNotAnImageException.class )
    public ResponseEntity<Object> handleFileIsNotAnImageException( WebRequest request, FileIsNotAnImageException e ) {
        return handler( request, e, "Invalid file format", e.getMessage(), HttpStatus.BAD_REQUEST, logExceptions );
    }

    @ExceptionHandler( MinioException.class )
    public ResponseEntity<Object> handleIOException( WebRequest request, MinioException e ) {
        return handler( request,
                e,
                "Minio error",
                "There was an error while storing the file",
                HttpStatus.INTERNAL_SERVER_ERROR,
                true
        );
    }

    //endregion

    //region Generic

    @ExceptionHandler( NotImplementedException.class )
    public ResponseEntity<Object> handleNotImplementedException( WebRequest request, NotImplementedException e ) {
        return handler( request, e, "Not implemented", e.getMessage(), HttpStatus.NOT_IMPLEMENTED, logExceptions
        );
    }

    @ExceptionHandler( NotFoundException.class )
    public ResponseEntity<Object> handleNotFoundException( WebRequest request, NotFoundException e ) {
        return handler( request,
                e,
                e.getTitle() == null ? "Not found" : e.getTitle(),
                e.getMessage(),
                HttpStatus.NOT_FOUND,
                logExceptions
        );
    }

    @ExceptionHandler( FileNotFoundException.class )
    public ResponseEntity<Object> handleFileNotFoundException( WebRequest request, FileNotFoundException e ) {
        return handler( request,
                e,
                "Not found",
                e.getMessage(),
                HttpStatus.NOT_FOUND,
                logExceptions
        );
    }

    @ExceptionHandler( AlreadyExistException.class )
    public ResponseEntity<Object> handleAlreadyExistException( WebRequest request, AlreadyExistException e ) {
        return handler( request,
                e,
                e.getTitle() == null ? "Already exists" : e.getTitle(),
                e.getMessage(),
                HttpStatus.CONFLICT,
                logExceptions
        );
    }

    @ExceptionHandler( AccessDeniedException.class )
    public ResponseEntity<Object> handleAccessDeniedException(
            WebRequest request, AccessDeniedException e
    ) {
        return handler( request, e, "Access Denied", e.getMessage(), HttpStatus.FORBIDDEN, logExceptions );
    }

    @ExceptionHandler( MaxUploadSizeExceededException.class )
    public ResponseEntity<Object> handleMaxUploadSizeExceededException(
            WebRequest request, MaxUploadSizeExceededException e
    ) {
        return handler( request,
                e,
                "Upload size exceeded",
                "The file size was greater than the server limit",
                HttpStatus.BAD_REQUEST,
                logExceptions
        );
    }

    @ExceptionHandler( NotAuthorizedException.class )
    public ResponseEntity<Object> handleNotAuthorizedException(
            WebRequest request, NotAuthorizedException e
    ) {
        return handler( request,
                e,
                "Not authorized.",
                e.getMessage(),
                HttpStatus.FORBIDDEN,
                logExceptions
        );
    }

    @ExceptionHandler( NotAllowedException.class )
    public ResponseEntity<Object> handleNotAllowedException(
            WebRequest request, NotAllowedException e
    ) {
        return handler( request,
                e,
                "Forbidden",
                e.getMessage(),
                HttpStatus.FORBIDDEN,
                logExceptions
        );
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, @NotNull HttpHeaders headers, @NotNull HttpStatusCode status, @NotNull WebRequest request
    ) {
        Map<String, String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map( field -> Map.entry( field.getField(),
                        getFieldErrorMessage( field, "error" )
                ) )
                .collect( Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        ( value1, value2 ) -> value1 + "; " + value2
                ) );


        return handler( request, ex, "Invalid Request", "The body couldn't be validated", errors, status, true );
    }

    @ExceptionHandler( ConstraintViolationException.class )
    public ResponseEntity<Object> handleConstraintViolationException(
            WebRequest request, ConstraintViolationException e
    ) {
        return handler( request, e, "Invalid data", e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY, true );
    }

    @ExceptionHandler( Exception.class )
    public ResponseEntity<Object> handleException( WebRequest request, Exception e ) {
        return handler( request,
                e,
                "An internal server error occurred",
                "If this problem persists, then please contact support@empaia.org.",
                HttpStatus.INTERNAL_SERVER_ERROR,
                true
        );
    }

    //endregion

    private ResponseEntity<Object> handler(
            WebRequest request, Exception e, String title, String description, HttpStatus status, boolean doLog
    ) {
        AdvisorError error = AdvisorError.builder()
                .error( title )
                .errorDescription( description )
                .timestamp( LocalDateTime.now() )
                .build();

        if ( doLog ) {

            log.error( "Error on request: {}", request.getDescription( true ) );
            log.error( title, e );
        }

        return new ResponseEntity<>( error, status );
    }

    private ResponseEntity<Object> handler(
            WebRequest request,
            Exception e,
            String title,
            String description,
            Map<String, String> errorMessages,
            HttpStatusCode status,
            boolean doLog
    ) {
        AdvisorError error = AdvisorError.builder()
                .error( title )
                .errorDescription( description )
                .errors( errorMessages )
                .timestamp( LocalDateTime.now() )
                .build();

        if ( doLog ) {

            log.error( "Error on request: {}", request.getDescription( true ) );
            log.error( title, e );
        }

        return new ResponseEntity<>( error, status );
    }

    private String getFieldErrorMessage( FieldError error, String defaultMessage ) {
        return error.getDefaultMessage() == null ? defaultMessage : error.getDefaultMessage();
    }
}
