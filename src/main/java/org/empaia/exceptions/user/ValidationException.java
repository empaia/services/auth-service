package org.empaia.exceptions.user;

public class ValidationException extends RuntimeException {

    public static final String SPECIAL_CHARACTERS_NOT_ALLOWED_MSG = "Special characters not allowed.";

    public ValidationException( String error ) {
        super( error );
    }
}
