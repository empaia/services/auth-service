create table roles
(
    role_id int,
    role_name varchar,
    role_description varchar,
    role_flag int
);


insert into roles ( role_id, role_name, role_description, role_flag ) values ( 1, 'Administrator', 'Org admin', 1 );
insert into roles ( role_id, role_name, role_description, role_flag ) values ( 2, 'Accountant', 'Org Acc', 2 );
insert into roles ( role_id, role_name, role_description, role_flag ) values ( 3, 'Vendor', 'Org Ven', 4 );
insert into roles ( role_id, role_name, role_description, role_flag ) values ( 4, 'Member', 'Org Mem', 8 );
insert into roles ( role_id, role_name, role_description, role_flag ) values ( 5, 'Manager', 'Organization Manager', 16 );
insert into roles ( role_id, role_name, role_description, role_flag ) values ( 6, 'Pathologist', 'Pathologist', 32 );
insert into roles ( role_id, role_name, role_description, role_flag ) values ( 7, 'App Maintainer', 'App Maintainer', 64 );
insert into roles ( role_id, role_name, role_description, role_flag ) values ( 8, 'Data Manager', 'Data Manager', 128 );

create table organization_roles
(
    organization_role_id int,
    organization_role_name varchar
);

insert into organization_roles ( organization_role_id, organization_role_name ) values ( 1, 'Hospital' );
insert into organization_roles ( organization_role_id, organization_role_name ) values ( 2, 'AI Vendor' );
insert into organization_roles ( organization_role_id, organization_role_name ) values ( 3, 'Data Provider' );

create table account_states
(
    account_state_id int,
    state_name varchar
);

insert into account_states ( account_state_id, state_name ) values ( 1, 'ACTIVE' );
insert into account_states ( account_state_id, state_name ) values ( 2, 'INACTIVE' );
insert into account_states ( account_state_id, state_name ) values ( 3, 'UNVERIFIED' );
insert into account_states ( account_state_id, state_name ) values ( 4, 'REQUIRES_EMAIL_VERIFICATION' );
insert into account_states ( account_state_id, state_name ) values ( 5, 'REQUIRES_ACCOUNT_ACTIVATION' );

create table user_types
(
    user_type_id int,
    name varchar
);

insert into user_types ( user_type_id, name ) values ( 0, 'ADMIN' );
insert into user_types ( user_type_id, name ) values ( 1, 'USER' );
insert into user_types ( user_type_id, name ) values ( 2, 'SUPPORTER' );

create table countries
(
    country_code varchar,
    de varchar,
    en varchar
);

insert into countries ( country_code, de, en ) values ( 'DE', 'Deutschland', 'Germany' );