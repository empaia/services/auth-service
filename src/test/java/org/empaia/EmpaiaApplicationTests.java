package org.empaia;

import org.empaia.configuration.TestConfiguration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ActiveProfiles( TestConfiguration.PROFILE )
@ContextConfiguration( classes = TestConfiguration.class )
class EmpaiaApplicationTests {

    @Test
    void contextLoads() {
        Assertions.assertTrue( true );
    }
}