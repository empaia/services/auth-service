package org.empaia.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TextUtilsTest {

    @Test
    public void normalizeAccentsStringTest() {
        String result = TextUtils.normalize( "Çâêîôûàèùëïüáéíóúñüäöüß" );

        Assertions.assertEquals( "Caeiouaeueiuaeiounuaou", result );
    }

    @Test
    public void normalizeBasicTest() {
        String result = TextUtils.normalize( "Hello, World" );

        Assertions.assertEquals( "HelloWorld", result );
    }

    @Test
    public void normalizeNullTest() {
        Assertions.assertNull( TextUtils.normalize( null ) );
    }

    @Test
    public void normalizeSpecialTest() {
        Assertions.assertEquals( "azertya-_", TextUtils.normalize( "\n['\"#$]azertyâ-_" ) );
    }
}
