package org.empaia.configuration;

import org.empaia.configs.EmpaiaConfiguration;
import org.empaia.configs.KeycloakInstanceConfiguration;
import org.empaia.configs.PasswordComplexityConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile( "test" )
public class TestConfiguration {

    public static final String PROFILE = "test";

    @Bean
    public EmpaiaConfiguration createEmpaiaConfiguration() {
        EmpaiaConfiguration config = new EmpaiaConfiguration();

        config.setEnvironment( "testing" );
        config.setServerUrl( "http://testing.test" );
        config.setTokenSecret( "testing_sk" );
        config.setFrontendAuthUri( "http://testing.test/front" );

        return config;
    }

    @Bean
    public KeycloakInstanceConfiguration createConfiguration() {
        KeycloakInstanceConfiguration config = new KeycloakInstanceConfiguration();

        config.setServerUrl( "https://cannot.reach/" );
        config.setRealm( "empaia" );
        config.setClientId( "empaia_client_id" );
        config.setClientSecret( "empaia_client_sk" );

        return config;
    }

    @Bean
    public PasswordComplexityConfiguration passwordComplexityConfiguration() {
        PasswordComplexityConfiguration passwordComplexityConfiguration = new PasswordComplexityConfiguration();
        passwordComplexityConfiguration.setMinimumDigits( 0 );
        passwordComplexityConfiguration.setMinimumLength( 0 );
        passwordComplexityConfiguration.setMaximumLength( 128 );
        passwordComplexityConfiguration.setMinimumLowercase( 0 );
        passwordComplexityConfiguration.setMinimumUppercase( 0 );
        passwordComplexityConfiguration.setMinimumSpecialChars( 0 );

        return passwordComplexityConfiguration;
    }
}
