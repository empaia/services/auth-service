package org.empaia.configuration;

import lombok.extern.slf4j.Slf4j;
import org.empaia.configs.AuthServiceVersion;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

@ActiveProfiles( TestConfiguration.PROFILE )
@Slf4j
public class TestAuthServiceVersion {


    @Test
    void testGetVersionList() {
        String currentVersion = "0.1.2";
        assert AuthServiceVersion.getVersionUntilCurrentVersion(currentVersion, currentVersion).isEmpty();
        assert AuthServiceVersion.getVersionUntilCurrentVersion("0.0.1", "0.1.2").size() == 101;
        assert AuthServiceVersion.getVersionUntilCurrentVersion("0.0.1", "0.0.35").size() == 34;
        assert AuthServiceVersion.getVersionUntilCurrentVersion("0.0.90", "0.1.5").size() == 15;
        assert AuthServiceVersion.getVersionUntilCurrentVersion("0.0.99", "0.2.0").size() == 101;
        assert AuthServiceVersion.getVersionUntilCurrentVersion("0.1.1", "0.1.25").size() == 24;
        Assertions.assertArrayEquals(
                List.of("0.0.98", "0.0.99", "0.1.0", "0.1.1", currentVersion).toArray(),
                AuthServiceVersion.getVersionUntilCurrentVersion("0.0.97", currentVersion).toArray()
        );
        Assertions.assertArrayEquals(
                List.of(currentVersion).toArray(),
                AuthServiceVersion.getVersionUntilCurrentVersion("0.1.1", currentVersion).toArray()
        );
        assert AuthServiceVersion.getVersionUntilCurrentVersion("0.1.3", currentVersion).isEmpty();
        assert AuthServiceVersion.getVersionUntilCurrentVersion("1.0.0", currentVersion).isEmpty();
        assert AuthServiceVersion.getVersionUntilCurrentVersion("1.1.0", currentVersion).isEmpty();
        assert AuthServiceVersion.getVersionUntilCurrentVersion("1.1.2", currentVersion).isEmpty();
        assert AuthServiceVersion.getVersionUntilCurrentVersion("1.1.3", currentVersion).isEmpty();
        assert AuthServiceVersion.getVersionUntilCurrentVersion("2.0.0", currentVersion).isEmpty();

        currentVersion = "5.0.0";
        var versions = AuthServiceVersion.getVersionUntilCurrentVersion("3.9.1", currentVersion);
        Assertions.assertEquals("3.9.2", versions.get(0));
        Assertions.assertEquals(currentVersion, versions.get(versions.size()-1));
        // from 4.0.0 to 5.0.0: 100*100
        // from 3.9.1 to 4.0.0: 99 + 90*100
        Assertions.assertEquals(19099, versions.size());
    }
}
