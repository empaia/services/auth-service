#!/bin/bash
if [[ "$CI_RUNNER_DESCRIPTION" == *"empaia-charite"* ]]; then
  docker compose --profile pytest build --build-arg "USE_CHARITE_PROXY=1" auth-service
else
  docker compose --profile pytest build auth-service
fi