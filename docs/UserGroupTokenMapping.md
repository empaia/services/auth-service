# User Group Token Mapping

The mapping of roles and rights for users of organizations is somewhat special.
When a new Organization is created, there will be added a Group to the Keycloak realm with the normalized name of the org as String-ID:

![img.png](readme_img/img.png)

For now it will contain 4 subgroups: Accountant, Admin, Member and Vendor.
To map this subgroups to audiences those audiences have to be added as a String-Array with the key __"allowed_audience"__:

![img_1.png](readme_img/img_1.png)

Responsible for the final mapping to the Token is an Audience Mapper in the Keycloak Client Scopes:

![img_2.png](readme_img/img_2.png)

![img_3.png](readme_img/img_3.png)

This one will map **ALL** assigned Subgroups to the Token.
Additionally there can be created other Mapper that only map a single grou.
For example:

![img_4.png](readme_img/img_4.png)

At the moment this configuration has to be done by hand for each new organization.

**UPDATE:**
* The following endpoint can be used to achieve this:
  * GET `/api/domain_value/organization_user_roles` to get available user roles
  * GET `/api/client_group/organization` to get client_groups of an organization
  * POST `/api/organization_audience_management` to give user role of certain organization access to a certain client_group
* TODO:
  * Find out if the `backend` service creates the mapper script or if it has to be created manually.
  * Currently solved via importing minimal preconfigured keycloak realm
