# Auth Service Java Development

## Maven Unit Tests

`mvn clean test` will run the unit tests and generate a coverage report using jacoco

## Dependency analysis

`mvn dependency-check:check` will generate a dependency analysis report

## Javadoc

`mvn javadoc:javadoc` will generate the javadoc