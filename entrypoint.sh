#!/bin/sh
host="$(echo ${KEYCLOAK_AUTH_SERVER_URL} | cut -d'/' -f3 | cut -d':' -f1)"
port="$(echo ${KEYCLOAK_AUTH_SERVER_URL} | cut -d'/' -f3 | cut -d':' -f2)"
while ! nc -z ${host} ${port};
do
  echo Waiting for keycloak at ${host}:${port} ...;
  sleep 1;
done;
echo Keycloak port is available, starting!
env
java -Djava.security.egd=file:/dev/./urandom -jar app.jar
