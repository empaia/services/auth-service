# Auth Service

The auth service is responsible for managing user and client authentication and authorization.

The auth service has version V2. It has an
[API concept](https://gitlab.com/empaia/services/auth-service/-/wikis/New-API-Concept) and a
[role concept](https://gitlab.com/empaia/services/auth-service/-/wikis/New-Role-Concept).

The permission matrix for the V2 API, which is verified by Python permission tests, is
[here](docs/api_auth_test_results_v2.md#)

For details on the V2 API refer to

* the [Swagger-UI reference](http://localhost:8765/docs/swagger-ui/index.html)
* the [OpenAPI specifications](http://localhost:8765/v3/api-docs)
  (the auth service must be running locally for these links to work, see section [Running](#running)).

## Running

Copy `sample.env` as `.env`. Nothing needs to be edited.
Run docker compose with the `pytest` profile to start the `auth-service` container and its dependencies
(`keycloak` and `notification-service`):

```bash
docker compose --profile pytest up -d
```

When keycloak is started for the first time, then the auth service realm, super admin user, auth service client,
portal client, and vault client need to be created. The settings (client ids, client secrets, usernames, passwords)
are read from the `.env` file. Edit the following variables in the `.env` file to change the default settings:

* `AAA_SERVICE_CLIENT_*`
* `PORTAL_CLIENT_*`
* `VAULT_SERVICE_CLIENT_*`
* `SUPER_ADMIN_*`

Client settings (redirect_uris and web_origins) are set to `*` by default. For updating these, see

Run
[tests/data_generators/keycloak_initialize.py](./tests/data_generators/keycloak_initialize.py)
to create the global clients:

```bash
cp sample.env .env
cp sample.initialize.env .initialize.env
# edit .env for changing the client and user credentials
source .venv/bin/activate
cd tests
poetry install # if not yet done
python -m tests.data_generators.keycloak_initialize
```

## Test Data Generation

There are Python data generator scripts to create test data. The data generators for the V2 API are located in
[tests/data_generators/auth_service/v2/](./tests/data_generators/auth_service/functional_tests).
They require the active Python virtual environment and that the current working directory is
[tests/](./tests).

```bash
source .venv/bin/activate
cd tests
python -m tests.data_generators.auth_service.v2.create_user test_user.json # or pass any other *_user.json file name from the resources directory
python -m tests.data_generators.auth_service.v2.create_organizations test_organizations.json # or pass any other *_organizations.json file name from the resources directory
python -m tests.data_generators.auth_service.v2.configure_deployments test_deployment_configurations.json # or pass any other *_deployment_configurations.json file name from the resources directory
python -m tests.data_generators.auth_service.v2.gather_orga_data
python -m tests.data_generators.auth_service.v2.wipe_all_auth_service_data 
```

### Tools

To inspect access tokens and client tokens run one of the following commands

```bash
source .venv/bin/activate
cd tests
python -m tests.ctl get_user_token_data <email_address> <password> <client_id>
python -m tests.ctl get_client_token_data <client_id> <client_secret>
```

**Note**: don't run the following scripts on an auth service in a production deployment, otherwise all data is lost.

To recreate the auth service sql db run:

```bash
./recreate_auth_service_db.sh
```

To recreate a fresh empaia realm in keycloak run:

```bash
./recreate_empaia_realm.sh
```

To recreate both, a fresh empaia realm in keycloak and the auth service sql db, run:

```bash
./recreate_all.sh
```

## Development

The auth service is based on the Java [Spring Boot framework](https://spring.io/projects/spring-boot). During
development it is built
through the [maven build tool](https://maven.apache.org/).

### Install IntelliJ Community Edition

Follow the download and installation instructions on https://www.jetbrains.com/de-de/idea/download.

* Add `Temurin JDK 17` as project JDK
* Add Lombok plugin for IDEA

### Create IntelliJ Project

1. Start IntelliJ and open the directory containing this README.md as a new maven project
2. Install EnvFile plugin via `File -> Settings -> Plugins`
3. [Delegate build and run actions to Maven](https://www.jetbrains.com/help/idea/delegate-build-and-run-actions-to-maven.html#delegate_to_maven)

### Database

Data that is not stored in Keycloak is stored in a PostgreSQL DB. DB migrations are implemented with
[Flyway](https://flywaydb.org/). The Auth Service uses
[versioned](https://flywaydb.org/documentation/concepts/migrations#versioned-migrations)
[SQL migrations](https://flywaydb.org/documentation/concepts/migrations#sql-based-migrations).
They are defined in [src/main/resources/db/migration](src/main/resources/db/migration). Each migration has a
specific version. If it is applied, then the version is stored in the DB so that Flyway can identify the migration
state. If new migrations are added, only these are applied and the stored version is set to the last migration.

Undo migrations do not exist yet, so undoing a migration is only possible from restoring a DB backup that was
created before the migration.

When the Auth Service is set up with an empty database, Flyway will apply all migrations.

Flyway checks and applies the migrations only when the Auth Service starts.

The [pgAdmin](https://www.pgadmin.org/) tool can be used for inspecting the database and current data of the
PostgreSQL DB.

### MinIO

MinIO is used for storing and serving user profile pictures and organization logos.

TODO: document configuration variables

### Running the Auth Service

1. Create a new run configuration via `Run -> Edit Configurations...` similar as in this screenshot:
   ![Run Configuration](./docs/images/create_run_configuration.png)*Run Configuration*
2. Enable EnvFile plugin and add [development.env](./development.env)
3. Run this run configuration

### Running JUnit Tests

1. Create a new `maven` run configuration via `Run -> Edit Configurations...`, add the `development` env file and set
   the command line to `verify -Dsurefire.skipAfterFailureCount=1`
2. Run this maven run configuration

## Running Python Tests

### Preparation

```bash
1. cp sample.env .env 
2. docker compose --profile dev up -d
3. cd tests
4. python3 -m venv .venv
5. poetry install
```

#### Running

First wipe all data, because the auth service tests require a clean state:

```bash
python -m tests.ctl drop_all_data
```

Now run the tests:

```bash
poetry run pytest tests/tests/auth_service/ --maxfail=1
```

To run a specific test and filter the relevant output run:

```bash
poetry run pytest tests/tests/auth_service/ -k "anonymous_access" --maxfail=1 | grep -E "(response.*status)|(curl.*-X)"
```

#### Details About API Tests

The API permission tests are implemented as `controller tests` (routes are managed by Spring
`RestControllers`, and all routes such a controller are tested by a `controller test`). The tests are defined in
[tests/api/controller_tests](tests/api/controller_tests).

The entrypoints of the API tests are located in [tests/api](./tests/api).
They define the user context for all controller tests.

##### Running Specific API Tests

While running the auth tests they can be filtered by [access role](tests/utils/aaa/access_role.py),
method (DELETE, GET, POST, PUT), and endpoint (access role and method are case-insensitive).

For example:

```bash
poetry run pytest tests/tests/auth_service/ --maxfail=1 --access-role registered_user  --method post --endpoint /api/organization/logo
```

If `--access-role` is omitted, all access roles are handled, if `--method` is missing, all methods are handled, and
if `--endpoint` is missing, all endpoints are handled. `--skip-schemathesis` omits the schemathesis checks. To get
verbose output of successful tests, pass the `-rA` option.

Another option i `--repeat-last-failed-request`. If it is given, the access role, method, and endpoint of the
last failed request are stored in the file `.last-failed-request.json` next to [conftest.py](tests/conftest.py). As long
as
the test for this request fails, this only request is run by pytest. This option is useful, if multiple tests
may fail, but the order of the tests is different, then this option ensures that the last failed test is repeated.

## Permission Matrix

The permission matrices for the [V2 API](./docs/api_auth_test_results_v2.md) are automatically generated by
running the auth tests.

The file also contains a hash value from the auth test results. The GitLab [CI pipeline](.gitlab-ci.yml) verifies that
the test
results match this hash value to enforce that the permission matrix is always up-to-date.