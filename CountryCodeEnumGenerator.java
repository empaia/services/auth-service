import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.Locale;


public class CountryEnumGenerator {
    public static void main(String[] argv) throws IOException {

        Path templateFile = FileSystems.getDefault().getPath(argv[0]);
        Path targetFile = FileSystems.getDefault().getPath(argv[1]);

        createCountryClass(templateFile, targetFile);
    }

    private static String getCountryEnumItemsString() {
        String s = "";
        final int countryCodesPerLine = 25;
        int nextNewlineCount = countryCodesPerLine;
        for ( String countryCode : Locale.getISOCountries() ) {
            if ( s.length() > 0 ) {
                s += ",";
                if (nextNewlineCount == 0) {
                    if (s.length() > 0) {
                        s += "\n    ";
                    }
                    nextNewlineCount = countryCodesPerLine;
                } else {
                    s += " ";
                }
            }
            s += countryCode;
            nextNewlineCount -= 1;
        }
        s += ";\n";
        return s;
    }

    private static void createCountryClass(Path templateFile, Path targetFile) throws IOException {
        final String placeholder = "<<INSERT ENUM ITEMS HERE>>";

        System.out.println("Replacing placeholder in " + templateFile);

        try (BufferedReader reader = Files.newBufferedReader(templateFile, StandardCharsets.UTF_8)) {
            try (
                    BufferedWriter writer = Files.newBufferedWriter(
                            targetFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE,
                            StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING
                    )
            ) {
                String line = reader.readLine();
                while ( line != null ) {
                    line = line.replace(placeholder, getCountryEnumItemsString());
                    writer.write(line + "\n");
                    line = reader.readLine();
                }
            }
        }

        System.out.println("Wrote " + targetFile);
    }
}
