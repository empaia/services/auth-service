#===========================================#
## !!! ATTENTION !!!
#===========================================#
# This Multistage-Build is intended to
# CLEAN-BUILD the project with the same
# SDK as it will run on later.
#===========================================#


#===========================================#
## DOCKER IMAGE (STAGE 1 BUILDER)
#===========================================#

FROM eclipse-temurin:17.0.8_7-jdk-alpine@sha256:039f727ed86402f37524b5d01a129947e2f061d5856901d07d73a454e689bb13 AS maven_build


#===========================================#
## MAVEN ARGUMENTS
#===========================================#

ARG MAVEN_VERSION=3.9.4
ARG USER_HOME_DIR="/root"
ARG MAVEN_SHA=deaa39e16b2cf20f8cd7d232a1306344f04020e1f0fb28d35492606f647a60fe729cc40d3cba33e093a17aed41bd161fe1240556d0f1b80e773abd408686217e
ARG MAVEN_BASE_URL=https://downloads.apache.org/maven/maven-3/${MAVEN_VERSION}/binaries
ARG USE_CHARITE_PROXY=0
RUN if [ "$USE_CHARITE_PROXY" = 1 ]; then \
    export http_proxy=http://proxy.charite.de:8080; \
    export https_proxy=http://proxy.charite.de:8080; \
    export HTTP_PROXY=http://proxy.charite.de:8080; \
    export HTTPS_PROXY=http://proxy.charite.de:8080; \
    echo "Charite proxy variables set"; \
else \
    echo "Charite proxy variables not set"; \
fi

#===========================================#
## CURL
#===========================================#

# hadolint ignore=DL3018
RUN apk update \
    && apk add --no-cache curl \
    && rm -rf /var/lib/apt/lists/*


#===========================================#
## MAVEN MANUAL INSTALL
#===========================================#

# Maven depends on openjdk8-jre, manual installation is necessary
# hadolint ignore=DL4006
RUN mkdir -p /usr/share/maven /usr/share/maven/ref \
    && curl -fsSL -o /tmp/apache-maven.tar.gz ${MAVEN_BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
    && echo "${MAVEN_SHA}  /tmp/apache-maven.tar.gz" | sha512sum -c - \
    && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
    && rm -f /tmp/apache-maven.tar.gz \
    && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

ENV MAVEN_HOME /usr/share/maven
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"


#===========================================#
## CONFIGURABLE ENV VARIABLES
#===========================================#

# We set env variables here to prevent maven
# rebuilds each project build.
ENV BRANCH_NAME "develop"


#===========================================#
## BUILD AUTH SERVICE ARTIFACT & SONAR SCAN
#===========================================#

COPY src /usr/src/app/src
COPY pom.xml /usr/src/app

# add maven proxy settings if on charite runner
COPY settings.proxy-charite.xml "$MAVEN_CONFIG/settings.proxy-charite.xml"
RUN if [ "$USE_CHARITE_PROXY" = 1 ]; then \
    mv "$MAVEN_CONFIG/settings.proxy-charite.xml" "$MAVEN_CONFIG/settings.xml"; \
fi

RUN mvn -f /usr/src/app/pom.xml clean --update-snapshots --batch-mode package -Dmaven.test.skip=true

#===========================================#
## DOCKER IMAGE (STAGE 2)
#===========================================#

FROM eclipse-temurin:17.0.8_7-jdk-alpine@sha256:039f727ed86402f37524b5d01a129947e2f061d5856901d07d73a454e689bb13

ARG USE_CHARITE_PROXY=0
RUN if [ "$USE_CHARITE_PROXY" = 1 ]; then \
    export http_proxy=http://proxy.charite.de:8080; \
    export https_proxy=http://proxy.charite.de:8080; \
    export HTTP_PROXY=http://proxy.charite.de:8080; \
    export HTTPS_PROXY=http://proxy.charite.de:8080; \
    echo "Charite proxy variables set"; \
else \
    echo "Charite proxy variables not set"; \
fi


#===========================================#
## ENVIRONMENT VARIABLES
#===========================================#

ENV TZ=Europe/Berlin
ENV JAVA_OPTS=-XX:+UseContainerSupport


#===========================================#
## DEPLOYMENT
#===========================================#

# Copy compiled jar into container.
COPY --from=maven_build /usr/src/app/target/auth-service*.jar /app/app.jar
COPY entrypoint.sh /app/entrypoint.sh


#===========================================#
## SETUP IMAGE & CLEANUP
#===========================================#

# Install needed packages and set timezone info.
# hadolint ignore=DL3018
RUN apk update \
    && apk add --no-cache tzdata curl jq tcpdump nmap nmap-scripts bind-tools nmap-ncat \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone


#===========================================#
## PORTS
#===========================================#

EXPOSE 8765


#===========================================#
## RUNTIME
#===========================================#

WORKDIR /app/

ENTRYPOINT ["./entrypoint.sh"]


#===========================================#
## HEALTHCHECK
#===========================================#

# Actuator health check.
HEALTHCHECK --start-period=15s --interval=1m --timeout=10s --retries=5 \
            CMD curl --silent --fail --request GET 0.0.0.0:8765/actuator/health \
                | jq --exit-status '.status == "UP"' || exit 1
