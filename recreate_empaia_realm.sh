#!/bin/bash
source .venv/bin/activate
export PYTHONPATH=`pwd`
poetry run python3 tests/data_generators/keycloak_initialize.py --force-empaia-realm-and-all-data-deletion

