import difflib
import os
import sys

from tests.utils.api_test.api_results_manager import APIResults


def read_last_line(filename: str):
    with open(filename, "rb") as f:
        try:
            f.seek(-2, os.SEEK_END)
            while f.read(1) != b"\n":
                f.seek(-2, os.SEEK_CUR)
        except OSError:
            f.seek(0)
        last_line = f.readline().decode()
    return last_line


def read_api_results_hash(filename: str):
    api_results_hash = None
    if os.path.exists(filename):
        last_line = read_last_line(filename)
        if last_line.startswith(APIResults.api_results_hash_prefix):
            api_results_hash = last_line.split(APIResults.api_results_hash_prefix)[-1]
    return api_results_hash


def main():
    previous_api_auth_test_results = sys.argv[1]
    previous_hash_value = read_api_results_hash(previous_api_auth_test_results)
    print(f"Hash value from {previous_api_auth_test_results}: {previous_hash_value}")
    api_auth_test_results = sys.argv[2]
    hash_value = read_api_results_hash(api_auth_test_results)
    print(f"Hash value from {api_auth_test_results}: {hash_value}")
    sys.stdout.write(f"Comparing api auth test result hashes... ")
    if previous_hash_value != hash_value:
        print("ERROR.")
        with open(previous_api_auth_test_results) as f:
            previous_result_lines = f.readlines()
        with open(api_auth_test_results) as f:
            current_result_lines = f.readlines()
        sys.stdout.writelines(
            difflib.unified_diff(
                previous_result_lines,
                current_result_lines,
                fromfile=previous_api_auth_test_results,
                tofile=api_auth_test_results,
            )
        )
        filename = os.path.basename(api_auth_test_results)
        raise AssertionError(f"API auth test results have changed, but the {filename} was not updated.")
    else:
        print("ok.")


if __name__ == "__main__":
    main()
