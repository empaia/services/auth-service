version: "3.8"

services:

  auth-service:
    profiles: ["pytest"]
    build:
      context: "."
    environment:
      ACTIVE_PROFILE: production
      EMPAIA_ACTION_TOKEN_EXPIRATION_MS: ${AAA_ACTION_TOKEN_EXPIRATION_MS}
      EMPAIA_EMAIL_VERIFICATION_TOKEN_EXPIRATION_MS: ${AAA_EMAIL_VERIFICATION_TOKEN_EXPIRATION_MS}
      EMPAIA_LOG_EXCEPTIONS: ${AAA_EMPAIA_LOG_EXCEPTIONS}
      EMPAIA_POSTGRES_PASSWORD: ${AAA_DB_PASSWORD}
      EMPAIA_POSTGRES_URL: jdbc:postgresql://auth-db/empaia
      EMPAIA_POSTGRES_USERNAME: ${AAA_DB_USER}
      EMPAIA_SWAGGER_TITLE_SUFFIX: dev
      EMPAIA_SWAGGER_AUTH_URL: ${AAA_SWAGGER_AUTH_URL}
      EMPAIA_SWAGGER_SERVER_URL: ${AAA_SWAGGER_SERVER_URL}
      EMPAIA_TOKEN_SECRET: 886WeQAcqopd3SPhjmdsFA8YX9Vp6KGM
      EMPAIA_USER_SEEDING: "false"
      EMPAIA_AUTH_AUDIENCE: ${AAA_AUDIENCE}
      EMPAIA_RUN_KEYCLOAK_CLIENT_MIGRATION: "false" # WARNING! will add new clients/client scopes for every organization if not present
      EMPAIA_RUN_KEYCLOAK_CLEANUP: "false" # WARNING! will delete unapproved users, organizations and legacy or invalid (naming) clients from keycloak
      EMPAIA_PORTAL_VALID_REDIRECT_URIS: ${AAA_PORTAL_VALID_REDIRECT_URIS}
      EMPAIA_PORTAL_VALID_WEB_ORIGINS: ${AAA_PORTAL_VALID_WEB_ORIGINS}
      FRONTEND_AUTH_URL: http://localhost:8081/ # trailing slash is required!
      PORTAL_BASE_URL: ${AAA_PORTAL_BASE_URL}
      KEYCLOAK_AUTH_SERVER_URL: ${IDP_URL}
      KEYCLOAK_ADMIN_USER: ${KEYCLOAK_USER}
      KEYCLOAK_ADMIN_PASSWORD: ${KEYCLOAK_PASSWORD}
      KEYCLOAK_SSL_REQUIRED: none
      KEYCLOAK_BEARER_ONLY: "true"
      KEYCLOAK_CREDENTIALS_SECRET: ${AAA_CLIENT_SECRET}
      KEYCLOAK_PRINCIPAL_ATTRIBUTE: preferred_username
      KEYCLOAK_REALM: empaia
      KEYCLOAK_RESOURCE: ${AAA_CLIENT_ID}
      KEYCLOAK_USE_RESOURCE_ROLE_MAPPINGS: "true"
      MINIO_ACCESS_KEY: ${MINIO_ACCESS_KEY}
      MINIO_BUCKET: ${MINIO_BUCKET}
      MINIO_SECRET_KEY: ${MINIO_SECRET_KEY}
      MINIO_SECURE: ${MINIO_SECURE}
      MINIO_URL: ${MINIO_URL}
      MINIO_EXTERNAL_URL: ${MINIO_EXTERNAL_URL}
      SOS_URL: ${SOS_URL}
      RABBITMQ_MESSAGE_TIMEOUT: ${AAA_RABBIT_MQ_MESSAGE_TIMEOUT}
      RABBITMQ_SERVER_PORT: 42690 # doesn't start without
      NO_PROXY: ${NO_PROXY}
    ports:
      - 127.0.0.1:8765:8765
    depends_on:
      - auth-db
      - kcl

  kcl:
    image: registry.gitlab.com/empaia/integration/portal-workspace/keycloak21-theme:0.5.17@sha256:042b05c965e5f1b5e7fff37bfee6e454dbdbded0a57c0b5735dfe104f6a590cd
    profiles: ["pytest", "dev"]
    environment:
      KC_PROXY: edge
      KC_DB: postgres
      KC_DB_URL_HOST: kcl-db
      KC_DB_URL_DATABSE: ${KEYCLOAK_DB_DATABASE}
      KC_DB_PASSWORD: ${KEYCLOAK_DB_PASSWORD}
      KC_DB_PORT: ${KEYCLOAK_DB_PORT}
      KC_DB_USERNAME: ${KEYCLOAK_DB_USER}
      KEYCLOAK_ADMIN: ${KEYCLOAK_USER}
      KEYCLOAK_ADMIN_PASSWORD: ${KEYCLOAK_PASSWORD}
      KC_HOSTNAME_STRICT: "false"
      KC_HTTP_ENABLED: "true"
    depends_on:
      - kcl-db
    ports:
      - 127.0.0.1:8080:8080
    command:
      - start-dev --features scripts --http-relative-path=/auth --spi-theme-static-max-age=-1 --spi-theme-cache-themes=false --spi-theme-cache-templates=false

  minio-service:
    image: minio/minio
    profiles: ["pytest", "dev"]
    command: server --console-address ":9001" /home/shared
    environment:
      MINIO_ROOT_USER: ${MINIO_ACCESS_KEY}
      MINIO_ROOT_PASSWORD: ${MINIO_SECRET_KEY}
      NO_PROXY: ${NO_PROXY}
    ports:
      - 127.0.0.1:9000:9000
      - 127.0.0.1:9001:9001

  minio-service-init:
    image: minio/mc
    profiles: [ "pytest", "dev" ]
    environment:
      NO_PROXY: ${NO_PROXY}
    depends_on:
      - minio-service
    entrypoint: >
      /bin/sh -c "
      sleep 5;
      /usr/bin/mc alias set myminio http://minio-service:9000 ${MINIO_ACCESS_KEY} ${MINIO_SECRET_KEY};
      /usr/bin/mc mb myminio/${MINIO_BUCKET};
      /usr/bin/mc policy set public myminio/${MINIO_BUCKET};
      exit 0;
      "
  
  sos:
    image: registry.gitlab.com/empaia/services/shout-out-service:0.3.8@sha256:b2eb34e6c811cf67ccb062be6057542f580b6e26f9643b0ef35de57e715818f5
    profiles: [ "pytest", "dev" ]
    depends_on:
      - sos-db
    environment:
      SOS_ROOT_PATH: ${SOS_ROOT_PATH}
      SOS_DISABLE_OPENAPI: "False"
      SOS_CORS_ALLOW_CREDENTIALS: ${SOS_CORS_ALLOW_CREDENTIALS}
      SOS_CORS_ALLOW_ORIGINS: ${SOS_CORS_ALLOW_ORIGINS}
      SOS_API_V1_INTEGRATION: ${SOS_API_V1_INTEGRATION}
      SOS_DEBUG: ${SOS_DEBUG}
      SOS_DB_HOST: ${SOS_DB_HOST}
      SOS_DB_PORT: ${SOS_DB_PORT}
      SOS_DB_USERNAME: ${SOS_DB_USERNAME}
      SOS_DB_PASSWORD: ${SOS_DB_PASSWORD}
      SOS_DB: ${SOS_DB}
      SOS_ENABLE_SINK_AMQP: ${SOS_ENABLE_SINK_AMQP}
      SOS_AMQP_HOST: ${SOS_AMQP_HOST}
      SOS_AMQP_USERNAME: ${SOS_AMQP_USERNAME}
      SOS_AMQP_PASSWORD: ${SOS_AMQP_PASSWORD}
      SOS_IDP_URL: ${IDP_URL}
      SOS_AUDIENCE: ${SOS_AUDIENCE}
      PYTHONUNBUFFERED: 1
      NO_PROXY: ${NO_PROXY}
    command: run.sh --host=0.0.0.0 --port=6060
    ports:
      - 127.0.0.1:6060:6060
    volumes:
      - sos-rsa-vol:/opt/app/bin/rsa

  sosd:
    image: registry.gitlab.com/empaia/services/shout-out-service:0.3.8@sha256:b2eb34e6c811cf67ccb062be6057542f580b6e26f9643b0ef35de57e715818f5
    profiles: [ "pytest", "dev" ]
    depends_on:
      - sos-db
    environment:
      SOS_DEBUG: ${SOS_DEBUG}
      SOS_DB_HOST: ${SOS_DB_HOST}
      SOS_DB_PORT: ${SOS_DB_PORT}
      SOS_DB_USERNAME: ${SOS_DB_USERNAME}
      SOS_DB_PASSWORD: ${SOS_DB_PASSWORD}
      SOS_DB: ${SOS_DB}
      SOS_DAEMON_POLL_INTERVAL: ${SOS_DAEMON_POLL_INTERVAL}
      SOS_ENABLE_SINK_SMTP: ${SOS_ENABLE_SINK_SMTP}
      SOS_SMTP_MESSAGE_FROM: ${SOS_SMTP_MESSAGE_FROM}
      SOS_SMTP_HOST: ${SOS_SMTP_HOST}
      SOS_SMTP_PORT: ${SOS_SMTP_PORT}
      SOS_SMTP_SOURCE_ADDRESS: ${SOS_SMTP_SOURCE_ADDRESS}
      SOS_ENABLE_SINK_AMQP: ${SOS_ENABLE_SINK_AMQP}
      SOS_AMQP_HOST: ${SOS_AMQP_HOST}
      SOS_AMQP_USERNAME: ${SOS_AMQP_USERNAME}
      SOS_AMQP_PASSWORD: ${SOS_AMQP_PASSWORD}
      PYTHONUNBUFFERED: 1
      NO_PROXY: ${NO_PROXY}
    command: rund.sh
    volumes:
      - sos-rsa-vol:/opt/app/bin/rsa
    ports:
      - 127.0.0.1:5556:5556

  maildev:
    image: maildev/maildev:2.1.0@sha256:f7429227b8f471b3fe761767d86a8794a2fc7488bccdcda46ea6d5ba5c2c7bf5
    profiles: [ "pytest", "dev" ]
    ports:
      - 0.0.0.0:1080:1080 # web ui
      - 0.0.0.0:1025:1025 # smtp server

  rabbitmq:
    image: docker.io/rabbitmq:3-management
    profiles: [ "pytest", "dev" ]
    environment:
      RABBITMQ_DEFAULT_USER: ${SOS_AMQP_USERNAME}
      RABBITMQ_DEFAULT_PASS: ${SOS_AMQP_PASSWORD}
    volumes:
      - rabbitmq-vol:/var/lib/rabbitmq
    ports:
      - 0.0.0.0:15672:15672
      - 127.0.0.1:5672:5672

  auth-db:
    image: docker.io/postgres:14@sha256:af2042bab527803d9b928ac2b2364604d85aaf55f63158fb6c5697b4cfef239d
    profiles: [ "pytest", "dev" ]
    environment:
      POSTGRES_DB: empaia
      POSTGRES_USER: ${AAA_DB_USER}
      POSTGRES_PASSWORD: ${AAA_DB_PASSWORD}
    volumes:
      - auth-db-vol:/var/lib/postgresql/data
    ports:
      - 127.0.0.1:5432:5432

  kcl-db:
    image: docker.io/postgres:14@sha256:af2042bab527803d9b928ac2b2364604d85aaf55f63158fb6c5697b4cfef239d
    profiles: [ "pytest", "dev" ]
    environment:
      POSTGRES_DB: ${KEYCLOAK_DB_DATABASE}
      POSTGRES_USER: ${KEYCLOAK_DB_USER}
      POSTGRES_PASSWORD: ${KEYCLOAK_DB_PASSWORD}
    volumes:
      - kcl-db-vol:/var/lib/postgresql/data

  sos-db:
    image: docker.io/postgres:14@sha256:3a0d5a45e2292c966d5f70d425bc4d4bb148191fc54b50bb5ec7059b35ca71de
    profiles: [ "pytest", "dev" ]
    environment:
      POSTGRES_USER: ${SOS_DB_USERNAME}
      POSTGRES_PASSWORD: ${SOS_DB_PASSWORD}
      POSTGRES_DB: ${SOS_DB}
    ports:
      - 127.0.0.1:6543:5432
    volumes:
      - sos-db-vol:/var/lib/postgresql/data:rw

volumes:
  auth-db-vol:
  kcl-db-vol:
  sos-db-vol:
  sos-rsa-vol:
  rabbitmq-vol:

