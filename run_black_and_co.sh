#!/bin/bash
poetry run black tests
poetry run isort tests
poetry run pycodestyle tests
poetry run pylint tests